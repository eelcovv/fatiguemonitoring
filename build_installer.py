# run this script as
#
# python setup.py
#
# to create the installer of the gt_route_installer script
#
import fatigue_monitoring
from hmc_utils.misc import get_python_version_number

# to get a list of require modules for the multiarray module, we have to do this in a clean
# environment, so this has to be at the top of the script
global module_list_to_be_fixed

import os.path
import sys

import re
import subprocess
import shutil
# this import can only be done after running the versioneer script.
# See  https://github.com/warner/python-versioneer
# note that to run versioneer under windows you have to put python in front of it and use the full
# path to the script:
import yaml
# see http://python.6.x6.nabble.com/PyQt-4-7-and-py2exe-error-td1922933.html
# the import below are required for preventing error message on the matplotlib module
import os

import pandas as pd

# get the installation folder
import site

module_list_to_be_fixed = []

# set this flag to true if you want to generate the settings file. Only needed one time
generate_settings_file = False
try:
    # get the configuration file from the command line, otherwise assume build_installer.yml
    # if it fails
    configuration_file = sys.argv[1]
except IndexError:
    script_dir = os.path.split(__file__)[0]
    configuration_file = os.path.join(script_dir, "build_installer.yml")

# read the run_pyinstaller.yml file to get the setup information
with open(configuration_file, "r") as stream:
    settings = yaml.load(stream=stream)

print("+++ Settings read from {} +++".format(configuration_file))
for key, value in settings.items():
    try:
        print("{:20s}{:40s}".format(key, value))
    except TypeError:
        for key2, value2 in value.items():
            print("{:10s}{:20s}{}".format(key, key2, value2))

# copy the settings dictionary values to the global options used in the script
run_pyinstaller = settings["nsis"]["run_pyinstaller"]
create_the_script = settings["nsis"]["create_the_script"]
compile_the_script = settings["nsis"]["compile_the_script"]

# set here the default program name and which process step you wan to do
# copy the values from the settings dictionary so you can alter them in the yaml file
dist_name = "dist"
my_program_name = settings["general"]["application_name"]
source_code_directory = settings["general"]["source_code_directory"]
main_source_code_list = settings["general"]["main_source_code_list"]
organisation = settings["general"]["organisation_name"]
version_file = settings["general"]["version_file"]
if not re.search(".py$", version_file):
    version_file += ".py"

compressor_type = settings["nsis"]["compressor"]

# all libs which are problematic (can not be found) should be copied from dlls to dist
dll_list_to_be_fixed = ["geos_c.dll",
                        "geos.dll",
                        "hdf5.dll",
                        "msmpi.dll",
                        "libiomp5md.ll",
                        "numpy/core/numpy-atlas.dll",
                        "mkl_core.dll",
                        "mkl_def.dll",
                        "mkl_avx2.dll",
                        "mkl_intel_thread.dll",
                        "mkl_vml_avx2.dll",
                        "mkl_vml_def.dll"
                        ]

# here the problematic modules which are raising an error after starting the application. multiarray
# is an example the get_required_modules results in a list of modules required for multiarray.
# All this modules need to be explicitly copied to the dist dir. This is done by the script

# get the location of the python installation based on the location of os. Should point to the
# Anaconda directory
python_installation_directory = sys.prefix
# get the directory of the libraries
python_library_directory = os.path.join(python_installation_directory, "Library", "bin")
python_site_packages = os.path.join(python_installation_directory, "Lib", "site-packages")
python_user_packages = "C:\\Data\\Eelco\\Python\\Xmirror\\Lib\\site-packages"

# for version control versioeer is used
myrevisionstring = fatigue_monitoring.__version__
print("analysing version string: {}".format(myrevisionstring))
git_sha = subprocess.check_output(["git", "rev-parse", "HEAD"]).strip().decode("utf-8")
match = re.search("([.|\d]+)([+]*)(.*)", myrevisionstring)
if bool(match):
    myversionstring = match.group(1)
    mybuildstring = match.group(3)
else:
    myversionstring = None
    mybuildstring = None
print("full version is {}".format(myrevisionstring))
# write the revision string to file. This file is loaded in the main source code, so it can be
# brought into the executable

# at this point you want to move one lel up
if not os.path.exists(source_code_directory):
    print("changing directory")
    os.chdir("..")

if not os.path.exists(source_code_directory):
    raise FileNotFoundError("Could not find the directory {} from {}".format(
        source_code_directory, os.getcwd()))

git_version_python_file = os.path.join(source_code_directory, version_file)
print("writing version to {}".format(git_version_python_file))
with open(git_version_python_file, "w") as fp:
    fp.write("VERSIONTAG = '{}'\n".format(myrevisionstring))
    fp.write("GIT_SHA = '{}'\n".format(git_sha))

    python_version = get_python_version_number(sys.version_info)
    fp.write("PYTHON_VERSION = '{}'\n".format(python_version))
    now = pd.to_datetime("now")
    fp.write("BUILD_DATE = '{}'\n".format(now.strftime("%Y%m%d")))

print("clean version is {}".format(myversionstring))
print("build tag is {}".format(mybuildstring))

print("loading extra required dlls {}".format(dll_list_to_be_fixed))
print("loading extra required modules {}".format(module_list_to_be_fixed))
# the executable name
my_program_installation_directory = "{}-{}".format(my_program_name, myversionstring)
my_program_executable = "{}-{}-NSIS-zip".format(my_program_name, myversionstring)
my_icon_name = settings["general"]["icon_name"]

installation_folder = site.getsitepackages()[0]
print("installation folder: {}".format(site.getsitepackages()))

# below here: create a template for the nsis script to be created. The arguments between {} are
# passed via the call to the template
NSIS_SCRIPT_TEMPLATE = r"""
;--------------------------------
; load the required modules for nsis
!include nsDialogs.nsh
!include LogicLib.nsh
;--------------------------------
;Include Modern UI
!include "MUI2.nsh"

;--------------------------------
;Define some variables
;--------------------------------
!define pyinstall_outdir '{output_dir}\'
!define exe '{program_desc}.exe'
!define exe_out '{program_name}.exe'
!define organisation '{organisation}'
;--------------------------------

;--------------------------------
;General settings
;--------------------------------
; Sets the title bar text (although NSIS seems to append "Installer")
Caption "{program_desc}"

Name '{program_name}.exe'
OutFile ${{exe_out}}
Icon '{icon_name}'
; Use XPs styles where appropriate
; XPStyle on

; You can opt for a silent install, but if your packaged app takes a long time
; to extract, users might get confused. The method used here is to show a dialog
; box with a progress bar asicon_name the installer unpacks the data.
;SilentInstall silent
AutoCloseWindow true
ShowInstDetails nevershow

; Set the compressor. Use lzma for the best result, zlib for a quick run. SOLID makes sure all is compressed within one
; block, which gives better results
SetCompressor /SOLID '{compressor}'
;--------------------------------

; prevent the administrator to give permission to run the executable
!define MULTIUSER_EXECUTIONLEVEL User
;!define MULTIUSER_NOUNINSTALL ;Uncomment if no uninstaller is created
!include MultiUser.nsh

; the lines below pop up a dialog to the user in order to ask for an installation directory
InstallDir 'C:\Apps\{program_desc}\{program_folder}'
DirText "Installing {program_name} Tool. Please select a installation directory"

;--------------------------------
;Pages
;--------------------------------

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "License.txt"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH

  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH

;--------------------------------
;Languages
;--------------------------------

  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections
;--------------------------------

; here the actual installation takes plat
Section "{program_desc}"
    DetailPrint "Extracting application..."
    SetDetailsPrint none

    SetOutPath $INSTDIR
    File /r '${{pyinstall_outdir}}\*'

    GetTempFileName $0
    ;DetailPrint $0
    Delete $0
    StrCpy $0 '$0.bat'
    FileOpen $1 $0 'w'
    FileWrite $1 '@echo off$\r$\n'
    StrCpy $2 $TEMP 2
    FileWrite $1 '$2$\r$\n'
    FileWrite $1 'cd $INSTDIR$\r$\n'
    FileWrite $1 '${{exe}}$\r$\n'
    FileClose $1
    ; Hide the window just before the real app launches. Otherwise you have two
    ; programs with the same icon hanging around, and it's confusing.
    HideWindow
    nsExec::Exec $0
    Delete $0
SectionEnd
"""


class NSISScript(object):
    """""
    class to create a NSIS script. Used later
    """""

    # makensis is the executable which turns the created nsis script into an installation executable. Must be located
    # in the path of the script
    NSIS_COMPILE = "makensis"

    def __init__(self, program_name=None, program_desc=None,
                 program_folder=None,
                 script_1=None,
                 organisation=organisation,
                 dist_dir=None,
                 dll_list=[],
                 required_modules=[],
                 continuum_dir_name=None,
                 icon_name=None,
                 compressor="lzma"
                 ):
        self.program_name = program_name
        self.program_desc = program_desc
        self.program_folder = program_folder
        self.script_1 = script_1
        self.organisation = organisation
        self.dist_dir = dist_dir
        self.icon_name = icon_name
        self.pathname = "setup_%s.nsi" % self.program_desc
        self.dlls_to_be_copied = dll_list
        self.modules_to_be_copied = required_modules
        self.continuum_dir_name = continuum_dir_name
        self.compressor = compressor

    def create(self):
        """""
        Create a nsis script based on the string template defined above. Pass all the variables and write to file
        """""
        contents = NSIS_SCRIPT_TEMPLATE.format(
            program_name=self.program_name,
            script_1=self.script_1,
            program_desc=self.program_desc,
            program_folder=self.program_folder,
            organisation=self.organisation,
            output_dir=self.dist_dir,
            continuum_dir_name=self.continuum_dir_name,
            icon_name=self.icon_name,
            compressor=self.compressor
        )

        with open(self.pathname, "w") as outfile:
            outfile.write(contents)

    def copy_dll_pos(self):
        # this routine is used to fix a list of dlls
        for file_name in self.dlls_to_be_copied:
            dll_file_list = list()
            dll_file_list.append(os.path.join(self.dist_dir, "dlls", file_name))
            dll_file_list.append(os.path.join(python_library_directory, file_name))
            dll_file_list.append(os.path.join(python_site_packages, file_name))
            copied = False
            for dll_file in dll_file_list:
                if os.path.exists(dll_file):
                    print("copy {} -> {}".format(dll_file, file_name))
                    shutil.copy(dll_file, self.dist_dir)
                    copied = True
                    break
            if not copied:
                print("could not find dll file {}".format(file_name))

    def copy_required_modules(self):
        # this routine is used to fix a list of dlls
        for module_file in self.modules_to_be_copied:
            module_file_base = os.path.split(module_file)[1]
            if not os.path.exists(os.path.join(self.dist_dir, module_file_base)):
                try:
                    print("copy {} -> {}".format(module_file, self.dist_dir))
                    shutil.copy(module_file, self.dist_dir)
                    print("OK")
                except SystemError:
                    print("can not copy this module file")

    def compile(self):
        subproc = subprocess.Popen(
            # "/P5" uses realtime priority for the LZMA compression stage.
            # This can get annoying though.
            [self.NSIS_COMPILE, self.pathname, "/P5"], env=os.environ)
        subproc.communicate()

        retcode = subproc.returncode

        if retcode:
            raise RuntimeError("NSIS compilation return code: %d" % retcode)


dist_dir = "/".join([dist_name, my_program_name])
exclude_import = list()

# run the pyinstaller. We want to add multiple executbables into one installer. Since the
# executables share the same python libraries we are going to merge the dist directories created
# per installer.
if run_pyinstaller:
    for cnt, main_source_code_name in enumerate(main_source_code_list):
        # the build file of the first executables are stored in the 'dist'
        dist_out = dist_name
        if cnt > 0:
            # all next executables are stored in dist_1, dist_2 etc
            dist_out += "_{}".format(cnt)
        # run the pyinstaller
        pyinstaller = "pyinstaller -y --distpath {}".format(dist_out)
        for exclude_imp in exclude_import:
            pyinstaller += " --exclude-module {}".format(exclude_imp)
        source_code = os.path.join(source_code_directory, main_source_code_name)
        pyinstaller += " {}".format(source_code)
        print(pyinstaller)
        subprocess.Popen(pyinstaller).wait()
        print("done")
        # we have now created a new dist dir with a directory in it of the name of the script +
        # in this directories all the libraries + an executable with the name of the script.exe.
        # We are going to renamve the directory to global name which makes it easier to merge the
        # directories of all the scripts later # use mv to rename the directory to the
        # my_program_name
        main_source_code_base = os.path.splitext(main_source_code_name)[0]
        mv_from = os.path.join(dist_out, main_source_code_base)
        mv_to = os.path.join(dist_out, my_program_name)
        mv_command = str("move {} {}".format(mv_from, mv_to))
        print(mv_command)
        if os.path.exists(mv_from):
            subprocess.Popen(mv_command, shell=True).wait()
        else:
            print("Could not find file {}".format(mv_from))

        # now also rename the executable
        mv_from = os.path.join(dist_out, my_program_name, main_source_code_base + ".exe")
        mv_to = os.path.join(dist_out, my_program_name, my_program_name + ".exe")
        mv_command = str("move {} {}".format(mv_from, mv_to))
        print(mv_command)
        if os.path.exists(mv_from):
            subprocess.Popen(mv_command, shell=True).wait()
        else:
            print("Could not find file {}".format(mv_from))

    # done with building all the dist directories.
    # Merge the output of all scripts together into the first
    for cnt, main_source_code_name in enumerate(main_source_code_list):
        if cnt > 0:
            dist_out = dist_name + "_{}".format(cnt)
            subprocess.Popen("Robocopy.exe {} {}"
                             "".format(os.path.join(dist_out, my_program_name),
                                       os.path.join(dist_name, my_program_name))).wait()

# Create the installer
script_1 = main_source_code_list[0]
script = NSISScript(program_name=my_program_executable,
                    program_desc=my_program_name,
                    script_1=script_1,
                    program_folder=my_program_installation_directory,
                    organisation=organisation,
                    dist_dir=dist_dir,
                    dll_list=dll_list_to_be_fixed,
                    required_modules=module_list_to_be_fixed,
                    icon_name=my_icon_name,
                    compressor=compressor_type
                    )
if create_the_script:
    print("*** creating the NSIS setup script***")
    script.create()

# now copy the dlls which need to be fixed
script.copy_dll_pos()

# copy the modules which need to be fixed
script.copy_required_modules()

# run the nsis script
if compile_the_script:
    print("*** compiling the NSIS setup script***")
    script.compile()
