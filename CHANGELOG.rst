=========
Changelog
=========

Version 1.9.6
=============
- Update to Python 3.6
- Update to pyscaffold 3.0 directory structure with new version control integration

Version 1.9.2
=============
- Time zone must now be specified in the settings file or on the command line

Version 1.9.2
=============
- Time zone must now be specified in the settings file or on the command line

Version 1.9.0
=============
- New class definition and ready for pre-release

Version 1.6.1
=============

- First setup of pyscaffold