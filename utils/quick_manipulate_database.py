#!/bin/sh
"""true" -*- python -*-
exec python -tt `echo $0 | sed -e 's@\\\\@/@g' -e 's@^/cygdrive/\([a-zA-Z]\)/@\1:/@' -e 's@^//\([a-zA-Z]\)/@\1:/@'` ${1+"$@"}

small script to create the slides from one  case directories with images
using the 'make_latex_slide.py' script
"""

# small script to manipulate the columns of our data base. In this case, read the data base, turn the travel_distance
# from km to nautical miles and write it back to file
# also, the data is interpolated on a regular grid of 30 minutes, so you can fix the holes if you have some missing
# data. Since
import sys
from os.path import splitext
import pandas as pd
import argparse
import numpy as np
import logging
from HMC.Tools.miscellaneous import  clear_argument_list
from scipy.constants.constants import nautical_mile
import LatLon


from fms_utilities import travel_distance_and_heading_from_coordinates

sys.argv = clear_argument_list(sys.argv)

# parse the command line arguments
parser = argparse.ArgumentParser(description="A script for manipulate the fatigue database",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('database', help="Name of the excel data base to manipulate settings")
parser.add_argument('--overwrite', help="Over write the previous data base", default=False,
                    action="store_true")
parser.add_argument('--debug', help="Set debugging mode", default=False,
                    action="store_true")
parser.add_argument('--test', help="Only do what you want to do, do not write to file", default=False,
                    action="store_true")
parser.add_argument('--convert_km_to_miles', help="Convert the travel_distance column from km to nautical miles",
                    default=False, action="store_true")
parser.add_argument('--generate_linear_columns', help="Create a linearly interpolated column from 0 to max_index",
                    default=False, action="store_true")
parser.add_argument('--out_file', help="Name of the text output file")

sys.argv = clear_argument_list(sys.argv)
args = parser.parse_args()

db_dict = pd.read_excel(args.database, sheetname=None)

if args.overwrite:
    out_file = args.database
else:
    # get the filename with extension and put _new.xls behind it
    out_file = splitext(args.database)[0] + "_new.xls"

info_sheet = "General"

# get the general info sheet and create a datetime range from the beginning to the end with a frequency of 30 minutes
db_general = db_dict[info_sheet]
date_time_start = db_general["DateTime"].values[0]
date_time_end = db_general["DateTime"].values[-1]
date_time_range = pd.date_range(date_time_start, date_time_end, freq='30min')

db_dict_new = dict()

# loop over the sheets and convert the distance in the miles sheet
for sheet_name, db in db_dict.iteritems():

    # first make sure that the indexing is again based on the DataTime column
    db.set_index("DateTime", inplace=True)


    # copy to a new data frame (do not overwrite the old) because otherwise the new time sample are not even spaced
    db_new = db.resample('30min', base=30)
    # now interpolate the missing values
    db_new.interpolate(inplace=True, method='time')

    # now, for the info sheet, calculate the travel distance based on the GPS longitude/latitude coordinates
    if sheet_name == info_sheet:
        # calculate the travel distance from the coordinates
        travel_distance_and_heading_from_coordinates(db_new)

    # store the new data frames into a new dictionary
    db_dict_new[sheet_name] = db_new

if not args.test:
    # write the data to an excel file. Each slice is stored in a separate page
    with pd.ExcelWriter(out_file, append=False) as writer:
        db_dict_new[info_sheet].to_excel(writer, sheet_name=info_sheet)
        for sheet_name, db in db_dict_new.iteritems():
            if sheet_name == info_sheet:
                continue
            db.to_excel(writer, sheet_name=sheet_name)


# may need this later
#        end_index = 70
#        new_distance = db.travel_distance.values
#        first_distance = 0
#        last_distance = db.travel_distance[end_index]
#        new_distance[:end_index] = linspace(0, last_distance, end_index)
#        db["travel_distance"] = new_distance

