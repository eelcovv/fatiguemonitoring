#!/bin/sh
"""true" -*- python -*-
exec python -tt `echo $0 | sed -e 's@\\\\@/@g' -e 's@^/cygdrive/\([a-zA-Z]\)/@\1:/@' -e 's@^//\([a-zA-Z]\)/@\1:/@'` ${1+"$@"}

small script to process all the databases in this directory and create the
images using the 'make_plots_fatigue_database.py' script
"""

import platform
import os
import subprocess
import sys
from collections import OrderedDict

def make_annotations(description, font_size=18):
    """

    Parameters
    ----------
    description :
        
    font_size :
         (Default value = 18)

    Returns
    -------

    """
    # turn the ordered dict with key=values into a list of annotations
    annot_list = list()
    lx = 1.01
    ly = 0.9
    dy = 0.1
    dy2 = 0.15
    for key, value in description.iteritems():
        annot_list.append("{}@({},{})s{}c{}".format(key, lx, ly, font_size, "black"))
        ly -= dy
        annot_list.append("{}@({},{})s{}c{}".format(value, lx, ly, font_size,
                                                    "#0083D1"))
        print("Annotation key={}  value={} : {}".format(key, value,
                                                        annot_list[-2:]))
        ly -= dy2
    return annot_list


test = True  # only show the command, do not plot
if "--test" in sys.argv:
    test = True
one_plot = False  # only show one single plot and hold
if "--one" in sys.argv:
    one_plot = True

# name of the python script to run to create the images. Must be in PATH

platform = platform.platform()
print("platform is {}".format(platform))
if platform == "Linux-3.14.0-x86_64-with-debian-jessie-sid":
    path_to_script = "/home/eelcovv/Python/fatigue_monitoring"
else:
    path_to_script = "C:\\Data\\Eelco\\Python\\PyCharm\\fatigue_monitoring"
script_name = "make_plots_fatigue_database.py"
if path_to_script:
    script_name = os.path.join(path_to_script, script_name)
# base name of the data bases to plot

# list of cases to run based on their extension to the filebase
case_list = [
    [True, "fms_fsg_hpo2_fac_hpo2_6A_history.xls",
     OrderedDict([("Code", "Python"), ("Method", "6A-Acc"), ("AC Filter",
         "Butter-HP_2"), ("SG Filter", "Butter-HP_2")])],
    [True, "fms_fsg_hpo2_fac_hpo2_LS_history.xls",
     OrderedDict([("Code", "Python"), ("Method", "LS-Acc"), ("AC Filter",
         "Butter-HP_2"), ("SG Filter", "Butter-HP_2")])],
    [True, "fms_fsg_bpo2_fac_bpo2_6A_history.xls",
     OrderedDict([("Code", "Python"), ("Method", "6A-Acc"), ("AC Filter",
         "Butter-BP_2"), ("SG Filter", "Butter-BP_2")])],
    [True, "fms_fsg_bpo2_fac_bpo2_LS_history.xls",
     OrderedDict([("Code", "Python"), ("Method", "LS-Acc"), ("AC Filter",
         "Butter-BP_2"), ("SG Filter", "Butter-BP_2")])],
    [True, "WAF_1200.mat",
     OrderedDict([("Code", "Matlab"), ("Method", "6-Acc"), ("AC Filter",
         "TopHat-BP"), ("SG Filter", "TopHat-BP")])],
]

# list of date range to run for each case
data_ranges = [
    [False, "2011-11-22", "2011-12-16"],
    [True, "2011-12-01", "2011-12-16"],
    [False, "2011-12-10", "2011-12-16"]
]

# the basis command. If Test=True echo is used to only show the command
plot_script = []
if test:
    plot_script.append("echo")
plot_script.append(sys.executable)
plot_script.append(script_name)
if one_plot:
    plot_script.extend(["--select_locations", "HZ3"])
    plot_script.append("--no_damage_plot")
    plot_script.append("--debug")
else:
    plot_script.append("--no_block")
    plot_script.append("--debug")

# loop over the cases
for process, file_name, annotations in case_list:
    if not process:
        continue

    cmd = plot_script + [file_name]
    if annotations:
        cmd.append("--annotation")
        all_annotations = make_annotations(annotations)
        cmd.extend(all_annotations)

    file_base, extension = os.path.splitext(file_name)
    if extension == ".mat":
        cmd.append("--write")

    # loop over the date ranges
    for process_this_range, start_date, end_date in data_ranges:

        if not process_this_range:
            continue
        # build the plot command and submit it to the system
        cmd += ["--start_date", start_date, "--end_date", end_date]
        try:
            subprocess.Popen(cmd).wait()
        except SystemError as err:
            print(err)
