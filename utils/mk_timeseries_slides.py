#!/bin/sh
"""true" -*- python -*-
exec python -tt `echo $0 | sed -e 's@\\\\@/@g' -e 's@^/cygdrive/\([a-zA-Z]\)/@\1:/@' -e 's@^//\([a-zA-Z]\)/@\1:/@'` ${1+"$@"}

small script to create the slides from one  case directories with images
using the 'make_latex_slide.py' script
"""

import platform
import os
import re
import subprocess
import sys
import logging
from glob import glob

logging.basicConfig(level=logging.DEBUG, format='%(message)s')
log = logging.getLogger()

resolution_ext = ""

slide_layout_cluster_settings = True
slide_layout_cluster_locations = False

test = False
if "--test" in sys.argv:
    test = True

platform = platform.platform()
log.info("platform is {}".format(platform))
if platform == "Linux-3.14.0-x86_64-with-debian-jessie-sid":
    path_to_script = "/home/eelcovv/Python/fatigue_monitoring"
else:
    path_to_script = "C:\\Data\\Eelco\\Python\\PyCharm\\fatigue_monitoring"
script_name = "make_latex_slide.py"
if path_to_script:
    script_name = os.path.join(path_to_script, script_name)

# list of the case with a key to identify  and [title, directory]. The title
# is used as a column name in the slide
slide_title_start = "Timeseries"
# case list has a key with list containing [adding to main title, image_dir
case_dict = dict(
        pyt6Ahphp=["Filters SG/AC: HP/HP; $\sharp$ Acc: 6",
        "fms_fsg_hpo2_fac_hpo2_6A_111214T000000_images"],
        pyt6Abpbp=["Filters SG/AC: BP/BP; $\sharp$ Acc: 6",
            "fms_fsg_bpo2_fac_bpo2_6A_111214T000000_images"],
        pytLShphp=["Filters SG/AC: HP/HP; $\sharp$ Acc: All",
            "fms_fsg_hpo2_fac_hpo2_LS_111214T000000_images"],
        pytLSbpbp=["Filters SG/AC: BP/BP; $\sharp$ Acc: All",
            "fms_fsg_bpo2_fac_bpo2_LS_111214T000000_images"]
)

# with one slide with step over the filter settings to animate the lines for
# each setting. The order of the cases is defined here
case_order = [
        "pyt6Ahphp",
        "pytLShphp",
        "pyt6Abpbp",
        "pytLSbpbp",
        ]

# the key is the first part belonging to the images
plot_type_dict = dict(
    ACC_MS2_N2=["Accelleration"],
    SG_sigma_N2=["Coldspot Stress"],
    HS_sigma_N2=["Hotspot Stress"],
    TOWER_ACC0=["Tower Origin Acceleration Linear"],
    TOWER_ACC1=["Tower Origin Acceleration Rotational"]
)

exclude_locations = ["MAIN_BRACE"]

this_dir = "slides/fatigue"

if slide_layout_cluster_locations:
    log.info("+++++++++++ Start creatig slides for cluster  locations ++++++++++++")
    # loop over the cases
    for key, values in iter(sorted(case_dict.iteritems())):
        [case_title, case_dir] = values

        slide_base_name = "time_series_{}".format(key)

        slide_title = "{} {}".format(slide_title_start, case_title)

        for plot_type, [sub_title_base] in plot_type_dict.iteritems():

            regexpstr = "{}_(.*)_\d+T\d+{}.png".format(plot_type,resolution_ext)
            regexp = re.compile(regexpstr)

            log.info("---- Creating slide for {} {} -----".format(key, plot_type))

            slide_header = [
                        r"\begin{frame}",
                        r"\frametitle{" + slide_title + "}",
                        r"\framesubtitle{%"]

            slide_mid = []


            slide_ending = [
                        r"\vspace{10\baselineskip}",
                        r"\end{frame}"
                        ]


            slide_file_name = "{}_{}.tex".format(slide_base_name, plot_type)

            sub_title = sub_title_base + " \@ "

            file_list = glob(os.path.join(case_dir,
                plot_type+"*{}.png".format(resolution_ext)))

            log.debug("plotting {} {} {}".format(slide_base_name, slide_title,
                sub_title))

            positions = dict()
            for file_name in file_list:
                log.debug("adding file {}".format(file_name))

                file_base = os.path.split(file_name)[1]
                log.debug("matching {} with {}".format(regexpstr, file_base))
                match = regexp.search(file_base)
                if bool(match):
                    position = match.group(1)
                    log.debug("adding position {}".format(position))
                else:
                    log.debug("pattern not found")

                if position in exclude_locations:
                    continue

                positions[position] = file_name

            for cnt, [position, file_name] in enumerate(iter(sorted(positions.iteritems()))):

                position = re.sub("_", " ", position)

                if bool(re.search("Tower Origin", position)):
                    position = ""

                slide_header.append(r"\only<" + "{}".format(cnt + 1) +
                        ">{" + sub_title + position + "}%")

                slide_mid.append(r"\only<" + "{}".format(cnt + 1) + 
                        r">{\includegraphics[width=1.0\textwidth]{" + this_dir + "/" + file_name + r"}}")

            slide_header.append( r"}")


            ########### write to file

            log.info("writing to latex file {}".format(slide_file_name))
            with open(slide_file_name, "w") as fp:
                for line in slide_header + slide_mid + slide_ending:
                    log.debug(line)
                    fp.write(line+"\n")


if slide_layout_cluster_settings:
    log.info("+++++++++++ Start creatig slides for cluster settings ++++++++++++")
    for i_plot, [plot_type, frame_title] in enumerate(plot_type_dict.iteritems()):

        slide_base_name = "time_series_{}".format(plot_type)

        slide_title = "{} {} ".format(slide_title_start, frame_title[0])
        slide_sub_title = ""

        regexpstr = "{}_(.*)_\d+T\d+{}.png".format(plot_type,resolution_ext)
        regexp = re.compile(regexpstr)

        # loop over the cases
        slide_header = dict()
        slide_mid = dict()
        slide_file_name = dict()
        slide_ending = [
                    r"\vspace{10\baselineskip}",
                    r"\end{frame}"
                    ]

        log.info("---- Creating slide for {} {} -----".format(plot_type,
            frame_title))

        for cnt, case_key in enumerate(case_order):

            case_title, case_dir = case_dict[case_key]

            file_list = glob(os.path.join(case_dir, plot_type+"*"+resolution_ext+".png"))

            log.debug("plotting {} {} {}".format(slide_base_name, slide_title,
                slide_sub_title))

            for file_name in file_list:
                log.debug("adding file {}".format(file_name))

                file_base = os.path.split(file_name)[1]
                log.debug("matching {} with {}".format(regexpstr, file_base))
                match = regexp.search(file_base)
                if bool(match):
                    position = match.group(1)
                else:
                    log.debug("pattern not found")

                if position in exclude_locations:
                    continue

                position = re.sub(r"\(|\)|\s+|_", "", position)

                if re.match("^TOWER.*", plot_type):
                    position = ""

                log.debug("adding position {}".format(position))

                if cnt == 0:
                    slide_header[position] = [
                                r"\begin{frame}",
                                r"\frametitle{" + slide_title + position + "}",
                                r"\framesubtitle{%"]

                    slide_mid[position] = []

                    slide_file_name[position] = \
                    "{}_{}.tex".format(slide_base_name, position)


                try:
                    slide_header[position].append(r"\only<" + "{}".format(cnt + 1) + ">{" + case_title + "}%")
                except KeyError:
                    log.warning("could not add position {}".format(position))
                    continue

                slide_mid[position].append(r"\only<" + "{}".format(cnt + 1) + r">{\includegraphics[width=1.0\textwidth]{" + this_dir + "/" + file_name + r"}}")

                if cnt == len(case_order)-1:
                    slide_header[position].append( r"}")

        ########### write to file

        log.info("writing to latex file {}".format(slide_file_name))
        for pos, slide_head in slide_header.iteritems():
            with open(slide_file_name[pos], "w") as fp:
                for line in slide_header[pos] + slide_mid[pos] + slide_ending:
                    log.debug(line)
                    fp.write(line+"\n")
