#!/bin/sh
"""true" -*- python -*-
exec python -tt `echo $0 | sed -e 's@\\\\@/@g' -e 's@^/cygdrive/\([a-zA-Z]\)/@\1:/@' -e 's@^//\([a-zA-Z]\)/@\1:/@'` ${1+"$@"}

small script to create the slides from the case directories with images
using the 'make_latex_slide.py' script
"""

import platform
import os
import re
import subprocess
import logging
import sys
from glob import glob

logging.basicConfig(level=logging.DEBUG, format='%(message)s')
log = logging.getLogger()
log.info("")

slide_layout_compare_python_matlab = True
slide_layout_cluster_locations = True

this_dir = "slides/fatigue"

test = False
if "--test" in sys.argv:
    test = True

platform = platform.platform()
log.info("platform is {}".format(platform))
if platform == "Linux-3.14.0-x86_64-with-debian-jessie-sid":
    path_to_script = "/home/eelcovv/Python/fatigue_monitoring"
else:
    path_to_script = "C:\\Data\\Eelco\\Python\\PyCharm\\fatigue_monitoring"
script_name = "make_latex_slide.py"
if path_to_script:
    script_name = os.path.join(path_to_script, script_name)

dir_ext = "_images_from_2011-12-01_to_2011-12-16"

# list of the case with a key to identify  and [title, directory]. The title
# is used as a column name in the slidee
case_dict = dict(
        pyt6Abpbp=["Filters SG/AC: BP/BP; $\sharp$ Acc: 6",
        "fms_fsg_bpo2_fac_bpo2_6A_history" + dir_ext],
        pytLSbpbp=["Filters SG/AC: BP/BP; $\sharp$ Acc: All",
        "fms_fsg_bpo2_fac_bpo2_LS_history" + dir_ext],
        pyt6Ahphp=["Filters SG/AC: HP/HP; $\sharp$ Acc: 6",
        "fms_fsg_hpo2_fac_hpo2_6A_history" + dir_ext],
        pytLShphp=["Filters SG/AC: HP/HP; $\sharp$ Acc: All",
        "fms_fsg_hpo2_fac_hpo2_LS_history" + dir_ext],
        mat6Abpbp=["Filters SG/AC: BP/BP; $\sharp$ Acc: 6",
        "WAF_1200" + dir_ext]
)

case_order = [
        "pyt6Ahphp",
        "pytLShphp",
        "pyt6Abpbp",
        "pytLSbpbp",
]

plot_type_dict = dict(
    Damage_Python_D=["Damage"],
    Stress_Python_S=["Stress"]
)

exclude_locations = ["MAIN_BRACE"]

# specifies which directories from the case_dict to combine on a slide
# and the title belonging to this slied
combinations = dict(
    pyt6Abpbp_mat6Abpbp="Python vs Matlab",
    pyt6Abpbp_pyt6Ahphp="Filter SG Band vs High Pass (Acc 6)",
    pytLSbpbp_pyt6Abpbp="Acc All vs Acc 6 (Band BP)",
    pytLShphp_pyt6Ahphp="Acc All vs Acc 6 (Band HP)"
)

data_ranges = [
    [False, "2011-11-22", "2011-12-16"],
    [False, "2011-11-22", "2011-11-28"],
    [True, "2011-12-01", "2011-12-16"]
]

# the basic command. If Test=True echo is used to only show the command
slide_script = []
if test:
    slide_script.append("echo")
slide_script.append(sys.executable)
slide_script.append(script_name)
slide_script.append("--force")
slide_script.extend(["--slide_directory", "slides/fatigue"])

# loop over the cases

if slide_layout_compare_python_matlab:
    for comb, title in combinations.iteritems():
        case_dirs = list()
        column_titles = list()
        slide_base_name = "fatigue_monitoring"
        for key in comb.split('_'):
            slide_base_name += "_{}".format(key)
            case_dirs.append(case_dict[key][1])
            case_dirs[-1] = re.sub("_from.*", "", case_dirs[-1])
            column_titles.append(case_dict[key][0])
            if re.match("^pyt.*", key):
                column_titles[-1] = "PYTHON\\\\" + column_titles[-1] 
            else:
                column_titles[-1] = "MATLAB\\\\" + column_titles[-1] 

            if not (comb == "pyt6Abp_mat6Abp"):
                # only for the matlab python comparison you want 'Python' in the
                # column title, so remove it
                re.sub("Python ", "", column_titles[-1])

        log.info("combining {}".format(case_dirs))

        # loop over the date ranges
        for process_this_range, start_date, end_date in data_ranges:
            if not process_this_range:
                continue

            date_range = "_from_{}_to_{}".format(start_date, end_date)
            slide_name = slide_base_name + date_range
            log.info("creating slide {}".format(slide_name))
            # build the plot command and submit it to the system
            cmd = slide_script + ["--slide_name", slide_name]
            cmd += ["--title", title]
            cmd += ["--column_titles"]

            for col_title in column_titles:
                cmd.append(col_title)

            cmd += ["--directories"]
            for case in case_dirs:
                cmd.append(case + date_range)

            try:
                subprocess.Popen(cmd).wait()
            except SystemError as err:
                log.warning(err)

if slide_layout_cluster_locations:

    log.info("+++++++++++ Start creating slides for cluster settings ++++++++++++")
    for i_plot, [plot_type, frame_title] in enumerate(plot_type_dict.iteritems()):

        slide_base_name = "{}".format(plot_type)

        slide_title = frame_title[0]
        slide_sub_title = ""

        regexpstr = "{}_(.*).png".format(plot_type)
        regexp = re.compile(regexpstr)

        # loop over the cases
        slide_header = dict()
        slide_mid = dict()
        slide_file_name = dict()
        slide_ending = [
                    r"\vspace{10\baselineskip}",
                    r"\end{frame}"
                    ]

        for cnt, case_key in enumerate(case_order):

            case_title, case_dir = case_dict[case_key]
            log.info("found for {} case_title {} case_dir: {} ".format(case_key, case_title, case_dir))

            search_string = os.path.join(case_dir, "*" + plot_type + "*.png")

            log.info("searching with {}".format(search_string))

            file_list = glob(search_string)

            log.info("found files {} ".format(file_list))

            for file_name in file_list:
                log.info("adding file {}".format(file_name))

                file_base = os.path.split(file_name)[1]
                log.info("matching {} with {}".format(regexpstr, file_base))
                match = regexp.search(file_base)
                if bool(match):
                    position = match.group(1)
                else:
                    log.info("pattern not found")

                if position in exclude_locations:
                    continue

                position = re.sub(r"\(|\)|\s+|_", "", position)

                log.info("adding position {}".format(position))

                if cnt == 0:
                    slide_header[position] = [
                                r"\begin{frame}",
                                r"\frametitle{" + slide_title +" $@$ " + position + "}",
                                r"\framesubtitle{%"]

                    slide_mid[position] = [r"\vspace{-1\baselineskip}"]

                    slide_file_name[position] = \
                    "{}_{}.tex".format(slide_base_name, position)


                try:
                    slide_header[position].append(r"\only<" + "{}".format(cnt + 1) + ">{" + case_title + "}%")
                except KeyError:
                    log.warning("could not add position {}".format(position))
                    continue

                slide_mid[position].append(r"\only<" + "{}".format(cnt + 1) + r">{\includegraphics[width=0.75\textwidth]{" + this_dir + "/" + file_name + r"}}")

                if cnt == len(case_order)-1:
                    slide_header[position].append( r"}")

        ########### write to file

        log.info("writing to latex file {}".format(slide_file_name))
        for pos, slide_head in slide_header.iteritems():
            with open(slide_file_name[pos], "w") as fp:
                for line in slide_header[pos] + slide_mid[pos] + slide_ending:
                    log.info(line)
                    fp.write(line+"\n")

