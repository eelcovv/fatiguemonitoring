"""""
A small utility to return a list of required modules for a given module to import. Can be used to 
fix the error on multiarray. See http://comments.gmane.org/gmane.comp.python.py2exe/4847
and
http://stackoverflow.com/questions/35824089/py2exe-file-numpy-core-multiarray-pyc-line-10-in-load-importerror-dll-loa
"""""

import sys


def get_required_modules(module_names):
    """""

    Parameters
    ----------
    module_names :
        

    Returns
    -------
    type
        ""

    """
    initial = set(sys.modules)
    # change this import as needed for testing

    modules = map(__import__, module_names)

    # determine which modules have been added by the import statement
    loaded = set(sys.modules) - initial

    # remove "placeholder" modules
    loaded -= set(name for name in loaded if sys.modules[name] is None)

    # remove built-in modules
    loaded -= set(name for name in loaded if not hasattr(sys.modules[name], '__file__'))

    # finally, find the extension modules
    required_modules = list()
    for name in loaded:
        fn = sys.modules[name].__file__
        if fn.lower().endswith('.pyd'):
            required_modules.append(fn)

    return required_modules


if __name__ == "__main__":
    required_modules = get_required_modules(["numpy.core.multiarray"])
    print("Required modules:")
    for module in required_modules:
        print(module)
