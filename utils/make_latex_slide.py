#!/bin/sh
"""true" -*- python -*-
exec python -tt `echo $0 | sed -e 's@\\\\@/@g' -e 's@^/cygdrive/\([a-zA-Z]\)/@\1:/@' -e 's@^//\([a-zA-Z]\)/@\1:/@'` ${1+"$@"}

create a slide based on a list of images
"""

import os
import re
import argparse
from collections import OrderedDict
import logging
from glob import glob
from hmc_utils.misc import (query_yes_no, print_banner)
from hmc_utils.file_and_directory import make_directory

parser = argparse.ArgumentParser(description="A script to create latex slides",
                                 formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-d', '--debug', help="Print lots of debugging statements",
                    action="store_const",
                    dest="log_level", const=logging.DEBUG, default=logging.INFO)
parser.add_argument('-v', '--verbose', help="Be verbose", action="store_const", dest="log_level",
                    const=logging.INFO)
parser.add_argument('-q', '--quiet', help="Be quiet: no output", action="store_const",
                    dest="log_level",
                    const=logging.WARNING)
parser.add_argument("--title", help="Main title of the slide", default="Python vs Matlab")
parser.add_argument("--slide_name", help="Name of the slide", default="fatigue_monitoring")
parser.add_argument("--force", action="store_true", help="Force to overwrite a previous slide)")
parser.add_argument("--column_titles", nargs="+", help="A title per column")
parser.add_argument("--directories", nargs="+", type=str,
                    help="Directories containing all images per column)")
parser.add_argument("--slide_directory", default="slides/fatigue", help="Location of the slide)")

# mutual exclusive parameters for append direction
args = parser.parse_args()

init_logging("logger.yaml")

log = logging.getLogger(__name__)
log.setLevel(args.log_level)

# create a dictionary and store the images per location and model for easier later processing
# the ordered dict contains the left and right column of the slide. It must be ordered in order to maintain the order
# of the directies suplied via the command line
image_dict = dict(
    Damage=OrderedDict(),
    Stress=OrderedDict(),
)

# per plot type (Damage or STress) add an extra level per case. This title is used to set the column header
for key, dict_2 in image_dict.iteritems():
    for cnt, dir_name in enumerate(args.directories):
        dict_2[dir_name] = dict()
        dict_2[dir_name]["title"] = args.column_titles[cnt]
        dict_2[dir_name]["locations"] = dict()

print_banner("Start scanning image directory {}".format(args.directories))
# scan the image directoy and get all the image. The name is of the fo
for cnt, dir_name in enumerate(args.directories):
    images = glob(os.path.join(dir_name, "*"))
    log.debug("dir = {} im = {}".format(dir_name, images))
    for image in images:
        image_directory, image_base = os.path.split(image)
        match = re.match("fatigue_(.*?)_(.*?)_(\w)_(.*)\.png", image_base)
        if bool(match):
            plot_type_full = match.group(1)  # Damage or Stress
            code_source = match.group(2)  # matlab or python
            plot_type = match.group(3)  # D or S i.e. damage or stress
            location = match.group(4)
            log.info("Adding image made with cs= {} for pt {} ptf {} log {} {}"
                     "".format(code_source, plot_type, plot_type_full, location, image))
            # store the image into the dictionary
            image_dict[plot_type_full][dir_name]["locations"][location] = image

# from here create the latex commands

log.info(image_dict)

for plot_type, im_dict in image_dict.iteritems():
    log.info("plot type {}".format(plot_type))

    # create the header of the slide
    slide_text_lines = [
        r"\begin{frame}",
        r"\frametitle{" + args.title + ": " + plot_type + "}",
        r"\framesubtitle{%"
    ]

    # create a list of location in the sub title
    location_list = list()
    for code_source, im_dict2 in im_dict.iteritems():
        log.debug("Processing {}".format(plot_type))
        for cnt, location in enumerate(sorted(im_dict2["locations"].keys())):
            if location not in location_list:
                location_list.append(location)
                slide_text_lines.append(
                    r"\only<" + "{}".format(cnt + 1) + ">{Hotspot Location: " + location + "}%")

    slide_text_lines.extend([
        r"}",
        r"\begin{columns}[T]"])

    # start adding the images. Each plot_type add the location to a new slide
    for code_source, im_dict2 in im_dict.iteritems():
        log.debug("Processing {}".format(plot_type))
        slide_text_lines.extend([
            "",
            r"\column{0.5\textwidth}",
            r"\HMCBlue{" + im_dict2["title"] + "}"
        ])
        for cnt, location in enumerate(sorted(im_dict2["locations"].keys())):
            image = args.slide_directory + "/" + im_dict2["locations"][location]
            log.debug("Adding for {} location {}".format(plot_type, location))
            image = re.sub("\\", "/", image)
            slide_text_lines.append(
                r"\only<" + "{}".format(cnt + 1) + r">{"
                                                   r"\includegraphics[width=1.1\columnwidth]{" + image + r"}}"
            )

    slide_text_lines.extend([
        "",
        r"\end{columns}",
        r"\vspace{10\baselineskip}",
        r"\end{frame}"]
    )

    print_banner("Writing to file")

    slide_name = "{}_{}.tex".format(args.slide_name, plot_type)
    print_banner("Start creating latex slide {}".format(slide_name))

    with open(slide_name, "w") as fp:
        for line in slide_text_lines:
            log.debug(line)
            fp.write(line + "\n")
