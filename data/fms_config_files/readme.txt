
The SACS coeffient input files
SACSdatabase_coldspots_ROg.xls # original by ROg for the 2011 transit
SACSdatabase_coldspots_ROg2.xls # same of ROg, except  PIV PS location is added

# the stress concentration factors
SACSdatabase_HO.xls # original by HO
SACSdatabase_hotspots_evv.xls # new by evv to add the new locations

# the new SACS coefficient. The new feature now is that the coefficients are 
read from the original SACS output file. 

AccelCoeffHEXTravBlockhigh76.txt
AccelCoeffHEXTravBlockhigh90.txt


# the yannis RAO is RAO_7. The mat file is the original. The nc file is just the stored netcdf version of it
RAO_7.mat
RAO_7.nc

# the optimized rao
rao_ss_ss_cp_rx_rel_v1.nc
