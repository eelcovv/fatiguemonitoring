"""
The plotting of the data during a FMS session is done here
"""
from __future__ import division

import os
import re
from builtins import range

import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from numpy import (linspace, nanmean, nanmin, nanmax, pi)

import fatigue_monitoring.fms_utilities as fut
from hmc_utils.plotting import clean_up_plot

sns.set(context="notebook")
# rcParams.update({'font.size': 12})
# rcParams['figure.figsize'] = (14, 12)

# matplotlib.use('TkAgg')

# the number of text labels per graph which remain constant. The remained label can be update per
# plot and need to be cleared every time
N_LABEL_OFFSET = 3

LINE_WIDTHS = list([1, 2])

FIGURE_X_SIZE = 16
FIGURE_Y_SIZE = 8


def save_plot_to_file(fig, image_base, settings):
    """build a file name base on the image_base and settings and dump the figure to file

    Parameters
    ----------
    fig :
        the figure to save
    image_base :
        a base name of the file still without extension
    settings :
        the settings object holding all the info and settings
    """
    log = fut.get_logger(__name__)
    if bool(re.match("^\.", settings.image_type)):
        dotstr = ""
    else:
        dotstr = "."
    image_name = os.path.join(settings.image_directory, image_base + dotstr + settings.image_type)
    log.debug("saving plot to {}".format(image_name))
    fig.savefig(image_name, dpi=settings.plot_properties["dpi"])


def save_plot_lines_to_file(axes, image_base, settings, add_zoom_plot, t_start=None, t_end=None):
    """Prepares for the data line dump routine by creating a file name and checking if we want to
    dump the zoomed data only.

    Parameters
    ----------
    axes : list
         list of the axis of the current plot holding all the sub plot.
    image_base : str
        The image base. Just use the same name as the image
    settings : dict
        This dict holds all the flags controlling the behaviour of the routine
    add_zoom_plot : bool
        Indicates if we added a zoom plot. If that is the case, we can skip every other sub plot as
        it contains the same data (even it is zoomed, the line data is the same)
    t_start : date/time or None
        The start of the time range to dump (Default value = None)
    t_end : date/time or None
        The end of the time range to dump (Default value = None)

    Returns
    -------
    list
        A list of handles for line to save

    Notes
    -----
    * To control if you want to save the zoomed data range only, you should use the
      *dump_plots_use_zoomed_range* option in the settings file. This will select the data range as
      given by the *plot_properties* section.
    * *add_zoomed_plot* is only needed to see if we used a zoomed version in the sub plot so we can
      skip it with the loop over all the subplots
    """
    ext = re.sub("^\.", "", settings.dump_plot_type)
    data_file = os.path.join(settings.image_directory, image_base)
    every = 1
    ts = te = None
    if add_zoom_plot:
        # in case we used the zoomed plot we need to skip evey other plot as the data lines contain
        # the same
        every = 2
    if settings.dump_plot_use_zoomed_range:
        # on request we can also just save the zoomed range specified by the zoomed plot options
        ts = t_start
        te = t_end
        data_file += "_zoomed_t{:04.0f}_{:04.0f}".format(ts, te)
    # finally, call the routine to dump the file
    dump_line_data(data_file, axes, file_type=ext, every=every, t_start=ts, t_end=te)


def dump_line_data(file_base, axes, file_type="xsl", every=2, t_start=None, t_end=None):
    """Dump the data of the lines to a file

    Parameters
    ----------
    file_name : str
        name of the file to write
    axes : list
        list of axes each holding a list of plot lines which are going to be dumped
    file_type :
        type of the data file. Either xls (handy for reading) or msg_pack (faster and smaller)
        (Default value = "xsl")
    every : int
        dump every other line. Handy if you want to skip the zoomed plots (Default value = 2)
    t_start : date/time or None
        start time series from. If None start at beginning (Default value = None)
    t_end :date/time or None
        end time series from. If None end at last time (Default value = None)

    """

    log = fut.get_logger(__name__)
    df_dict = dict()
    for i_ax, ax in enumerate(axes):
        if i_ax % every == 0:
            # only plot the multiples of every
            lines = ax.get_lines()
            col_name = [line.get_label() for line in lines]
            data_lines = list()
            for i_line, line in enumerate(lines):
                # get the x and y data of this lines in store as serie in the list of data_lines
                xd = line.get_xdata()
                yd = line.get_ydata()
                data_lines.append(pd.Series(yd, index=xd, name=col_name[i_line]))

            # concatenate the lines in a data frame. The difference in length of the lines (which
            # happens apparently) is no nicely taken care of by pandas
            try:
                df = pd.concat(data_lines, axis=1)
                df.index.name = "Time [s]"
            except ValueError:
                # not data in this sub plot, so just continue to the next
                continue
            else:
                # select a data range is possible
                if t_start is not None:
                    df = df[df.index > t_start]
                if t_end is not None:
                    df = df[df.index < t_end]

            # add the lines of this subplot (stored in the df dataframe) into the collection
            # of dataframes
            df_dict["ax_{}".format(i_ax)] = df

    # we have a collection of dataframes in a dictionary, each data frame containing the data of
    # all the lines of that subplot. Now dump the data to either excel of msg_pack format
    if df_dict:
        file_name = file_base + "." + file_type
        if file_type == "xls":
            log.info("Dumping excel data to file {}".format(file_name))
            with pd.ExcelWriter(file_name, append=False) as writer:
                for sheet_name in sorted(df_dict.keys()):
                    df = df_dict[sheet_name]
                    log.info("sheet {}".format(sheet_name))
                    try:
                        df.to_excel(writer, sheet_name=sheet_name)
                    except AttributeError:
                        pass
        elif file_type == "msg_pack":
            log.info("Dumping msg pack data to file {}".format(file_name))
            with open(file_name, 'wb') as writer:
                for sheet_name in sorted(df_dict.keys()):
                    df = df_dict[sheet_name]
                    log.info("sheet {}".format(sheet_name))
                    try:
                        df.to_msgpack(writer)
                    except AttributeError:
                        log.warning("Could not write sheet {}".format(sheet_name))
        else:
            log.warning("data type {} not implemented".format(file_type))


def clean_up_plot_text_labels(lines, text_labels=[], n_offset=N_LABEL_OFFSET):
    """
    Clean the text labes in the current plot

    Parameters
    ----------
    text_labels : list
        List of text labels to clean. Default value = []
    n_offset : int
        Number of labels in the *text_labels* list to skip Default value = 3, i.e. keep the
        first three headers

    """
    for line in lines:
        if line is None:
            continue
        try:
            line.pop(0).remove()
        except (IndexError, AttributeError) as err:
            try:
                line.remove()
            except (ValueError, TypeError) as err:
                del line
    lines = []
    # also clean up the channel labels (except the first n_offset labels as that is the heading)
    if text_labels:
        for label in text_labels[n_offset:]:
            label.set_text("")


def plot_signal_accelerations(file_base_name, mdf_file, accelerometer_groups, settings, mat_file,
                              handles=dict(figure=None, axes=[], lines=[], text_labels=[]),
                              dump_data_to_file=True):
    """Create figure with 3 subplots, each showing the acceleration per channel for all locations

    Parameters
    ----------
    file_base_name : str
        base name of the input file
    mdf_file : :obj:`MDFFile`
        object holding all the info to the data
    accelerometer_groups : :obj:``
        object holding the selection of acceleration channels
    settings :
        object holding all the settings values
    mat_file :
        data frame holding the matlab data if it was available
    plot_handles :
        handles containing [figure, axis, lines, text] for update the plot
    handles :
        (Default value = dict(figure=None)
    axes :
        (Default value = [])
    lines : lines
        List of lines to pot
        (Default value = [])
    text_labels :
        (Default value = [])

    Returns
    -------
    dict:
        The handle to all signal acceleration plots

    
    """

    log = fut.get_logger(__name__)
    log.info("Plotting acceleration with file base {}".format(file_base_name))

    image_root = "ACC"
    plot_type = settings.signal_accelerometers_plot_properties["type"]
    if plot_type == 0:
        plot_raw_signal = False
        plot_type_name = "Acceleration"
        log.debug("plotting converted acceleration signals in m/s2 ")
        plot_units = "MS2"
        image_root += "_{}_".format(plot_units)
        y_label_quant = "a"
        y_label_units = "[m/s$^2$]"
    elif plot_type == 1:
        plot_type_name = "Raw ACC signal"
        plot_raw_signal = True
        log.debug("plotting raw acceleration signals in mA")
        plot_units = "SmA"
        image_root += "_{}_".format(plot_units)
        y_label_quant = "S"
        y_label_units = "[mA]"
    else:
        log.warning("plot type not recognised")
        raise AssertionError("plot type not recognised")

    if settings.plot_properties["add_zoom_plot"]:
        add_zoom_plot = True
        n_columns = 2
        fig_x_size = FIGURE_X_SIZE
        t_start = settings.plot_properties["zoom_range"][0]
        t_end = settings.plot_properties["zoom_range"][1]
        t_length = t_end - t_start
        if t_length < 0:
            log.warning("zoom range length is negative. Probably an error is raised later")
    else:
        n_columns = 1
        add_zoom_plot = False
        fig_x_size = FIGURE_X_SIZE / 2

    file_base_name = re.sub("_UTC.*", "", file_base_name)

    # number of columns (left and right plot)
    n_sub_plot = 3

    # maximum number of lines we can add to one sub plot
    max_lines_per_plot = 4

    # the position of the text labels at the top
    y_header_label = 1.1

    component_index = dict(X=0, Y=1, Z=2)
    component_names = list(['X', 'Y', 'Z'])
    component_line = dict(X='r-', Y='g-', Z='b-')

    gs = gridspec.GridSpec(n_sub_plot, n_columns)  # create some extra space
    gs.update(bottom=0.05, top=0.95)

    # open the plot canvas
    if handles["figure"] is None:
        fig = plt.figure(figsize=(fig_x_size, FIGURE_Y_SIZE))
        title = "Accelerometer components per position"
        log.info("Initialising figure canvas {}".format(title))
        fig.canvas.set_window_title(title)
        handles["figure"] = fig
    else:
        fig = handles["figure"]

    plt.ion()
    plt.show()

    # create a list with the axis
    if not handles["axes"]:
        log.debug("creating axes")
        axes = list()
        for i in range(n_sub_plot):

            # add right column: whole signal over 30 min
            axes.append(fig.add_subplot(gs[i, 0]))
            axes[-1].set_xlabel("")
            axes[-1].set_ylabel("${}_{}$ {}".format(y_label_quant, component_names[i],
                                                    y_label_units))

            # add left column: zoomed plot
            if add_zoom_plot:
                axes.append(fig.add_subplot(gs[i, 1]))
                axes[-1].set_xlim(t_start, t_start + t_length)
                axes[-1].set_xlabel("")
                axes[-1].set_ylabel("${}_{}$ {}".format(y_label_quant, component_names[i],
                                                        y_label_units))

        # only the bottom two figures at the time label
        axes[-1].set_xlabel("Time [s]")
        if add_zoom_plot:
            axes[-2].set_xlabel("Time [s]")

        if add_zoom_plot:
            axes[1].annotate("Zoomed {} s".format(t_length), xycoords="axes fraction",
                             xy=(0.01, y_header_label))

        handles["axes"] = axes
    else:
        axes = handles["axes"]

    plot_title = settings.plot_properties["case_description"]["title"]
    title_position = settings.plot_properties["case_description"]["position"]
    title_color = settings.plot_properties["case_description"]["color"]

    if not handles["text_labels"]:
        log.debug("Setting text labels")
        text_labels = list()
        text_labels.append(axes[0].text(title_position[0], title_position[1], "{:60s}".format(" "),
                                        transform=axes[0].transAxes, color=title_color, va='top'))
        text_labels.append(axes[0].annotate("{:60s}".format(" "), xycoords="axes fraction",
                                            xy=(0.01, y_header_label)))
        text_labels.append(axes[0].annotate("{:60s}".format(" "), xycoords="axes fraction",
                                            xy=(0.75, y_header_label)))
        while len(text_labels) < N_LABEL_OFFSET:
            # fill the remaining fixed labels with nothing
            text_labels.append(None)

        # add an extra label for each axis
        for index, ax in enumerate(axes):
            text_labels.append(ax.annotate("{:03}".format(index), xycoords="axes fraction",
                                           xy=(0.01, 0.03)))

        # store all the label handles
        handles["text_labels"] = text_labels
    else:
        text_labels = handles["text_labels"]

    if not handles["lines"]:
        lines = [None for i in range(max_lines_per_plot * len(axes))]
        handles["lines"] = lines
    else:
        lines = handles["lines"]

    # loop over all the positions
    for i_group, (position, acc_group) in enumerate(accelerometer_groups.device_locations.items()):
        log.debug("checking {} {} {}".format(position, acc_group.names, acc_group.labels))
        log.debug(settings.locations[position]["plot_this"])
        if settings.locations[position]["plot_this"]:
            # we are going to plot this position as the plot_this flag is true. First: clear up all
            # the lines
            clean_up_plot_text_labels(lines, text_labels=text_labels, n_offset=N_LABEL_OFFSET)

            # next loop over the channels and plot them
            log.debug("plotting {} {} {}".format(i_group, position, acc_group.names))
            ph = list()
            for i_channel, name in enumerate(acc_group.names):
                component_label = acc_group.components[i_channel]
                short_label = acc_group.short_labels[i_channel]

                # reference to which plot (X, Y, Z) we are going to make
                plot_index = n_columns * component_index[component_label]

                if plot_raw_signal:
                    limits = settings.signal_accelerometers_plot_limits[short_label][1]
                else:
                    limits = settings.signal_accelerometers_plot_limits[short_label][0]
                log.debug("limits {} : {}".format(short_label, limits))

                log.debug("adding {} {} ({}) component = {}"
                          "".format(name, acc_group.labels[i_channel], short_label,
                                    component_label))

                if not plot_raw_signal:
                    # the converted signal is stored in the data frame with a different name
                    name = fut.accelerometer_name_of_converted_signal(name)

                # df is the data frame containing the current acceleration info
                df = mdf_file.data[name]
                # create a time label of the start time.
                time_zero_label = re.sub("\+.*", "", "{}".format(np.datetime64(df.index[0], 's')))
                df_time = (df.index - df.index[0]).astype('timedelta64[ms]') / 1000.0

                coordinates_at_position = fut.set_default_dimension(
                    settings.locations[position]["coordinates"]).magnitude
                log.debug("found coordinates at {} : {}"
                          "".format(position, coordinates_at_position))
                text_labels[0].set_text("{}".format(plot_title))
                text_labels[1].set_text("{} @ {} ({:.1f}, {:.1f}, {:.1f})"
                                        "".format(plot_type_name, position,
                                                  coordinates_at_position[0],
                                                  coordinates_at_position[1],
                                                  coordinates_at_position[2]))
                text_labels[2].set_text("{}".format(time_zero_label))

                # values = df.values - df.values.mean()
                values = df.values
                log.debug("Plotting signal with mean/min/max : {}/{}/{}"
                          "".format(nanmean(values), nanmin(values), nanmax(values)))

                # loop over the columns and plot the full range (l=0) and zoomed range (l=1)
                for l in range(n_columns):
                    # plot the full (l=0) and zoomed (l=1)

                    # start with stetting the axis range for the current plot`
                    axes[plot_index + l].relim()
                    axes[plot_index + l].autoscale_view()
                    log.debug("SETTING limit {} (min/max signal {}/{})"
                              "".format(limits, values.min(), values.max()))

                    set_auto_scale = False
                    if None not in limits and \
                            not settings.signal_accelerometers_plot_properties["force_autoscale"]:
                        log.debug("Switching off auto scale")
                        set_auto_scale = False
                    else:
                        # autoscale is not working, so impose the y limits by the min/max values
                        limits = (values.min(), values.max())
                        log.debug("Autoscale does not work: impose limits {}".format(limits))

                    # plot the accelerometer data for the current location and channel
                    log.debug("scale propertie now {} {} ".format(limits, set_auto_scale))
                    axes[plot_index + l].set_ylim(bottom=limits[0], top=limits[1],
                                                  auto=set_auto_scale)
                    line = axes[plot_index + l].plot(df_time, values, '-r', label="AC-Py",
                                                     linewidth=LINE_WIDTHS[l])
                    lines[plot_index + l] = line
                    if l == 0 and i_channel == 0:
                        ln, = line
                        ph.append(ln)

                    # if the matlab file was loaded, add the matlab data as well
                    if mat_file is not None:
                        df_matlab_towac = mat_file.fms_timeseries_dict["TowerAccAll"]
                        df_matlab_time = (df_matlab_towac.index -
                                          df_matlab_towac.index[0]).astype('timedelta64[ms]')
                        df_matlab_time /= 1000.0
                        ac_label = acc_group.labels[i_channel]
                        try:
                            values_ml_towac = df_matlab_towac[ac_label].values
                            values_ml_towac -= nanmean(values_ml_towac)

                        except KeyError:
                            log.debug("short channel {} not present in the matlab data"
                                      "".format(ac_label))
                        else:
                            line = axes[plot_index + l].plot(df_matlab_time, values_ml_towac, "-b",
                                                             label="AC-ML", linewidth=1)

                            lines[plot_index + l + len(axes)] = line
                            if l == 0 and i_channel == 0:
                                ln, = line
                                ph.append(ln)

                    # finally, add a line of the mru signal as a check
                    if settings.add_mru_signal_to_accelerometer_plot and not plot_raw_signal:
                        mru_name = fut.mru_six_dof_names(position)[component_index[component_label]]
                        try:
                            mru_val = mdf_file.data[mru_name].values

                            log.info("Plotting MRU signal at location: {}".format(mru_name))
                            line = axes[plot_index + l].plot(df_time, mru_val, 'g--',
                                                             linewidth=LINE_WIDTHS[l],
                                                             label="MRU")

                            lines[plot_index + l + 2 * len(axes)] = line
                            if l == 0 and i_channel == 0:
                                ln, = line
                                ph.append(ln)
                        except KeyError:
                            log.debug("Can not add MRU to plot. No name: {}".format(mru_name))

                    # add a label with the current short name of the channel. Replace the A label
                    # with AX, AY, AZ # note tha that you have 2 to the text_labels index because
                    # the first to labels where header labels
                    text_labels[plot_index + N_LABEL_OFFSET + l].set_text(
                        "{}".format(re.sub("A", "A" + component_label, short_label)))

            axes[n_columns - 1].legend(handles=ph, loc=1, bbox_to_anchor=(1.22, 0.94),
                                       title="Signal")

            log.info("showing plot at position {}" + position)
            plt.draw()
            plt.pause(0.05)
            log.info("done")

            # build a image base name and dump the file as plot or as data on request
            position_str = re.sub("\s+", "_", position)
            image_str = "{}N{}_{}_".format(image_root, n_columns, position_str)
            image_base = re.sub(settings.mdf_file_base, image_str, file_base_name)

            if settings.save_images:
                save_plot_to_file(fig, image_base, settings)

            if settings.dump_plot_lines_to_file:
                save_plot_lines_to_file(axes, image_base, settings, add_zoom_plot, t_start=t_start,
                                        t_end=t_end)

    return handles


def plot_signal_mru(file_base_name, mdf_file, mru_names, settings,
                    handles=dict(figure=None, axes=[], lines=[], text_labels=[])):
    """create figure with 3 subplots  the heave roll and pitch, respectively.

    Parameters
    ----------
    file_base_name : str
        base name of the input file
    mdf_file :
        object holding all the info to the data
    mru_names : list
        list of names
    settings :
        object holding all the settings values
    handles :
        (Default value = dict(figure=None)

    Returns
    -------
    dict:
        Handles to plot properties
    """

    log = fut.get_logger(__name__)
    log.debug("start plotting mru data with file base {} and {}".format(file_base_name, mru_names))

    component_line = list(["r-", "g-", "b-", "-c", "-k", "-y"])

    title = "MRU "

    plot_title = settings.plot_properties["case_description"]["title"]

    title_position = settings.plot_properties["case_description"]["position"]
    title_color = settings.plot_properties["case_description"]["color"]

    plot_type = settings.signal_mru_plot_properties["type"]
    if plot_type == 0:
        title += " displacement"
    elif plot_type == 1:
        title += " velocity"
    elif plot_type == 2:
        title += " acceleration"
    else:
        log.warning("Not implemented for type {}".format(plot_type))
        return

    if settings.plot_properties["add_zoom_plot"]:
        add_zoom_plot = True
        n_columns = 2
        fig_x_size = FIGURE_X_SIZE
        t_start = settings.plot_properties["zoom_range"][0]
        t_end = settings.plot_properties["zoom_range"][1]
        t_length = t_end - t_start
        if t_length < 0:
            log.warning("zoom range length is negative. Probably an error is raised later")
    else:
        n_columns = 1
        add_zoom_plot = False
        fig_x_size = FIGURE_X_SIZE / 2

    file_base_name = re.sub("_UTC.*", "", file_base_name)

    n_sub_plot = 0
    for name in mru_names:
        if name in list(settings.signal_mru_plot_limits.keys()):
            n_sub_plot += 1

    # the position of the text labels at the top
    y_header_label = 1.1

    gs = gridspec.GridSpec(n_sub_plot, n_columns)  # create some extra space
    gs.update(bottom=0.05, top=0.95)

    # open the plot canvas
    if handles["figure"] is None:
        fig = plt.figure(figsize=(fig_x_size, FIGURE_Y_SIZE))
        log.info("Initialising figure canvas {}".format(title))
        fig.canvas.set_window_title(title)
        handles["figure"] = fig
    else:
        fig = handles["figure"]

    plt.ion()
    plt.show()

    six_dof_names_mru_acc = fut.six_dof_names(settings.mru_acc_name)

    six_dof_names_cog_acc = fut.six_dof_names("COG")

    # create a list with the axis
    if not handles["axes"]:
        log.info("creating axes")
        axes = list()

        index = 0
        for name in mru_names:
            if name not in list(settings.signal_mru_plot_limits.keys()):
                continue

            record_index = mdf_file.dataset_records_index[name]
            label = mdf_file.dataset_records[record_index].label
            unit = mdf_file.dataset_records[record_index].unit
            if unit == "rad":
                # the unit in the mdf file have already beed converted to degrees, so overrule it
                # here
                unit = "deg"
            if plot_type == 1:
                unit += "/s"
            elif plot_type == 2:
                unit += "/s$^2$"

            # add right column: whole signal over 30 min
            axes.append(fig.add_subplot(gs[index, 0]))
            axes[-1].set_xlabel("")
            axes[-1].set_ylabel("{} [{}]".format(label, unit))

            # add left column: zoomed plot
            if add_zoom_plot:
                axes.append(fig.add_subplot(gs[index, 1]))
                axes[-1].set_xlim(t_start, t_start + t_length)
                axes[-1].set_xlabel("")
                axes[-1].set_ylabel("{} [{}]".format(label, unit))

            index += 1

        # only the bottom two figures at the time label
        axes[-1].set_xlabel("Time [s]")
        if add_zoom_plot:
            axes[-2].set_xlabel("Time [s]")

        if add_zoom_plot:
            axes[1].annotate("Zoomed {} s".format(t_length), xycoords="axes fraction",
                             xy=(0.01, y_header_label))

        handles["axes"] = axes
    else:
        axes = handles["axes"]

    if not handles["text_labels"]:
        log.info("Setting text labels")
        text_labels = list()
        text_labels.append(axes[0].text(title_position[0], title_position[1], "{:60s}".format(" "),
                                        transform=axes[0].transAxes, color=title_color, va='top'))
        text_labels.append(axes[0].annotate("{:60s}".format(" "), xycoords="axes fraction",
                                            xy=(0.01, y_header_label)))
        text_labels.append(axes[0].annotate("{:60s}".format(" "), xycoords="axes fraction",
                                            xy=(0.75, y_header_label)))
        while len(text_labels) < N_LABEL_OFFSET:
            text_labels.append(None)

        for index, ax in enumerate(axes):
            text_labels.append(ax.annotate("{:03}".format(index), xycoords="axes fraction",
                                           xy=(0.01, 0.03)))

        handles["text_labels"] = text_labels
    else:
        text_labels = handles["text_labels"]

    if not handles["lines"]:
        # add factor 2 because one graph may contain 4 lines
        lines = [None for i in range(5 * len(axes))]
        handles["lines"] = lines
    else:
        lines = handles["lines"]

    clean_up_plot(lines)
    clean_up_plot_text_labels(text_labels)

    # loop over the mru channel names and add a line to one of the sub plots
    index = 0
    for i_channel, name in enumerate(mru_names):

        if name not in list(settings.signal_mru_plot_limits.keys()):
            # we have not defined the this mru channel in the plot limits, so skip it
            log.debug("Did not find an entry for {}. Skipping".format(name))
            continue

        # reference to the plot
        plot_index = n_columns * index

        # get the current unit and make sure to convert radians to degrees
        record_index = mdf_file.dataset_records_index[name]
        unit = mdf_file.dataset_records[record_index].unit

        if unit == "rad":
            unit = "deg"  # we have converted to degrees already
        else:
            factor = 1.0

        # df_raw contains the unfiltered raw Heave Roll Pitch data. Only subtract the mean
        values_raw = factor * mdf_file.data[name].values
        values_raw -= nanmean(values_raw)

        # look up the limits for this plot and update the name for the plot
        if plot_type == 0:
            # displacement: Heave, Roll, Pitch
            limits = settings.signal_mru_plot_limits[name][0]
            name_data = fut.mru_smooth_name(name)
            log.debug("FOUND MRU SMOOTH NAME : {}".format(name_data))
        elif plot_type == 1:
            # Time derivative of  Heave, Roll, Pitch (velocity)
            limits = settings.signal_mru_plot_limits[name][1]
            name_data = fut.mru_acceleration_name(name)
        elif plot_type == 2:
            # Second time derivative of  Heave, Roll, Pitch (acceleration)
            limits = settings.signal_mru_plot_limits[name][2]
            name_data = fut.mru_acceleration_name(name, 2)

            # set the name to the corresponding motion at the tower origin as stored in the data
            # frame
            if re.search("Surge", name, re.IGNORECASE):
                mru_acc_name = six_dof_names_mru_acc[0]
                cog_acc_name = six_dof_names_cog_acc[0]
            elif re.search("Sway", name, re.IGNORECASE):
                mru_acc_name = six_dof_names_mru_acc[1]
                cog_acc_name = six_dof_names_cog_acc[1]
            elif re.search("Heave", name, re.IGNORECASE):
                mru_acc_name = six_dof_names_mru_acc[2]
                cog_acc_name = six_dof_names_cog_acc[2]
            elif re.search("Roll", name, re.IGNORECASE):
                mru_acc_name = six_dof_names_mru_acc[3]
                cog_acc_name = six_dof_names_cog_acc[3]
            elif re.search("Pitch", name, re.IGNORECASE):
                mru_acc_name = six_dof_names_mru_acc[4]
                cog_acc_name = six_dof_names_cog_acc[4]
            elif re.search("Heading", name, re.IGNORECASE):
                mru_acc_name = six_dof_names_mru_acc[5]
                cog_acc_name = six_dof_names_cog_acc[5]
            log.debug("ADDED mru_acc_names {} for {} ".format(mru_acc_name, name))
            log.debug("ADDED cog_acc_names {} for {} ".format(cog_acc_name, name))

            if settings.signal_mru_plot_properties["show_accelerator_data"]:
                # note that Heave and Pitch are multiplied with - as itch/roll definition of MRU is
                # opposite log.debug("name = {} and name_data = {}  {} - {}".format(name, name_data,
                # mru_acc_name, mru_names)) values_acc = sign * factor *
                # mdf_file.data[mru_acc_name].values

                # the cog_acc is already in deg/s2, so no need to convert to radians
                values_cog_acc = mdf_file.data[cog_acc_name].values

        # make a copy of the time signal
        time_r = mdf_file.data.time_r

        # create a time label of the start time.
        time_zero_label = re.sub("\+.*", "",
                                 "{}".format(np.datetime64(mdf_file.data[name].index[0], 's')))

        text_labels[0].set_text("{}".format(plot_title))
        text_labels[1].set_text(title)
        text_labels[2].set_text("{}".format(time_zero_label))

        # get the current smoothed data belonging to the plot type (0: displacement, 1: velocity,
        # 2: acceleration)
        log.debug("Getting values of smoothed channel plot_type {}:  {} f={}"
                  "".format(plot_type, name_data, factor))
        values = factor * mdf_file.data[name_data].values

        log.info("Plotting signal with mean : {}".format(nanmean(values)))

        for l in range(n_columns):
            # plot the full (l=0) and zoomed (l=1)
            axes[plot_index + l].relim()
            axes[plot_index + l].autoscale_view(True, True, True)
            set_auto = True
            if None not in limits and not settings.signal_mru_plot_properties["force_autoscale"]:
                set_auto = False
            else:
                log.debug("Setting limit {} : {} ".format(name_data, limits))

            axes[plot_index + l].set_ylim(bottom=limits[0], top=limits[1], auto=set_auto)
            lines[plot_index + l] = axes[plot_index + l].plot(time_r, values,
                                                              component_line[i_channel])

            # add another line with the raw data.
            if plot_type == 0 and settings.signal_mru_plot_properties["show_raw_data"]:
                lines[plot_index + l + len(axes)] = axes[plot_index + l].plot(time_r, values_raw,
                                                                              'k--')

            if plot_type == 2 and settings.signal_mru_plot_properties["show_accelerator_data"]:
                mean_val = nanmean(values_cog_acc)
                lines[plot_index + l + 2 * len(axes)] = \
                    axes[plot_index + l].plot(time_r, values_cog_acc - mean_val, 'c--')

        index += 1

    plt.draw()
    plt.pause(0.05)

    image_root = "MRU_D{}".format(plot_type)
    image_str = "{}N{}_".format(image_root, n_columns)
    image_base = re.sub(settings.mdf_file_base, image_str, file_base_name)

    if settings.save_images:
        save_plot_to_file(fig, image_base, settings)

    if settings.dump_plot_lines_to_file:
        save_plot_lines_to_file(axes, image_base, settings, add_zoom_plot, t_start=t_start,
                                t_end=t_end)

    return handles


def plot_signal_strain_gauges(file_base_name, mdf_file, strain_gauge_groups, settings, mat_file,
                              handles=dict(figure=None, axes=[], lines=[], text_labels=[])):
    """create figure with 3 subplots, each showing the acceleration per channel for all locations

    Parameters
    ----------
    file_base_name : str
        base name of the input file
    mdf_file :
        object holding all the info to the data
    strain_gauge_groups :
        object holding the selection of acceleration channels
    settings :
        object holding all the settings values
    mat_file :
        if a matlab file was present, try to plot is as well
    handles : dict
        handles containing [figure, axis, lines, text] for update the plot
        Default value = dict(figure=None)

    Returns
    -------
    list
        A list of handles for line to save
    """

    log = fut.get_logger(__name__)
    log.debug("start plotting strain gauge data file base {}".format(file_base_name))

    plot_title = settings.plot_properties["case_description"]["title"]
    title_position = settings.plot_properties["case_description"]["position"]
    title_color = settings.plot_properties["case_description"]["color"]

    # define some settings and labels depending of the type of the image to plot
    image_root = "SG"
    plot_type = settings.signal_strain_gauges_plot_properties["type"]
    if plot_type == 0:
        # plot the raw SG signal
        log.info("plotting raw strain gauge signals in mA")
        record_index = mdf_file.dataset_records_index[strain_gauge_groups.name_list[0]]
        unit = mdf_file.dataset_records[record_index].unit
        plot_units = unit
        signal_string = "SG Raw signal"
        image_root += "_S{}".format(plot_units)
        y_label_quant = "S"
        y_label_units = "{}".format(plot_units)
    elif plot_type == 1:
        # plot the SG signal converted in strain
        log.info("plotting strain in micro-epsilon ")
        signal_string = "Strain"
        image_root += "_EPS"
        y_label_quant = "$\epsilon$"
        y_label_units = "$\mu$-"
    elif plot_type == 2:
        # plot the SG signal converted in stress
        log.info("plotting stress in Pa ")
        signal_string = "Stress"
        image_root += "_sigma"
        y_label_quant = "$\sigma$"
        y_label_units = "MPa"
    else:
        raise (AssertionError("Plot type {} not recognised. ".format(plot_type)))

    if settings.plot_properties["add_zoom_plot"]:
        # create a second column with a zoomed part of the time series
        add_zoom_plot = True
        n_columns = 2
        fig_x_size = FIGURE_X_SIZE
        t_start = settings.plot_properties["zoom_range"][0]
        t_end = settings.plot_properties["zoom_range"][1]
        t_length = t_end - t_start
        if t_length < 0:
            log.warning("zoom range length is negative. Probably an error is raised later")
    else:
        n_columns = 1
        add_zoom_plot = False
        fig_x_size = FIGURE_X_SIZE / 2

    file_base_name = re.sub("_UTC.*", "", file_base_name)

    n_sub_plot = 4

    # the position of the text labels at the top
    y_header_label = 1.1

    gs = gridspec.GridSpec(n_sub_plot, n_columns)  # create some extra space
    gs.update(bottom=0.05, top=0.95)

    title = "Strain Gauge signals per position"
    # open the plot canvas
    if handles["figure"] is None:
        fig = plt.figure(figsize=(fig_x_size, FIGURE_Y_SIZE))
        log.info("Initialising figure canvas {}".format(title))
        fig.canvas.set_window_title(title)
        handles["figure"] = fig
    else:
        fig = handles["figure"]

    plt.ion()
    plt.show()

    # create a list with the axis
    if not handles["axes"]:
        log.info("creating axes")
        axes = list()
        for i in range(n_sub_plot):
            # add right column: whole signal over 30 min
            axes.append(fig.add_subplot(gs[i, 0]))
            axes[-1].set_xlabel("")
            axes[-1].set_ylabel("{} [{}]".format(y_label_quant, y_label_units))

            # add left column: zoomed plot
            if add_zoom_plot:
                axes.append(fig.add_subplot(gs[i, 1]))
                axes[-1].set_xlim(t_start, t_start + t_length)
                axes[-1].set_xlabel("")
                axes[-1].set_ylabel("{} [{}]".format(y_label_quant, y_label_units))

        # only the bottom two figures at the time label
        axes[-1].set_xlabel("Time [s]")
        if add_zoom_plot:
            axes[-2].set_xlabel("Time [s]")

        if add_zoom_plot:
            axes[1].annotate("Zoomed {} s".format(t_length), xycoords="axes fraction",
                             xy=(0.01, y_header_label))

        handles["axes"] = axes
    else:
        axes = handles["axes"]

    if not handles["text_labels"]:
        # the first time the handles is None and the text labels still need to be put in the image
        log.info("Setting text labels")
        text_labels = list()
        text_labels.append(axes[0].text(title_position[0], title_position[1], "{:60s}".format(" "),
                                        transform=axes[0].transAxes, color=title_color, va='top'))
        text_labels.append(axes[0].annotate("{:60s}".format(" "), xycoords="axes fraction",
                                            xy=(0.01, y_header_label)))
        text_labels.append(axes[0].annotate("{:60s}".format(" "), xycoords="axes fraction",
                                            xy=(0.75, y_header_label)))

        while len(text_labels) < N_LABEL_OFFSET:
            text_labels.append(None)

        for index, ax in enumerate(axes):
            text_labels.append(ax.annotate("{:03}".format(index), xycoords="axes fraction",
                                           xy=(0.01, 0.03)))

        handles["text_labels"] = text_labels
    else:
        # the second call we just receive the text labels of the previous plot
        text_labels = handles["text_labels"]

    n_lines_per_plot = 4
    if not handles["lines"]:
        # For  the first call to the plot, te handles is None, so we fill it with None
        lines = [None for ii in range(n_lines_per_plot * len(axes))]
        handles["lines"] = lines
    else:
        lines = handles["lines"]

    for i_group, (position, sg_group) in enumerate(strain_gauge_groups.device_locations.items()):
        log.debug("checking {} {} {}".format(position, sg_group.names, sg_group.labels))
        try:
            log.debug(settings.locations[position]["plot_this"])
        except KeyError:
            log.debug("Position {} does not exist. Skipping")
            continue
        if settings.locations[position]["plot_this"]:
            # we are going to plot this position as the plot_this flag is true. First: clear up
            # all the lines
            clean_up_plot(lines)
            clean_up_plot_text_labels(text_labels)

            # next loop over the channels and plot them
            log.debug("plotting {} {} {}".format(i_group, position, sg_group.names))
            ph = list()
            for i_channel, name in enumerate(sg_group.names):

                # the name of the stress as obtained from the converted accelerometer data
                name_acc = fut.stress_from_acc_name(name, position, i_channel)

                # get the name of the strain belong to the current signal
                name_strain = fut.strain_name_of_converted_signal(name, position, i_channel)
                name_stress = fut.stress_name_of_converted_signal(name, position, i_channel)

                # short labels contains the reference to the channel such as SG01, SG02, etc.
                short_label = sg_group.short_labels[i_channel]

                # reference to which plot  we are going to make. Take the seond column into account
                plot_index = n_columns * i_channel

                limits = settings.signal_strain_gauges_plot_limits[short_label][plot_type]
                log.debug("limits {} : {}".format(short_label, limits))

                log.debug("adding {} {} ({})".format(name, sg_group.labels[i_channel], short_label))

                if plot_type == 0:
                    # we want to plot the raw signal
                    # df is the data frame containing the current strain signal in mA
                    log.debug("Get the raw data {}".format(name))
                    df = mdf_file.data[name]
                elif plot_type == 1:
                    # we want to plot the converted signal. Use the name_strain data to get the
                    # strain data
                    log.debug("Get the strain data {}".format(name_strain))
                    df = mdf_file.data[name_strain]
                else:
                    log.debug("Get the stress data {}".format(name_stress))
                    df = mdf_file.data[name_stress]

                # create a time label of the start time.
                time_zero_label = re.sub("\+.*", "", "{}".format(np.datetime64(df.index[0], 's')))

                # create a time series with t=0 at the first time step
                df_time = (df.index - df.index[0]).astype('timedelta64[ms]') / 1000.0

                try:
                    coordinates_at_position = settings.locations[position]["coordinates"]
                except KeyError:
                    log.debug("No coordinates given at {}".format(position))
                    coordinates_string = "(No coordinates)"
                else:
                    log.debug(
                        "found coordination at {} : {}".format(position, coordinates_at_position))
                    coordinates_string = "({:.1f}, {:.1f}, {:.1f})".format(
                        coordinates_at_position[0],
                        coordinates_at_position[1],
                        coordinates_at_position[2])

                text_labels[0].set_text("{}".format(plot_title))
                text_labels[1].set_text(
                    "{} @ {} {}".format(signal_string, position, coordinates_string))
                text_labels[2].set_text("{}".format(time_zero_label))

                # values contain the numerical array containing either the raw signal for
                # plot_type==0, or the strain
                values = df.values

                if plot_type == 1:
                    # create strain plot in micro-epsilon
                    values *= 1e6
                elif plot_type == 2:
                    # create stress plot in MPa, so convert strain to stress by multiplying with
                    # E_youngs_modules
                    values *= 1e-6  # unit MN/m2

                log.debug("Plotting signal with mean/min/max : {}/{}/{}"
                          "".format(nanmean(values), values.min(), values.max()))

                for l in range(n_columns):
                    # plot the full (l=0) and zoomed (l=1)
                    axes[plot_index + l].relim()
                    axes[plot_index + l].autoscale_view(True, True, True)
                    log.debug("SETTING limit {} ".format(limits))
                    set_auto = True
                    if None not in limits and \
                            not settings.signal_strain_gauges_plot_properties["force_autoscale"]:
                        set_auto = False
                    axes[plot_index + l].set_ylim(bottom=limits[0], top=limits[1], auto=set_auto)
                    line = axes[plot_index + l].plot(df_time, values, "-r", label="SG",
                                                     linewidth=LINE_WIDTHS[l])

                    lines[plot_index + l] = line
                    if l == 0 and i_channel == 0:
                        ln, = line
                        ph.append(ln)

                    if plot_type == 2:
                        # for the stress plot, include the accelerometer values if available
                        try:
                            log.debug("get data of {}".format(name_acc))
                            values_acc = mdf_file.data[name_acc].values * 1e-6  # MN/m2
                        except KeyError:
                            log.debug("Can not plot name acc")
                        else:
                            line = axes[plot_index + l].plot(df_time, values_acc, "-g", label="AC",
                                                             linewidth=1)
                            lines[plot_index + l + len(axes)] = line
                        if l == 0 and i_channel == 0:
                            ln, = line
                            ph.append(ln)

                        # also try to add the matlab data if available
                        if mat_file is not None:
                            number = sg_group.numbers[i_channel]
                            short_label_acc = re.sub("SG", "AC", short_label)
                            log.debug("Adding the StressStr mat file of channel number {} {} {}"
                                      "".format(number, short_label, short_label_acc))
                            df_matlab_str = mat_file.fms_timeseries_dict["StressStrAll"]
                            df_matlab_acc = mat_file.fms_timeseries_dict["StressAccAll"]
                            df_matlab_time = (df_matlab_str.index -
                                              df_matlab_str.index[0]).astype('timedelta64[ms]')
                            df_matlab_time /= 1000.0
                            try:
                                values_ml_sg = df_matlab_str[short_label].values
                            except KeyError:
                                log.debug("short channel {} not present in the matlab data"
                                          "".format(short_label))
                            else:
                                line = axes[plot_index + l].plot(df_matlab_time, values_ml_sg,
                                                                 "-b", label="SG-ML",
                                                                 linewidth=LINE_WIDTHS[l])

                                lines[plot_index + l + 2 * len(axes)] = line
                                if l == 0 and i_channel == 0:
                                    ln, = line
                                    ph.append(ln)
                            try:
                                values_ml_ac = df_matlab_acc[short_label_acc].values
                            except KeyError:
                                log.debug("short channel {} not present in the matlab data"
                                          "".format(short_label_acc))
                            else:
                                line = axes[plot_index + l].plot(df_matlab_time, values_ml_ac, "-k",
                                                                 label="AC-ML", linewidth=1)
                                lines[plot_index + l + 3 * len(axes)] = line
                                if l == 0 and i_channel == 0:
                                    ln, = line
                                    ph.append(ln)

                    # add a label with the current short name of the channel. Replace the A label
                    # with AX, AY, AZ note tha that you have N_LABEL_OFFSET to the text_labels index
                    # because the first two labels are header labels
                    text_labels[plot_index + N_LABEL_OFFSET + l].set_text("{}".format(short_label))

            log.debug("Adding legend with len {}".format(len(ph)))
            # add a position label only in the to plot (for j_plot==1)
            axes[n_columns - 1].legend(handles=ph, loc=1, bbox_to_anchor=(1.22, 0.94),
                                       title="Signal")

            log.info("showing plot at position {}".format(position))

            plt.draw()
            plt.pause(0.05)
            log.info("done")

            position_str = re.sub("\s+", "_", position)
            image_str = "{}_N{}_{}_".format(image_root, n_columns, position_str)
            image_base = re.sub(settings.mdf_file_base, image_str, file_base_name)
            if settings.save_images:
                save_plot_to_file(fig, image_base, settings)

            if settings.dump_plot_lines_to_file:
                save_plot_lines_to_file(axes, image_base, settings, add_zoom_plot, t_start=t_start,
                                        t_end=t_end)

    return handles


def plot_signal_huisman(file_base_name, mdf_file, hs_names, settings,
                        handles=dict(figure=None, axes=[], lines=[], text_labels=[])):
    """create figure with 3 subplots  with huisman signals: TowerAngle TravelingBlock

    Parameters
    ----------
    file_base_name :
        base name of the input file
    mdf_file :
        object holding all the info to the data
    settings :
        object holding all the settings values
    hs_names : list
       List of the names of the huis man lines
    handles : dict
        handles containing [figure, axis, lines, text] for update the plot
        (Default value = dict(figure=None)

    Returns
    -------
    list
        A list of handles for line to save
    """

    log = fut.get_logger(__name__)
    log.debug("start plotting huisman data with file base {}".format(file_base_name))

    plot_title = settings.plot_properties["case_description"]["title"]
    title_position = settings.plot_properties["case_description"]["position"]
    title_color = settings.plot_properties["case_description"]["color"]

    image_root = "HM_"
    component_line = list(['r-', 'g-', 'b-'])

    title = "Huisman signal"

    if settings.plot_properties["add_zoom_plot"]:
        add_zoom_plot = True
        n_columns = 2
        fig_x_size = FIGURE_X_SIZE
        t_start = settings.plot_properties["zoom_range"][0]
        t_end = settings.plot_properties["zoom_range"][1]
        t_length = t_end - t_start
        if t_length < 0:
            log.warning("zoom range length is negative. Probably an error is raised later")
    else:
        n_columns = 1
        add_zoom_plot = False
        fig_x_size = FIGURE_X_SIZE / 2

    file_base_name = re.sub("_UTC.*", "", file_base_name)

    n_sub_plot = min(3, len(hs_names))

    # the position of the text labels at the top
    y_header_label = 1.1

    gs = gridspec.GridSpec(n_sub_plot, n_columns)  # create some extra space
    gs.update(bottom=0.05, top=0.95)

    # open the plot canvas
    if handles["figure"] is None:
        fig = plt.figure(figsize=(fig_x_size, FIGURE_Y_SIZE))
        log.info("Initialising figure canvas {}".format(title))
        fig.canvas.set_window_title(title)
        handles["figure"] = fig
    else:
        fig = handles["figure"]

    plt.ion()
    plt.show()

    # create a list with the axis
    if not handles["axes"]:
        log.info("creating axes")
        axes = list()
        for i in range(n_sub_plot):

            name = hs_names[i]
            record_index = mdf_file.dataset_records_index[name]
            label = mdf_file.dataset_records[record_index].label
            unit = mdf_file.dataset_records[record_index].unit
            if unit == "rad":
                unit = "deg"

            # add right column: whole signal over 30 min
            axes.append(fig.add_subplot(gs[i, 0]))
            axes[-1].set_xlabel("")
            axes[-1].set_ylabel("{} [{}]".format(label, unit))

            # add left column: zoomed plot
            if add_zoom_plot:
                axes.append(fig.add_subplot(gs[i, 1]))
                axes[-1].set_xlim(t_start, t_start + t_length)
                axes[-1].set_xlabel("")
                axes[-1].set_ylabel("{} [{}]".format(label, unit))

        # only the bottom two figures at the time label
        axes[-1].set_xlabel("Time [s]")
        if add_zoom_plot:
            axes[-2].set_xlabel("Time [s]")

        if add_zoom_plot:
            axes[1].annotate("Zoomed {} s".format(t_length), xycoords="axes fraction",
                             xy=(0.01, y_header_label))

        handles["axes"] = axes
    else:
        axes = handles["axes"]

    if not handles["text_labels"]:
        log.info("Setting text labels")
        text_labels = list()
        text_labels.append(axes[0].text(title_position[0], title_position[1], "{:60s}".format(" "),
                                        transform=axes[0].transAxes, color=title_color))
        text_labels.append(axes[0].annotate("{:60s}".format(" "), xycoords="axes fraction",
                                            xy=(0.01, y_header_label)))
        text_labels.append(axes[0].annotate("{:60s}".format(" "), xycoords="axes fraction",
                                            xy=(0.75, y_header_label)))
        for index, ax in enumerate(axes):
            text_labels.append(ax.annotate("{:03}".format(index), xycoords="axes fraction",
                                           xy=(0.01, 0.03)))

        handles["text_labels"] = text_labels
    else:
        text_labels = handles["text_labels"]

    if not handles["lines"]:
        # add factor 2 because one graph may contain 2 lines
        lines = [None for i in range(2 * len(axes))]
        handles["lines"] = lines
    else:
        lines = handles["lines"]

    clean_up_plot(lines)
    clean_up_plot_text_labels(text_labels[3:])

    for i_channel, name in enumerate(hs_names):

        # reference to the plot
        plot_index = n_columns * i_channel

        # look up the limits for this plot
        try:
            limits = settings.signal_huisman_plot_limits[name]
        except (AttributeError, KeyError):
            limits = [None, None]

        # df is the data frame containing the current acceleration info
        df = mdf_file.data[name]

        # get the current unit and make sure to convert radians to degrees
        record_index = mdf_file.dataset_records_index[name]
        unit = mdf_file.dataset_records[record_index].unit

        if unit == "rad":
            log.debug("Convert for {} to deg".format(name))
            factor = 180.0 / pi
        else:
            factor = 1.0

        time_r = mdf_file.data.time_r

        # create a time label of the start time.
        time_zero_label = re.sub("\+.*", "", "{}".format(np.datetime64(df.index[0], 's')))

        text_labels[0].set_text("{}".format(plot_title))
        text_labels[1].set_text(title)
        text_labels[2].set_text("{}".format(time_zero_label))

        # convert to degrees if required
        values = df.values * factor

        log.info("Plotting signal with stat : {}".format(nanmean(values)))

        for l in range(n_columns):
            # plot the full (l=0) and zoomed (l=1)
            axes[plot_index + l].relim()
            axes[plot_index + l].autoscale_view(True, True, True)
            set_auto = True
            if None not in limits and \
                    not settings.signal_huisman_plot_properties["force_autoscale"]:
                set_auto = False
            else:
                log.debug("Setting limit {} : {} ".format(name, limits))

            axes[plot_index + l].set_ylim(bottom=limits[0], top=limits[1], auto=set_auto)
            lines[plot_index + l] = \
                axes[plot_index + l].plot(time_r, values, component_line[i_channel])

    plt.draw()
    plt.pause(0.05)

    image_str = "{}N{}_".format(image_root, n_columns)
    image_base = re.sub(settings.mdf_file_base, image_str, file_base_name)
    if settings.save_images:
        save_plot_to_file(fig, image_base, settings)

    if settings.dump_plot_lines_to_file:
        save_plot_lines_to_file(axes, image_base, settings, add_zoom_plot, t_start=t_start,
                                t_end=t_end)


def plot_s_n_curve(settings):
    """Create figure with the sn curves

    Parameters
    ----------
    settings :
        object holding all the settings values

    Returns
    -------
    list
        A list of handles for line to save
    """

    log = fut.get_logger(__name__)

    log.info("Plotting SN curves")

    plot_title = settings.plot_properties["case_description"]["title"]
    title_position = settings.plot_properties["case_description"]["position"]
    title_color = settings.plot_properties["case_description"]["color"]

    title = "SN curves"

    fig = plt.figure()
    fig.canvas.set_window_title(title)
    plt.xlabel("S [MPa]")
    plt.ylabel("N [-]")

    for location in list(settings._data_dict["locations"].keys()):
        try:
            sn_data = settings.sn_curve_data[location]
        except KeyError:
            continue
        log.info("Adding location {}".format(location))

        plt.loglog(sn_data[0], sn_data[1], 'ro-', label="{}".format(location))

        try:
            plt.loglog(sn_data[0], sn_data[2], 'g<-', label="{}+WAFO".format(location))
        except IndexError:
            log.info("No wafo profile calculated")
        break

    plt.legend()
    plt.draw()
    plt.pause(0.05)

    image_base = "SN_curves"
    if settings.save_images:
        save_plot_to_file(fig, image_base, settings)


def plot_hot_spot_stresses(file_base_name, mdf_file, strain_gauge_groups, settings, mat_file,
                           handles=dict(figure=None, axes=[], lines=[], text_labels=[])):
    """create figure with 3 subplots, each showing the acceleration per channel. Do this for all
    locations

    Parameters
    ----------
    file_base_name :
        base name of the input file
    mdf_file :
        object holding all the info to the data
    strain_gauge_groups :
        object holding the selection of acceleration channels
    settings :
        object holding all the settings values
    handles :
        handles containing [figure, axis, lines, text] for update the plot (Default value = dict(figure=None)
    mat_file : str
        matlab file

    Returns
    -------
    list
        A list of handles for line to save
    """

    log = fut.get_logger(__name__)
    log.info("start plotting hot spot stresses {}".format(file_base_name))

    plot_title = settings.plot_properties["case_description"]["title"]
    title_position = settings.plot_properties["case_description"]["position"]
    title_color = settings.plot_properties["case_description"]["color"]

    # define some settings and labels depending of the type of the image to plot
    image_root = "HS_sigma"

    # plot the stress
    log.info("plotting hotspot stresses")
    plot_units = "MPa"
    signal_string = "HS Stress SG"
    y_label_quant = "$\sigma_{HS}$"
    y_label_units = "{}".format(plot_units)

    if settings.plot_properties["add_zoom_plot"]:
        # create a second column with a zoomed part of the time series
        add_zoom_plot = True
        n_columns = 2
        fig_x_size = FIGURE_X_SIZE
        t_start = settings.plot_properties["zoom_range"][0]
        t_end = settings.plot_properties["zoom_range"][1]
        t_length = t_end - t_start
        if t_length < 0:
            log.warning("zoom range length is negative. Probably an error is raised later")
    else:
        n_columns = 1
        add_zoom_plot = False
        fig_x_size = FIGURE_X_SIZE / 2

    file_base_name = re.sub("_UTC.*", "", file_base_name)

    n_sub_plot = 4

    # the position of the text labels at the top
    y_header_label = 1.1

    gs = gridspec.GridSpec(n_sub_plot, n_columns)  # create some extra space
    gs.update(bottom=0.05, top=0.95)

    title = "Hot spot stresses per position"
    # open the plot canvas
    if handles["figure"] is None:
        fig = plt.figure(figsize=(fig_x_size, FIGURE_Y_SIZE))
        log.info("Initialising figure canvas {}".format(title))
        fig.canvas.set_window_title(title)
        handles["figure"] = fig
    else:
        fig = handles["figure"]

    # create a list with the axis
    if not handles["axes"]:
        log.info("creating axes")
        axes = list()
        for i in range(n_sub_plot):
            # add right column: whole signal over 30 min
            axes.append(fig.add_subplot(gs[i, 0]))
            axes[-1].set_xlabel("")
            axes[-1].set_ylabel("{} [{}]".format(y_label_quant, y_label_units))

            # add left column: zoomed plot
            if add_zoom_plot:
                axes.append(fig.add_subplot(gs[i, 1]))
                axes[-1].set_xlim(t_start, t_start + t_length)
                axes[-1].set_xlabel("")
                axes[-1].set_ylabel("{} [{}]".format(y_label_quant, y_label_units))

        # only the bottom two figures at the time label
        axes[-1].set_xlabel("Time [s]")
        if add_zoom_plot:
            axes[-2].set_xlabel("Time [s]")

        if add_zoom_plot:
            axes[1].annotate("Zoomed {} s".format(t_length), xycoords="axes fraction",
                             xy=(0.01, y_header_label))

        handles["axes"] = axes
    else:
        axes = handles["axes"]

    if not handles["text_labels"]:
        # the first time the handles is None and the text labels still need to be put in the image
        log.info("Setting text labels")
        text_labels = list()
        text_labels.append(axes[0].text(title_position[0], title_position[1], "{:60s}".format(" "),
                                        transform=axes[0].transAxes, color=title_color,
                                        verticalalignment='top'))
        text_labels.append(axes[0].annotate("{:60s}".format(" "), xycoords="axes fraction",
                                            xy=(0.01, y_header_label)))
        text_labels.append(axes[0].annotate("{:60s}".format(" "), xycoords="axes fraction",
                                            xy=(0.75, y_header_label)))

        while len(text_labels) < N_LABEL_OFFSET:
            text_labels.append(None)

        for index, ax in enumerate(axes):
            text_labels.append(
                ax.annotate("{:03}".format(index), xycoords="axes fraction", xy=(0.01, 0.03)))

        handles["text_labels"] = text_labels
    else:
        # the second call we just receive the text labels of the previous plot
        text_labels = handles["text_labels"]

    n_lines_per_plot = 4
    if not handles["lines"]:
        # For  the first call to the plot, te handles is None, so we fill it with None
        lines = [None for ii in range(n_lines_per_plot * len(axes))]
        handles["lines"] = lines
    else:
        lines = handles["lines"]

    n_hotspot_angles = settings.fatigue_settings["n_hotspot_angles"]
    phi_list = linspace(0, 360, n_hotspot_angles, endpoint=False)

    line_colors = list(["-r", "-g", "-b", "-c"])
    for i_group, (position, sg_group) in enumerate(strain_gauge_groups.device_locations.items()):
        log.debug("checking {} {} {}".format(position, sg_group.names, sg_group.labels))
        try:
            log.debug(settings.locations[position]["plot_this"])
        except KeyError:
            log.debug("Location {} does not exist. Skipping.")
            continue
        if settings.locations[position]["plot_this"]:
            # we are going to plot this position as the plot_this flag is true. First: clear up all
            # the lines
            clean_up_plot(lines)
            clean_up_plot_text_labels(text_labels[3:])

            try:
                limits = settings.signal_hotspots_plot_properties["plot_limits"][position][0]
                log.debug("Found hot spot plot limits {}".format(limits))
            except (AttributeError, KeyError) as err:
                limits = [None, None]
                log.debug("No hot spot plot limits found: {}".format(err))

            # next loop over the angles
            log.debug("plotting {} {} {}".format(i_group, position, sg_group.names))
            ph = list()
            for i_phi, phi_deg in enumerate(phi_list[0::2]):

                # the name of the hot spot stress as obtained from the strain gauge
                name_hs_str, str_short_extension = fut.hot_spot_stress_name(position, "SG", phi_deg)

                # the name of the hot spot stres stress as obtained from the converted accelerometer
                # data
                name_hs_acc, acc_short_extension = fut.hot_spot_stress_name(position, "AC", phi_deg)

                # reference to which plot  we are going to make. Take the seond column into account
                plot_index = n_columns * i_phi

                log.debug("adding {})".format(name_hs_str))

                try:
                    df = mdf_file.data[name_hs_str]
                except KeyError:
                    log.warning("Can not get hotspot stress for {}. Skipping".format(name_hs_str))
                    continue

                try:
                    df_acc = mdf_file.data[name_hs_acc]
                except KeyError:
                    df_acc = None
                    log.warning("Can not get hotspot stress for {}. Only plotting the str".format(
                        name_hs_acc))

                # create a time label of the start time.
                time_zero_label = re.sub("\+.*", "", "{}".format(np.datetime64(df.index[0], 's')))

                # create a time series with t=0 at the first time step
                df_time = (df.index - df.index[0]).astype('timedelta64[ms]') / 1000.0

                try:
                    coordinates_at_position = settings.locations[position]["coordinates"]
                except KeyError:
                    log.debug("No coordinates given at {}".format(position))
                    coordinates_string = "(No coordinates)"
                else:
                    log.debug(
                        "found coordination at {} : {}".format(position, coordinates_at_position))
                    coordinates_string = "({:.1f}, {:.1f}, {:.1f})".format(
                        coordinates_at_position[0],
                        coordinates_at_position[1],
                        coordinates_at_position[2])

                text_labels[0].set_text("{}".format(plot_title))
                text_labels[1].set_text(
                    "{} @ {} {}".format(signal_string, position, coordinates_string))
                text_labels[2].set_text("{}".format(time_zero_label))

                # values contain the numerical array containing either the raw signal for
                # plot_type==0, or the strain
                values = df.values * 1e-6

                log.debug("Plotting HS signal stress with std : {}".format(values.std()))

                for l in range(n_columns):
                    # plot the full (l=0) and zoomed (l=1)
                    axes[plot_index + l].relim()
                    axes[plot_index + l].autoscale_view(True, True, True)
                    set_auto = True
                    if None not in limits and not settings.signal_strain_gauges_plot_properties[
                        "force_autoscale"]:
                        set_auto = False
                    axes[plot_index + l].set_ylim(bottom=limits[0], top=limits[1], auto=set_auto)
                    line = axes[plot_index + l].plot(df_time, values, '-r', label="SG-PY",
                                                     linewidth=LINE_WIDTHS[l])
                    lines[plot_index + l] = line
                    if l == 0 and i_phi == 0:
                        ln, = line
                        ph.append(ln)

                    if df_acc is not None:
                        values_acc = df_acc.values * 1e-6  # MN/m2
                        log.debug("Plotting HS signal acc with std : {}".format(values_acc.std()))
                        line = axes[plot_index + l].plot(df_time, values_acc, "-g", label="AC-PY",
                                                         linewidth=LINE_WIDTHS[l])
                        lines[plot_index + l + len(axes)] = line
                        if l == 0 and i_phi == 0:
                            ln, = line
                            ph.append(ln)

                    # add a label with the current short name of the channel. Replace the A label
                    # with AX, AY, AZ note tha that you have N_LABEL_OFFSET to the text_labels index
                    # because the first to labels where header lables
                    text_labels[plot_index + N_LABEL_OFFSET + l].set_text(
                        "$\phi={:d}$".format(int(phi_deg)))

                    # also try to add the matlab data if available
                    if mat_file is not None:
                        name_hs_str_matlab = fut.hot_spot_stress_name_matlab(position, 0, i_phi)
                        name_hs_acc_matlab = fut.hot_spot_stress_name_matlab(position, 1, i_phi)

                        log.debug("Adding the HotStressStr mat file of channel number {} {}"
                                  "".format(name_hs_str_matlab, name_hs_acc_matlab))
                        df_matlab_str = mat_file.fms_timeseries_dict["HotStressStrAll"]
                        df_matlab_acc = mat_file.fms_timeseries_dict["HotStressAccAll"]
                        df_matlab_time = (df_matlab_str.index - df_matlab_str.index[0]).astype(
                            'timedelta64[ms]')
                        df_matlab_time /= 1000.0
                        try:
                            values_ml_hs_sg = df_matlab_str[name_hs_str_matlab].values
                        except KeyError:
                            log.debug(
                                "name {} not present in the matlab data".format(name_hs_str_matlab))
                        else:
                            line = axes[plot_index + l].plot(df_matlab_time, values_ml_hs_sg, "-b",
                                                             label="SG-ML",
                                                             linewidth=LINE_WIDTHS[l])

                            lines[plot_index + l + 2 * len(axes)] = line
                            if l == 0 and i_phi == 0:
                                ln, = line
                                ph.append(ln)
                        try:
                            values_ml_ac = df_matlab_acc[name_hs_acc_matlab].values
                        except KeyError:
                            log.debug("short channel {} not present in the matlab data".format(
                                name_hs_acc_matlab))
                        else:
                            line = axes[plot_index + l].plot(df_matlab_time, values_ml_ac, "-k",
                                                             label="AC-ML",
                                                             linewidth=LINE_WIDTHS[l])
                            lines[plot_index + l + 3 * len(axes)] = line
                            if l == 0 and i_phi == 0:
                                ln, = line
                                ph.append(ln)

            log.debug("Adding legend with len {}".format(len(ph)))
            # add a position label only in the to plot (for j_plot==1)
            axes[n_columns - 1].legend(handles=ph, loc=1, bbox_to_anchor=(1.22, 0.94),
                                       title="TowerOACC")

            log.info("showing plot at position {}" + position)
            plt.draw()
            plt.pause(0.05)
            log.info("done")

            position_str = re.sub("\s+", "_", position)
            image_str = "{}_N{}_{}_".format(image_root, n_columns, position_str)
            image_base = re.sub(settings.mdf_file_base, image_str, file_base_name)
            if settings.save_images:
                save_plot_to_file(fig, image_base, settings)

            if settings.dump_plot_lines_to_file:
                save_plot_lines_to_file(axes, image_base, settings, add_zoom_plot, t_start=t_start,
                                        t_end=t_end)

    return handles


def plot_tower_origin_accelerations(file_base_name, mdf_file, settings, mat_file, plot_type,
                                    handles=dict(figure=None, axes=[], lines=[], text_labels=[]),
                                    dump_data_to_file=True):
    """create figure with 3 subplots  the heave roll and pitch, respectively.

    Parameters
    ----------
    file_base_name :
        base name of the input file
    mdf_file :
        object holding all the info to the data
    settings :
        object holding all the settings values
    mat_file :
        pass the matlab object witht the data if available
    plot_type :
        0: plot linear acceleration, 1: plot rotational acelleration
    handles :
        handles containing [figure, axis, lines, text] for update the plot (Default value = dict(figure=None)
    axes :
        (Default value = [])
    lines :
        (Default value = [])
    text_labels :
        (Default value = [])

    Returns
    -------
    list
        A list of handles for line to save
    """

    log = fut.get_logger(__name__)
    log.debug("start plotting tower origin acceleration".format(file_base_name))

    title = "TowerAccO "

    plot_title = settings.plot_properties["case_description"]["title"]
    title_position = settings.plot_properties["case_description"]["position"]
    title_color = settings.plot_properties["case_description"]["color"]

    if plot_type == 0:
        title += " linear accelerations"
        sub_plot_names = ["AX", "AY", "AZ"]
        units = "m/s2"
        log.debug("setting plot type 0 with names {}".format(sub_plot_names))
    elif plot_type == 1:
        title += " rotational accelerations"
        sub_plot_names = ["RXX", "RYY", "RZZ"]
        log.debug("setting plot type 1 with names {}".format(sub_plot_names))
        units = "deg/s2"
    else:
        log.warning("Not implemented for type {}".format(plot_type))
        return

    if settings.plot_properties["add_zoom_plot"]:
        add_zoom_plot = True
        n_columns = 2
        fig_x_size = FIGURE_X_SIZE
        t_start = settings.plot_properties["zoom_range"][0]
        t_end = settings.plot_properties["zoom_range"][1]
        t_length = t_end - t_start
        if t_length < 0:
            log.warning("zoom range length is negative. Probably an error is raised later")
    else:
        n_columns = 1
        add_zoom_plot = False
        fig_x_size = FIGURE_X_SIZE / 2

    file_base_name = re.sub("_UTC.*", "", file_base_name)

    n_sub_plot = 3

    # the position of the text labels at the top
    y_header_label = 1.1

    gs = gridspec.GridSpec(n_sub_plot, n_columns)  # create some extra space
    gs.update(bottom=0.05, top=0.95)

    # open the plot canvas
    if handles["figure"] is None:
        fig = plt.figure(figsize=(fig_x_size, FIGURE_Y_SIZE))
        log.info("Initialising figure canvas {}".format(title))
        fig.canvas.set_window_title(title)
        handles["figure"] = fig
    else:
        fig = handles["figure"]

    plt.ion()
    plt.show()

    six_dof_names_tow_acc = fut.six_dof_names(settings.tower_origin_acc_name)

    # create a list with the axis
    if not handles["axes"]:
        log.info("creating axes")
        axes = list()
        for index, name in enumerate(sub_plot_names):

            # add right column: whole signal over 30 min
            axes.append(fig.add_subplot(gs[index, 0]))
            axes[-1].set_xlabel("")
            axes[-1].set_ylabel("{} [{}]".format(name, units))

            # add left column: zoomed plot
            if add_zoom_plot:
                axes.append(fig.add_subplot(gs[index, 1]))
                axes[-1].set_xlim(t_start, t_start + t_length)
                axes[-1].set_xlabel("")
                axes[-1].set_ylabel("{} [{}]".format(name, units))

        # only the bottom two figures at the time label
        axes[-1].set_xlabel("Time [s]")
        if add_zoom_plot:
            axes[-2].set_xlabel("Time [s]")

        if add_zoom_plot:
            axes[1].annotate("Zoomed {} s".format(t_length), xycoords="axes fraction",
                             xy=(0.01, y_header_label))

        handles["axes"] = axes
    else:
        axes = handles["axes"]

    if not handles["text_labels"]:
        log.info("Setting text labels")
        text_labels = list()
        text_labels.append(axes[0].text(title_position[0], title_position[1], "{:60s}".format(" "),
                                        transform=axes[0].transAxes, color=title_color,
                                        verticalalignment='top'))
        text_labels.append(axes[0].annotate("{:60s}".format(" "), xycoords="axes fraction",
                                            xy=(0.01, y_header_label)))
        text_labels.append(axes[0].annotate("{:60s}".format(" "), xycoords="axes fraction",
                                            xy=(0.75, y_header_label)))
        while len(text_labels) < N_LABEL_OFFSET:
            text_labels.append(None)

        for index, ax in enumerate(axes):
            text_labels.append(
                ax.annotate("{:03}".format(index), xycoords="axes fraction", xy=(0.01, 0.03)))

        handles["text_labels"] = text_labels
    else:
        text_labels = handles["text_labels"]

    if not handles["lines"]:
        # add factor 2 because one graph may contain 4 lines
        lines = [None for i in range(5 * len(axes))]
        handles["lines"] = lines
    else:
        lines = handles["lines"]

    clean_up_plot(lines)
    clean_up_plot_text_labels(text_labels)

    # loop over the dof components and add a line to one of the sub plots
    ph = list()
    for i_channel, name in enumerate(sub_plot_names):

        # reference to the plot
        plot_index = n_columns * i_channel

        tower_acc_name = settings.tower_origin_acc_name + "_{}".format(name)

        # get the calculated tower origin from the data frame
        values = mdf_file.data[tower_acc_name].values

        # look up the limits for this plot and update the name for the plot
        limits = settings.signal_tower_origin_plot_limits[name]

        time_r = mdf_file.data.time_r

        # create a time label of the start time.
        time_zero_label = re.sub("\+.*", "", "{}".format(
            np.datetime64(mdf_file.data[tower_acc_name].index[0], 's')))

        text_labels[0].set_text("{}".format(plot_title))
        text_labels[1].set_text(title)
        text_labels[2].set_text("{}".format(time_zero_label))

        log.info("Plotting signal with mean : {}".format(nanmean(values)))

        for l in range(n_columns):
            # plot the full (l=0) and zoomed (l=1)
            axes[plot_index + l].relim()
            axes[plot_index + l].autoscale_view(True, True, True)
            set_auto = True
            if None not in limits and not settings.signal_tower_origin_plot_properties[
                "force_autoscale"]:
                set_auto = False
            else:
                log.debug("Setting limit {} : {} ".format(tower_acc_name, limits))

            axes[plot_index + l].set_ylim(bottom=limits[0], top=limits[1], auto=set_auto)
            line = axes[plot_index + l].plot(time_r, values, '-r', label="FromACC",
                                             linewidth=LINE_WIDTHS[l])
            lines[plot_index + l] = line

            if l == 0 and i_channel == 0:
                ln, = line
                ph.append(ln)

            # also try to add the matlab data if available
            if mat_file is not None:
                try:
                    df_matlab_tow = mat_file.fms_timeseries_dict["TowerOAcc"]
                except KeyError as err:
                    log.warning("{}".format(err))
                    continue

                df_matlab_time = (df_matlab_tow.index - df_matlab_tow.index[0]).astype(
                    'timedelta64[ms]')
                df_matlab_time /= 1000.0

                values_ml_tow_acc = df_matlab_tow[tower_acc_name].values

                if settings.signal_tower_origin_plot_properties["subtract_mean"]:
                    log.debug("subtracking mean value from matlab signal tower origin {}"
                              "".format(nanmean(values_ml_tow_acc)))
                    values_ml_tow_acc -= nanmean(values_ml_tow_acc)
                else:
                    log.debug("keep mean intact {}" "".format(nanmean(values_ml_tow_acc)))

                line = axes[plot_index + l].plot(df_matlab_time, values_ml_tow_acc, "-b",
                                                 label="Matlab",
                                                 linewidth=1)

                lines[plot_index + l + 2 * len(axes)] = line
                if l == 0 and i_channel == 0:
                    ln, = line
                    ph.append(ln)

            # finally, add a line of the mru signal as a check
            if settings.add_mru_signal_to_accelerometer_plot:
                mru_name = "MRU_at_" + settings.tower_origin_name + "_{}".format(name)
                try:
                    mru_val = mdf_file.data[mru_name].values

                    log.info("Plotting TOWER MRU signal at location: {}".format(mru_name))
                    line = axes[plot_index + l].plot(time_r, mru_val, 'g--',
                                                     linewidth=LINE_WIDTHS[l],
                                                     label="MRU")
                    lines[plot_index + l + 2 * len(axes)] = line
                    if l == 0 and i_channel == 0:
                        ln, = line
                        ph.append(ln)
                except KeyError:
                    log.debug("Can not add MRU to plot. No name: {}".format(mru_name))

    log.debug("Adding legend with len {}".format(len(ph)))
    # add a position label only in the to plot (for j_plot==1)
    axes[n_columns - 1].legend(handles=ph, loc=1, bbox_to_anchor=(1.22, 0.94), title="TowerOACC")

    plt.draw()
    plt.pause(0.05)

    image_root = "TOWER_ACC{}_".format(plot_type)
    image_str = "{}N{}_".format(image_root, n_columns)
    image_base = re.sub(settings.mdf_file_base, image_str, file_base_name)
    if settings.save_images:
        save_plot_to_file(fig, image_base, settings)

    if settings.dump_plot_lines_to_file:
        save_plot_lines_to_file(axes, image_base, settings, add_zoom_plot, t_start=t_start,
                                t_end=t_end)

    return handles
