# -*- coding: utf-8 -*-

# note that this encoding specification above is required to be able to process the degrees symbol
# in the routine 'retrieve_longitude_latitude(longitude_latitude_string)' below!

"""
The class definition for reading FMS data files
"""
from __future__ import division

import os
import re
import string
from builtins import object
from builtins import range
from collections import OrderedDict

import matplotlib.pyplot as plt
import netCDF4 as nc
import numpy as np
import pandas as pd
import scipy.io as sio
from cycler import cycler
from matplotlib.gridspec import GridSpec
from past.utils import old_div
from pytz import timezone
from scipy.constants import g as g0
from scipy.interpolate import (interp1d)
from xlrd.biffh import XLRDError

import fatigue_monitoring.fms_utilities as fut
from LatLon.LatLon import LatLon
from fatigue_monitoring.fms_utilities import MATLAB_HOT_SPOT_NAME_LIST
from hmc_marine.data_readers import LiftDynReader
from hmc_marine.wave_spectra import (omega_e_vs_omega)
from hmc_utils import Q_
from hmc_utils.date_time import matlabnum2date
from hmc_utils.misc import (get_logger, Timer)
from hmc_utils.numerical import (get_nearest_index, get_range_from_string, find_idx_nearest_val)


def six_dof_names(name):
    """
    create a list of names belong to the six dof components given a base name 'name
    :param name: base of the six dof names
    :return:
    """
    return [name + "_" + ext for ext in ("AX", "AY", "AZ", "RXX", "RYY", "RZZ")]


def wind_direction_in_degrees(direction_name):
    """
    return the wind direction in degrees based on the name. Very ugly, do better later
    :param direction_name:
    :return:
    """
    wind_dir_names = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW",
                      "W", "WNW", "NW",
                      "NNW"]
    wind_angles = [direction * 360.0 / 16.0 for direction in range(16)]
    index = wind_dir_names.index(direction_name)
    return wind_angles[index]


# from PDFlib.PDFlib import PDFlib
def retrieve_longitude_latitude(longitude_latitude_string):
    """
    small utility to turn the string as found in the meteoconsult : From\?5650N5 circle 00'E?to?58 circle 24'N?5 circle 15\'E
    into a longitude and latitude
    :param longitude_latitude_string: as defined with the circles the asci charicaters for degrees
    :return:
    """
    # first turn the ascii string as obtained from the text file into unicode
    longitude_latitude_string = longitude_latitude_string.decode('utf-8', 'ignore')

    # now replace this degrees symbol
    longitude_latitude_string = longitude_latitude_string.replace(u'°', ' ').replace('\'',
                                                                                     ' ').replace(
        '"', ' ')

    # get rid of the question marks in the string
    location = re.sub("\?", " ", longitude_latitude_string)

    # the left over string is now 52 4 N 43 6 W (52 degrees, 4 minutes north, 43 degrees and 6 minutes west)
    # the pattern below matches to this layout of the string
    match = re.match("\s+(\d+)\s+(\d+)\s+([N|S])\s+(\d+)\s+(\d+)\s+([E|W])", location)

    # retrieve the components of the match
    lat_deg = match.group(1)
    lat_min = match.group(2)
    lat_dir = match.group(3)
    lon_deg = match.group(4)
    lon_min = match.group(5)
    lon_dir = match.group(6)
    # build the longitude and latitude string which includes the seconds as well
    lat_str = "{} {} 0 {}".format(lat_deg, lat_min, lat_dir)
    lon_str = "{} {} 0 {}".format(lon_deg, lon_min, lon_dir)

    # finally, use the string2latlon to get the longitude and latitude in decimal notion
    final_string = LatLon.string2latlon(lat_str, lon_str, 'd% %m% %S% %H')

    # return the latlon object
    return final_string


class FMSDatabase(object):
    def __init__(self, filename, wave_direction_definition=0, read_msg_pack_if_exists=True,
                 end_date_time=None, start_date_time=None, time_zone="UTC", heading_name="HEADING"):

        self.log = get_logger(__name__)

        self.wave_direction_definition = wave_direction_definition
        self.read_msg_pack_if_exists = read_msg_pack_if_exists

        self.filename = filename

        if start_date_time is not None:
            self.start_date_time = pd.Timestamp(start_date_time, tz=time_zone)
            self.start_date_time = self.start_date_time.tz_convert(tz="UTC")
            self.log.debug(
                "Set start {} to UTC time {}".format(start_date_time, self.start_date_time))
        else:
            self.start_date_time = None

        if end_date_time is not None:
            self.end_date_time = pd.Timestamp(end_date_time, tz=time_zone)
            self.end_date_time = self.end_date_time.tz_convert(tz="UTC")
            self.log.debug(
                "Set end time {} to UTC time {}".format(end_date_time, self.end_date_time))
        else:
            self.end_date_time = None

        if self.start_date_time is not None and self.end_date_time is not None:
            # check if start date is before end date
            if self.end_date_time < self.start_date_time:
                raise ValueError(
                    "Start date {} occurs later than end date {}. Please check your arguments"
                    "".format(self.start_date_time, self.end_date_time))

        # the local time zone of the monitoring PC
        self.tz_local = timezone(time_zone)

        # this date frame is going to contain the data
        self.data_frames = dict()
        self.total_number_of_voyages = 0

        self.log.debug("Opening the pdf file (or text equivalent {}".format(self.filename))

        self.read_the_hindcast_trajectory(heading_name=heading_name)

    def read_the_hindcast_trajectory(self, heading_name="HEADING"):
        """
        Read the hind cast data base trajectory
        """

        self.log.debug("Opening file {}".format(self.filename))
        try:
            df = pd.read_excel(self.filename, sheetname="General", index_col="DateTime")
        except XLRDError:
            df = pd.read_excel(self.filename, index_col="DateTime")

        # make sure that the index is a datetime index
        df.index = [pd.Timestamp(datetime) for datetime in df.index.values]

        # store the time per sea state in the data frame
        df["time_per_sea_state"] = df["travel_time"].diff()

        median = df["time_per_sea_state"].median()

        # the first value is Nan, so just fill it with the most common value in the list
        df["time_per_sea_state"].fillna(median, inplace=True)

        # set the time zone to the local time zone
        try:
            df = df.tz_localize(self.tz_local, ambiguous='NaT')
        except TypeError as err:
            self.log.debug(err)
            self.log.debug("no problem. just continue")

        self.log.debug("index size before  {}".format(df.index.size))

        # now convert it to utc. We have to deal with the daylight saving time, so do some tricking here
        df.index = df.index.tz_convert("UTC")
        # in order to drop the NaT columns, first create a column TMP out of the datetimeindex,
        # then you can use the notnull method to set the none NaT values and then clear TMP again
        df["TMP"] = df.index.values
        df = df[df.TMP.notnull()]
        df.drop(["TMP"], axis=1, inplace=True)

        # select the data range if given
        if self.start_date_time is not None:
            df = df[df.index >= self.start_date_time]

        if self.end_date_time is not None:
            df = df[df.index <= self.end_date_time]

        self.log.debug("{}".format(df))

        heading = df[heading_name]

        # calculate the relative wave direction with respect to the heading for all wave types
        offset = 180
        for wave_type in ("wind", "swell", "swell1", "swell2", "tot"):
            direction_name = "direction_wave_{}".format(wave_type)
            rel_direction_name = "rel_{}".format(direction_name)
            try:
                df[rel_direction_name] = np.mod(
                    heading - (df[direction_name] - offset), 360)
            except KeyError:
                self.log.debug("Could not get wave type {}. Skipping".format(wave_type))

        self.data_frames[self.total_number_of_voyages] = df
        self.total_number_of_voyages += 1


class NOAADatabase(object):
    def __init__(self, filename, wave_direction_definition=0, read_msg_pack_if_exists=True):

        self.log = get_logger(__name__)

        self.wave_direction_definition = wave_direction_definition
        self.read_msg_pack_if_exists = read_msg_pack_if_exists

        self.filename = filename

        # this date frame is going to contain the data
        self.data_frames = dict()
        self.total_number_of_voyages = 0

        self.log.debug("Opening the pdf file (or text equivalent {}".format(self.filename))

        self.read_the_noaa_forecast_trajectory()

    def read_the_noaa_forecast_trajectory(self):
        """
        routine to read the noaa trajectory
        :return:
        """

        self.log.debug("Opening file ".format(self.filename))
        df = pd.read_excel(self.filename)

        self.log.debug("{}".format(df))

        try:
            heading = df["heading"]
        except KeyError:
            heading = df["HEADING"]

        # calculate the relative wave direction with respect to the heading for all wave types
        offset = 180
        for wave_type in ("wind", "swell", "swell1", "swell2", "tot"):
            direction_name = "direction_wave_{}".format(wave_type)
            rel_direction_name = "rel_{}".format(direction_name)
            try:
                df[rel_direction_name] = np.mod(
                    heading - (df[direction_name] - offset), 360)
            except KeyError:
                self.log.debug("Could not get wave type {}. Skipping".format(wave_type))

        self.data_frames[self.total_number_of_voyages] = df
        self.total_number_of_voyages += 1


class MeteoConsultDatabase(object):
    def __init__(self, filename, wave_direction_definition=0, read_msg_pack_if_exists=True):

        self.log = get_logger(__name__)

        self.wave_direction_definition = wave_direction_definition
        self.read_msg_pack_if_exists = read_msg_pack_if_exists

        self.filename = filename

        # this date frame is going to contain the data
        self.data_frames = dict()
        self.total_number_of_voyages = 0

        self.log.debug("Opening the pdf file (or text equivalent {}".format(self.filename))

        self.read_the_meteo_file()

    def read_the_meteo_file(self):

        try:
            weather_per_day = self.get_days_from_text_file()
        except IOError as err:
            self.log.warning(err)
            raise IOError(err)

    def get_days_from_text_file(self):
        """
        open the text file that is obtained from the meteoconsult pdf report using pdftotext -layout
        :return: a list of dataframes with each data frame the weather data of one day
        """
        weather_data_per_day = list()
        start_location_per_day = list()
        start_date_per_day = list()
        number_of_samples = 0
        # these columns correspond to the columns in the pdf file
        pdf_info_columns = ["DateTime", "Dir_wind", "Speed_wind_10m", "Gusts_wind_10m",
                            "Speed_wind_50m",
                            "Gusts_wind_50m", "Hs_total", "Hm_total", "Tp_total", "Dir_swell",
                            "Hs_swell", "Tp_swell",
                            "Weather", "Visibility"]
        # these columns correspond with the columns being stored in the data frame (a selection). The first is
        # the name as found in the pdf file, the second is a convenient alias
        columns_selection = list([
            ("DateTime", "date_time"),
            (None, "travel_time"),
            (None, "latitude"),
            (None, "longitude"),
            (None, "travel_distance"),
            (None, "speed_sustained"),
            ("Hs_total", "Hs_tot"),
            ("Tp_total", "Tp_tot"),
            (None, "direction_wave_wind"),
            ("Hs_swell", "Hs_swell"),
            ("Tp_swell", "Tp_swell"),
            ("Dir_swell", "direction_wave_swell"),
            ("Speed_wind_10m", "speed_wind"),
            ("Dir_wind", "direction_wind"),
            (None, "heading"),
        ])

        # open the data file
        voyage_start_date_time = None
        with open(self.filename) as fp:

            # loop over all the line of the text file
            for cnt, line in enumerate(fp.readlines()):

                # split this line into separate columns
                columns = line.split()
                if not columns:
                    # skip the empty lines
                    continue

                # get the first column. Make sure to remove the ? signs which occur in the strings sometimes
                first_col = re.sub("\?", " ", columns[0])
                try:
                    # try to convert the first column into a datetime string.
                    date_time = pd.to_datetime(first_col, box=True)
                except ValueError:
                    # if we cannot change the first column into a date time (either a date or a time), this row
                    # is something else, header, footer, whatever, and we can skip it
                    pass
                else:
                    # ok, we were able to change the first column into a datetime string. Now get the data
                    if re.match("\d", first_col[0]):
                        # this is a time string. Overrule the date with the last date encountered
                        date_time = pd.datetime(last_date.year, last_date.month, last_date.day,
                                                date_time.hour,
                                                date_time.minute)
                        if voyage_start_date_time is None:
                            # store the very first start date
                            voyage_start_date_time = date_time
                        self.log.debug(" time string={}; results = {}".format(first_col, date_time))
                        columns[0] = date_time
                        weather_data_per_day[-1].append(columns)
                        number_of_samples += 1
                    else:
                        # this is a date string (eg. Tue 28 Jul). Store it for the next lines
                        last_date = date_time
                        fromto = columns[1]
                        self.log.debug("FROM {} ".format(fromto))
                        match = re.match("From(.*)to(.*)", fromto)
                        coordinates_from = retrieve_longitude_latitude(match.group(1))
                        coordinates_to = retrieve_longitude_latitude(match.group(2))
                        self.log.info(
                            "At {} going from/to  {}  ---> {}".format(last_date, coordinates_from,
                                                                      coordinates_to))

                        # store the info in list
                        start_location_per_day.append([coordinates_from, coordinates_to])
                        start_date_per_day.append(last_date)
                        weather_data_per_day.append(list())

        # create a datetime index sequence from the begin of the start date to the end of the last date
        last_sample_time = start_date_per_day[-1] + pd.Timedelta('23:30:00')
        date_time_range = pd.date_range(voyage_start_date_time, last_sample_time, freq='3h')
        # creat a list of all the columns we want to store in the dataframe. This is the second row of the
        # columns_selection list above
        df_columns = [name[1] for name in columns_selection]
        df = pd.DataFrame(index=date_time_range,
                          data=np.nan,
                          columns=df_columns)

        for cnt, day_record in enumerate(weather_data_per_day):
            coordinates_from = start_location_per_day[cnt][0]  # coordinates start of day
            coordinates_to = start_location_per_day[cnt][1]  # coordinates at start of -next- day
            # the coordinates from and to are one day separated. Each records takes 3 hours. so per 3-hours records: 1/8
            delta_coord_per_record = old_div((coordinates_to - coordinates_from), 8)
            distance_travelled_this_day = old_div(coordinates_from.distance(coordinates_to), 1.851)
            heading = coordinates_from.heading_initial(coordinates_to)
            speed_in_kn = old_div(distance_travelled_this_day, 24.0)
            self.log.debug("Day {} : from {} to {} . Distance {} Speed {} Heading {}"
                           "".format(start_date_per_day[cnt], coordinates_from, coordinates_to,
                                     distance_travelled_this_day, speed_in_kn, heading))
            self.log.debug(day_record)
            # loop over all the time samples in this day records
            coordinates_now = coordinates_from
            for i_row, row in enumerate(day_record):
                date_time = row[0]
                self.log.debug("row {} start at {}: {}".format(i_row, date_time, row))

                # first store all the columns from the pdf file which we selected and store them for this time record
                for col_name in columns_selection:
                    col_name_in_pdf = col_name[0]
                    col_name_in_dateframe = col_name[1]
                    if col_name_in_pdf is not None:
                        index = pdf_info_columns.index(col_name_in_pdf)
                        value = row[index]
                        try:
                            value = float(value)
                        except ValueError:
                            # this is not a number. If it is a wind direction string, turn it into degrees
                            if bool(re.match("Dir", col_name_in_pdf)):
                                # convert the wind directioni into degrees
                                value = wind_direction_in_degrees(value)
                        except TypeError:
                            # a date time field gives a type error. Do nothing
                            pass

                        self.log.debug("index {} of {} with value {}  store to {}"
                                       "".format(index, col_name_in_pdf, value,
                                                 col_name_in_dateframe))

                        df.ix[date_time, col_name_in_dateframe] = value

                # now store some of our own columns
                df.ix[date_time, "speed_sustained"] = speed_in_kn
                df.ix[date_time, "heading"] = heading

                # get the new position of this record and store the latitude and longitude
                coordinates_now += delta_coord_per_record
                cur_lat, cur_lon = coordinates_now.to_string("D")
                df.ix[date_time, "latitude"] = float(cur_lat)
                df.ix[date_time, "longitude"] = float(cur_lon)

                # the direction of the wind waves is unknow, so just set to zero for now

        # assume that the wind wave come from the same direction as the wind direction
        df["direction_wave_wind"] = df["direction_wind"]

        # we have now the Hs swell, Tp swell and Dir swell, so we can define a swell spectrum
        # next to that we have a Hs tot, Tp tot and Dir tot (which is assumed equal to the wind direction)
        # we should calculate the wind Hs by subtracting iteratively varying the jonswap spectrum with Hs_wind, add
        # it to the swell spectrum and calculate the Hs_tot as 4sigma. However, I just do it simple now
        df["Hs_wind"] = df["Hs_tot"] - df["Hs_swell"]
        df["Tp_wind"] = df["Tp_tot"]

        # store the travel time relative from the beginning in hours
        df["travel_time"] = old_div((df.index - df.index[0]), pd.Timedelta(1, unit='h'))

        # from the current speed and the travel time CALCULATE the displacement per row, and then the total distance
        # as the cumsum
        displacement = df["speed_sustained"] * df["travel_time"].diff().fillna(0)
        df["travel_distance"] = displacement.cumsum()

        # store the time per sea state in the data frame
        df["time_per_sea_state"] = df["travel_time"][1] - df["travel_time"][0]

        # wave direction is either coming from or going to
        if self.wave_direction_definition == 0:
            offset = 0
        else:
            offset = 180
        df["rel_direction_wave_wind"] = np.mod(
            df["heading"] - (df["direction_wave_wind"] - offset), 360)
        df["rel_direction_wave_swell"] = np.mod(
            df["heading"] - (df["direction_wave_swell"] - offset), 360)

        self.data_frames[self.total_number_of_voyages] = df

        self.total_number_of_voyages += 1

        # does not work yet. Try with the text version of the pdf as obtained with save as text from
        # pdftotext (from the poppler util in cygwin)

        # p = PDFlib()
        # p.set_option("errorpolicy=return")
        # p.set_option("SearchPath={{" + "." + "}}")
        # p.set_info("Creator", "PDFlib starter sample")
        # p.set_info("Title", "starter_pdfmerge")
        # indoc = p.open_pdi_document(self.filename, "")
        # if (indoc == -1):
        #    self.log.warning("Error: {}".format(p.get_errmsg()))
        #    raise
        # endpage = p.pcos_get_number(indoc, "length:pages")
        # with p.pcos_get_stream(indoc) as  s:
        #    print(s)


class SafeTransMatlabDatabase(object):
    """
    Read the matlab Safetrans file
    """

    def __init__(self, filename, wave_direction_definition=0, start_date_string='20000101',
                 read_msg_pack_if_exists=True):

        self.log = get_logger(__name__)

        self.wave_direction_definition = wave_direction_definition
        self.start_date_string = start_date_string
        self.read_msg_pack_if_exists = read_msg_pack_if_exists

        self.filename = filename

        # the value is a dictionary with a name field to store the base name as will be stored in the dataframe and
        # a empty data field which will be assigned later
        # some version of the RAO file from matlab call the RAO 'RAO' and other 'RAOs'. Here take care that all becomes
        # RAO by adding
        self._matdata_dict = dict(
            SafeTrans=dict(name="SafeTrans", type="data")
        )

        # this dictionary is going to carry all the dataframe of the individual journeys
        self.total_number_of_voyages = 0

        # the second values is the replacement of the names to get more convenient naming
        self.matlab_aliases = fut.MATLAB_ALIASES

        # number of data point in one row of the matlab file

        # extract the base of the file name to read
        try:
            self.file_base_name, self.extension = os.path.splitext(filename)
        except IndexError:
            self.extension = None

        self.msg_pack_filename = self.file_base_name + ".msg_pack"

        # if the file ends with .mat, do the reading of the data now
        if self.read_msg_pack_if_exists and os.path.exists(self.msg_pack_filename):
            self.log.info("Reading the rao_vessel msg_pack trajectory database {}.".format(
                self.msg_pack_filename))
            try:
                with Timer(name="read_msgpack") as t:
                    self.data_frames = pd.read_msgpack(self.msg_pack_filename)
                self.log.debug("Successfully read the data base")
            except IOError as err:
                self.log.warning(err)
                raise
        else:
            # in case there is no msg_pack file, just read the matlab file
            self.data_frames = fut.msg_pack_information_dictionary("Matlab", self.filename)
            if re.match(".mat$", self.extension):
                self.read_matlab_datafile()
            else:
                raise IOError("File extension of matlab file must be .mat")

            # when done, write the data base to a message pack file
            self.write_message_pack_data_base()

    def read_matlab_datafile(self):
        # try the read the data
        try:
            matlab_structure = sio.loadmat(self.filename)
        except IOError as err:
            self.log.info(err)
            raise

        # loop over the data file and create field of the class
        for key, value in matlab_structure.items():
            # store the type of the current value
            type_of_value = type(value)
            self.log.debug("Found matlab data {} ({})".format(key, type_of_value))

            if type_of_value is np.ndarray:

                data_header = value['dataHeader']
                data_names = data_header[0][0][0]
                n_names = data_names.shape[0]
                data = value['data'][0][0].T

                data_container = dict()

                column_list = []

                for cnt in range(n_names):
                    name = data_names[cnt][0]
                    alias = self.matlab_aliases[name]
                    self.log.debug("reading {} {} to {}".format(cnt, name, alias))
                    data_container[alias] = data[cnt, :]
                    column_list.append(alias)

                travel_times = data_container['travel_time']

                zeros_index = np.where(travel_times == 0)[0]
                self.log.debug("zeros {}".format(zeros_index))
                n_columns = len(column_list)

                for id in range(zeros_index.size):

                    i_start = zeros_index[id]

                    try:
                        i_end = zeros_index[id + 1]
                    except IndexError:
                        # for the final track take the size of the total time array to get the size of the last track
                        i_end = travel_times.size

                    n_track = i_end - i_start

                    self.log.debug("Reading track {} {}".format(id, n_track))

                    # create a empty data frame to hold the data of this track
                    df = pd.DataFrame(data=np.empty((n_track, n_columns)), columns=column_list)

                    for col_name, col_data in data_container.items():
                        df[col_name] = data_container[col_name][i_start:i_end]

                    time_per_sea_state = 3  # time per sea state in hours
                    df = df[df["travel_time"] % time_per_sea_state == 0]

                    # store the time per sea state in the data frame
                    df["time_per_sea_state"] = time_per_sea_state

                    if self.wave_direction_definition == 0:
                        offset = 0
                    else:
                        offset = 180
                    df["rel_direction_wave_wind"] = np.mod(
                        df["heading"] - (df["direction_wave_wind"] - offset), 360)
                    df["rel_direction_wave_swell"] = np.mod(
                        df["heading"] - (df["direction_wave_swell"] - offset), 360)

                    # create a date time range. Since it is not stored in the matlab  data base, just generate
                    # a range for each journey starting from a given start date where each voyage leaves one second
                    # later.
                    df['date_time'] = pd.date_range(self.start_date_string, periods=len(df.index),
                                                    freq='3h')
                    df["date_time"] = pd.to_datetime(df["date_time"]) + \
                                      pd.Timedelta(seconds=self.total_number_of_voyages)
                    df.set_index("date_time", inplace=True)

                    # store the current seastae in the dictionary
                    self.data_frames[self.total_number_of_voyages] = df

                    self.total_number_of_voyages += 1
            else:
                # store the non-array data, which is the strings
                setattr(self, key, value)
        self.log.debug("-------- done reading the data --------------")

        # turn the rao, frequency and direction array as read by matlab into object attributes such that you can
        # refer to the values as rao.FreqRange, rao.DirRange (assuming the object was called rao)
        for key, value in self._matdata_dict.items():
            try:
                setattr(self, value["name"], value["data"])
            except KeyError:
                pass

    def write_message_pack_data_base(self):
        try:
            self.log.debug("Dumping data base to {}".format(self.msg_pack_filename))
            pd.to_msgpack(self.msg_pack_filename, self.data_frames)
        except IOError as err:
            self.log.warning(err)
            raise

    def properties(self):
        # purpose: return all properties of the class in one dictionary, but
        # exclude the classes
        return dict(
            (key, getattr(self, key)) for key in dir(self) if key not in dir(self.__class__))

    def list_properties(self):
        # list of the propery fields and its values of this class
        info = ""
        for k, v in list(self.properties().items()):
            tv = type(v)
            if tv is np.ndarray:
                info += "{:<40} : {:<8} ({})\n".format(k, v.mean(), v.size)
            elif tv is dict:
                info += "{:<40} : {}".format(k, type(v))
            elif tv is str or tv is bytes:  #
                info += "{:<40} : {}\n".format(k, v)
            else:
                info += "{:<40} : {}\n".format(k, v)

        info += "\n"
        for key, value in self._matdata_dict.items():
            try:
                mean_value = value["data"].mean()
                shape = value["data"].shape
            except KeyError:
                mean_value = None
                shape = None
            info += "{:<40} : {} {}\n".format(key, mean_value, shape)

        self.log.info(info)


class RaoMatlabDatabase(object):
    """
    Read the matlab RAO file
    """

    def __init__(self, filename, location_name="TowO", rao_settings=None,
                 direction_range=None,
                 frequency_range=None,
                 f_min_used_for_direction_pick=0.0,
                 f_max_used_for_direction_pick=None,
                 f_clip_min=0.0,
                 f_clip_max=None,
                 raise_debug_plots=False,
                 dump_rao_in_out=False,
                 plot_frequency_indices=None,
                 tower_dof_phase_zero="TowO_ACC_RXX",
                 n_velocity=0,
                 min_velocity=Q_(0, "meter/second"),
                 max_velocity=Q_(5, "meter/second"),
                 rao_dump_original_as_netcdf=False,
                 rao_save_one_d_dir_and_freq=False,
                 date_time_start=None,
                 date_time_end=None,
                 debug_plot=False
                 ):

        self.log = get_logger(__name__)

        self.debug_plot = debug_plot

        # set the rao tune settings
        try:
            # here we can set the variables via a dictionary is this is passed. Only to ensure
            # backward compatibility
            rao_tune_settings = rao_settings["rao_tune"]
            # if this succeeds it means we have passed a dictionary in stead of individual
            # parameters. Set the vars here
            self.direction_range = rao_tune_settings["direction_range"]
            self.frequency_range = rao_tune_settings["frequency_range"]
            self.f_min_used_for_direction_pick = rao_tune_settings["f_min_used_for_direction_pick"]
            self.f_max_used_for_direction_pick = rao_tune_settings["f_max_used_for_direction_pick"]
            self.f_clip_min = rao_tune_settings["f_clip_min"]
            self.f_clip_max = rao_tune_settings["f_clip_max"]
            self.raise_debug_plots = rao_tune_settings["raise_debug_plots"]
            self.dump_rao_in_out = rao_tune_settings["dump_rao_in_out"]
            self.plot_frequency_indices = rao_tune_settings["plot_frequency_indices"]
            self.tower_dof_phase_zero = rao_tune_settings["tower_dof_phase_zero"]
            self.rao_dump_original_as_netcdf = rao_settings["rao_dump_original_as_netcdf"]
            self.rao_save_one_d_dir_and_freq = rao_settings["rao_save_one_d_dir_and_freq"]
        except (KeyError, TypeError):
            # set default settings for rao_tune_settings
            self.direction_range = direction_range
            self.frequency_range = frequency_range
            self.f_min_used_for_direction_pick = f_min_used_for_direction_pick
            self.f_max_used_for_direction_pick = f_max_used_for_direction_pick
            self.f_clip_min = f_clip_min
            self.f_clip_max = f_clip_max
            self.raise_debug_plots = raise_debug_plots
            self.dump_rao_in_out = dump_rao_in_out
            self.plot_frequency_indices = plot_frequency_indices
            self.tower_dof_phase_zero = tower_dof_phase_zero
            self.rao_dump_original_as_netcdf = rao_dump_original_as_netcdf
            self.rao_save_one_d_dir_and_freq = rao_save_one_d_dir_and_freq

        self.filename = filename
        self.location_name = location_name

        # the value is a dictionary with a name field to store the base name as will be stored in
        # the dataframe and a empty data field which will be assigned later some version of the RAO
        # file from matlab call the RAO 'RAO' and other 'RAOs'. Here take care that all becomes
        # RAO by adding
        self._matdata_dict = dict(
            RAOs=dict(name="RAOs", type="complex"),
            RAO=dict(name="RAOs", type="complex"),
            FreqRange=dict(name="FreqRange", type="f_axis"),
            DirRange=dict(name="DirRange", type="t_axis"),
        )

        self.data = None
        self.RAOs = None
        self.lift_dyn = None
        self.data_zero_velocity = None
        self.current_speed_in_knots = 0
        # number of data point in one row of the matlab file
        self.n_data_points = None
        self.frequencies = None  # all frequencies in rad/s
        self.directions = None  # all directions in rad
        self.dir_2d_mesh = None
        self.freq_2d_mesh = None

        # it is possible to have a list of RAO's
        self.times = None
        self.rao_list_all_times = list()
        self.date_time_start = date_time_start
        self.date_time_end = date_time_end
        self.current_time = None
        self.current_date_time = None
        self.current_time_index = None
        self.hours_since_start = None
        self.hours_since_end = None
        self.time_units = None
        self.time_calendar = None

        self.dirs_2d_uniform_list = list()
        self.freq_2d_uniform_list = list()

        self.frequencies_deg = None
        self.directions_deg = None
        self.complex_transfer = None

        self.delta_frequency = None
        self.delta_directions = None
        self.delta_area = None

        # number of degrees of freedom as obtained from the RAO shape, and the number of frequencies
        # and directions
        self.n_dof = None
        self.n_frequencies = None
        self.n_directions = None
        self.shape2D = None
        self.six_dof_names = None
        self.rao_per_dof = None
        self.unity_name = "Unity"
        self.n_complex_frequencies_per_dof = None

        # this is going to hold the directional distribution based on the first loaded RAO
        self.directional_distributions_1d = OrderedDict()
        self.frequency_distributions_1d = OrderedDict()

        self.f_clip_min_index = None
        self.f_clip_max_index = None

        self.n_time_steps_to_show = 0

        # extract the base of the file name to read
        try:
            self.file_base_name, self.extension = os.path.splitext(filename)
        except IndexError:
            self.extension = None

        self.log.debug("Reading {} from {}".format(self.filename, os.getcwd()))
        if re.match(".mat$", self.extension):
            # if the file ends with .mat, do the reading of the data now
            self.read_matlab_datafile()
        elif self.extension in (".stf_plt"):
            # if the file ends with .stf_plt, read the lift_dyn RAO
            self.read_liftdyn()
        elif re.match(".nc$", self.extension):
            # if the file ends with .nc, read the netcdf file format
            self.read_netcdf()
        else:
            # other formats are not supported. Raise an error
            raise ValueError("RAO should be a matlab (.mat) or a liftdyn (.stf_plt) file")

        direction_range_requested = self.direction_range
        frequency_range_requested = self.frequency_range

        # if the direction and frequency ranges are defined, resample the RAO on a new mesh
        if direction_range_requested is not None and frequency_range_requested is not None:
            self.log.debug("START RESAMPLING THE RAO : {} {}"
                           "".format(direction_range_requested, frequency_range_requested))
            self.resample_rao(direction_range_requested, frequency_range_requested)

        # we have read the 6 x n_freq x n_dirs array into self.RAOs. Now set some values based on
        # this array
        self.postprocess_data()

        # here we calculate the transformed RAO over a discrete range of velocities in m/s
        self.n_velocity = n_velocity
        self.min_velocity = min_velocity.to_base_units()
        self.max_velocity = max_velocity.to_base_units()
        # knots
        if self.n_velocity > 0:
            self.velocity_range = Q_(
                np.linspace(self.min_velocity.magnitude, self.max_velocity.magnitude,
                            self.n_velocity, endpoint=True), "meter/second")
            self.data_vs_velocity = list()
            self.log.debug("velocity range {}".format(self.velocity_range))
            self.calculate_rao_per_dof_vs_velocity()
        else:
            # velocity range not defined. Set it to one velocity only: 0 m/s
            self.velocity_range = list([Q_(0.0, "meter/second")])

        self.calculate_rao_zero_directional_distribution()

        if not re.match(".nc$", self.extension) and self.rao_dump_original_as_netcdf:
            # in case we have read a matlab or liftdyn rao, dump it as a netcdf file
            new_file_name = self.file_base_name + ".nc"
            self.log.info("Writing original RAO {} to new file {}".format(filename, new_file_name))
            rao_magnitude = np.abs(self.RAOs)
            self.create_netcdf_rao(rao_magnitude, new_file_name,
                                   description="Original RAO at start of reading")

        try:
            if self.rao_save_one_d_dir_and_freq:
                for comp in list(self.directional_distributions_1d.keys()):
                    self.log.info(
                        "Saving the 1D frequency and direction distribution {}".format(comp))
                    file_base = "_".join([self.file_base_name, comp])
                    self.frequency_distributions_1d[comp].to_csv(file_base + "_freq1d.csv")
                    self.directional_distributions_1d[comp].to_csv(file_base + "_dirs1d.csv")
        except TypeError:
            self.log.debug("rao settings not defined. Can not save the freq1d.csv and dirs1.csv")

    def resample_rao(self, directions_range, frequency_range):
        """
        a routine to resample the RAO on a new mesh.

        Parameters
        ----------
        directions_range: str
            a string as start:end:spacing with the direction range
        frequency_range: str
            a string as start:end:spacing with the frequency range

        Notes
        -----

        Note that the rao are cyclic in the theta direction axis.In order to take this boundaries in
        the theta direction correctly into account, we double the data planes from n_freq x n_dirs
        arrays to n_freq x 2n_dirs array, resample the double array, and then collect only
        the first half of the double plane again

        """

        # extract the new direction and frequencies from the input string
        new_directions = np.deg2rad(get_range_from_string(directions_range))
        new_frequencies = get_range_from_string(frequency_range)

        self.lift_dyn.resample_rao(new_directions, new_frequencies)

        self.log.debug("\n------------------------\n")

        # overwrite  the old with the new RAOS
        self.RAOs = self.lift_dyn.data
        self.directions = new_directions
        self.frequencies = new_frequencies

    def count_number_of_time_steps_to_show(self):

        self.n_time_steps_to_show = 0
        t_index = self.current_time_index
        counting = True
        while counting:
            try:
                this_time = self.times[t_index]
                if this_time > self.hours_since_end:
                    raise IndexError
            except IndexError:
                counting = False
            else:
                t_index += 1
                self.n_time_steps_to_show += 1

    def read_netcdf(self):
        """
        Read the RAO data from a netcdf file
        :return:
        """

        self.log.info("reading netcdf file {}".format(self.filename))
        ds = nc.Dataset(self.filename)
        variables = ds.variables
        rao_dict = OrderedDict()

        for var_name, var_data in variables.items():
            self.log.info("Reading variable {}".format(var_name))

            data_in = var_data[:]
            self.log.debug("data_in shape: {}".format(data_in.shape))
            self.log.debug("data_in type: {}".format(data_in.dtype))

            if var_name == "direction":
                self.directions = data_in
            elif var_name == "frequency":
                self.frequencies = data_in
            elif var_name == "time":
                self.times = data_in
                self.time_units = ds.variables["time"].units
                self.time_calendar = ds.variables["time"].calendar

                if self.date_time_start is not None:
                    self.hours_since_start = nc.date2num(pd.to_datetime(self.date_time_start),
                                                         units=self.time_units,
                                                         calendar=self.time_calendar)
                else:
                    self.hours_since_start = self.times[0]

                if self.date_time_end is not None:
                    self.hours_since_end = nc.date2num(pd.to_datetime(self.date_time_end),
                                                       units=self.time_units,
                                                       calendar=self.time_calendar)
                else:
                    self.hours_since_end = self.times[-1]

                self.current_time = self.hours_since_start

                time_index = np.where(self.times[:] == self.current_time)[0]
                if time_index.size > 0:
                    self.current_time_index = time_index[0]

                    self.count_number_of_time_steps_to_show()
                else:
                    self.current_time_index = None

                self.log.debug("Found current time {} with index {}".format(self.current_time,
                                                                            self.current_time_index))
            else:
                # in all other cases we are reading the RAO data
                try:
                    # this section is used to read the complex data rao. Since the complex type does
                    # not exist in netcdf, it was actually stored as compound data of two floats
                    # with the real part and a float with # the imaginary part.
                    # See http://unidata.github.io/netcdf4-python/#section10
                    rao_real = data_in["real"]
                    rao_imag = data_in["imag"]
                    datac = np.empty(data_in.shape, np.complex128)
                    datac.real = rao_real
                    datac.imag = rao_imag
                    self.rao_list_all_times.append(datac)
                except IndexError:
                    # if the section above failed, it means there is not compound complex data used,
                    # but that the real and imaginary part are explicitly stored in two separate
                    # variables RAO_NAME_abs and RAO_NAME_phase. Read them separately and and store
                    # them in an ordered dict
                    self.log.debug("Not compount complex data, so need to read the other case")
                    match = re.match("(.*)_(abs|phase)", var_name)
                    if bool(match):
                        rao_name = match.group(1)
                        rao_part = match.group(2)
                        try:
                            # this component was already initialised. Add now the second part )W
                            rao_dict[rao_name][rao_part] = data_in
                        except KeyError:
                            # the adding of the rao part failed, so initialise it with a dict and
                            # then add it
                            rao_dict[rao_name] = dict()
                            rao_dict[rao_name][rao_part] = data_in

        self.log.debug("Try to convert the abs/phase format into complex numbers")
        for rao_name, rao_data in rao_dict.items():
            # in case we have read the abs/phase format as explained above, the rao_list is still
            # empty but we have store the abs and phase in the rao_dict. Here we loop over the dict,
            # create a complex number from tbe rao absolute value (magnitude) and phase and append
            # them in the rao_list.
            self.log.debug("Adding RAO {}".format(rao_name))
            self.rao_list_all_times.append(rao_data["abs"] * np.exp(1j * rao_data["phase"]))

        # at this point we have added 6 RAO to the rao_list. convert the list to a
        # 6 x n_freq x n_dirs matrix
        self.log.debug("Converting the 6 RAO's into a matrix")
        self.set_rao_from_list()

    def shift_to_next_time(self, n_shift=1):
        """
        increase the current time index with on and get the next time and data
        :return:
        """

        next_index = self.current_time_index + n_shift
        success = True
        try:
            self.current_time = self.times[next_index]
            if self.current_time > self.hours_since_end:
                self.log.debug("Exeeding date time end set by user : {}".format(self.current_time))
                raise IndexError
        except IndexError:
            self.log.debug("Last time step reached")
            success = False
        else:
            self.current_time_index = next_index
            self.set_rao_from_list()

        return success

    def set_rao_from_list(self):
        """
        Works in combination with the netcdf reader. It is possible to have a time list of rao's.
        Set the current time
        """

        rao_list = list()
        for cnt, rao_component in enumerate(self.rao_list_all_times):
            self.log.debug("set comt {} with shape {}".format(cnt, rao_component.shape))
            if self.current_time_index is not None:
                self.log.debug("we have a time index: {}".format(self.current_time_index))
                rao_list.append(rao_component[self.current_time_index])
                self.current_date_time = nc.num2date(self.current_time, units=self.time_units,
                                                     calendar=self.time_calendar)
            else:
                self.log.debug("no time dimension.")
                rao_list.append(rao_component)

        self.RAOs = np.array(rao_list)

    def read_liftdyn(self):
        """
        reading the liftdyn rao data format (file extension must be stf_plt
        :return: Nothing, just stores the 6 x n_freq x n_dir array
        """

        # create instance of class LiftDynReader
        self.lift_dyn = LiftDynReader(self.filename)

        self.directions = self.lift_dyn.directions
        self.frequencies = self.lift_dyn.frequencies

        self.RAOs = self.lift_dyn.data

    def read_matlab_datafile(self):
        # try the read the data
        try:
            matlab_structure = sio.loadmat(self.filename)
        except IOError as err:
            self.log.info(err)
            raise IOError("Generated an error here to do with IO of the RAO: {}".format(err))

        # loop over the data file and create field of the class
        for key, value in matlab_structure.items():
            # store the type of the current value
            type_of_value = type(value)
            self.log.debug("Found matlab data {} ({})".format(key, type_of_value))

            if type_of_value is np.ndarray:

                shape_mat = value.shape

                try:
                    # try to store the current data of the matlab with the name given by 'key' in
                    # the _matdata dictionary. If this fails, then we did not anticipate on this
                    # data and with can continue otherwise the data is stored in the dictionary
                    # field 'key' with the label 'data'
                    if value.shape[0] == 1:
                        # for 1-D array it is handier to remove one direction otherwise you have
                        # to refer to the number as value[0, i] all the time
                        self.log.debug("Reducing range of {}".format(key))
                        value = value.reshape(-1)

                    if len(shape_mat) > 1:
                        # take care of the Fotran like row major ordering of matlab. this make a
                        # more python array where each 2D RAO can be referred to as rao_x = value[0]
                        # rao_y = value[1], with the index i running from 0 to 5 to refer to the 6
                        # DOF. Otherwise (without the transpose) you have to do
                        # rao_x = value[:,:,0], etc.
                        # http://stackoverflow.com/questions/37117779/transferring-matlab-3d-matrices-to-python-3d-arrays-via-
                        value = np.ascontiguousarray(value.T)

                    self._matdata_dict[key]["data"] = value

                    self.log.debug("Stored matlab data {} to our data_dict.".format(key))
                except KeyError:
                    # the key field is not available in our matdata structure, so skip it
                    self.log.debug("Matlab key {} not in our data_dict. Skipping".format(key))
            else:
                # store the non-array data, which is the strings
                setattr(self, key, value)
        self.log.debug("-------- done reading the data --------------")

        # turn the rao, frequency and direction array as read by matlab into object attributes such
        # that you can refer to the values as rao.FreqRange, rao.DirRange (assuming the object was
        # called rao)
        for key, value in self._matdata_dict.items():
            try:
                setattr(self, value["name"], value["data"])
            except KeyError:
                pass

        self.frequencies = self.FreqRange  # !! The RAO is already in rad per seconds
        self.directions = np.deg2rad(self.DirRange)  # deg to rad

    def postprocess_data(self):
        """
        after reading the data do some post processing to set the relevant fields
        :return: nothing
        """

        # calculate the 2D mesh belonging to the directions and frequencies
        self.dir_2d_mesh, self.freq_2d_mesh = np.meshgrid(self.directions, self.frequencies,
                                                          indexing='ij')

        # store the delta frequencies/direction and the array of the mesh. Note that direction is
        # in rad, frequecy rad/s, so the delta area has the units rad^2 /
        self.delta_frequency = self.frequencies[1] - self.frequencies[0]
        self.delta_directions = self.directions[1] - self.directions[0]
        self.delta_area = self.delta_frequency * self.delta_directions  # rad^2/s

        # number of degrees of freedom as obtained from the RAO shape, and the number of frequencies
        # and directions
        self.n_dof = self.RAOs.shape[0]
        self.n_frequencies = self.RAOs.shape[1]
        self.n_directions = self.RAOs.shape[2]

        if self.f_clip_min is not None:
            self.f_clip_min_index = get_nearest_index(self.frequencies, self.f_clip_min)
            self.log.debug("Found f clip min with index {} {} {}".format(self.f_clip_min,
                                                                         self.f_clip_min_index,
                                                                         self.frequencies[
                                                                             self.f_clip_min_index]))
        if self.f_clip_max is not None:
            self.f_clip_max_index = get_nearest_index(self.frequencies, self.f_clip_max)
            self.log.debug("Found f clip max with index {} {} {}".format(self.f_clip_max,
                                                                         self.f_clip_max_index,
                                                                         self.frequencies[
                                                                             self.f_clip_max_index]))

        # shape contains the original 2D spectrum shape
        self.shape2D = self.RAOs[0].shape

        self.six_dof_names = six_dof_names(self.location_name + "_ACC")
        self.log.debug("six dof names {}".format(self.six_dof_names))
        # create also a unity transfer function for validation purpose: the transfer spectrum is
        # equal to the sea spectrum, so the variance must be equal too

        # merge the direction and frequency into one 1D array so we can put it in a pandas DataFrame
        self.n_complex_frequencies_per_dof = self.n_frequencies * self.n_directions

        # store the 1D version of the 2D Rao planes in order to be able to tread the data the same
        # as the 1D time series
        rao_per_dof_list = []
        for i in range(self.n_dof):
            # if i==0:
            rao_per_dof_list.append(self.RAOs[i].flatten())

        # also add the unity raos
        rao_per_dof_list.append(np.full(self.RAOs[0].flatten().shape, complex(1, 0), complex))

        self.rao_per_dof = np.array(rao_per_dof_list[:]).reshape(
            self.n_dof + 1, self.n_complex_frequencies_per_dof).T

        # create a multiindex for the index, so we can store per Direction D, all frequencies F
        self.multi_index = pd.MultiIndex.from_product(
            [np.rad2deg(self.directions), self.frequencies], names=["D", "F"])

        # create the data frame with the following structure:
        # index

        #            ACC_AX   ACC_AY   ACC_AZ   ACC_RXX  ACC_RYY  ACC_RZZ
        #  D F
        #  0 0.01    0j      0j      0j      0j      0j      0j
        #  0 0.02    0j      0j      0j      0j      0j      0j
        # ..
        # 15 0.01    0j      0j      0j      0j      0j      0j
        # 15 0.02    0j      0j      0j      0j      0j      0j
        # ... etc

        # with reshape we turn the 3D array as read from matlab
        # (dimension n_dof * n_frequencies * n_directions) into
        # a 2D array of n_complex_frequencies_per_dof * self.n_dof. The reason is that we can then
        # treat the 1D array of frequencies per DOF the same a the time series further in the code,
        # so we can use the same routines as we use in the time domain
        self.data_zero_velocity = pd.DataFrame(self.rao_per_dof,
                                               columns=self.six_dof_names + [self.unity_name],
                                               index=self.multi_index)
        # the data refers to the rao we work with, the data_zero_velocity hold the original RAO
        # related to the zero velocity. In the code you can update the data rao with the current
        # velocity
        self.data = self.data_zero_velocity.copy()

        self.log.debug("Created data frame\n{}".format(self.data.head(10)))

    def write_dimensions_to_netcdf(self, ds, include_time_dimension=False, data_type="f8"):
        """
        a routine to initialise the coordinates of a net cdf data set  with
        :param ds: netcdf data set
        :param include_time_dimension: if true, also add a infinite time dimension
        :return: nothing, you can use the data set by reference
        """

        # dictionary carrying the information per dimension.
        dimension_dict = OrderedDict()

        if include_time_dimension:
            # in case you want to add the time, start with the time dimension
            dimension_dict["time"] = dict(units="hours since 0001-01-01 00:00:00", type=data_type,
                                          values=None)
        dimension_dict["frequency"] = dict(units="1/s", type=data_type, values=self.frequencies)
        dimension_dict["direction"] = dict(units="deg", type=data_type, values=self.directions)

        # loop over the dimensions and create the netcdf reference for this dimension
        for dim_name, properties in dimension_dict.items():
            # extract the properties of the current dimension
            dim_values = properties["values"]
            if dim_values is None:
                dim_length = None
            else:
                dim_length = dim_values.size
            dim_type = properties["type"]
            dim_units = properties["units"]
            # create the dimension
            ds.createDimension(dim_name, dim_length)
            # create the variable and fill the values
            dimension_dict[dim_name]["ds_var"] = ds.createVariable(dim_name, dim_type, (dim_name,),
                                                                   zlib=True)
            if dim_values is not None:
                dimension_dict[dim_name]["ds_var"][:] = dim_values
            else:
                # if the values were none, we are dealing with the time. Set the type of calender
                dimension_dict[dim_name]["ds_var"].calendar = "gregorian"
            dimension_dict[dim_name]["ds_var"].units = dim_units

    def create_netcdf_rao(self, raos_estimate, file_name, description=None, save_as_complex=False,
                          rao_phase=None):
        """
        Dump the current rao to a net cdf file.

        Parameters
        ----------
        raos_estimate: ndarray
            6 x n_freq x n_dir size array containing the magnitude of 6 DOF RAO's to be stored
            to file
        file_name: str
            name of the net cdf file
        description: str, optional
            a description of the current data, default = None
        save_as_complex: bool, optional
            if true, the net cdf data is stored as a 'complex', where a Compound argument is
            created to store the real and imaginary part. This works, but the disadvantage is that
            this compount data is not understood by Paraview. In case you want to import the data
            into Paraview, set save_as_complex to false. Then the real and imaginary part of each
            DOF component is stored in a separate variable each. Default =  false
        rao_phase: ndarray
            A 6 x n_freq x n_dir array with the phase in radians of the rao to be stored. It is
            assumed that the raos_estimate contains the magnitude as we have estimated it,
            the phase is obtained from the current RAO. In case the rao_phase argument is not None,
            this phase is taken in stead of the phase of the current RAOs
        """

        # open the net cdf data set
        self.log.info("Saving RAO to {}".format(file_name))
        ds = nc.Dataset(file_name, "w", format="NETCDF4")
        if description is not None:
            ds.description = description

        ds.history = "Created at {}".format(pd.to_datetime("now").tz_localize("UTC"))

        if save_as_complex:
            # here an example to save the data as a complex, by creating a tuple of two float and
            # then use compountType this is nice, but not so generic as you can not read the data
            # into paraview very easy create complex type in data set
            complex128 = np.dtype([("real", np.float64), ("imag", np.float64)])
            complex128_t = ds.createCompoundType(complex128, "complex128")
            # create empty complex data array which will hold the data per RAO component
            data_c = np.empty((self.n_frequencies, self.n_directions), complex128)

        # create the dimensions and fill then with the values
        self.write_dimensions_to_netcdf(ds)

        self.log.debug("Dimensions {}".format(ds.dimensions))

        # loop over the dof components and store them as complex data to the data set
        for cnt, dof_name in enumerate(self.six_dof_names):

            if rao_phase is None:
                # the phase of the current dof RAO is obtained from the pase of the standard RAO
                phase_2d = np.angle(self.RAOs[cnt])
            else:
                # the phase obtained from the rao_phase arguemnt when it is not None
                phase_2d = rao_phase[cnt]

            self.log.debug("Creating data array: {}".format(dof_name))
            if cnt < 3:
                # first three components are the linear accelerations RAO's in m/m
                units = "m/m"
            else:
                # last three components are the rotational accelerations RAO's in deg/m
                units = "deg/m"

            if save_as_complex:
                # create a complex numpy array from the estimated rao magnitude and the angle of the
                # base rao we have already loaded
                rao_c = raos_estimate[cnt] * np.exp(1j * phase_2d)
                # turn the original numpy complex in our own complex_128 which can be written to
                # netcdf
                data_c["real"] = rao_c.real
                data_c["imag"] = rao_c.imag
                variable = ds.createVariable(dof_name, complex128_t, ("frequency", "direction"),
                                             zlib=True)
                # fill the data of the new variable
                variable[:] = data_c
                variable.units = units
            else:
                # save the absolute and phase part of the RAO explicityly as two separate data sets.
                # Not so nice as complex, but at least you can read it directly into Paraview, which
                # is handy
                for part in ["abs", "phase"]:
                    var_name = "_".join([dof_name, part])
                    variable = ds.createVariable(var_name, "f8", ("frequency", "direction"),
                                                 zlib=True)
                    if part == "abs":
                        # the magnitude of the current dof RAO is the magnitude as estimated by the
                        # spectra
                        variable[:] = raos_estimate[cnt]
                        variable.units = units
                    else:
                        variable[:] = phase_2d
                        variable.units = "rad"

        self.log.debug("variables {}".format(ds.variables))

        # we are done. Close the data set
        ds.close()

    def dump_current_rao_to_file(self, file_name, wave_info_per_type=None):
        """
        Dump the currently selected rao to a simple message pack file
        :param file_name:
        :return: nothing
        """
        pd.to_msgpack(file_name, dict(
            rao=self.data[self.six_dof_names],
            frequencies=self.frequencies,
            direction=self.directions,
            rao_shift_speed_in_knots=self.current_speed_in_knots,
            wave_info_per_type=wave_info_per_type
        ))

    def pick_rao_for_speed(self, speed=Q_(0.0, "meter/seconds")):
        """
        Set the current rao data field using the one with this speed in knots

        Parameters
        ----------
        speed: :obj:`UnitRegistry`
            Current speed in to pick the RAO from. Can be given in knots but does not matter
            as we use dimensionful quantities

        """

        speed_in_meterps = speed.to_base_units()

        self.current_speed_in_knots = speed.to("knots")

        # get index of velocity
        i_max = find_idx_nearest_val(self.velocity_range.magnitude, speed_in_meterps.magnitude)

        self.log.debug("picking rao {} for speed {}".format(i_max, speed_in_meterps,
                                                            self.current_speed_in_knots))

        # set the rao data to the current velocity
        self.data = self.data_vs_velocity[i_max]

    def calculate_rao_zero_directional_distribution(self):
        """
        calculate the directional normalized distribution of all raos
        """

        debug_plot = self.raise_debug_plots
        dump_rao_in_out = self.dump_rao_in_out

        if dump_rao_in_out or debug_plot:
            # create plot for debugging only
            gs = GridSpec(6, 2)
            plot_name = "RAO_WAMIT_VS_1D"
            fig = plt.figure(plot_name, figsize=(10, 10))

        if self.plot_frequency_indices is None:
            self.plot_frequency_indices = list()
        plot_freq_indices = set(self.plot_frequency_indices)

        # create a data frame for the 1d directional distribution per DOF and one for the 1d
        # frequencies note that since we want to store both th e maxnitude and the phase, we care a
        # list of the directional dist
        for comp in ["abs", "phase"]:
            self.directional_distributions_1d[comp] = pd.DataFrame(index=self.directions,
                                                                   columns=self.six_dof_names,
                                                                   data=np.zeros((self.n_directions,
                                                                                  self.n_dof)))
            self.directional_distributions_1d[comp].index.name = "Direction [rad]"

            self.frequency_distributions_1d[comp] = pd.DataFrame(index=self.frequencies,
                                                                 columns=self.six_dof_names,
                                                                 data=np.zeros((self.n_frequencies,
                                                                                self.n_dof)))
            self.frequency_distributions_1d[comp].index.name = "Frequency [rad/s]"

        # the list that are going to hold the 2D meshes of the uniform distributions
        self.dirs_2d_uniform_list = list()
        self.freq_2d_uniform_list = list()

        axis = dict()
        figs = dict()
        rao_comp_list = list()
        i_freq_max_list = list()
        j_dirs_max_list = list()
        i_freq_max = None
        j_dirs_max = None

        for i_dof, dof_name in enumerate(self.six_dof_names):

            if debug_plot or dump_rao_in_out:
                # open the plots in case we want to show it
                figs[dof_name], axis[dof_name] = plt.subplots(nrows=2, ncols=1, sharex=True)
                unit = "m/m" if i_dof < 3 else "deg/m"
                # note that axis[0] is the RAO magnitude plot at the top, axis[1] is the phase plot
                # at tbe bottom
                axis[dof_name][0].set_ylabel("normalized RAO magnitude [{}]".format(unit))
                axis[dof_name][0].set_ylim((0, 1))
                axis[dof_name][1].set_ylabel("RAO phase [rad]")
                axis[dof_name][1].set_xlabel(r"Directional angle $\theta$ [rad]")
                axis[dof_name][1].set_xlim((0, 2 * np.pi))
                axis[dof_name][1].set_ylim((-2 * np.pi, 2 * np.pi))
                axis[dof_name][1].set_xticks([0, 0.5 * np.pi, np.pi, 1.5 * np.pi, 2 * np.pi])
                axis[dof_name][1].set_xticklabels(['$0$', '$1/2\pi$', '$\pi$', '$3/2\pi$',
                                                   '2$\pi$'])
                axis[dof_name][1].set_yticks([-2 * np.pi, -np.pi, 0, np.pi, 2 * np.pi])
                axis[dof_name][1].set_yticklabels(['$-2\pi$', '$-\pi$', '$0$', '$\pi$', '$2\pi$'])

            # store the rao magnitude and phase angle in a list called rao_comp
            rao_comp = [abs(self.RAOs[i_dof]), np.angle(self.RAOs[i_dof])]
            rao_comp_list.append(rao_comp)

            # set frequency intervals in which we expect the most response
            f_min = self.f_min_used_for_direction_pick
            f_max = self.f_max_used_for_direction_pick

            n_lines = 0
            for i_freq in range(self.n_frequencies):
                if (f_max is not None and self.frequencies[i_freq] > f_max) or \
                        (f_min is not None and self.frequencies[i_freq] < f_min):
                    # we are outside of the relevant frequency range. go to the next line
                    continue
                direction_distribution = rao_comp[0][i_freq, :]
                n_lines += 1
                max_response = direction_distribution.max()
                if max_response > self.directional_distributions_1d["abs"][dof_name].max():
                    i_freq_max = i_freq
                    j_dirs_max = direction_distribution.argmax()

                    # store the current direction distribution and the frequency distribution at the
                    # direction j_dir_max
                    for ii, (comp, data) in enumerate(self.directional_distributions_1d.items()):
                        # comp is 'abs' or 'phase'. ii is 0 (abs) and 1 (phase)
                        self.directional_distributions_1d[comp][dof_name] = rao_comp[ii][i_freq, :]
                        self.frequency_distributions_1d[comp][dof_name] = \
                            rao_comp[ii][:, j_dirs_max]

                if debug_plot and i_freq in plot_freq_indices:
                    # add a plot of the magnitude and phase along the direction axis for the current
                    # i_freq component
                    for ii in range(2):
                        axis[dof_name][ii].plot(self.directions, rao_comp[ii][i_freq, :],
                                                '-x', linewidth=2,
                                                label="{}".format(i_freq))

            # normalize the direction distribution such that the max response is 1
            max_response = self.directional_distributions_1d["abs"][dof_name].max()
            self.directional_distributions_1d["abs"][dof_name] /= max_response

            # store the uniform mesh per DOF for later reference
            DD, FF = np.meshgrid(self.directional_distributions_1d["abs"][dof_name],
                                 self.frequency_distributions_1d["abs"][dof_name])
            self.dirs_2d_uniform_list.append(DD)
            self.freq_2d_uniform_list.append(FF)

            i_freq_max_list.append(i_freq_max)
            j_dirs_max_list.append(j_dirs_max)

        for i_dof, dof_name in enumerate(self.six_dof_names):
            if debug_plot:
                for ii, (part, component) in enumerate(self.directional_distributions_1d.items()):
                    # f_scale = max_response if ii == 0 else 1
                    f_scale = 1
                    axis[dof_name][ii].plot(self.directions, component[dof_name] * f_scale, '--or',
                                            linewidth=1, label="picked: {}".format(j_dirs_max))
                    if ii == 0:
                        axis[dof_name][0].text(0.0, 1.05, "RAO magnitude/phase vs direction for {}"
                                                          "".format(dof_name),
                                               transform=axis[dof_name][ii].transAxes, fontsize=12)
                        axis[dof_name][0].text(0.8, 1.05, "Line at index {} ({:.2f} rad)"
                                                          "".format(j_dirs_max,
                                                                    self.directions[j_dirs_max]),
                                               transform=axis[dof_name][ii].transAxes, fontsize=12)
                    else:
                        # for the angle plot, also add the relative phase with respect to the tower
                        # dof phase zere
                        axis[dof_name][1].plot(self.directions,
                                               component[dof_name] - component["TowO_ACC_RXX"],
                                               '--vb', linewidth=1
                                               , label="Relative to $RXX$".format(j_dirs_max))

                        axis[dof_name][1].plot(self.directions,
                                               component[dof_name] - component["TowO_ACC_AZ"],
                                               '-->y', linewidth=1,
                                               label="Relative to $A_Z$".format(j_dirs_max))

                        axis[dof_name][1].legend()

            if dump_rao_in_out or debug_plot:
                plt.figure(plot_name)
                dof_label = re.sub(".*_", "", dof_name)
                ax1 = plt.subplot(gs[i_dof, 0])
                if i_dof == 0:
                    ax1.set_title("RAO 2D WAMIT")
                cs1 = ax1.contourf(rao_comp_list[i_dof][0].T)
                ax1.set_ylabel("Dir [index]")
                cbar1 = fig.colorbar(cs1)
                cbar1.ax.set_ylabel("{} [-]".format("{}".format(dof_label)))
                ax1.text(0.8, 0.85, "{}1) {}".format(string.ascii_lowercase[i_dof], dof_label),
                         transform=ax1.transAxes, fontsize=10)
                xlim = ax1.get_xlim()
                ylim = ax1.get_ylim()
                ax1.plot([i_freq_max_list[i_dof], i_freq_max_list[i_dof]], ylim, '--r', linewidth=1)
                ax1.plot(xlim, [j_dirs_max_list[i_dof], j_dirs_max_list[i_dof]], '--g', linewidth=1)

                ax2 = plt.subplot(gs[i_dof, 1], sharex=ax1, sharey=ax1)
                if i_dof == 0:
                    ax2.set_title("RAO 2D F x D")
                rao2 = self.dirs_2d_uniform_list[i_dof] * self.freq_2d_uniform_list[i_dof]
                cs2 = ax2.contourf(rao2.T)
                ax2.plot([i_freq_max_list[i_dof], i_freq_max_list[i_dof]], ylim, '--r', linewidth=1)
                ax2.plot(xlim, [j_dirs_max_list[i_dof], j_dirs_max_list[i_dof]], '--g', linewidth=1)
                ax2.text(0.8, 0.85, "{}2) {}".format(string.ascii_lowercase[i_dof], dof_label),
                         transform=ax2.transAxes, fontsize=10)
                ax2.set_ylabel("Direction index [-]")
                if i_dof == 5:
                    ax1.set_xlabel("Frequency index [-]")
                    ax2.set_xlabel("Frequency index [-]")
                cbar2 = fig.colorbar(cs2)
                cbar2.ax.set_ylabel("{} [-]".format("abs"))

        if dump_rao_in_out:
            # dump the plot if requested
            plt.figure(plot_name)
            file_name_base = os.path.join(os.getcwd(), os.path.split(self.file_base_name)[1])

            plt.savefig(file_name_base + "_rao_wamit_vs_estimate.png")
            for i_dof, dof_name in enumerate(self.six_dof_names):
                file_name = "_".join([file_name_base, dof_name, "direction1d"]) + ".png"
                self.log.debug("Saving plot {}".format(file_name))
                figs[dof_name].savefig(file_name)

        if debug_plot:
            # dump the plot if requested
            plt.ioff()
            plt.show()

    def calculate_rao_per_dof_vs_velocity(self):
        """
        Calculate the transformed raos for a varying velocity range and store in a list
        """

        # set up plot only if we want to create a debugging rao plot
        i_dof_to_plot = 0
        if self.debug_plot:
            six_dof_name = six_dof_names("RAO")[i_dof_to_plot]
            plot_title = "{} vs frequency for varying velocity shift".format(six_dof_name)
            fig, axis = plt.subplots(nrows=2, ncols=1, sharex=True)
            # plt.subplots_adjust(right=0.8)
            fig.canvas.set_window_title(plot_title)

            axis[0].set_title(plot_title)
            axis[1].set_xlabel("Frequency $\omega$ [rad/s]")
            axis[0].set_ylabel("RAO magnitude [-]")
            axis[0].grid(True)
            axis[0].set_title(plot_title)
            axis[1].set_ylabel("Encountered Frequency $\omega_e$ [rad/s]")
            axis[1].grid(True)
            m_cycler = cycler(marker=["o", "+", "x", ">"])
            c_cycler = cycler(color=["r", "g", "b", "c"])
            axis[0].set_prop_cycle(m_cycler + c_cycler)
            axis[0].set_prop_cycle(c_cycler)

        for velocity in self.velocity_range:
            self.log.debug(
                "Transforming RAO with velocity {} ({})".format(velocity, velocity.to("knots")))

            # create a new roa per for based on the original one. Set to zero and then put the last
            # row with the unity back to one
            rao_per_dof_trans = self.rao_per_dof.copy()
            # column 0-n_dof -1 contain the RAO's , the last column n_dof contains just a unit RAO
            # of 1
            rao_per_dof_trans[:, :self.n_dof] = complex(0, 0)  # initialise rao's with complex zeros
            # initialise unity response with complex 1
            rao_per_dof_trans[:, self.n_dof] = complex(1, 0)

            self.log.debug("RAO here {} {}".format(type(rao_per_dof_trans[0, 0]),
                                                   rao_per_dof_trans.shape))

            # loop over the 6 RAO's
            for i_dof in range(self.n_dof):

                self.log.debug("dof direction {}".format(i_dof))
                # turn onde-D RAO data back into 2D shape and copy to a new array rao_data_trans
                rao_data = self.rao_per_dof[:, i_dof].reshape(self.shape2D).T
                rao_data_trans = rao_data.copy()

                # loop over the directions of the 2D RAO
                for i_dir in range(self.n_directions):
                    # get direction in rad
                    direction = self.directions[i_dir]

                    # the new frequencies for this velocity in m/s. Note that these omega_new are
                    # the 'true' frequencies in the fixed domain, the frequencies of the RAO are the
                    # encountered frequencies of the moving ref the omega_new is not equidistant
                    velocity_in_heading_direction = velocity.to_base_units().magnitude * np.cos(
                        direction)
                    omega_new = omega_e_vs_omega(self.frequencies, velocity_in_heading_direction)

                    f_inter = interp1d(self.frequencies, rao_data[i_dir, :], bounds_error=False,
                                       fill_value=complex(0, 0))

                    # obtain the RAO data as a function of the true frequencies and interpolate on a
                    # regular grid
                    rao_data_trans[i_dir, :] = f_inter(omega_new)

                    # create debugging plots only for the zero direction (following waves)
                    if self.debug_plot and i_dof == i_dof_to_plot and \
                            abs(direction - 0 * np.pi) < 1e-3:
                        omega_c = g0 / (2 * velocity.magnitude)
                        # axis[0].plot(self.frequencies, abs(rao_data[i_dir, :]), '-o',
                        #          label="$R(\omega_e)$ {:.1f} kn ({:.1f} m/s)".format(vel_in_knots,
                        # velocity))
                        line = axis[0].plot(self.frequencies, abs(rao_data_trans[i_dir, :]))
                        # axis[0].plot(omega_new, abs(rao_data_trans[i_dir, :]), '-+y',
                        #          label="$R(\omega_e)$ {:.1f} ({:.1f} m/s)".format(vel_in_knots,
                        # velocity))
                        if omega_c < self.frequencies[-1]:
                            axis[0].plot([omega_c, omega_c], axis[0].get_ylim(),
                                         '--' + line[0].get_color())
                        # axis[0].legend(loc=(1.02, 0.5), title="Speed")

                        axis[1].plot(self.frequencies, omega_new, "-" + line[0].get_color(),
                                     label="{:.1f} kn".format(velocity.to("knots")))
                        axis[1].legend(loc="outside", title="Speed")

                # copy the rao for all this dof to the new rao_per dof. Flatten the 2D array to
                # store a 1D data use transpose again to get the right direction/freq order
                rao_per_dof_trans[:, i_dof] = rao_data_trans.T.flatten()

            # store the data
            self.log.debug("Adding RAO to {} {}".format(len(self.data_vs_velocity), velocity))
            self.data_vs_velocity.append(pd.DataFrame(rao_per_dof_trans,
                                                      columns=self.six_dof_names +
                                                              [self.unity_name],
                                                      index=self.multi_index))

        # finally, show the plots and wait
        if self.debug_plot:
            out_file = "{}_vs_freq_and_vel.png".format(six_dof_name)
            self.log.info("Saving RAO plot to {}".format(out_file))
            fig.savefig(out_file)
            plt.ioff()
            plt.show()

    def properties(self):
        """
        Return all attributes with their values of this class
        """
        d = dict((key, getattr(self, key)) for key in dir(self) if key not in dir(self.__class__))
        return d

    def list_properties(self):
        """
        Show all attributes with their values of this class
        """
        info = ""
        for k, v in list(self.properties().items()):
            tv = type(v)
            if tv is np.ndarray:
                info += "{:<40} : {:<8} ({})\n".format(k, v.mean(), v.size)
            elif tv is dict:
                info += "{:<40} : {}".format(k, type(v))
            elif tv is str or tv is bytes:  #
                info += "{:<40} : {}\n".format(k, v)
            else:
                info += "{:<40} : {}\n".format(k, v)

        info += "\n"
        for key, value in self._matdata_dict.items():
            try:
                mean_value = value["data"].mean()
                shape = value["data"].shape
            except KeyError:
                mean_value = None
                shape = None
            info += "{:<40} : {} {}\n".format(key, mean_value, shape)

        self.log.info(info)


class FmsMatlabDatabase(object):
    """
    Read the Matlab data base file and store all the data in a class
    """

    def __init__(self, filename, hot_spot_name_list=None):

        self.log = get_logger(__name__)

        self.filename = filename

        # initialize the list of hot spots in the order as stored in the matlab database
        if hot_spot_name_list:
            self.hot_spot_name_list = hot_spot_name_list
        else:
            # the default order of the hot spots in the matlab code
            self.hot_spot_name_list = MATLAB_HOT_SPOT_NAME_LIST

        # defined the names as found in the matlab data base. The key is the name as occurs in the
        # matlab data base the value is a dictionary with a name field to store the base name as
        # will be stored in the dataframe and a empty data field which will be assigned later
        self._matdata_dict = dict(
            FatigueHotMeas_base=dict(name="D_HS_SG", type="hotspot_damage"),
            FatigueHotCalc_base=dict(name="D_HS_AC", type="hotspot_damage"),
            HotAccStd_base=dict(name="S_HS_AC", type="hotspot_stress"),
            ColdStrStd_base=dict(name="S_CS_SG", type="coldspot_stress"),
            ColdAccStd_base=dict(name="S_CS_AC", type="coldspot_stress"),
            T=dict(name="mat_datetime_sec", type=None),
            t=dict(name="mat_datetime", type=None),
            RFCHotcalcTOT_base=dict(name="RFC_HS_AC", type="rfc_stresses"),
            RFCHotmeasTOT_base=dict(name="RFC_HS_SG", type="rfc_stresses"),
            StressAccAll=dict(name="StressAccAll", type="time_series"),
            StressStrAll=dict(name="StressStrAll", type="time_series"),
            HotStressStrAll=dict(name="HotStressStrAll", type="time_series"),
            HotStressAccAll=dict(name="HotStressAccAll", type="time_series"),
            TowerOAcc=dict(name="TowerOAcc", type="time_series"),
            TowerAccAll=dict(name="TowerAccAll", type="time_series")
        )

        # number of data point in one row of the matlab file
        self.n_data_points = None

        self.n_hot_spots = len(self.hot_spot_name_list)  # total number of hot spots
        self.n_hot_spot_angles = 8  # number of angles per hot spot
        # total number of channels
        self.n_hot_spot_columns = self.n_hot_spot_angles * self.n_hot_spots

        try:
            self.file_base_name, self.extension = os.path.splitext(filename)
        except IndexError:
            self.extension = None

        if re.match(".mat$", self.extension):
            self.read_matlab_datafile()
        else:
            raise IOError("File extension of matlab file must be .mat")

        self.fms_database_dict = dict()
        self.fms_timeseries_dict = dict()

        self.start_date_time = None
        self.end_date_time = None

    def read_matlab_datafile(self):
        # try the read the data
        try:
            matlab_structure = sio.loadmat(self.filename)
        except IOError as err:
            self.log.info(err)
            raise

        # loop over the data file and create field of the class
        for key, value in matlab_structure.items():
            # store the type of the current value
            type_of_value = type(value)
            self.log.debug("Found matlab data {} ({})".format(key, type_of_value))

            if type_of_value is np.ndarray:
                if not self.n_data_points:
                    # for the first type that is a numpy array: store the number of values in the
                    # array and create an index array. Assumes that all arrays have the same size
                    self.n_data_points = np.max(value.shape)
                    index = np.arange(0, self.n_data_points)
                    self.log.debug(" set n_points {} and index {} of type {}"
                                   "".format(self.n_data_points, index.shape, type(index)))

                try:
                    # try to store the current data of the matlab with the name given by 'key' in
                    # the _matdata dictionary. If this fails, then we did not anticipate on this
                    # data and with can continue otherwise the data is stored in the dictionary
                    # field 'key' with the label 'data'
                    self._matdata_dict[key]["data"] = value.reshape(-1)
                    self.log.info("Stored matlab data {} to our data_dict.".format(key))
                except KeyError:
                    # the key field is not available in our matdata structure, so skip it
                    self.log.info("Matlab key {} not in our data_dict. Skipping".format(key))
            else:
                # store the non-array data, which is the strings
                setattr(self, key, value)
        self.log.debug("-------- done reading the data --------------")

    def create_time_series_database(self):

        self.log.debug("+++++++++ creating the time series dataframes ++++++++++++")

        # store the matlab date time. Translate to a datetime format first
        # The T field contains the time rounded on seconds with the full date number, the 't' data
        # contains the time in ms starting at zero. So combine them to get the high resolution time
        t0 = matlabnum2date(self._matdata_dict["T"]["data"][0])
        t1 = matlabnum2date(self._matdata_dict["T"]["data"][-1])

        # some trickery to get the high resolution time into date_time but it works
        dts_format = "{:04d}-{:02d}-{:02d}T{:02d}:{:02d}:{:02d}.{:03d}"
        delta_t_ms = int(
            1000 * (self._matdata_dict["t"]["data"][1] - self._matdata_dict["t"]["data"][0]))
        t_length_ms = int(delta_t_ms * self.n_data_points)
        self.log.debug("creating time series with delta_t {} ms and lenght {} ms"
                       "".format(delta_t_ms, t_length_ms))

        start_date_time = np.datetime64(
            dts_format.format(t0.year, t0.month, t0.day, t0.hour, t0.minute, t0.second, 0),
            'ms')
        end_date_time = start_date_time + np.timedelta64(t_length_ms, 'ms')

        date_time = np.arange(start_date_time, end_date_time, step=delta_t_ms)

        self.start_date = t0.strftime("%Y-%m-%d")
        self.end_date = t1.strftime("%Y-%m-%d")

        for key, data_dict2 in self._matdata_dict.items():
            self.log.debug("Analysing field key={} ".format(key))

            data_type = data_dict2["type"]

            self.log.debug("Analysing field key={} with data type".format(data_type))

            # check if this is a hot spot data item. If so, store it as a dataframe
            if data_type is not None and data_type in ("time_series"):
                # ok, this is a hot spot with the phi channels
                self.log.debug(
                    "Storing time series data with # points {}".format(self.n_data_points))

                p_data = self._matdata_dict[key]["data"]

                if key in (
                        "StressStrAll", "StressAccAll", "HotStressStrAll", "HotStressAccAll",
                        "TowerOAcc",
                        "TowerAccAll"):
                    # take care of importing the cold spot stresses in the same order as the
                    # channels
                    if key == "StressStrAll":
                        columns = fut.STRESS_STR_NAMES
                    elif key == "StressAccAll":
                        columns = fut.STRESS_ACC_NAMES
                    elif key == "HotStressStrAll":
                        columns = fut.ML_HS_STRESS_STR_NAMES
                    elif key == "HotStressAccAll":
                        columns = fut.ML_HS_STRESS_ACC_NAMES
                    elif key == "TowerOAcc":
                        columns = fut.six_dof_names("TowO_ACC")
                    elif key == "TowerAccAll":
                        tsm_names = ['TSM - ACC 12 - AX', 'TSM - ACC 13 - AY', 'TSM - ACC 14 - AZ']
                        cfd_names = ['COFF SB FWD - AX - 04', 'COFF SB FWD - AY - 05',
                                     'COFF SB FWD - AZ- 06']
                        rx4_names = ['RX4 - AX - 01']
                        columns = tsm_names + cfd_names + rx4_names

                    self.log.debug(
                        "Converting matlab data {} with shape {} (/{} = {}) to cols {}: {}".format(
                            key, p_data.shape, self.n_data_points,
                            old_div(p_data.size, float(self.n_data_points)), len(columns),
                            columns))
                    df_data = pd.DataFrame(columns=columns, index=date_time)
                    df_data.fillna(inplace=True)
                    df_data.index.name = fut.INDEX_NAME

                    # set pointer to the current data array
                    for i_row, name in enumerate(columns):
                        self.log.debug("Adding row {} with name {}".format(i_row, name))
                        ii = i_row * self.n_data_points
                        df_data[name] = p_data[ii:ii + self.n_data_points]

                    # store this data base in the dictionary
                    self.log.debug("adding the data frame for key {}".format(key))
                    self.fms_timeseries_dict[key] = df_data

                else:
                    self.log.debug("Key {} not implemented. SKipping".format(key))
                    continue

    def create_fatigue_database_dict(self):

        self.log.debug("+++++++++ creating the dataframes ++++++++++++")

        # store the matlab date time. Translate to a datetime format first
        date_time = matlabnum2date(self._matdata_dict["t"]["data"])

        self.start_date = date_time[0].strftime("%Y-%m-%d")
        self.end_date = date_time[-1].strftime("%Y-%m-%d")

        # loop over all the positions
        for i_pos, position in enumerate(self.hot_spot_name_list):
            self.log.debug("Creating the  data frames for potiion {}".format(position))

            # create data frame for this position to store the hot spot measured and calculated
            # damage
            hot_spot_damage_df = pd.DataFrame(columns=fut.DAMAGE_COLUMNS, index=date_time)
            hot_spot_damage_df.fillna(inplace=True)
            hot_spot_damage_df.index.name = fut.INDEX_NAME

            # create data frame for this position to store the hot spot measured and calculated
            # damage
            hot_and_cold_spot_stress_df = pd.DataFrame(columns=fut.STRESS_COLUMNS, index=date_time)
            hot_and_cold_spot_stress_df.fillna(inplace=True)
            hot_and_cold_spot_stress_df.index.name = fut.INDEX_NAME

            # loop over all the data items defined in the constructor above
            for key, data_dict2 in self._matdata_dict.items():

                # set pointer to the current data array
                try:
                    p_data = data_dict2["data"]
                except KeyError:
                    continue

                data_type = data_dict2["type"]

                # check if this is a hot spot data item. If so, store it as a dataframe
                if data_type in ("hotspot_damage", "hotspot_stress", "coldspot_stress"):
                    # ok, this is a hot spot with the phi channels
                    self.log.debug("Storing hot spot {} with # points {} at location {}"
                                   "".format(key, data_dict2["data"].size, position))

                    # loop over the hot spot channels (normally 8) and copy the data per channel
                    # into the data frame
                    for i_phi in range(self.n_hot_spot_angles):
                        # loop over all the data types
                        # position in matlab file
                        i_row = i_pos * self.n_hot_spot_angles + i_phi
                        ii = i_row * self.n_data_points

                        data_name = "{}{}".format(data_dict2["name"], i_phi)

                        if data_type == "hotspot_damage":
                            # copy the measured hot spot fatigue to the dataframe
                            hot_spot_damage_df[data_name] = p_data[ii:ii + self.n_data_points]
                        else:
                            try:
                                hot_and_cold_spot_stress_df[data_name] = p_data[
                                                                         ii:ii + self.n_data_points]
                            except ValueError:
                                # the cold spot have only 4 channels + there is one location with
                                # only 1 channel, hence the number of data columns in p_data is
                                # only 25 (56/2 - 3)
                                continue
                else:
                    self.log.debug("Key {} not yet implemented. Skipping".format(key))

            # store the whole data frame into the dictionary
            dam_at_pos = "D_{}".format(position)

            # store this data base in the dictionary
            self.fms_database_dict[dam_at_pos] = hot_spot_damage_df

            # store the whole data frame into the dictionary
            stress_at_pos = "S_{}".format(position)

            # store this data base in the dictionary
            self.fms_database_dict[stress_at_pos] = hot_and_cold_spot_stress_df

        self.log.debug("Done creating the dataframe dictionary")

    def write_database(self):
        # write the dictionary with databases per position to an excel file
        new_file = re.sub("\.mat", ".xls", self.filename)
        self.log.info("Writing matlab data to data base {}".format(new_file))
        with pd.ExcelWriter(new_file, append=False) as writer:

            # now loop over the rest to dump the stresses at all positions
            for (position, group_df) in self.fms_database_dict.items():
                self.log.info("Writing sheet {}".format(position))
                if position == fut.GENERAL_SHEET_NAME:
                    continue

                if bool(re.match("^D_.*", position)):
                    group_df[fut.DAMAGE_COLUMNS].to_excel(writer, sheet_name=position)
                elif bool(re.match("^S_.*", position)):
                    group_df[fut.STRESS_COLUMNS].to_excel(writer, sheet_name=position)

    def properties(self):
        # purpose: return all properties of the class in one dictionary, but
        # exclude the classes
        return dict(
            (key, getattr(self, key)) for key in dir(self) if key not in dir(self.__class__))

    def list_properties(self):
        # list of the propery fields and its values of this class
        info = ""
        for k, v in list(self.properties().items()):
            tv = type(v)
            if tv is np.ndarray:
                info += "{:<40} : {:<8} ({})\n".format(k, v.mean(), v.size)
            elif tv is dict:
                info += "{:<40} : {}".format(k, type(v))
            elif tv is str or tv is bytes:  #
                info += "{:<40} : {}\n".format(k, v)
            else:
                info += "{:<40} : {}\n".format(k, v)

        info += "\n"
        for key, value in self._matdata_dict.items():
            try:
                mean_value = value["data"].mean()
                shape = value["data"].shape
            except KeyError:
                mean_value = None
                shape = None
            info += "{:<40} : {} {}\n".format(key, mean_value, shape)

        self.log.info(info)
