"""
A collection of utilities used in the FMS code. Mainly for creating the names of all the
columns in the DataFrame containing all quantities related to the stain gauge and accelerometer
data
"""
from __future__ import division, absolute_import

import os
import re
from builtins import object
from builtins import range
from builtins import str

import hmc_marine.data_readers as dr
import pandas as pd
import progressbar as pb
import scipy.optimize as optimization
import signal_processing.filters as spf
import yaml
from hmc_utils import Q_
from hmc_utils.file_and_directory import (make_directory)
from hmc_utils.misc import (clear_path, get_logger, set_default_dimension)
from numpy import (array, vstack, linspace, power, log10, ones)
from numpy.linalg import lstsq
from scipy.constants.constants import g as g0

# -----------------------------
# global variable definitions
# -----------------------------
PROGRESS_WIDGETS = [pb.Percentage(), ' ', pb.Bar(marker='.', left='[', right=']'), ""]
MESSAGE_FORMAT = "{:40}"

RAO_ABS_PHASE_EXT = ["abs", "phase"]
RAO_SYMMETRY_TYPES = ["weighted", "keep_left", "keep_right"]

INCL_G_NAME = "incl_g"

MSG_FORMAT = "{:30s} : {}"

# definition of the columns names for the Damage and Stress HotSpot stress from the
# StrainGauges and Accelerometers (D_HS_SG and D_HS_AC and S_HS_SG and  S_HS_SG_AC,
# respectively) and also the ColdSpot stresses for the strain gauges and accelerometers.
# For the hot spot locations we have 8 channels per item, for the cold spot location only 4
HS_DAMAGE_SGS_NAMES = ["D_HS_SG{}".format(i) for i in range(8)]  # Measured damage at hot spots
HS_DAMAGE_ACC_NAMES = ["D_HS_AC{}".format(i) for i in range(8)]  # Calculated damage at hot spots
HS_STRESS_SGS_NAMES = ["S_HS_SG{}".format(i) for i in range(8)]  # Measured stress at hot spots
HS_STRESS_ACC_NAMES = ["S_HS_AC{}".format(i) for i in range(8)]  # Calculated stress at hot spots
CS_STRESS_SGS_NAMES = ["S_CS_SG{}".format(i) for i in range(4)]  # Measured stress at cold spots
CS_STRESS_ACC_NAMES = ["S_CS_AC{}".format(i) for i in range(4)]  # Calculated stress at cold spots

# column names to store the number of stress cycles returned from the rain flow algorithm and the
# second moments
NC_ACC_NAMES = ["N_HS_AC{}".format(i) for i in range(8)]  # Calculated stress at hot spot
M2_ACC_NAMES = ["M2_HS_AC{}".format(i) for i in range(8)]  # Calculated stress at hot spot

STRESS_STR_NAMES = ["SG{:02d}".format(i + 1) for i in
                    range(25)]  # Names of all the measured Stress columns
STRESS_ACC_NAMES = ["AC{:02d}".format(i + 1) for i in
                    range(25)]  # Names of all the calculated Stress columns

# the Matlab naming convention used to store the accelerometer and strain gauge data
ML_HS_STRESS_STR_NAMES = ["SG{:02d}".format(i + 1) for i in
                          range(56)]  # Names of all the measured Stress columns
ML_HS_STRESS_ACC_NAMES = ["AC{:02d}".format(i + 1) for i in
                          range(56)]  # Names of all the calculated Stress columns

MATLAB_HOT_SPOT_NAME_LIST = ["HZ3", "RX4", "SL1", "PIVot (SB)", "Q02", "AD (SB)", "AD (PS)"]

# the measured and calculated damages are combined for the cold spot only
DAMAGE_COLUMNS = HS_DAMAGE_SGS_NAMES + HS_DAMAGE_ACC_NAMES
# the measured and calculated stresses are combined for bot hot spots and cold spots
STRESS_COLUMNS_SGS = HS_STRESS_SGS_NAMES + CS_STRESS_SGS_NAMES
STRESS_COLUMNS_ACC = HS_STRESS_ACC_NAMES + CS_STRESS_ACC_NAMES
STRESS_COLUMNS = STRESS_COLUMNS_SGS + STRESS_COLUMNS_ACC

# the names of the sheets in the output data base.
# the General sheet will contain all the information per time stamp which is not related to the
# stress and damage locations, such as the gps location, the tower origin 6-DOF, the sea states
# values Hs etc.
GENERAL_SHEET_NAME = "General"
# the info shseet is the front page containing some information about the simulation
INFO_SHEET_NAME = "Information"
# the voyage sheets are used for the prediction data base
VOYAGE_SHEET_NAME = "VoyageSheet"
VOYAGE_DICT_NAME = "Voyage"
INDEX_NAME = "DateTime"
VOYAGE_INDEX_NAMES = ["Info", "ID"]

# This list connect the names as used in the matlab output files to the names as used in
# the fatigue monitoring package
MATLAB_ALIASES = {
    "t_abs": "date_time",
    "Time [h]": "travel_time",
    "pos_lat": "latitude",
    "pos_lon": "longitude",
    "Dist [nm]": "travel_distance",
    "Sust Speed [kn]": "speed_sustained",
    "Hs Tot [m]": "Hs_tot",
    "Tp Tot [s]": "Tp_tot",
    "Dir Tot [deg]": "direction_wave_tot",
    "Hs Wind [m]": "Hs_wind",
    "Tp Wind [s]": "Tp_wind",
    "Dir Wind [deg]": "direction_wave_wind",
    "Hs Swell [m]": "Hs_swell",
    "Tp Swell [s]": "Tp_swell",
    "Dir Swell [deg]": "direction_wave_swell",
    "Vw [m/s]": "speed_wind",
    "Vw dir [deg]": "direction_wind",
    "Current [m/s]": "speed_current",
    "Current wrt Ship [deg]": "direction_current",
    "Speed [kn]": "speed_trough_water",
    "Water Depth [m]": "depth",
    "Heading [deg]": "heading",
    "Drift [deg]": "direction_drift",
    "Course over ground [deg]": "course_over_ground"
}

# in the plotting utilities we want to have the connection in opposite direction
MATLAB_ALIASES_REV = dict()
for key, value in MATLAB_ALIASES.items():
    MATLAB_ALIASES_REV[value] = key


def progress_bar_message(counter, maximum, total_time):
    """
    Message used by the progress bar to write to screen for the prediction mode

    Parameters
    ----------
    counter : int
        Current step of the sequence of 0 to *maximum*

    maximum : int
        Maximum number of steps to take

    total_time : float
        Total processing time so far


    Returns
    -------
    str:
        Message of the progress bar:

        Voyage *counter* of *maximum*.  Running time: *total_time* s

    """
    return " Voyage {} of {}.  Running time: {:6.0f} s".format(counter, maximum, total_time)


def progress_bar_file(counter=0, maximum=None, file_name=None, total_time=0):
    """
    Message used by the progress bar to write to screen for the monitoring mode

    Parameters
    ----------
    counter : int
        Current step of the sequence of 0 to *maximum*

    maximum : int
        Maximum number of steps to take

    file_name: str or None
        Current file name being processed

    total_time : float
        Total processing time so far


    Returns
    -------
    str:
        Message of the progress bar:

        *counter*/*maximum*: *file_name* *total_time* s

    """
    return "{:3d}/{:3d}: {} {:6.0f} s".format(counter, maximum, file_name, total_time)


def progress_bar_message_reading(counter):
    """ The screen message during reading of files

    Parameters
    ----------
    counter : int
        The counter
        
    Returns
    -------
    str:
        The message: Reading trajectory *counter*

    """
    return " Reading trajectory {}".format(counter)


def get_multiplier_and_operation(operation):
    """ Get the multiplier from a string with a operation

    Parameters
    ----------
    operation :
        a string with a type of operation (mean, std, var, max, min) and a multiplier like 4*std

    Returns
    -------
    int:
        Multiplier

    Notes
    -----
    * An operation is given as std, var, mean, etc. , but may have a multiplier like 4*std
      (in order to report the Hs) directly from the standard deviation. This routine extracts the
      multiplier
    """
    log = get_logger(__name__)
    multiplier = 1
    match = re.match("^(\d)\*.*", operation)
    if bool(match):
        try:
            multiplier = float(match.group(1))
        except ValueError:
            log.warning("Could not convert multiplier to float. keep 1")
        # remove the multiplier and continue
        operation = re.sub(".*\*", "", operation)
    return operation, multiplier


def maximum_exceeded(value_to_test, maximum=None):
    """Check if a value exceeds a certain maximum if given


    Parameters
    ----------
    value_to_test : int
        the value to test
    maximum : int, optional
        the maximum to test to. Default is None, which means that result is always False

    Returns
    -------
    bool :
        True of the maximum was given and exceeded (>=) by the value

    Notes
    -----
    * If the maximum is not defined the result is always False

    """
    exceeded = False
    try:
        if maximum is not None and value_to_test >= maximum:
            exceeded = True
    except TypeError:
        pass
    return exceeded


def convert_ampere(ampere, a, b, ampere_offset=12):
    """Calculate the gravity or strain based on the ampere generated by the accelerometer

    Parameters
    ----------
    ampere : float
        mA] input milli ampere from the meter
    a : float
        m/s2)/mA] acceleration in m/s2 per mA OR [micro/mA] strain per mA
    b : float
        acceleration for ampere = ampere_offset or strain for ampere = ampere_offset
    ampere_offset : float, optional
        fixed ampere_offset given by the device. Normally 12 mA, but can be different.
        Default value = 12 mA

    Returns
    -------
    float:
        Resulting ampere/strain for given *ampere* and coefficients *a* and *b* and *offset*

    Notes
    -----
    * The results is calculated as

    .. math::

        a * (A - A_{off}) +  b

    where :math:`A` is the current in milli ampere of the device and :math:`a` and :math:`b` are the
    coefficients given as an input

    * This function is used in the least square fitting of the measurement only, where  actually
      the coefficients :math:`a` and :math:`b` and the offset :math:`A_{off}` are calculated based
      on the measurement relating the current with the value of the device

    """
    return a * (ampere - ampere_offset) + b


def damage_at_position_name(position, prefix_damage="D"):
    """

    Parameters
    ----------
    position :
        
    prefix_damage :
        (Default value = "D")

    Returns
    -------

    
    """
    # return the name of the sheet which stores the damage of the stresses for the position
    return "{}_{}".format(prefix_damage, position)


def stress_at_position_name(position, prefix_stress="S"):
    """

    Parameters
    ----------
    position :
        
    prefix_stress :
        (Default value = "S")

    Returns
    -------

    
    """
    # return the name of the sheet which stores the damage of the stresses for the position
    return "{}_{}".format(prefix_stress, position)


def scf_at_position_name(position, prefix_scf="SCF_X"):
    """

    Parameters
    ----------
    position :
        
    prefix_scf :
        (Default value = "SCF_X")

    Returns
    -------

    
    """
    # return the name of the stress concentration factor of the stresses for the position
    tmp = re.sub("[()]", "", re.sub("\s+", "_", position))
    return "{}_{}".format(prefix_scf, tmp)


def get_time_stamp(mdf_file, file_base_string, utc_string="_UTC.*", time_zone="UTC"):
    """
    Get the time stamp of a file name with a prefix 'file_base_string'

    Parameters
    ----------
    mdf_file : str
        Full name of the mdf file to get the time stamp from
    file_base_string: str
        Base name of the mdf file as defined in the settings file
    time_zone : str
        specify the time zone of the logging system. In this case europe summer time
        (always, also in winter). Force this with Etc/GMT-2, which results in UTC+002
        (in POSSIX convention) Default value = "UTC"
    utc_string : str
        Name of the utc time zone. Default value = "_UTC.*"

    Returns
    -------
    :obj:`DateTimeStamp`
        Date time of the current time in the mdf file name
    """
    file_name = os.path.split(mdf_file)[1]
    file_base = os.path.splitext(file_name)[0]
    time_stamp = re.sub(file_base_string, "", file_base)
    time_stamp = re.sub(utc_string, "", time_stamp)
    # the time_stamp is already expressed in the local time zone, but add it to the time stamp
    time_index = pd.to_datetime(time_stamp, yearfirst=True).tz_localize(time_zone)
    return time_index


def accelerometer_name_of_converted_signal(name):
    """
    Create a new name for the acceleration data based on the old name

    Parameters
    ----------
    name : str
        The name of the channel to convert to a new name

    Returns
    -------
    str:
        Name + the extension MS2 so show it now is an acceleration
    """
    # just return the new name of a accelerometer converted to ms2
    return name + "_MS2"


def hot_spot_stress_name(position, signal_source, phi):
    """Create a new name for the hot spot at the location 'position'

    Parameters
    ----------
    position :
        name of the location of the hotspot
    signal_source : str
        * SG : the hot spot signal based on the strain gauge,
        * AC : the hot spot name based on the accelerometer
    phi : float
        the current phi angle of the hot spot in degrees

    Notes
    -----
    The name of the hot spot stress either get an extension SG for signal_source = "SG" for the strain
    gauge data of an extension AC for the signal_source = "AC" for the calculated sources

    Example
    -------
    For the position = RX4 and signal_source = 0 and phi = 30 the new name is

    >>> hot_spot_stress_name("RX4", signal_source="SG", phi=30)
    ('RX4_HOTSPOT_STRESS_SG_P030', 'SG')


    Returns
    -------
    tuple:
        (new_name, extension) of the new new and the type SG or AC

    """

    if signal_source == "SG":
        extension = "SG"
    elif signal_source == "AC":
        extension = "AC"
    else:
        raise ValueError("signal_source must be either AC or SG. Found {}".format(signal_source))

    return "{}_HOTSPOT_STRESS_{}_P{:03d}".format(position, extension, int(phi)), extension


def hot_spot_stress_name_matlab(position, signal_source, i_phi):
    """Create a new name for the hot spot at the location 'position' as used in the matlab database

    Parameters
    ----------
    position : str
        name of the location of the hotspot
    signal_source : int
        0. the hot spot signal based on the strain gauge
        1. the hot spot name based on the accelerometer
    i_phi : int
        one base integer referring to the phi index as use in the matlab data files. In the matlab
        file the channels are refered by their index which correspond to the location as follows
        ["HZ3", "RX4", "SL1", "PIVot (SB)", "Q02", "AD (SB)", "AD (PS)"]

    Returns
    -------
    str
        Name of the channel belong to the matlab index 1..56

    Notes
    -----
    The name depend on the *signal_source* parameter to either SG or AC.
    Since the matlab data for all position is stored in one big array of 56 (7x8) channels,
    this requires some hard coding of position references.

    Examples
    --------

    >>> hot_spot_stress_name_matlab("RX4", signal_source="AC", i_phi=4)
    'AC13'

    The 13 indicates that the for getting angle with index 4 of the RX4 channel in the matlab
    array we need to get the 13th channel

    """

    log = get_logger(__name__)

    i_name = -1
    for cnt, name in enumerate(MATLAB_HOT_SPOT_NAME_LIST):
        match = re.search(r"{}".format(name), position)
        if bool(match) or name == position:
            i_name = cnt
            break

    if i_name < 0:
        log.warning(
            "Could not find appropriate name in list of matlab names for position {}".format(
                position))
        name = None
    else:
        i_channel = i_phi + 8 * i_name
        if signal_source == "SG":
            name = ML_HS_STRESS_STR_NAMES[i_channel]
        elif signal_source == "AC":
            name = ML_HS_STRESS_ACC_NAMES[i_channel]
        else:
            raise ValueError("signal_source argument must be SG or AC. Found {}"
                             "".format(signal_source))

    return name


def strain_name_of_converted_signal(name, location, i_channel):
    """New value for the strain name

    Parameters
    ----------
    name : str
        Name of the original channel in mA

    location : str
        The location of the channel

    i_channel : int
        The number of the channel

    Returns
    -------
    New name which store the strain of the channel


    """
    return "_".join([name, clean_up_string(location), "{:d}".format(i_channel), "STRAIN"])


def stress_name_of_converted_signal(name, location, i_channel):
    """Return the name of the stress signal new value for the stress name

    Parameters
    ----------
    name :
        base name of the signal as stored in the MDF file
    location :
        location of the signal. The location name will be cleaned to remove spaces and brackets
    i_channel :
        counter of the index of this location

    Returns
    -------
    str:
        Name of the channel name

    Examples
    --------

    >>> stress_name_of_converted_signal("Bla", "PIV (SB)", 3)
    'Bla_PIV_SB_3_STRESS'


    """
    return "_".join([name, clean_up_string(location), "{:d}".format(i_channel), "STRESS"])


def mru_smooth_name(name):
    """

    Parameters
    ----------
    name :


    Returns
    -------


    """
    # just return the new name of a accelerometer converted to ms2
    return name + "_smooth"


def clean_up_string(string):
    """remove all brackets and  spaces from a string

    Parameters
    ----------
    string :
        return:

    Returns
    -------

    
    """
    return re.sub("[()]", "", re.sub("\s", "_", string))


def stress_from_acc_name(name, location, i_channel):
    """

    Parameters
    ----------
    name :

    location :

    i_channel :


    Returns
    -------


    """
    # just return the new name of a accelerometer converted to ms2
    return "_".join([name, clean_up_string(location), "{:d}".format(i_channel), "ACC"])


def mru_acceleration_name(name, order=1):
    """just return the new name of a motion converted to m/s^order

    Parameters
    ----------
    name :
        base name
    order :
        order of gradient (Default value = 1)

    Returns
    -------


    """
    result = name
    if order > 0:
        result += "_dot"
        if order == 2:
            # for the second derivative add another _dot
            result += "_dot"
    return result


def mru_six_dof_names(name, order=2):
    """create a list of names belong to the six dof components given a base name 'name' obtain by converting from
    the mru signal

    Parameters
    ----------
    name :
        base of the six dof names
    order :
        (Default value = 2)

    Returns
    -------


    """
    return six_dof_names("MRU_at_" + name, order)


def six_dof_names(name, order=2):
    """create a list of names belong to the six dof components given a base name 'name

    Parameters
    ----------
    name :
        base of the six dof names
    order :
        (Default value = 2)

    Returns
    -------


    """
    if order == 0:
        C = "M"
        R = "M"
    elif order == 1:
        C = "V"
        R = "W"
    elif order == 2:
        C = "A"
        R = "R"
    else:
        raise ValueError("Order must be 0, 1, or 2")
    return [name + "_" + ext for ext in (C + "X", C + "Y", C + "Z", R + "XX", R + "YY", R + "ZZ")]


def dump_hot_spot_rao_to_file(hot_spot_rao=None, hot_spot_spectrum=None, freq=None, dir=None,
                              file_name=None):
    """
    Dump current RAO of the hot spot to file

    Parameters
    ----------
    hot_spot_rao :
        object carrying the hot spot rao to dump (Default value = None)
    hot_spot_spectrum :
        object carrying the hot spot stress spectrum to dump (Default value = None)
    freq :
        1d array carrying the frequencies (Default value = None)
    dir :
        1d array carrying the directions (Default value = None)
    file_name :
        name of the file to dump the spectrum to (Default value = None)

    """

    log = get_logger(__name__)
    file_name = re.sub("[()]", "", re.sub("\s", "_", file_name))
    log.debug("Dumping the 2d rao/spectrum to {}".format(file_name))

    pd.to_msgpack(file_name, dict(
        rao=hot_spot_rao,
        spectrum=hot_spot_spectrum,
        frequencies=freq,
        direction=dir
    ))


def dump_this_hot_spot_location(dump_hot_spot_per_location, current_stress_name):
    """Check if the current hot spot location/angle is in the list we have defined in the dictionary

    Parameters
    ----------
    dump_hot_spot_per_location :
        a dictionary with keys holding the location names to dump and a phi_range string
        holding the the phis angle to dump for this location. Example::

            dump_rao_hot_spot_per_time_step:
                AD (SB):
                phi_angles: 0:180:45
                write_it: true

    current_stress_name :
        the name of the current stress name. If it is in the dump_hot_spot_per_location dict,
        return true

    Returns
    -------


    """
    log = get_logger(__name__)
    dump_this_stress_name = False
    for position, properties in dump_hot_spot_per_location.items():
        if not properties["write_it"]:
            # to location is swiched off, continue to next
            continue
        phi_angles = [int(s) for s in properties["phi_angles"].split(":")]
        try:
            phi_range = list(range(phi_angles[0], phi_angles[1], phi_angles[2]))
        except IndexError:
            log.warning("phi range must be given as a string 'phi_start:phi_end_phi_step'")
            phi_range = None
            continue
        for phi_deg in phi_range:
            dump_stress_name, dump_short_label = hot_spot_stress_name(position=position,
                                                                      signal_source="AC",
                                                                      phi=phi_deg)
            if dump_stress_name == current_stress_name:
                dump_this_stress_name = True
                # we found the current stress name in the dictionary so we can leave
                break

    return dump_this_stress_name


def make_time_series_file_name(time_series_settings, time_stamp, create_directory=False,
                               psd_file_name=False):
    """Based on the current setting file create a time series file name

    Parameters
    ----------
    time_series_settings : dict
        the dictionary containing the time series settings
    time_stamp : date_time
        the current time stamp
    create_directory : bool
        if true create the output directory if possible (Default value = False)
    psd_file_name : bool
        if true create a name for the power spectral density (Default value = False)

    Returns
    -------
    str:
        Name of the output file

    
    """
    file_base_name = time_series_settings["export_file_base"]
    if psd_file_name:
        file_base_name += "_" + time_series_settings["spectrum_file_extension"]
    file_type = time_series_settings["export_file_type"]

    output_directory = time_series_settings["export_out_directory"]

    if create_directory:
        # create a directory if it does not yet exist
        make_directory(output_directory)

    # create the output file name
    date_time_string = time_stamp.tz_convert("UTC").strftime("%Y%m%dT%H%M%S%z")
    file_name = "_".join([file_base_name, date_time_string]) + ".{}".format(file_type)
    out_file = os.path.join(output_directory, file_name)

    return out_file


def filter_signal_robust(signal, filter_type="block", f_cut_low=None,
                         f_cut_high=None, f_sampling=1,
                         f_width_edge=0.05,
                         ripple_db=100,
                         order=2,
                         constant_value=None,
                         use_filtfilt=True,
                         max_number_of_tries=5
                         ):
    """
    Filter the signal in a robust manner by retrying a number of times when failing

    Parameters
    ----------

    signal: ndarray
        Input 1D signal
    filter_type: {"mean", "butterworth", "kaiser", "block", "none"}
        Type of the filter to use. Default = block.
    f_cut_low: Quantity or float or None
        Low  cut-off frequency in Hz. Default = None, which means that f_cut_high must be given and
        that we are dealing with a low-pass filter
    f_cut_high: Quantity or float or None
        High  cut-off frequency in Hz. Default = None, which means that f_cut_low must be given and
        that we are dealing with a high-pass filter
    f_sampling: Quantity or float
        Sampling frequency. Default = 1 Hz
    f_width_edge: Quantity or float
        Sharpness of the stop band of the Kaiser filter. Only used when filter_type == "kaiser".
        A smaller f_width_edge approaches the ideal block filter better. Default = 0.05 Hz
    ripple_db: float, optional
        The desired attenuation in the stop band in dB. Only used when filter_type == "kaiser" .
        Default = 100 dB
    order: int
        Order of the Butterworth filter. Only used when filter_type=="butterworth". Default = 2.
        Higher values causes to approach the ideal block band filter, but also a more instable
        filter.
    constant_value: float
        Constant to use for the Kaiser filter when a phase shift correction is applied. Default =
        None, which means that the phase shift is not correct. Since we use filtfilt by default,
        this is not required. Only used when filter_type == "kaiser"
    use_filtfilt: bool
        If true use the filtfilt filter from scipy to take out the phase shift. Default = True.
    max_number_of_tries: int
        Number of retries to filter the signal

    Returns
    -------
    ndarray:
        Filtered signal or None if we were not able to filter the signal after
        *max_number_of_retries* attempts

    Raises
    ------
    ValueError:
        * In case no cut off frequencies are defined
        * In case that wfiltlo >= wfiltup

    See Also
    --------
    signal_processing.filter_signal

    Notes
    -----
    * This function is used to call the filter_signal function multiple times with an increasing
      filter  edge width (for the kaiser filter) or decreasing order (for the Butterworth filter)
    * After retrying *max_numer_of_retries* times, stop processing and return None
    """

    log = get_logger(__name__)
    success = False
    number_of_tries = 0
    fo = order
    fw = f_width_edge
    filtered = None
    while not success and number_of_tries < max_number_of_tries:
        try:
            filtered = spf.filter_signal(signal,
                                         filter_type=filter_type,
                                         f_cut_low=f_cut_low,
                                         f_cut_high=f_cut_high,
                                         f_sampling=f_sampling,
                                         ripple_db=ripple_db,
                                         constant_value=constant_value,
                                         use_filtfilt=use_filtfilt,
                                         f_width_edge=fw,
                                         order=fo,
                                         )
        except ValueError:
            if filter_type in ("butterworth", "kaiser"):
                log.warning("Failed filtering for width= {} order={}".format(fw, fo))
                # we are using a butterworth or kaiser filter, so we can retry to filter with a
                # bit more robust setting
                log.warning("Next attempt with width= {} order={}".format(fw, fo))
                number_of_tries += 1
                if fw is not None:
                    # we are using a kaiser filter: increase the edge_width and try again
                    fw *= 2
                if fo is not None:
                    # we are using a butterworth filter: lower the order and try again
                    fo -= 1
            else:
                log.warning("Failed filtering. Return None")
                # only retry for the butterworth and kaiser filter, as for the other filters
                # changing the  edge width or order does not have an effect. So we can stop now
                number_of_tries = max_number_of_tries
        else:
            # we succeeded in filtering this signal so stop further looping
            success = True

    return filtered


class Settings(object):
    """
    Initialise a class to store all the setting of the program.

    Parameters
    ----------
    configuration_file : str
        if this filename is given: load it to set the values.
    store_default_settings_to_file : bool
        if this value is given, write the default value to a file. This implies
        that the configuration file name is used to write to
    dump_settings; bool
        If try write all the settings to file after initialising all values

    Notes
    -----

    * In case *configuration_file* is given, this file name is used to load the previously
      stored configuration settings.
    * If the *configuration_file* value is None, the default setting are taken.
    * In case 'default_configuration_file' is  given,  the default settings are stored to
      the this file

    """

    def __init__(self, configuration_file=None, store_default_settings_to_file=False,
                 dump_settings=False):

        self.image_directory = None
        self.fatigue_settings = None
        self.mdf_data_directory = None
        self.mdf_file_base = None
        self.mdf_file_has_string_pattern = None
        self.mdf_file_start_date_time = None
        self.mdf_file_end_date_time = None
        self.time_zone = None
        self.signal_mru_plot_properties = None
        self.signal_strain_gauges_plot_properties = None
        self.signal_accelerometers_plot_properties = None
        self.signal_huisman_plot_properties = None
        self.process_mru_signals = None
        self.process_acceleration_signals = None
        self.process_strain_gauge_signals = None
        self.process_huisman_signals = None
        self.process_gps_signals = None
        self.process_dls_signals = None
        self.regular_expressions = None
        self.filtering = None
        self.time_series = None
        self.store_default_settings_to_file = store_default_settings_to_file
        self.dump_settings = dump_settings

        self.replace_mdf_file_record_names = False
        self.export_fatigue_database = False
        self.dump_mdf_data_to_file = False
        self.save_images = False
        self.gps_info_columns = None
        self.read_matlab_record_if_available = False
        self.mru_reference_location_name = None
        self.mru_reference_location = None
        self.dof_motions = None
        self.tower_angle_name = None
        self.tower_origin_acc_name = None
        self.locations = None
        self.export_time_series = None

        self.signal_hotspots_plot_properties = None
        self.signal_tower_origin_plot_properties = None
        self.signal_accelerometers_plot_properties = None
        self.signal_huisman_plot_properties = None

        self.dump_mdf_type = None
        self.show_images = None
        self.rao_vessel = None

        self.log = get_logger(__name__)

        self.configuration_file = None
        self.location_configuration_file = None

        # this dictionary is going to carry all the settings
        self._data_dict = dict()

        # this dictionary will cary the sn_curve_data
        self.sn_curve_data = dict()

        # if the scf for the pivot's is calculated based on the stress signal, you can choose
        # to actually use them in stead of the SCF defined at the location. Set this flag True
        # if you want to do that
        self.use_internally_calculated_scf_for_pivots = None

        self.safetrans_msg_pack_trajectory_database = None

        if configuration_file:
            file_base, extension = os.path.splitext(configuration_file)
            if not bool(re.match(".y(a){0,1}ml", extension, re.IGNORECASE)):
                self.log.critical(
                    "Configuration file not of type YAML. Probably your passing a "
                    "wrong file on the command line. Can not proceed!")
                raise ImportError(
                    "Could not read yaml file: {}\nFile must have extension .yaml or "
                    ".yml".format(configuration_file))
            self.configuration_file = configuration_file
            self.execution_directory = os.path.split(configuration_file)[0]
        else:
            self.execution_directory = os.getcwd()

    def initialise_or_load_configuration(self):
        """
    Create all the  attributes are created by either calling the default settings or by loading
    the yaml file
    """
        if self.configuration_file and not self.store_default_settings_to_file:
            self.log.info("loading settings from configuration file {}"
                          "".format(self.configuration_file))
            # in this section the configuration is loaded externally
            try:
                with open(self.configuration_file, "r") as stream:
                    self._data_dict = yaml.load(stream=stream)
            except IOError as err:
                self.log.warning(
                    "Please supply a valid configuration file at the command line.")
                raise IOError(err)

        else:
            # create a dictionary with all the default settings
            self.set_default_settings()

        try:
            if self._data_dict["rao_vessel"]["rao_speed_correction"] and \
                    self._data_dict["rao_vessel"]["spectrum_speed_correction"]:
                raise ValueError(
                    "Cannot set both a speed correction for the RAO and the spectrum "
                    "in rao_vessel.")
        except TypeError:
            self.log.debug("No rao_vessel found. No problem, no rao related stuff can be done")

        if self._data_dict["flags"]["import_external_sacs_responses"]:
            # read the sacs response matrix from an external excel file
            self.import_sacs_response_data_base()

        if self._data_dict["flags"]["import_external_sacs_S_N_data"]:
            # read the sacs S-N data from an external excel file
            self.import_sacs_sn_data_base()

        if self.configuration_file and self.store_default_settings_to_file:
            # write the default settings to a yaml file
            self.log.info("writing default settings to configuration file {}"
                          "".format(self.configuration_file))
            with open(self.configuration_file, "w") as stream:
                yaml.dump(self._data_dict, stream=stream, default_flow_style=False)

        # we have loaded or created a _data_dict with all the settings.
        # No change the paths if required

        configuration_file_base = os.path.splitext(os.path.split(self.configuration_file)[1])[0]
        configuration_path = os.path.split(configuration_file_base)[0]
        if self._data_dict["file"]["image_directory"] is None:
            # If the image directory is set to none, give it the same name as the configuration
            # file + "_images"
            self._data_dict["file"]["image_directory"] = \
                os.path.join(configuration_path, configuration_file_base + "_images")

        try:
            if self._data_dict["rao_vessel"]["output_directory_extension"] is None:
                self._data_dict["rao_vessel"]["output_directory_extension"] = \
                    os.path.splitext(os.path.split(self.configuration_file)[1])[0]

            if self._data_dict["rao_vessel"]["output_directory"] is None:
                # If the output directory is set none, give  the same name as the
                # configuration file + "_rao_vessel"
                self.set_safetrans_msg_data_base_file_name()
                self._data_dict["rao_vessel"]["output_directory"] = \
                    "_".join([os.path.splitext(self.safetrans_msg_pack_trajectory_database)[0],
                              self._data_dict["rao_vessel"]["output_directory_extension"]])

            if self._data_dict["rao_vessel"]["output_filebase"] is None:
                # If the rao_vessel filebase is to none, give it the same name as the
                # configuration file + "_images"
                self._data_dict["rao_vessel"]["output_filebase"] = \
                    self._data_dict["rao_vessel"]["msg_pack_trajectory_database"]
        except TypeError:
            self.log.debug("No RAO related stuff was found")

        if self._data_dict["file"]["fatigue_data_base_name"] is None:
            # If the fatigue data base is none, give it the same name as the
            # configuration file + "_history.xls"
            self._data_dict["file"]["fatigue_data_base_name"] = \
                configuration_file_base + "_history.xls"

        # the base path and image path can be given absolute (in which case they remain unaltered)
        # or relative. In the later case, use the location of the configuration script or running
        # directory to set the base directory
        if self._data_dict["file"]["mdf_data_directory"] is not None:
            if not os.path.isabs(self._data_dict["file"]["mdf_data_directory"]):
                # in case not a absolute path is given for the base directory, the base directory
                # indicate the running directory relative to either the directory where the script
                # was executed if no configuration is given or relative to the location of the
                # configuration file
                self._data_dict["file"]["mdf_data_directory"] = \
                    os.path.join(self.execution_directory,
                                 self._data_dict["file"]["mdf_data_directory"])

        if not os.path.isabs(self._data_dict["file"]["image_directory"]):
            # the image directory is relative to the location of the configuration file, except when
            # an absolute path was given
            self._data_dict["file"]["image_directory"] = \
                os.path.join(configuration_path, self._data_dict["file"]["image_directory"])

        # clean up the path from spurious dots and slashes
        if self._data_dict["file"]["mdf_data_directory"] is not None:
            self._data_dict["file"]["mdf_data_directory"] = \
                clear_path(self._data_dict["file"]["mdf_data_directory"])
        self._data_dict["file"]["image_directory"] = \
            clear_path(self._data_dict["file"]["image_directory"])

        # calculate the S-N curves based on the coefficient per location
        self.calculate_s_n_curves()

        if self.dump_settings:
            try:
                new_config = os.path.splitext(self.configuration_file)[0] + "_dump.yaml"
                self.log.info("Writing current settings to {}".format(new_config))
                with open(new_config, "w") as stream:
                    yaml.dump(self._data_dict, stream=stream, default_flow_style=False)
            except IOError as err:
                self.log.warning("Could not write a dump file of the configuration.\n{}"
                                 "".format(err))

        # copy all the second level values as attributes to the Settings class
        for key1, dict1 in iter(sorted(self._data_dict.items())):
            if key1 in ("signal_accelerometers", "signal_strain_gauges", "signal_mru",
                        "signal_huisman", "signal_hotspots", "signal_dls", "signal_gps",
                        "signal_tower_origin"):
                if dict1 is not None:
                    for key2, dict2 in iter(sorted(dict1.items())):
                        value_dict = dict()
                        for key3, value in iter(sorted(dict2.items())):
                            value_dict[key3] = value
                        key = "{}_{}".format(key1, key2)
                        setattr(self, key, value_dict)
            elif key1 in ("regular_expressions", "plot_properties", "locations",
                          "material_properties", "filtering", "fatigue_settings",
                          "general_info_sheet", "rao_vessel", "time_series", "dof_motions"):
                value_dict = dict()
                if dict1 is not None:
                    for key2, value in iter(sorted(dict1.items())):
                        value_dict[key2] = value
                setattr(self, key1, value_dict)
            else:
                for key2, value in iter(sorted(dict1.items())):
                    setattr(self, key2, value)

    def get_settings_dict(self):
        """ """

        return self._data_dict

    def set_data_dict(self, key_name, value):
        """

        Set a field in the data_dict of the Settings object

        Parameters
        ----------
        key_name: str
            Name of the key to set
        value: object
            Value to store inthe key field
        """

        self._data_dict[key_name] = value
        if hasattr(self, key_name):
            setattr(self, key_name, value)



    def set_default_settings(self):
        """ """

        # the default settings are defined below. The values can be save do yaml file such they
        # can be easily changed and loaded in a different context

        # an empty directory with the sub categories to create different groups of settings
        self._data_dict["flags"] = dict()
        self._data_dict["file"] = dict()
        self._data_dict["values"] = dict()
        self._data_dict["fatigue_settings"] = dict()
        self._data_dict["general_info_sheet"] = dict()
        self._data_dict["material_properties"] = dict()
        self._data_dict["units"] = dict()
        self._data_dict["regular_expressions"] = dict()
        self._data_dict["filtering"] = dict()
        self._data_dict["plot_properties"] = dict()
        self._data_dict["locations"] = dict()
        self._data_dict["dof_motions"] = dict()
        self._data_dict["signal_accelerometers"] = dict()
        self._data_dict["signal_accelerometers"]["plot_properties"] = dict()
        self._data_dict["signal_accelerometers"]["plot_limits"] = dict()
        self._data_dict["signal_accelerometers"]["coefficients"] = dict()
        self._data_dict["signal_accelerometers"]["calibration"] = dict()
        self._data_dict["signal_strain_gauges"] = dict()
        self._data_dict["signal_strain_gauges"]["coefficients"] = dict()
        self._data_dict["signal_strain_gauges"]["plot_properties"] = dict()
        self._data_dict["signal_strain_gauges"]["plot_limits"] = dict()
        self._data_dict["rao_vessel"] = dict()

        self._data_dict["signal_hotspots"] = dict()
        self._data_dict["signal_hotspots"]["plot_properties"] = dict()

        self._data_dict["signal_gps"] = dict()
        self._data_dict["signal_gps"]["plot_properties"] = dict()

        self._data_dict["signal_dls"] = dict()
        self._data_dict["signal_dls"]["plot_properties"] = dict()

        self._data_dict["signal_mru"] = dict()
        self._data_dict["signal_mru"]["plot_properties"] = dict()
        self._data_dict["signal_mru"]["smooth_filter"] = dict()
        self._data_dict["signal_mru"]["plot_limits"] = dict()

        self._data_dict["signal_tower_origin"] = dict()
        self._data_dict["signal_tower_origin"]["plot_properties"] = dict()
        self._data_dict["signal_tower_origin"]["plot_limits"] = dict()

        self._data_dict["signal_huisman"] = dict()
        self._data_dict["signal_huisman"]["plot_properties"] = dict()
        self._data_dict["signal_huisman"]["plot_limits"] = dict()

        # an empty directory with the sub categories to create different groups of settings
        self._data_dict["time_series"] = dict()
        self._data_dict["time_series"]["export_name_list"] = [
            "TowO_ACC_AX",
            "TowO_ACC_AY",
            "TowO_ACC_AZ",
            "TowO_ACC_RXX",
            "TowO_ACC_RYY",
            "TowO_ACC_RZZ"]
        self._data_dict["time_series"]["export_file_base"] = "tower_origin_6dof"
        self._data_dict["time_series"]["export_file_type"] = "msg_pack"
        self._data_dict["time_series"]["export_out_directory"] = "time_series"
        self._data_dict["time_series"]["spectrum_file_extension"] = "psd_csd"
        self._data_dict["time_series"]["write_spectrum_after_psd_calculation"] = False
        self._data_dict["time_series"]["read_spectrum_when_exist"] = True

        # values which are directly accesible with settings.blabla
        self._data_dict["values"]["dump_mdf_type"] = "xls"
        self._data_dict["values"]["tower_angle_name"] = "Huisman_TowerAngle"
        self._data_dict["values"]["tower_angle_name"] = "Huisman_TowerAngle"
        self._data_dict["values"]["tower_origin_name"] = "PIVot (SB)"
        self._data_dict["values"]["mru_location_name"] = "MRU1"
        self._data_dict["values"]["mru_reference_location_name"] = "MRU1"
        self._data_dict["values"][
            "mru_angle_units"] = "degrees"  # or radians for the old format
        self._data_dict["values"]["mru_pitch_scale"] = 1  #
        self._data_dict["values"]["mru_heading_scale"] = 1
        self._data_dict["values"][
            "mru_heave_scale"] = -1  # the new octants give a pitch which is
        # multiplied with -1
        self._data_dict["values"]["mru_roll_scale"] = 1  # kongsberg same as in HMC
        self._data_dict["values"]["mru_surge_scale"] = 1  # kongsberg same as in HMC
        self._data_dict["values"]["mru_sway_scale"] = 1  # kongsberg same as in HMC
        self._data_dict["values"][
            "n_cut_head"] = 0  # do not take the first n_cut_head point for
        # the statistics
        self._data_dict["values"]["n_cut_tail"] = 0  # do not take the last n_cut_tail point for
        # the statistics
        # so use a pitch scale 1
        self._data_dict["values"]["tower_origin_acc_name"] = "TowO_ACC"
        self._data_dict["values"]["tower_origin_mru_name"] = "TowO_MRU"
        self._data_dict["values"]["mru_acc_name"] = "MRU_ACC"
        self._data_dict["values"]["exclude_locations"] = ["MAIN BRACE", "Tower Head (SB)"]
        self._data_dict["values"]["replace_mdf_file_record_names"] = dict(MRU_Picth="MRU_Pitch")
        self._data_dict["values"]["gps_info_columns"] = dict(
            longitude="GPS_LONGITUDE",
            latitude="GPS_LATITUDE",
            distance="travel_distance",
            heading="GPS_HEADING"
        )

        # the setting values per group. Starting with the flags (Booleans)
        self._data_dict["flags"]["save_images"] = True
        self._data_dict["flags"]["show_images"] = True
        self._data_dict["flags"]["debug"] = True
        self._data_dict["flags"]["calculate_scf_pivots_internally"] = True
        self._data_dict["flags"]["use_internally_calculated_scf_for_pivots"] = False
        self._data_dict["flags"]["dump_plot_lines_to_file"] = False
        self._data_dict["flags"]["dump_plot_use_zoomed_range"] = False
        self._data_dict["flags"]["dump_mdf_data_to_file"] = False
        self._data_dict["flags"]["process_acceleration_signals"] = True
        self._data_dict["flags"]["process_strain_gauge_signals"] = True
        self._data_dict["flags"]["process_mru_signals"] = True
        self._data_dict["flags"]["process_gps_signals"] = True
        self._data_dict["flags"]["process_dls_signals"] = True
        self._data_dict["flags"]["process_huisman_signals"] = True
        self._data_dict["flags"]["read_matlab_record_if_available"] = False
        self._data_dict["flags"]["block_last_plot"] = True
        self._data_dict["flags"]["use_all_accelerometers"] = True
        self._data_dict["flags"]["add_mru_signal_to_accelerometer_plot"] = True
        self._data_dict["flags"]["import_external_sacs_responses"] = True
        self._data_dict["flags"]["import_external_sacs_S_N_data"] = True
        self._data_dict["flags"]["export_fatigue_database"] = True
        self._data_dict["flags"]["export_time_series"] = False
        self._data_dict["flags"]["use_poison_for_pivots"] = False

        # the file related settings
        self._data_dict["file"]["dump_plot_type"] = "msg_pack"
        self._data_dict["file"]["mdf_data_directory"] = os.path.join(".")
        self._data_dict["file"]["fatigue_data_base_location"] = "."
        self._data_dict["file"]["fatigue_data_base_name"] = "fatigue_history.xls"
        self._data_dict["file"]["image_directory"] = "images"
        self._data_dict["file"]["image_type"] = "png"
        self._data_dict["file"]["mdf_file_base"] = "AMS_BALDER_"
        self._data_dict["file"]["mdf_file_has_string_pattern"] = "110226T000000"
        self._data_dict["file"]["mdf_file_start_date_time"] = "20110226T000000"
        self._data_dict["file"]["mdf_file_end_date_time"] = "20110227T000000"
        self._data_dict["file"]["time_zone"] = "UTC"
        self._data_dict["file"]["sacs_data_base_location"] = "."
        self._data_dict["file"]["sacs_response_data_file"] = "SACSdatabase_coldspots_ROg.xls"
        self._data_dict["file"]["sacs_S_N_data_file"] = "SACSdatabase_hotspots_evv.xls"
        self._data_dict["file"]["sacs_S_N_data_sheet"] = "HotspotData"

        self._data_dict["rao_vessel"]["data_base_location"] = "."
        self._data_dict["rao_vessel"]["csv_trajectory_files"] = [
            "Stavanger_LasPalmas_Sep_300.csv"]
        self._data_dict["rao_vessel"]["rayleigh_scaling_factor"] = 0.5
        self._data_dict["rao_vessel"]["read_msg_pack"] = True
        self._data_dict["rao_vessel"]["msg_pack_trajectory_database"] = \
            "Stavanger_LasPalmas.msg_pack"
        self._data_dict["rao_vessel"]["rao_data_location"] = "."
        self._data_dict["rao_vessel"]["rao_data_file_name"] = "RAO6.mat"
        self._data_dict["rao_vessel"]["rao_dump_original_as_netcdf"] = False
        self._data_dict["rao_vessel"]["rao_save_one_d_dir_and_freq"] = False
        self._data_dict["rao_vessel"]["rao_speed_correction"] = False
        self._data_dict["rao_vessel"]["output_directory"] = "rao_vessel"
        self._data_dict["rao_vessel"]["output_directory_extension"] = None
        self._data_dict["rao_vessel"]["output_filebase"] = "voyage"
        self._data_dict["rao_vessel"]["tower_angle"] = 90
        self._data_dict["rao_vessel"]["max_trajectories"] = None
        self._data_dict["rao_vessel"]["max_time_steps"] = None
        self._data_dict["rao_vessel"][
            "wave_direction_definition"] = 0  # 0: going_to (rao_vessel),
        self._data_dict["rao_vessel"]["spectrum_speed_correction"] = True

        # all settings related to the rao_tune mode
        self._data_dict["rao_vessel"]["rao_tune"] = dict()
        self._data_dict["rao_vessel"]["rao_tune"]["direction_range"] = "0:345:15"
        self._data_dict["rao_vessel"]["rao_tune"]["frequency_range"] = "0.01:2.5:0.01"
        self._data_dict["rao_vessel"]["rao_tune"]["f_min_used_for_direction_pick"] = 0.25
        self._data_dict["rao_vessel"]["rao_tune"]["f_max_used_for_direction_pick"] = 1.0
        self._data_dict["rao_vessel"]["rao_tune"]["f_clip_min"] = 0.2
        self._data_dict["rao_vessel"]["rao_tune"]["f_clip_max"] = 2.5
        self._data_dict["rao_vessel"]["rao_tune"]["speed_threshold_in_knots"] = 4.0
        self._data_dict["rao_vessel"]["rao_tune"]["hstot_threshold_in_meter"] = 0.1
        self._data_dict["rao_vessel"]["rao_tune"][
            "spectrum_threshold_for_rao_abs_in_wave_height"] \
            = 0.01
        self._data_dict["rao_vessel"]["rao_tune"]["clip_spectrum_in_1d_domain"] = True
        self._data_dict["rao_vessel"]["rao_tune"][
            "spectrum_threshold_for_rao_ang_in_wave_height"] \
            = 0.01
        self._data_dict["rao_vessel"]["rao_tune"]["spectrum_n_points_per_block"] = 2048
        self._data_dict["rao_vessel"]["rao_tune"][
            "set_rao_phase_on_wave_direction_index"] = False
        self._data_dict["rao_vessel"]["rao_tune"]["minimum_number_of_added_raos_per_bin"] = 1
        self._data_dict["rao_vessel"]["rao_tune"]["phase_resolution"] = 12
        self._data_dict["rao_vessel"]["rao_tune"]["create_zero_speed_rao"] = False
        self._data_dict["rao_vessel"]["rao_tune"]["average_type"] = "normal"  # or "inverse"
        self._data_dict["rao_vessel"]["rao_tune"]["raise_debug_plots"] = False
        self._data_dict["rao_vessel"]["rao_tune"]["dump_rao_in_out"] = True
        self._data_dict["rao_vessel"]["rao_tune"]["plot_frequency_indices"] = [28, 30, 32, 35]
        self._data_dict["rao_vessel"]["rao_tune"]["write_rao_per_time_step_to_netcdf"] = True
        self._data_dict["rao_vessel"]["rao_tune"]["rao_netcdf_file_name"] = "rao_vs_time.nc"
        self._data_dict["rao_vessel"]["rao_tune"]["rao_output_base_name"] = "na.nc"
        self._data_dict["rao_vessel"]["rao_tune"]["rao_output_as_complex"] = False
        self._data_dict["rao_vessel"]["rao_tune"]["estimate_phase_from_cross_spectra"] = False
        self._data_dict["rao_vessel"]["rao_tune"]["symmetry_rao"] = dict()
        self._data_dict["rao_vessel"]["rao_tune"]["symmetry_rao"]["magnitude"] = dict()
        self._data_dict["rao_vessel"]["rao_tune"]["symmetry_rao"]["magnitude"][
            "impose_symmetry"] \
            = True
        self._data_dict["rao_vessel"]["rao_tune"]["symmetry_rao"]["magnitude"][
            "type"] = "weighted"
        self._data_dict["rao_vessel"]["rao_tune"]["symmetry_rao"]["phase"] = dict()
        self._data_dict["rao_vessel"]["rao_tune"]["symmetry_rao"]["phase"][
            "impose_symmetry"] = True
        self._data_dict["rao_vessel"]["rao_tune"]["symmetry_rao"]["phase"]["type"] = "keep_left"
        self._data_dict["rao_vessel"]["rao_tune"]["symmetry_rao"]["phase"][
            "symmetry_rules"] = list(
            ["mirror",
             "mirror_negate",
             "mirror",
             "mirror_negate",
             "mirror",
             "mirror_negate"
             ])
        # 1: comming from(argos)
        self._data_dict["rao_vessel"]["rao_tune"]["tower_dof_phase_zero"] = "TowO_ACC_RXX"
        self._data_dict["rao_vessel"]["rao_tune"]["absolute_phase"] = False
        self._data_dict["rao_vessel"]["rao_tune"]["replace_nan_to_zero"] = True

        # information related to dumping the spectra and rao. Protect to make backward compatible
        self._data_dict["rao_vessel"][
            "dump_sea_state_per_time_step"] = False  # dump the sea states
        # dump the rao per time step
        self._data_dict["rao_vessel"]["dump_rao_acc_per_time_step"] = False
        # dump the rao per hot spot location
        self._data_dict["rao_vessel"]["dump_rao_hot_spot_per_time_step"] = dict()

        # in case hot spot location are added to the dictionary, it should look like
        # AD_PS
        #   phi_angle: 0
        #   write_it: true
        # the information in this dictionary only is stored in the total statistics which report
        # the overal track statistics
        self._data_dict["rao_vessel"]["info_sheet"] = dict()
        self._data_dict["rao_vessel"]["info_sheet"]["TOTAL_TIME"] = \
            dict(alias="travel_time", operation="max")
        self._data_dict["rao_vessel"]["info_sheet"]["TOTAL_DISTANCE"] = \
            dict(alias="travel_distance", operation="max")
        self._data_dict["rao_vessel"]["info_sheet"]["START_DATE_TIME"] = \
            dict(alias="date_time", operation="min")
        self._data_dict["rao_vessel"]["info_sheet"]["SEA_STATE"] = \
            dict(alias="Unity", operation="std")

        self._data_dict["rao_vessel"]["sea_state_spectra"] = dict()
        self._data_dict["rao_vessel"]["sea_state_spectra"]["fix_energy_deficit"] = True
        self._data_dict["rao_vessel"]["sea_state_spectra"]["sea_state_psd_database_name"] = \
            "sea_state_psd.nc"
        self._data_dict["rao_vessel"]["sea_state_spectra"]["reset_sea_state_psd_database"] = \
            False
        self._data_dict["rao_vessel"]["sea_state_spectra"][
            "write_new_sea_state_psd_to_database"] = \
            False
        self._data_dict["rao_vessel"]["sea_state_spectra"][
            "read_sea_state_psd_from_database_if_exist"] = True
        self._data_dict["rao_vessel"]["sea_state_spectra"]["wind_spectrum"] = dict()
        self._data_dict["rao_vessel"]["sea_state_spectra"]["wind_spectrum"]["energy_spectrum"] = \
            dict()
        self._data_dict["rao_vessel"]["sea_state_spectra"]["wind_spectrum"][
            "energy_spectrum"]["type"] = "jonswap"
        self._data_dict["rao_vessel"]["sea_state_spectra"]["wind_spectrum"][
            "energy_spectrum"]["gamma"] = 3.3
        self._data_dict["rao_vessel"]["sea_state_spectra"]["wind_spectrum"][
            "energy_spectrum"]["version"] = 0
        self._data_dict["rao_vessel"]["sea_state_spectra"]["wind_spectrum"][
            "directional_distribution"] = dict()
        self._data_dict["rao_vessel"]["sea_state_spectra"]["wind_spectrum"][
            "directional_distribution"]["version"] = 0
        self._data_dict["rao_vessel"]["sea_state_spectra"]["wind_spectrum"][
            "directional_distribution"]["spreading_factor"] = 5

        self._data_dict["rao_vessel"]["sea_state_spectra"]["swell_spectrum"] = dict()
        self._data_dict["rao_vessel"]["sea_state_spectra"]["swell_spectrum"][
            "energy_spectrum"] = dict()
        self._data_dict["rao_vessel"]["sea_state_spectra"]["swell_spectrum"][
            "energy_spectrum"]["type"] = "gauss"
        self._data_dict["rao_vessel"]["sea_state_spectra"]["swell_spectrum"][
            "energy_spectrum"]["version"] = 0
        self._data_dict["rao_vessel"]["sea_state_spectra"]["swell_spectrum"][
            "directional_distribution"] = dict()
        self._data_dict["rao_vessel"]["sea_state_spectra"]["swell_spectrum"][
            "directional_distribution"]["version"] = 0
        self._data_dict["rao_vessel"]["sea_state_spectra"]["swell_spectrum"][
            "directional_distribution"]["spreading_factor"] = 13

        # the information in this dictionary is stored in the individual voyage statistics file,
        # so it reports statistics for every 30 min. Since the value are directly obtained from the
        # rao_vessel data file, we do not have to define a operation
        self._data_dict["rao_vessel"]["voyage_info_sheet"] = list(
            ["date_time", "travel_time", "latitude", "longitude", "travel_distance",
             "speed_sustained", "Hs_tot", "Tp_tot", "direction_wave_tot", "Hs_wind", "Tp_wind",
             "direction_wave_wind", "Hs_swell", "Tp_swell", "direction_wave_swell",
             "speed_wind",
             "direction_wind", "speed_current", "direction_current", "speed_trough_water",
             "depth",
             "heading", "direction_drift"])

        self._data_dict["values"]["sacs_tower_model"] = "HEXwithRoller_V0060_Block23_3"

        self._data_dict["general_info_sheet"]["GPS_LATITUDE"] = dict(alias="GPS_LatitudeDez",
                                                                     operation="mean")
        self._data_dict["general_info_sheet"]["GPS_LONGITUDE"] = dict(alias="GPS_LongitudeDez",
                                                                      operation="mean")
        self._data_dict["general_info_sheet"]["GPS_HEADING"] = dict(alias="GPS_Currentheading",
                                                                    operation="mean")
        self._data_dict["general_info_sheet"]["GPS_SOG"] = dict(alias="GPS_SOGkn",
                                                                operation="mean")
        self._data_dict["general_info_sheet"]["DLS_DRAUGHT"] = dict(alias="DLS_Draught",
                                                                    operation="mean")
        self._data_dict["general_info_sheet"]["MRU_ROLL_SD"] = dict(alias="MRU_Roll",
                                                                    operation="std")
        self._data_dict["general_info_sheet"]["MRU_PITCH_SD"] = dict(alias="MRU_Pitch",
                                                                     operation="std")
        self._data_dict["general_info_sheet"]["MRU_HEAVE_SD"] = dict(alias="MRU_Heave",
                                                                     operation="std")
        self._data_dict["general_info_sheet"]["COG_AX_SD"] = dict(alias="COG_AX",
                                                                  operation="std")
        self._data_dict["general_info_sheet"]["COG_AY_SD"] = dict(alias="COG_AY",
                                                                  operation="std")
        self._data_dict["general_info_sheet"]["COG_AZ_SD"] = dict(alias="COG_AZ",
                                                                  operation="std")
        self._data_dict["general_info_sheet"]["COG_RXX_SD"] = dict(alias="COG_RXX",
                                                                   operation="std")
        self._data_dict["general_info_sheet"]["COG_RYY_SD"] = dict(alias="COG_RYY",
                                                                   operation="std")
        self._data_dict["general_info_sheet"]["COG_RZZ_SD"] = dict(alias="COG_RZZ",
                                                                   operation="std")

        self._data_dict["fatigue_settings"]["beta_wafo"] = 3
        # 0: use matlab algorith, 1: use WAFO
        self._data_dict["fatigue_settings"]["damage_calculation_method"] = 0
        self._data_dict["fatigue_settings"]["K_wafo"] = 0
        self._data_dict["fatigue_settings"]["show_sn_curves"] = False
        self._data_dict["fatigue_settings"]["n_stress_bins"] = 1000
        # set min larger than 0 to prevent devision by zero
        self._data_dict["fatigue_settings"]["stress_min"] = 1.0
        # S is expressed in MPa
        self._data_dict["fatigue_settings"]["stress_max"] = 1000.0
        # S is expressed in MPa
        self._data_dict["fatigue_settings"]["rainflow_cycle_threshold"] = 0.0
        # number of angles where the hotspot is calculated
        self._data_dict["fatigue_settings"]["n_hotspot_angles"] = 8

        self._data_dict["units"]["response_matrix"] = 1e3  # [kN/m2]
        self._data_dict["units"]["geometric_data"] = 1e-3  # [mm, or, mm2, mm3, etc]

        # the value settings
        self._data_dict["material_properties"]["E_youngs_modulus"] = 210e9  # [N/m2]
        self._data_dict["material_properties"]["nu_poison_coefficient"] = 0.3

        # some general plotting properties
        self._data_dict["plot_properties"]["dpi"] = 200
        # 0: plot acceleration, 1: plot real signal
        self._data_dict["plot_properties"]["add_zoom_plot"] = True
        # add a zoom plot if given
        self._data_dict["plot_properties"]["zoom_range"] = (1300, 1400)
        self._data_dict["plot_properties"]["case_description"] = dict(title="Default Settings",
                                                                      position=(-0.3, 1.1),
                                                                      color="blue")

        self._data_dict["filtering"]["use_matlab_engine"] = False
        self._data_dict["filtering"]["accelerometers"] = dict()
        self._data_dict["filtering"]["strain_gauges"] = dict()
        self._data_dict["filtering"]["mru"] = dict()
        self._data_dict["filtering"]["accelerometers"]["type"] = "butterworth_band"
        # frequency in Hz: corresponds to T=20 s
        self._data_dict["filtering"]["accelerometers"]["f_cut_off_low"] = 0.05
        # frequency in Hz, corresponds to T=4 s
        self._data_dict["filtering"]["accelerometers"]["f_cut_off_high"] = 0.25
        self._data_dict["filtering"]["accelerometers"][
            "order"] = 2  # order n f the filter: drop of at edge is 20*n dB/decade
        self._data_dict["filtering"]["strain_gauges"]["type"] = "butterworth_high"
        # frequency in Hz: corresponds to T=20 s
        self._data_dict["filtering"]["strain_gauges"]["f_cut_off_low"] = 0.05
        # order n f the filter: drop is 20*n dB/decade
        self._data_dict["filtering"]["strain_gauges"]["order"] = 2
        self._data_dict["filtering"]["mru"]["type"] = "butterworth_low"
        # frequency in Hz: corresponds to T=20 s
        self._data_dict["filtering"]["mru"]["f_cut_off_low"] = 0.05
        # frequency in Hz: corresponds to T=20 s
        self._data_dict["filtering"]["mru"]["f_cut_off_high"] = 0.25
        self._data_dict["filtering"]["mru"]["order"] = 2

        # define the filter to extract the strain gauges. First item applies to the name,
        # second to the label
        self._data_dict["regular_expressions"]["date_time"] = ["DateTime"]
        # reads Heave Roll Pitch
        self._data_dict["regular_expressions"]["signal_mru"] = ["MRU_[HRP]"]
        # Longitude/Latitude/Current_heading/Speed
        self._data_dict["regular_expressions"]["signal_gps"] = ["GPS_[LCS]"]
        # reads draught of DLS system
        self._data_dict["regular_expressions"]["signal_dls"] = ["DLS_[D]"]
        self._data_dict["regular_expressions"]["signal_strain_gauges"] = \
            ["BALDER__UI\d_A(\d+)_VI1", "(.*) - SG (\d+)"]
        self._data_dict["regular_expressions"]["signal_accelerometers"] = \
            ["BALDER__UI\d_A(\d+)_VI1", "(.*) - A([XYZ])\s*-\s*(\d+)",
             "(.*) - ACC\s(\d+)\s*-\s*A([XYZ])"]
        self._data_dict["regular_expressions"]["signal_huisman"] = \
            ["Huisman_.*", "Tower", "Traveling"]

        self._data_dict["locations"]["COG"] = dict()
        self._data_dict["locations"]["MRU1"] = dict()
        self._data_dict["locations"]["MRU2"] = dict()
        self._data_dict["locations"]["MRU3"] = dict()
        self._data_dict["locations"]["COFF SB FWD"] = dict()
        self._data_dict["locations"]["HZ3"] = dict()
        self._data_dict["locations"]["MAIN BRACE"] = dict()
        self._data_dict["locations"]["RX4"] = dict()
        self._data_dict["locations"]["TSM"] = dict()
        self._data_dict["locations"]["Tower Head (PS)"] = dict()
        self._data_dict["locations"]["Tower Head (SB)"] = dict()
        self._data_dict["locations"]["SL1"] = dict()
        self._data_dict["locations"]["Q02"] = dict()
        self._data_dict["locations"]["PIVot (SB)"] = dict()
        self._data_dict["locations"]["AD (SB)"] = dict()
        self._data_dict["locations"]["AD (PS)"] = dict()

        # define aliases for all names as the names as appearing in the mdf files varies over all
        # the other input files
        self._data_dict["locations"]["SL1"]["aliases"] = ["SW5 SSW5-1330"]
        self._data_dict["locations"]["AD (SB)"]["aliases"] = ["AD4 SADS-5110", "AD2 2312-5110"]
        self._data_dict["locations"]["AD (PS)"]["aliases"] = ["AD4 SADP-5240", "AD2 2442-5240"]
        # take the same values for the Main pivot and SB pivot
        self._data_dict["locations"]["PIVot (SB)"]["aliases"] = ["PIV 2140-WT2K",
                                                                 "PIV 2210-WT2J"]

        # the coordinates in meter of all the locations in absolute coordinates w.r.  stern, CL
        # (PS positive), deck in the matlab code, the absolute coordinates are put in respect to
        # the PIVot (SB]
        self._data_dict["locations"]["MRU1"]["coordinates"] = [132.3, 2.92, 7.3]
        self._data_dict["locations"]["MRU2"]["coordinates"] = [106.6, -7.390, 14.56]
        self._data_dict["locations"]["MRU3"]["coordinates"] = [106.6, -7.005, 14.56]
        self._data_dict["locations"]["COG"]["coordinates"] = [57.14, 0, -10.6]
        self._data_dict["locations"]["PIVot (SB)"]["coordinates"] = [47.5, 52.43, 1.10]
        self._data_dict["locations"]["COFF SB FWD"]["coordinates"] = \
            [110.44, -25.7, -26.76]  # Cofferdam CFD
        self._data_dict["locations"]["HZ3"]["coordinates"] = [50.2, 58.5, 69.9]  # HEX insert j
        self._data_dict["locations"]["MAIN BRACE"]["coordinates"] = \
            [110.9, -25.7, -26.8]  # same as CFD?
        self._data_dict["locations"]["RX4"]["coordinates"] = \
            [49.32, 59.23, 21.52]  # Lower cross brace
        self._data_dict["locations"]["TSM"]["coordinates"] = \
            [23.14, 43.67, -3.84]  # Tower support module
        self._data_dict["locations"]["Tower Head (PS)"]["coordinates"] = [52.0, 55.6,
                                                                          97.4]  # Outer
        self._data_dict["locations"]["Tower Head (SB)"]["coordinates"] = [52.7, 47.6,
                                                                          97.4]  # Inner

        # sigma_i = ( fct/A, sin(zeta)* Dz/2Izz, cos(zeta) * Dy/2Iyy )
        # if zeta is None but still cross_sectional_properites are given: use the flat plate
        # relation normally fct = 1, but it can be used to apply a phase shift (for Q02) or to set
        # the 3 values for the flat # plate. In that fact the force_x factor is set to an list of 3
        # values
        self._data_dict["locations"]["HZ3"]["bending_moment_angles"] = [270, 0, 90, 180]
        self._data_dict["locations"]["RX4"]["bending_moment_angles"] = [90, 180, 270, 0]
        self._data_dict["locations"]["SL1"]["bending_moment_angles"] = [180, 90, 0, 270]
        self._data_dict["locations"]["Q02"]["bending_moment_angles"] = [315, 45, 135, 225]
        self._data_dict["locations"]["AD (SB)"]["bending_moment_angles"] = [180, 270, 0, 90]
        self._data_dict["locations"]["AD (PS)"]["bending_moment_angles"] = [90, 270]

        self.set_force_x_factor()

        # define the response matrix per position. The numbers indicate
        # Fx/Ax Fx/Ay Fx/Az Fx/Rxx Fx/Ryy Fx/Rzz
        # My/Ax My/Ay My/Az My/Rxx My/Ryy My/Rzz
        # Mz/Ax Mz/Ay Mz/Az Mz/Rxx Mz/Ryy Mz/Rzz

        # self.init_response_matrix("_tower_angle_transit")
        # self.init_response_matrix("_tower_angle_straight")

        self.set_in_tower_flag()
        self._data_dict["locations"]["HZ3"]["in_tower"] = True
        self._data_dict["locations"]["RX4"]["in_tower"] = True
        self._data_dict["locations"]["Tower Head (PS)"]["in_tower"] = True
        self._data_dict["locations"]["Tower Head (SB)"]["in_tower"] = True

        self.set_is_square_tube_flag()
        self._data_dict["locations"]["AD (SB)"]["is_square_tube"] = True
        self._data_dict["locations"]["AD (PS)"]["is_square_tube"] = True

        self.set_acc_component_fit_weight()
        self._data_dict["locations"]["TSM"]["fit_weight"]["X"] = 1
        self._data_dict["locations"]["TSM"]["fit_weight"]["Y"] = 1
        self._data_dict["locations"]["TSM"]["fit_weight"]["Z"] = 1
        self._data_dict["locations"]["COFF SB FWD"]["fit_weight"]["Y"] = 1
        self._data_dict["locations"]["COFF SB FWD"]["fit_weight"]["Z"] = 1
        self._data_dict["locations"]["RX4"]["fit_weight"]["X"] = 1

        self.set_plot_this_locations()

        # 0: plot acceleration, 1: plot real signal
        self._data_dict["signal_accelerometers"]["plot_properties"]["plot_this"] = False
        self._data_dict["signal_accelerometers"]["plot_properties"]["type"] = 0
        self._data_dict["signal_accelerometers"]["plot_properties"]["force_autoscale"] = True

        # Define some properties of each component. The first value in the list is the direction
        # Negative means negative X,Y, or Z direction. If the value
        # is 2 (or -2) this accelerometer is the one which is used in the calculation
        # the next tupple allows to set the range min/max of the plot of the channel
        for i in range(1, 18):
            ac = "A{:02d}".format(i)
            self._data_dict["signal_accelerometers"]["plot_limits"][ac] = [(None, None),
                                                                           (None, None)]
            self._data_dict["signal_accelerometers"]["calibration"][ac] = None
            self._data_dict["signal_accelerometers"]["coefficients"][ac] = None

        # 0: plot heave/roll/pitch, 1: plot first time derivative
        self._data_dict["signal_mru"]["plot_properties"]["plot_this"] = True
        self._data_dict["signal_mru"]["plot_properties"]["type"] = 0
        # degree: k in  UnivariateSpline filter: the order of the spline. Must be larger than 3
        # because we need to take the 2nd order derivative. Maximum allowed is 5
        self._data_dict["signal_mru"]["smooth_filter"]["spline_degree"] = 5
        # smoothing_factor: s in  UnivariateSpline filter: the amount of smoothing
        self._data_dict["signal_mru"]["smooth_filter"]["spline_smoothing_factor"] = 0.25
        self._data_dict["signal_mru"]["smooth_filter"]["resample_factor"] = 15
        self._data_dict["signal_mru"]["smooth_filter"][
            "type"] = "spline"  # spline, resample or none
        self._data_dict["signal_mru"]["plot_properties"]["force_autoscale"] = \
            True  # add a zoom plot if given
        self._data_dict["signal_mru"]["plot_properties"]["show_raw_data"] = \
            True  # also show the non-smoothed data
        self._data_dict["signal_mru"]["plot_properties"][
            "show_accelerator_data"] = True  # also show the non-smoothed data
        self._data_dict["signal_mru"]["plot_limits"]["MRU_Heave"] \
            = [(-0.4, 0.4), (-0.2, 0.2), (-0.1, 0.1)]
        self._data_dict["signal_mru"]["plot_limits"]["MRU_Roll"] \
            = [(-1.5, 1.5), (-0.15, 0.15), (-0.1, 0.1)]
        self._data_dict["signal_mru"]["plot_limits"]["MRU_Pitch"] \
            = [(-1.0, 1.0), (-0.2, 0.2), (-0.1, 0.1)]

        # 0: plot AX/AY/AZ, 1:  plot RXX/RYY/RZZ
        self._data_dict["signal_tower_origin"]["plot_properties"]["plot_this"] = True
        self._data_dict["signal_tower_origin"]["plot_properties"]["subtract_mean"] = True
        self._data_dict["signal_tower_origin"]["plot_properties"]["type"] = 0
        self._data_dict["signal_tower_origin"]["plot_properties"]["force_autoscale"] = True
        self._data_dict["signal_tower_origin"]["plot_limits"]["AX"] = [-1, 1]
        self._data_dict["signal_tower_origin"]["plot_limits"]["AY"] = [-1, 1]
        self._data_dict["signal_tower_origin"]["plot_limits"]["AZ"] = [-1, 1]
        self._data_dict["signal_tower_origin"]["plot_limits"]["RXX"] = [-1, 1]
        self._data_dict["signal_tower_origin"]["plot_limits"]["RYY"] = [-1, 1]
        self._data_dict["signal_tower_origin"]["plot_limits"]["RZZ"] = [-1, 1]

        self._data_dict["signal_dls"]["plot_properties"]["plot_this"] = False
        self._data_dict["signal_gps"]["plot_properties"]["plot_this"] = False

        self._data_dict["signal_huisman"]["plot_properties"]["plot_this"] = False
        self._data_dict["signal_huisman"]["plot_properties"]["type"] = 0
        self._data_dict["signal_huisman"]["plot_properties"][
            "force_autoscale"] = True  # add a zoom plot if given
        self._data_dict["signal_huisman"]["plot_limits"]["Huisman_TowerAngle"] = [None, None]

        self._data_dict["signal_hotspots"]["plot_properties"]["plot_this"] = False
        self._data_dict["signal_hotspots"]["plot_properties"]["type"] = 0
        self._data_dict["signal_hotspots"]["plot_properties"][
            "force_autoscale"] = True  # add a zoom plot if given
        self._data_dict["signal_hotspots"]["plot_properties"]["plot_limits"] = dict()

        self.set_hotspot_plot_limits()

        # the Marin measurement values to calibrate all the signal_accelerometers. Each three points
        # are the measured the Marin measurement values to calibrate all the signal_accelerometers.
        # Each three points are the measured current in mA for the accelerometer down
        # (a=-9.81 m/s2), horizontal (a=0 m/s2) and up (a=9.81 m/s2). the values below are the
        # latest values report in "Fatigue Monitoring J-Lay Tower of DCV Balder, report
        # nr 26431-02, revision 01, 20 May 2014
        self._data_dict["signal_accelerometers"]["calibration"]["A01"] = [10.324, 12.013,
                                                                          13.723]
        self._data_dict["signal_accelerometers"]["calibration"]["A02"] = [10.382, 11.997, 13.65]
        self._data_dict["signal_accelerometers"]["calibration"]["A03"] = [10.335, 11.967,
                                                                          13.623]
        self._data_dict["signal_accelerometers"]["calibration"]["A09"] = [10.567, 12.243,
                                                                          13.911]
        self._data_dict["signal_accelerometers"]["calibration"]["A10"] = [10.394, 11.986,
                                                                          13.562]
        self._data_dict["signal_accelerometers"]["calibration"]["A11"] = [10.433, 11.973,
                                                                          13.552]
        self._data_dict["signal_accelerometers"]["calibration"]["A15"] = [10.468, 12.150,
                                                                          13.765]
        self._data_dict["signal_accelerometers"]["calibration"]["A16"] = [10.44, 12.11, 13.73]
        self._data_dict["signal_accelerometers"]["calibration"]["A17"] = [9.21, 10.88, 12.50]

        # Here, the least square fit coefficient resulting from the three measured mA - acceleration
        # values above Note the the coefficients are a[(m/s2)/mA] and the offset b [m/s2] with a
        # assumed A_offset on the signal
        #  acc(A)  = a * (A-A_offset) + b
        # normally this offset is A_offset=12 mA, however, sometimes the offset is 8 in the matlab
        # code for the Z coefficient as it already removes the g0 in the z-direction
        # Marin reports the sensor sensitivity S [mA/G]. The relation between a and S is: S = 9.81/a
        # In the old report of Marin : Fatigue Monitoring J-Lay tower of DVC Balder,
        # Report-nr 22960-02-HSS, Dec 2009
        # only the sensitivity S is report.
        # From the sign of the sensitivity the direction can be obtain. Here we use the sign
        # convention as well. For sensors A01, A02, A03, and A10, A11 we have the sensitivity from
        # the old report + the new measurements in the new report. In that case, we report in a
        # tuple the direction, and pre-amplification factor. This values are used to do a correct
        # fit on the given calibration parameters In case a list is given, this implies it is
        # interpreted as [a, b, offset] and there won't be a fit anymore note the in the model of
        # Richard A06 and A14 have offset of 8. This is because richard already subtracts the
        # 9.81 acceleration in the z-direction. The range of the accelerometer varies between 4 and
        # 20, so the average is 12: 12 mA corresponds with 0 m/s2. For amplification factors of 4,
        # the z-component will give 8 mA in rest.that is way Richard subtract 8.
        # Here 12 offset 12 is assumed. The -0.62 is based on the assumption that the mean z value
        # can not exceed 9.81

        self._data_dict["signal_accelerometers"]["coefficients"]["A01"] = (1, 12)
        self._data_dict["signal_accelerometers"]["coefficients"]["A02"] = (1, 12)
        self._data_dict["signal_accelerometers"]["coefficients"]["A03"] = (1, 12)
        self._data_dict["signal_accelerometers"]["coefficients"]["A04"] = [2.45, 0, 12]
        self._data_dict["signal_accelerometers"]["coefficients"]["A05"] = [2.45, 0, 12]
        self._data_dict["signal_accelerometers"]["coefficients"]["A06"] = \
            [-2.45, -0.62, 12]  # offset 12 is 8 in report Richard
        self._data_dict["signal_accelerometers"]["coefficients"]["A07"] = [2.45, 0, 12]
        self._data_dict["signal_accelerometers"]["coefficients"]["A09"] = (1, 12)
        self._data_dict["signal_accelerometers"]["coefficients"]["A10"] = (1, 12)
        self._data_dict["signal_accelerometers"]["coefficients"]["A11"] = (1, 12)
        self._data_dict["signal_accelerometers"]["coefficients"]["A12"] = \
            [2.47, 0, 12]  # 9.81/3.97
        self._data_dict["signal_accelerometers"]["coefficients"]["A13"] = \
            [-2.45, 0, 12]  # 9.81/4.01
        self._data_dict["signal_accelerometers"]["coefficients"]["A14"] = \
            [-2.46, -0.71, 12]  # 8 in report  of Richard
        self._data_dict["signal_accelerometers"]["coefficients"]["A15"] = (1, 12)
        self._data_dict["signal_accelerometers"]["coefficients"]["A16"] = (1, 12)
        self._data_dict["signal_accelerometers"]["coefficients"]["A17"] = (1, 12)

        self._data_dict["signal_strain_gauges"]["coefficients"]["SG01"] = [122.0, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG02"] = [121.7, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG03"] = [121.0, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG04"] = [121.2, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG05"] = [120.47, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG06"] = [120.51, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG07"] = [120.50, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG08"] = [120.25, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG09"] = [119.51, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG10"] = [120.22, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG11"] = [120.45, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG12"] = [120.35, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG13"] = [120.30, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG14"] = [120.50, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG15"] = [120.35, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG16"] = [120.5, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG17"] = [120.6, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG18"] = [120.7, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG19"] = [120.8, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG20"] = [120.6, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG21"] = [120.9, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG22"] = [120.9, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG23"] = [120.9, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG24"] = [120.4, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG25"] = [121.1, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG26"] = [121.0, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG27"] = [121.0, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG28"] = [121.0, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG29"] = [121.0, 0, 12]

        # these values are new in the new set up after 2016.
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG30"] = [120.63, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG31"] = [121.19, 0, 12]
        self._data_dict["signal_strain_gauges"]["coefficients"]["SG32"] = [121.19, 0, 12]

        # multiply the first coefficient with 1e-6 in order to get the unit micro-epsilon/mA. Note
        # that mA is used in the mdf file. The minus is applied here as well
        for key, value in self._data_dict["signal_strain_gauges"]["coefficients"].items():
            self._data_dict["signal_strain_gauges"]["coefficients"][key][0] *= -1e-6

        self.accelerometer_calibration_to_coeffients()

        # 0: plot acceleration, 1: plot real signal
        self._data_dict["signal_strain_gauges"]["plot_properties"]["type"] = 0
        self._data_dict["signal_strain_gauges"]["plot_properties"]["plot_this"] = False
        self._data_dict["signal_strain_gauges"]["plot_properties"]["force_autoscale"] = True
        self._data_dict["signal_strain_gauges"]["plot_limits"]["SG01"] = [(-0.4, 0.4),
                                                                          (-1.0, 1.0)]

        for i in range(1, 30):
            sg = "SG{:02d}".format(i)
            self._data_dict["signal_strain_gauges"]["plot_limits"][sg] = \
                [(None, None), (None, None), (None, None)]

    def replace_alias_name_with_hotspot_name(self, hot_spot_name_in_data_base):
        """
        Replace the name as found in the excel file with the name as defined in the mdf file

        Parameters
        ----------
        hot_spot_name_in_data_base : str
            Name in the data base

        Returns
        -------
        str:
            Alias name

        Notes
        -----
        Use the list of aliases per hot spot location for this

        """

        # in case no alias is found, the original name must be returned
        name_and_label = hot_spot_name_in_data_base.split()
        alias = name_and_label[0]
        self.log.debug(
            "replace_alias_name_with hot_spot_name {} ({})".format(hot_spot_name_in_data_base,
                                                                   alias))

        # loop over the the hot spot location as defined in the mdf file
        for location in list(self._data_dict["locations"].keys()):

            try:
                aliases = self._data_dict["locations"][location]["aliases"]
            except KeyError:
                continue

            self.log.debug("Get aliases at location {} : {}".format(location, aliases))
            # aliases is the list of alternative names as occur in the excel data bases

            # check if the name we have retrieved from the excel file is in the list of aliases of
            # this location
            if hot_spot_name_in_data_base in aliases:
                # ok, this name is an alias, so rename the name to the location name
                alias = location
                break

        # return with the new name if an alias was found
        self.log.debug("Allrighty, now the alias name is {}".format(alias))
        return str(alias)

    def set_transformation_coefficients_at_location(self, transformation_coefficients, name=""):
        """Store the transformation matrix into the _data_dict.

        Parameters
        ----------
        transformation_coefficients : list
            Coefficients

        name : str
            (Default value = "")

        Notes
        ------
        First the sacs excel data base needs to be read. Then, loop over the location in the sacs
        data base and store the Fx/AX, Fx/AY, etc values
        """
        log = get_logger(__name__)

        factor = self._data_dict["units"]["response_matrix"]

        # loop over rows of the excel sheet belonging to the current tower angle and tower model.
        for index, row in transformation_coefficients.items():
            # row contains the row in the excel data with all the columns. First we extract the name
            # of the l location stored in the 'hotspot name' column and see if we need to replace
            # when it is an alias of the
            # location name in the mdf file
            hot_spot_name = self.replace_alias_name_with_hotspot_name(row["hotspot name"])

            # nom loop over all the locations we have defined in the settings dict. These names
            # correspond to the names as found in the MDF file
            log.debug("{} hotspot in database: {} was {}"
                      "".format(index, hot_spot_name, row["hotspot name"]))
            for location in list(self._data_dict["locations"].keys()):
                # test if the current hotspot name matches the name in the mdf file
                if hot_spot_name == location:
                    # we have found a location mathcing this row. Store the transformation data as
                    # obtained in this row. The row contain Fx/AX, Fy/AX, etc, but we only need
                    # FX/AX..FX/RXX, MY/AX..MY/RXX, and MZ/etc
                    log.debug("Adding {} {}".format(hot_spot_name, location))
                    response_fx = list()
                    response_my = list()
                    response_mz = list()
                    for key, value in row.items():
                        # check the column name to see if it contain either Fx, My, or Mz.
                        # If so: store the values
                        if re.search("Fx", key):
                            log.debug("Storing Fx {} {}".format(key, value))
                            response_fx.append(value * factor)
                        elif re.search("My", key):
                            log.debug("Storing My {} {}".format(key, value))
                            response_my.append(value * factor)
                        elif re.search("Mz", key):
                            log.debug("Storing Mz {} {}".format(key, value))
                            response_mz.append(value * factor)

                    # combine the Fx, My, and Mz into on 2D list and store it in the settings
                    # dictionary
                    response = list((response_fx, response_my, response_mz))
                    log.debug("adding response to response_matrix\n{}".format(response))
                    self._data_dict["locations"][location]["response_matrix" + name] = response

    def get_fatigue_data_base_file_name(self):
        """ """
        log = get_logger(__name__)
        fatigue_data_base_location = self._data_dict["file"]["fatigue_data_base_location"]
        if fatigue_data_base_location is None:
            fatigue_data_base_location = "."
        fatigue_data_base_name = self._data_dict["file"]["fatigue_data_base_name"]
        if fatigue_data_base_name is None:
            fatigue_data_base_name = "fatigue_database.xls"

        fat_base, ext = os.path.splitext(fatigue_data_base_name)
        if ext == "":
            fatigue_data_base_name += ".xls"

        log.debug("fatigue db names {} {}"
                  "".format(fatigue_data_base_location, fatigue_data_base_name))
        fatigue_data_base_file = os.path.join(fatigue_data_base_location,
                                              fatigue_data_base_name)

        if not os.path.isabs(fatigue_data_base_file):
            # the excel file location is relative to the base directory of the configuration file,
            # except when a absolute path was given
            fatigue_data_base_file = os.path.join(self.execution_directory,
                                                  fatigue_data_base_file)

        # clear the path, ie change all bla/./blabla and bla//blabla into bla/blabla
        fatigue_data_base_file = clear_path(fatigue_data_base_file)

        return fatigue_data_base_file

    def get_fatigue_rao_vessel_output_database(self):
        """ """

        fatigue_data_base_outdir = self._data_dict["rao_vessel"]["output_directory"]
        if self._data_dict["rao_vessel"]["output_filebase"] is not None:
            fatigue_data_base_outfile = self._data_dict["rao_vessel"]["output_filebase"]
        else:
            fatigue_data_base_outfile = os.path.split(fatigue_data_base_outdir)[1]

        if self.safetrans_msg_pack_trajectory_database is not None:
            msg_pack_dir, msg_pack_file = os.path.split(self.safetrans_msg_pack_trajectory_database)
        else:
            msg_pack_dir = msg_pack_file = None

        if fatigue_data_base_outdir is None:
            if msg_pack_dir is not None:
                fatigue_data_base_outdir = msg_pack_dir + "_rao_vessel"
            else:
                fatigue_data_base_outdir = "default_rao_vessel"

        if fatigue_data_base_outfile is None:
            if msg_pack_file is not None:
                fatigue_data_base_outfile = os.path.splitext(msg_pack_file)[0]
            else:
                fatigue_data_base_outfile = "default"

        fatigue_data_base_file = os.path.join(fatigue_data_base_outdir,
                                              fatigue_data_base_outfile)
        fatigue_data_base_file = os.path.splitext(fatigue_data_base_file)[0]

        if not os.path.isabs(fatigue_data_base_file):
            # the excel file location is relative to the base directory of the configuration file,
            # except when a absolute path was given
            fatigue_data_base_file = os.path.join(self.execution_directory,
                                                  fatigue_data_base_file)

        # clear the path, ie change all bla/./blabla and bla//blabla into bla/blabla
        fatigue_data_base_file = clear_path(fatigue_data_base_file)

        return fatigue_data_base_file

    def get_safetrans_msg_data_base_file_name(self):
        return self.safetrans_msg_pack_trajectory_database

    def set_safetrans_msg_data_base_file_name(self):
        """ """
        safetrans_data_base_location = self._data_dict["rao_vessel"]["data_base_location"]
        safetrans_trajectory_file = self._data_dict["rao_vessel"][
            "msg_pack_trajectory_database"]

        if safetrans_trajectory_file is None:
            # it the message pack file is not given, base it on the first csv file
            safetrans_trajectory_file = \
                os.path.splitext(self._data_dict["rao_vessel"]["csv_trajectory_files"][0])[0] \
                + ".msg_pack"

        self.safetrans_msg_pack_trajectory_database = os.path.join(safetrans_data_base_location,
                                                                   safetrans_trajectory_file)

        if not os.path.isabs(self.safetrans_msg_pack_trajectory_database):
            # the excel file location is relative to the base directory of the configuration file,
            # except when a absolute path was given
            self.safetrans_msg_pack_trajectory_database = \
                os.path.join(self.execution_directory, self.safetrans_msg_pack_trajectory_database)

        # clear the path, ie change all bla/./blabla and bla//blabla into bla/blabla
        self.safetrans_msg_pack_trajectory_database = \
            clear_path(self.safetrans_msg_pack_trajectory_database)

    def get_rao_data_file_name(self):
        """ """
        rao_data_base_location = self._data_dict["rao_vessel"]["rao_data_location"]
        rao_vessel_rao_data_filename = self._data_dict["rao_vessel"]["rao_data_file_name"]

        rao_vessel_rao_data_filename = os.path.join(rao_data_base_location,
                                                    rao_vessel_rao_data_filename)

        if not os.path.isabs(rao_vessel_rao_data_filename):
            # the excel file location is relative to the base directory of the configuration file,
            # except when a absolute path was given
            rao_vessel_rao_data_filename = os.path.join(self.execution_directory,
                                                        rao_vessel_rao_data_filename)

        # clear the path, ie change all bla/./blabla and bla//blabla into bla/blabla
        rao_vessel_rao_data_filename = clear_path(rao_vessel_rao_data_filename)

        return rao_vessel_rao_data_filename

    def set_trajectory_file_name(self, matlab_trajectory_file):
        """
        Set the name for the trajectory

        Parameters
        ----------
        matlab_trajectory_file : str
            Name of the matlab trajectory

        """
        log = get_logger(__name__)

        file_base, file_ext = os.path.splitext(matlab_trajectory_file)
        matlab_location, matlab_file = os.path.split(matlab_trajectory_file)
        # self._data_dict["rao_vessel"]["output_directory"] = file_base + "_rao_vessel"
        # self.rao_vessel["output_directory"] = self._data_dict["rao_vessel"]["output_directory"]
        self._data_dict["rao_vessel"]["data_base_location"] = matlab_location

        # TODO: again a bit tricky, replace csv_trajectory file with matlab data base because later
        # we get the message pack filename from the first in the list of the matlab file.
        # Change later, but works for now
        self._data_dict["rao_vessel"]["csv_trajectory_files"] = [file_base + ".csv"]
        self.rao_vessel["csv_trajectory_files"] = [file_base + ".csv"]

    def set_csv_trajectory_files(self, csv_file_list):
        """Set the CSV trajectories

        Parameters
        ----------
        csv_file_list : list
            List with files

        """

        log = get_logger(__name__)
        first_csv_file = csv_file_list[0]
        first_csv_file_base = os.path.splitext(first_csv_file)[0]
        path_to_first_csv_file = os.path.split(first_csv_file)[0]

        if self._data_dict["rao_vessel"]["output_directory_extension"] is not None:
            dir_ext = self._data_dict["rao_vessel"]["output_directory_extension"]
        else:
            dir_ext = os.path.splitext(self.path.split(self.configuration_file)[1])[0]

        self._data_dict["rao_vessel"]["output_directory"] = \
            "_".join([first_csv_file_base, dir_ext])
        self.rao_vessel["output_directory"] = self._data_dict["rao_vessel"]["output_directory"]
        self._data_dict["rao_vessel"]["data_base_location"] = path_to_first_csv_file
        self.rao_vessel["data_base_location"] = self._data_dict["rao_vessel"][
            "data_base_location"]
        self._data_dict["rao_vessel"]["csv_trajectory_files"] = []
        self.rao_vessel["csv_trajectory_files"] = []
        for csv_file in csv_file_list:
            log.debug("adding file {}".format(csv_file))
            file_base, file_ext = os.path.splitext(os.path.split(csv_file)[1])
            if file_ext == ".csv":
                log.debug("adding CSV file {} to {}"
                          "".format(csv_file,
                                    self._data_dict["rao_vessel"]["csv_trajectory_files"]))
                self._data_dict["rao_vessel"]["csv_trajectory_files"].append(
                    file_base + file_ext)
                log.debug("and also to {}".format(self.rao_vessel["csv_trajectory_files"]))
                self.rao_vessel["csv_trajectory_files"].append(file_base + file_ext)
                log.debug("and also to {}".format(self.rao_vessel["csv_trajectory_files"]))
            elif file_ext in (".msg_pack", ".txt", ".xls"):
                # bit tricky. If a msg_pack was given on the command line, just pretend it was a
                # csv file, because when reading the csv file it is checked if a msg_pack is already
                # present anyway or a txt file with a weather report
                # TODO: improve the whole file setting reading
                log.debug("adding MSG_PACK_FILE file {}".format(csv_file))
                self._data_dict["rao_vessel"]["csv_trajectory_files"].append(file_base + ".csv")
                self.rao_vessel["csv_trajectory_files"].append(file_base + ".csv")
        log.debug("returning with {} {}"
                  "".format(self._data_dict["rao_vessel"]["csv_trajectory_files"],
                            self.rao_vessel["csv_trajectory_files"]))

    def get_filter_properties(self, device_name="accelerometers"):
        """
    Get the filter properties of the filter settings for *device_name*

    Parameters
    ----------
    device_name: {"accelerometer", "mru", "strain_gauges"}
        Name of the device to get the properties from. Default = "accelerometers"

    Returns
    -------
    tuple:
        (filter_type, f_cut_low, f_cut_high, f_width_edge, order, cval)
    """

        filter_properties = self._data_dict["filtering"][device_name]

        try:
            filter_type = filter_properties["type"]
        except KeyError:
            filter_type = None
        try:
            f_cut_low = set_default_dimension(filter_properties["f_cut_off_low"], "Hz",
                                              force_default_units=True)
        except KeyError:
            f_cut_low = None
        try:
            f_cut_high = set_default_dimension(filter_properties["f_cut_off_high"], "Hz",
                                               force_default_units=True)
        except KeyError:
            f_cut_high = None
        try:
            f_width_edge = set_default_dimension(filter_properties["f_width_edge"], "Hz",
                                                 force_default_units=True)
        except KeyError:
            f_width_edge = None
        try:
            order = filter_properties["order"]
        except KeyError:
            order = None
        try:
            cval = filter_properties["cval"]
        except KeyError:
            cval = None
        return filter_type, f_cut_low, f_cut_high, f_width_edge, order, cval

    def get_rao_vessel_data_base_file_names(self):
        """Get the list of file names pointing to the safe trans file.

        Returns
        -------
        list:
            File list of safe trans files

        Notes
        -----
        Multiple files can be given as they can be combined
        """

        rao_vessel_data_base_location = self._data_dict["rao_vessel"]["data_base_location"]
        rao_vessel_trajectory_files = self._data_dict["rao_vessel"]["csv_trajectory_files"]

        for cnt, file_name in enumerate(rao_vessel_trajectory_files):
            file_name = os.path.join(rao_vessel_data_base_location, file_name)

            if not os.path.isabs(file_name):
                # the excel file location is relative to the base directory of the configuration
                # file,
                # except when a absolute path was given
                file_name = os.path.join(self.execution_directory, file_name)

            # clear the path, ie change all bla/./blabla and bla//blabla into bla/blabla
            rao_vessel_trajectory_files[cnt] = clear_path(file_name)

        return rao_vessel_trajectory_files

    def import_sacs_response_data_base(self):
        """
        Import the sacs data model en store the values in the settings

        Notes
        -----
        Store the sacs data in the settings object. This will be saved to the config file later,
        so you do not have to read the xls file every time. In case no xls can be read, take the
        values from the configuration file

        """
        sacs_location = self._data_dict["file"]["sacs_data_base_location"]
        sacs_filebase = self._data_dict["file"]["sacs_response_data_file"]

        from_excel = False
        if not isinstance(sacs_filebase, list):
            extension = os.path.splitext(sacs_filebase)[1]
            if re.search("xls", extension):
                from_excel = True

        if from_excel:
            self.import_sacs_response_data_base_from_excel(sacs_location=sacs_location,
                                                           sacs_filebase=sacs_filebase)
        else:
            self.import_sacs_response_data_base_from_sacs_output(sacs_location=sacs_location,
                                                                 sacs_filebase_list=sacs_filebase)

    def import_sacs_response_data_base_from_sacs_output(self, sacs_location, sacs_filebase_list):
        """
        Read the sacs coefficients from the original sacs output files

        Parameters
        ----------
        sacs_location: str
            Location of the sacs coefficients data base
        sacs_filebase_list: list or str
            Names of the sacs coefficients data base. If not given as a list but as a string, turn
            it into a list
        """
        self.log.info("Reading from the original output files {} {}".format(sacs_location,
                                                                            sacs_filebase_list))
        if not isinstance(sacs_filebase_list, list):
            # if the sacs data base were not given as a list, make sure it is a list anyhow
            sacs_filebase_list = [sacs_filebase_list]

        for sacs_file_base in sacs_filebase_list:
            sacs_file = os.path.join(sacs_location, sacs_file_base)

            file_base = os.path.splitext(sacs_file)[0]
            match = re.search("(\d+)$", file_base)
            if bool(match):
                tower_angle = int(match.group(1))
            else:
                tower_angle = 99

            self.set_transformation_coefficients_at_location_from_sacs(sacs_file, tower_angle)

    def set_transformation_coefficients_at_location_from_sacs(self, sacs_file, tower_angle):
        """
        Set the sacs coefficients from the original sacs file using the
        *SACSForceMomentVs6DOFReader* of the hmc_marine module

        Parameters
        ----------
        sacs_file: str
            Sacs file name
        tower_angle: int
            Tower angle
        """

        # read the data file
        sacs_obj = dr.SACSForceMomentVs6DOFReader(sacs_file)

        # get the dictionary containing all the elements
        elements = sacs_obj.elements

        # loop over rows of the excel sheet belonging to the current tower angle and tower model.
        for sacs_element_name, element in elements.items():
            # row contains the row in the excel data with all the columns. First we extract the name
            # of the l location stored in the 'hotspot name' column and see if we need to replace
            # when it is an alias of the
            # location name in the mdf file
            hot_spot_name = self.replace_alias_name_with_hotspot_name(sacs_element_name)

            # nom loop over all the locations we have defined in the settings dict. These names
            # correspond to the names as found in the MDF file
            for location in list(self._data_dict["locations"].keys()):
                # test if the current hotspot name matches the name in the mdf file
                if hot_spot_name == location:
                    # we have found a location matching this element.
                    response_fx = list()
                    response_my = list()
                    response_mz = list()

                    for load_cond, force_vector in element.forces.items():
                        # sacs gives the force in kN. We want to store the force per g-force unit
                        # in N / (m/s**2)
                        fx_per_g = force_vector[0].to("N") / Q_(g0, "m/s^2")
                        response_fx.append(float(fx_per_g.magnitude))

                    for load_cond, moment_vector in element.moments.items():
                        # sacs gives the force in kN. We want to store the force per g-force unit
                        # in N / (m/s**2)
                        my_per_g = moment_vector[1].to("N*m") / Q_(g0, "m/s^2")
                        mz_per_g = moment_vector[2].to("N*m") / Q_(g0, "m/s^2")
                        response_my.append(float(my_per_g.magnitude))
                        response_mz.append(float(mz_per_g.magnitude))

                    # combine the Fx, My, and Mz into on 2D list and store it in the settings
                    # dictionary
                    response = list((response_fx, response_my, response_mz))
                    self.log.debug("adding response to response_matrix\n{}".format(response))
                    response_name = "_".join(["response_matrix", "{}".format(int(tower_angle))])
                    self._data_dict["locations"][location][response_name] = response

    def import_sacs_response_data_base_from_excel(self, sacs_location, sacs_filebase):
        """
        Import the sacs coefficients from the original sacs data base excel file:

        Parameters
        ----------
        sacs_location: str
            Location of the sacs coefficients data base
        sacs_filebase: str
            Name of the sacs coefficients data base. Must a string
        """

        sacs_file = os.path.join(sacs_location, sacs_filebase)

        if not os.path.isabs(sacs_location):
            # the excel file location is relative to the base directory of the configuration file,
            # except when a absolute path was given
            sacs_file = os.path.join(self.execution_directory, sacs_file)

        # clear the path, ie change all bla/./blabla and bla//blabla into bla/blabla
        sacs_file = clear_path(sacs_file)

        sacs_tower_model = self._data_dict["values"]["sacs_tower_model"]

        self.log.info(
            "Reading the sacs data file {} with towermodel {}".format(sacs_file,
                                                                      sacs_tower_model))
        try:
            # read the excel file and store it in a data frame
            df_sacs = pd.read_excel(sacs_file, skiprows=4)
        except IOError as err:
            self.log.warning(err)
            if not self.store_default_settings_to_file:
                self.log.warning(
                    "Please set the import_sacs_data_base_flag to false or provide a proper data "
                    "base excel file. Bye!")
                raise
            else:
                df_sacs = None

        if df_sacs is not None:
            # select the appropriate tower model by filtering the rows
            df_sacs = df_sacs[df_sacs["Tower Model"] == sacs_tower_model]

            # select the straight position values and the transit values
            df_sacs_90 = df_sacs[df_sacs["tower angle [deg]"] > 80]
            df_sacs_75 = df_sacs[df_sacs["tower angle [deg]"] < 80]

            # store the transformation coefficients in the settings
            self.set_transformation_coefficients_at_location(df_sacs_90, "_90")
            self.set_transformation_coefficients_at_location(df_sacs_75, "_75")

    def set_s_n_coefficients_at_location(self, sacs_dataframe):
        """Store the S-N coefficient and geometrical properties as read into the sacs_dataframe in
    to the settings object

    Parameters
    ----------
    sacs_dataframe : DataFrame
        Values of sacs data base

    """
        log = get_logger(__name__)

        factor = self._data_dict["units"]["geometric_data"]

        # loop over rows of the excel sheet belonging to the current tower angle and tower model.
        for index, row in sacs_dataframe.items():
            # row contains the row in the excel data with all the columns. First we extract the name
            # of the l location stored in the 'hotspot name' column and see if we need to replace
            # when it is an alias of the # location name in the mdf file
            hot_spot_name = self.replace_alias_name_with_hotspot_name(row["hotspot name"])

            # loop over all the locations we have defined in the settings dict. These names
            # correspond to the names as found in the MDF file, but may differ in the SACS data
            # bases. To solve this, aliases are defined which relate the names in the mdf file to
            # the names in the SACS file. Each mdf file location may have multiple aliases refering
            # tot the data bases
            self.log.debug("replaced hotspot {} in database with {}"
                           "".format(row["hotspot name"], hot_spot_name))
            for location in list(self._data_dict["locations"].keys()):
                # test if the current hotspot name matches the name in the mdf file
                self.log.debug("comparing hot_spot_name and location {} {}"
                               "".format(hot_spot_name, location))
                if hot_spot_name == location:
                    # we have found a location matching this row. Store the S-N data belonging
                    # to this location
                    self.log.debug("Adding {} {}".format(hot_spot_name, location))

                    scf_coefficients = dict()
                    geometric_properties = dict()
                    s_n_coefficients = dict()

                    for key, value in row.items():
                        self.log.debug("Col name / value {} {}".format(key, value))
                        if re.search("SCFAX", key):
                            log.debug("Storing SCF Ax {} {}".format(key, value))
                            scf_coefficients["X"] = value
                        elif re.search("SCFBEND_inplane", key):
                            log.debug("Storing SCF inplane {} {}".format(key, value))
                            scf_coefficients["inplane"] = value
                        elif re.search("SCFBEND_outplane", key):
                            log.debug("Storing SCF outplane {} {}".format(key, value))
                            scf_coefficients["outplane"] = value
                        elif re.search("Abrace", key):
                            log.debug(
                                "Storing geometric property Abrace {} {}".format(key, value))
                            geometric_properties["area"] = value * factor ** 2
                        elif re.search("Rbrace", key):
                            log.debug("Storing geometric property Rbrace into dy/dz {} {}"
                                      "".format(key, value))
                            geometric_properties["radius"] = value * factor
                        elif re.search("Iyy", key):
                            log.debug("Storing geometric property Iyy {} {}".format(key, value))
                            geometric_properties["I_moment_yy"] = value * factor ** 4
                        elif re.search("Izz", key):
                            log.debug("Storing geometric property Izz {} {}".format(key, value))
                            geometric_properties["I_moment_zz"] = value * factor ** 4
                        elif re.search("tbrace", key):
                            log.debug(
                                "Storing geometric property tbrace {} {}".format(key, value))
                            geometric_properties["thickness"] = value * factor
                        elif re.search("S-N: log\(K2\)", key):
                            log.debug(
                                "Storing S-N coefficient log(K2) {} {}".format(key, value))
                            s_n_coefficients["logK2"] = value
                        elif re.search("S-N: m", key):
                            log.debug("Storing S-N coefficient m0 {} {}".format(key, value))
                            s_n_coefficients["m0"] = value * 1.0
                        elif re.search("S-N: tB", key):
                            log.debug("Storing S-N coefficient tB {} {}".format(key, value))
                            s_n_coefficients["thickness_b"] = value * factor

                    log.debug("scf {}".format(scf_coefficients))
                    log.debug("geo {}".format(geometric_properties))
                    log.debug("S-N {}".format(s_n_coefficients))
                    force_x = self._data_dict["locations"][location]["force_x_factor"]
                    if force_x != 1:
                        # in case the external data base imported we need to divide the SCF with the
                        # force_x factor, as the cold spot stress is multiplied with force_x to get
                        # the 'warm spot' stress, and then only a scf of scf/force_x need to be
                        # applied to go to the hot spot stress
                        log.warning("Updating stress_concentration_factor at {} with {} to {}"
                                    "".format(location, s_n_coefficients, force_x))

                        # only the SCF_x is corrected, as the force_x_factor is only not equal to 1
                        # for the pivots, where the moments are zero. Note also that this correction
                        # should only be used with the old SCF excel input files where the pivot SCF
                        # is set to 14.5. In the future, for new input files, update the SCF to a
                        # decent value
                        self._data_dict["locations"][location]["stress_concentration_factors"][
                            "X"] = \
                            scf_coefficients["X"] / force_x
                    self._data_dict["locations"][location][
                        "cross_sectional_properties"] = geometric_properties
                    self._data_dict["locations"][location][
                        "s_n_coefficients"] = s_n_coefficients

                    # found a match so stop the loop
                    break

    def import_sacs_sn_data_base(self):
        """Import the sacs data model S_N data store the values in the settings

        Notes
        -----
        Store the sacs data in the settings object. This will be saved to the config file later,
        so you do not have to read the xls file every time. In case no xls can be read, take the
        values from the configuration file

        """
        log = get_logger(__name__)
        sacs_location = self._data_dict["file"]["sacs_data_base_location"]
        sacs_filebase = self._data_dict["file"]["sacs_S_N_data_file"]
        sacs_sheetname = self._data_dict["file"]["sacs_S_N_data_sheet"]

        sacs_file = os.path.join(sacs_location, sacs_filebase)

        if not os.path.isabs(sacs_location):
            # the excel file location is relative to the base directory of the configuration file,
            # except when a absolute path was given
            sacs_file = os.path.join(self.execution_directory, sacs_file)

        # clear the path, ie change all bla/./blabla and bla//blabla into bla/blabla
        sacs_file = clear_path(sacs_file)

        log.info("Reading the sacs data file {} with S_N curve data ".format(sacs_file))
        try:
            # read the excel file and store it in a data frame
            df_sacs = pd.read_excel(sacs_file, sheetname=sacs_sheetname)
        except IOError as err:
            log.warning(err)
            if not self.store_default_settings_to_file:
                log.warning(
                    "Please set the import_sacs_data_base_flag to false or provide a proper data "
                    "base excel file. Bye!")
                raise
            else:
                df_sacs = None

        if df_sacs is not None:
            self.set_s_n_coefficients_at_location(df_sacs)

    def calculate_s_n_curves(self):
        """Calculate the S_N curve based on the coefficient we have already read/imported"""

        # create empty Nx2 data array for each location to carry the SN curve data
        for location, coordinates in self._data_dict["locations"].items():

            # get the sn coefficients
            try:
                # get the s/n curve coefficients of the current location
                s_n_coefficients = self._data_dict["locations"][location]["s_n_coefficients"]
            except (KeyError, TypeError):
                self.log.debug(
                    "Location {} does not have a s/n curve coefficients. Skipping".format(
                        location))
                continue

            self.log.debug("Calculating SN curves location {}".format(location))
            self.log.debug("SN coefficients location{} : {}".format(location, s_n_coefficients))

            # get the cross sectional properties at this location
            try:
                cross_sectional_properties = self._data_dict["locations"][location][
                    "cross_sectional_properties"]
            except (KeyError, TypeError):
                self.log.debug(
                    "Location {} does not have cross sectional properties. Skipping".format(
                        location))
                continue

            self.log.debug("SN curve for location {}".format(location))

            # create data array
            fatigue_settings = self._data_dict["fatigue_settings"]
            n_data = int(fatigue_settings["n_stress_bins"])
            S_min = set_default_dimension(fatigue_settings["stress_min"], "MPa").to("MPa").magnitude
            S_max = set_default_dimension(fatigue_settings["stress_max"], "MPa").to("MPa").magnitude
            m0 = s_n_coefficients["m0"]
            logK2 = s_n_coefficients["logK2"]
            tB = set_default_dimension(s_n_coefficients["thickness_b"], "m")
            tt = set_default_dimension(Q_(cross_sectional_properties["thickness"]), "m")
            tt = max(tt, Q_(22, "mm"))

            # the correction coefficient to take into account the plate thickness
            tcorrection = pow(tB / tt, 0.25)

            # copy of matlab code
            # multiply range with tcorrection to get S/N curve in given range
            CorBins = linspace(S_min, S_max, n_data) / tcorrection
            LogCorBin = log10(CorBins)
            m5 = LogCorBin < (logK2 - 7.0) / m0
            s5 = (logK2 - 7.0) / m0
            K25 = 7.0 + (m0 + 2.0) * s5
            n_cycles_matlab = power(10.0, logK2 - m0 * LogCorBin)
            n_cycles_matlab[m5] = power(10.0, K25 - (m0 + 2.0) * LogCorBin[m5])

            # calculate the equivalent logK2 and m if we do not take into account the m0 and m0+2
            # difference but just assume logN = logK2_equi - m_equi*log(S) with m being a constandt
            # from that we can derive the parameters required for the WAFO fatigue model, which is
            # simply N(s) = K^(-1) s^(-beta)
            n_start = int(n_data / 10)
            A = vstack([log10(CorBins[n_start:]), ones(n_data - n_start)]).T
            m_equi, logK2_equi = lstsq(A, log10(n_cycles_matlab[n_start:]))[0]
            beta_wafo = -m_equi
            K_wafo = power(10, -logK2_equi) / power(1e6, beta_wafo)
            self.log.debug(
                "calculated equivalent m and logK2 {} {} (orig {})".format(m_equi, logK2_equi,
                                                                           logK2))
            self.log.debug(
                "corresponding wafo coeffients K and beta are {} {}".format(K_wafo, beta_wafo))

            # wafo assumes stress in Pa, whereas Richard in MPa
            n_cycles_wafo = power(CorBins * 1e6, -beta_wafo) / K_wafo

            # obsolete option in the code. Not using it anymore
            # fatigue_settings["K_wafo"] = K_wafo
            # fatigue_settings["beta_wafo"] = beta_wafo

            # store the data, take the correction on S into account
            self.sn_curve_data[location] = vstack((CorBins, n_cycles_matlab, n_cycles_wafo))

    def set_acc_component_fit_weight(self):
        """
        Set the acceleration components fit weight
        """
        # for each location the fit weight. Default is 0, so do not use to fit at all
        for location, coordinates in self._data_dict["locations"].items():
            self._data_dict["locations"][location]["fit_weight"] = dict(X=0, Y=0, Z=0)

    def init_response_matrix(self, extension=""):
        """ Init the response matrix

        Parameters
        ----------
        extension : str
            Extension string to add to the response matrix. Default value = ""
        """

        # for each location if it a location in the tower
        for location, coordinates in self._data_dict["locations"].items():
            self._data_dict["locations"][location]["response_matrix" + extension] = None

    def set_hotspot_plot_limits(self):
        """ """
        # for each location you can set the plot limits for the hot spot stress plots
        for location, coordinates in self._data_dict["locations"].items():
            self._data_dict["signal_hotspots"]["plot_properties"]["plot_limits"][location] = [
                (-50, 50)]

    def set_force_x_factor(self):
        """Set the force x factor"""

        # for each location you may want to multiply the force x with -1 to turn the phase
        for location, coordinates in self._data_dict["locations"].items():
            # if bool(re.search("Q02", location)):
            #    # the Q02 (TSM brace) has an opposite sign
            #    self._data_dict["locations"][location]["force_x_factor"] = -1
            if bool(re.search("PIV", location)):
                # for the pivot we define 3 factors
                # only second channel is used with amp facto 3.9. But to get the cold stresses of
                # the other channels right I remain the other -0.82 and 1.54 too
                self._data_dict["locations"][location][
                    "force_x_factor"] = 3  # [-0.82, 3.9, 1.54]
            else:
                self._data_dict["locations"][location]["force_x_factor"] = 1

    def set_is_square_tube_flag(self, in_tower=False):
        """ Set a flag if wew are on a square tube

    Parameters
    ----------
    in_tower : bool
        (Default value = False)


    """
        # for each location if it a location in the tower
        for location, coordinates in self._data_dict["locations"].items():
            self.log.debug("adding to locations: log={} coor={}".format(location, coordinates))
            self._data_dict["locations"][location]["is_square_tube"] = False

    def set_in_tower_flag(self, in_tower=False):
        """Set a flag if we are in the tower

        Parameters
        ----------
        in_tower : bool
            If tru set flag. Default value = False

        """
        # for each location if it a location in the tower
        for location, coordinates in self._data_dict["locations"].items():
            self.log.debug("adding to locations: log={} coor={}".format(location, coordinates))
            self._data_dict["locations"][location]["in_tower"] = in_tower

    def set_plot_this_locations(self, plot_this=True):
        """Set a flag if we want to plot this location

        Parameters
        ----------
        plot_this : bool
            If True, Plot this location. Default value = True

        """

        # for each location add a dictionary with the fields to plot
        for location, coordinates in self._data_dict["locations"].items():
            self._data_dict["locations"][location]["plot_this"] = plot_this

    def accelerometer_calibration_to_coeffients(self):
        """calculate the least square coefficients belonging to the calibration and store the
        calibration measurements in a new dict 'coefficients'.

        Notes
        -----
        * All the measurements which are not available are replaced with the mean value of all
          the other coefficients
        * We are reading the ampere at three values, one for each position of the accelerometer
          facing down (-9.81 m/s2) horizontal (0.0 m/s2) and facing up (+9.81 m/s2)
        * The values are fitted in order to obtain the coefficient a, b which are used to convert
          a ampere signal into a acceleration
        """

        cof_format = "{:6.4f}"

        acceleration_at_m_a_values = array([-g0, 0, g0])
        a_list = list()
        b_list = list()
        for key, mA_values in self._data_dict["signal_accelerometers"]["calibration"].items():
            if mA_values is not None:
                # apply a least square fit. Use the lambda construct in order to force the fit with
                # only 2 parameters # as the third parameter (the offset) is not fit. See
                # http://stackoverflow.com/questions/12208634/fitting-only-one-paramter-of-a-function-with-many-parameters-in-python

                sign = 1
                a_offset = 12
                try:
                    cof = self._data_dict["signal_accelerometers"]["coefficients"][key]
                except KeyError:
                    pass
                else:
                    if isinstance(cof, tuple):
                        sign = cof[0]
                        a_offset = cof[1]

                (aa, bb), cov = optimization.curve_fit(
                    lambda x, a, b: convert_ampere(x, a, b, ampere_offset=a_offset),
                    mA_values, acceleration_at_m_a_values)

                # multiply the amplification with the sign to set the direction of the accelerometer
                aa *= sign

                # only calculate the new coefficients if the stored coefficients are not yet given
                # in a list
                if not isinstance(cof, list):
                    # store the parameters in the accelerometers coefficient dic for location key.
                    # Type format the flow
                    self._data_dict["signal_accelerometers"]["coefficients"][key] = \
                        list([float(cof_format.format(aa)), float(cof_format.format(bb)),
                              float(a_offset)])
                    a_list.append(aa)
                    b_list.append(bb)

    def _props(self):
        """
    Return all properties of the class in one dictionary, but exclude the protected classes
    """
        return dict(
            (key, getattr(self, key)) for key in dir(self) if key not in dir(self.__class__))

    def __str__(self):
        """
    Overload print statement of the class with a pretty format of the attributes

    Returns
    -------
    str:
        String passed to the print statement
    """
        width = 80
        ret_str = "\n" + "-" * width + "\n"
        # loop over the dictionary props in alphabetical order. do no use the class attributes
        # because they do not
        # have the subcategories any more (as I removed that level from the class attributes)
        for key1, dict1 in iter(sorted(self._data_dict.items())):
            ret_str += "* {}:\n".format(key1)
            if dict1 is not None:
                for key2, string_value in iter(sorted(dict1.items())):
                    ret_str += "    {:.<30s} : {}\n".format(key2, string_value)

        return ret_str + "-" * width + "\n"


class DevicesAtLocation(object):
    """Class that defines the names of all measuring points + the corresponding device numbers at a
    given position

    Attributes
    ----------
    names : list
        List of device names belonging to this location
    labels : list
        List of device labels belonging to this location
    short_labels : list
        List of short labels belonging to this location
    numbers : list
        List of devices numbers belonging to this location
    components : list
        List of devices components belonging to this location

    Notes
    -----
    * The device is either a strain gauge or an accelerometer
    * A more detailed description of this class is given in *DeviceGroups*
    """

    def __init__(self, labels=[], names=[], numbers=[], components=[], short_labels=[]):
        self.names = names
        self.labels = labels
        self.short_labels = short_labels
        self.numbers = numbers
        self.components = components

    def add_device(self, label, name=None, number=None, component=None, short_label=None):
        """Add a device to the group

        Parameters
        ----------
        label : str
            Label of the device
        name : str, optional
            Name f the device.  Default value = None
        number : int, optional
            Number rof the device. Default value = None
        component : Object, optional
           Component belonging to the device. Default value = None
        short_label : str, optional
            Short name of the device. Default value = None

        """
        self.names.append(name)
        self.labels.append(label)
        self.short_labels.append(short_label)
        self.numbers.append(number)
        self.components.append(component)

    def swap_channels(self, ii, jj):
        """ Swap the order of all the info in 2 channels

        Parameters
        ----------
        ii : int
            index of the first channel to swap
        jj : int
            index of the second channel to swap

        Notes
        -----
        For the RX4 the order of SG channels has changed

        """
        self.names[ii], self.names[jj] = self.names[jj], self.names[ii]
        self.labels[ii], self.labels[jj] = self.labels[jj], self.labels[ii]
        self.short_labels[ii], self.short_labels[jj] = self.short_labels[jj], self.short_labels[ii]
        self.numbers[ii], self.numbers[jj] = self.numbers[jj], self.numbers[ii]
        self.components[ii], self.components[jj] = self.components[jj], self.components[ii]

    def get_channel_info_dict(self, ii):
        """Store all the channel info in a dictionary and return to user

        Parameters
        ----------
        ii : int
            Channel number to get the information from
        """
        channel_info = dict(
            name=self.names[ii],
            label=self.labels[ii],
            short_label=self.short_labels[ii],
            number=self.numbers[ii],
            component=self.components[ii]
        )
        return channel_info

    def set_channel_info_dict(self, ii, channel_info_dict):
        """Put all the channel info from the dictionary back into channel number ii

        Parameters
        ----------
        ii : int
            Channel number to copy the info to
        channel_info_dict : dict
            dictionary containing all info of the channel we want to set

        """
        self.names[ii] = channel_info_dict["name"]
        self.labels[ii] = channel_info_dict["label"]
        self.short_labels[ii] = channel_info_dict["short_label"]
        self.numbers[ii] = channel_info_dict["number"]
        self.components[ii] = channel_info_dict["component"]


class DeviceGroups(object):
    """Stores Devices (accelerometers or strain gauges) per group

    Parameters
    ----------
    name_list : list
        location names to add to the device group
    label_list : list
        Location labels to add to this device group
    group_list : list
        groups
    title : str
        Title of the type of device: 'Accelerometer' or 'Strain Gauge'

    Attributes
    ----------
    title : str
        Title of the group of devices. Either *Straing Gauge* of *Accelerometers*
    short_character :
        Short name of the devices with the form *XXDDC*, where *XX* is *SG* for strain
        gauges and *A* for accelerometers, *DD* is the number of the device as stored in the MDF
        file, and *C* is the component name of the acceleration vector, *X*, *Y* or *Z*. For
        the strain gauge the *C* is empty
    name_list : list
        List of real names of the channels  beloning to this location

    Notes
    -----
    * A device is either a accelerometer or a strain gauge.
    * A location (also referred to a position) in the tower or on the ship has a group of
      devices which belong to each other as the all measure a different component of the
      strain or acceleration at the same location.
    * For this reasons, devices are grouped together  in the *DevicesAtLocation* class.
    * The *DeviceGroups* in its turn stores all the *DevicesAtLocation* groups
    * Location and device name are obtained from the mdf file. The MDF file adds a 'name' and
      a 'label' to each channel (signal from either an accelerometer or strain gauge) with the
      following format

        1. name=BALDER__UI0_A105_VI1    label=RX4 - SG 05
        2. name=BALDER__UI2_A133_VI1    label=COFF SB FWD - AX - 04
        3. name=BALDER__UI2_A141_VI1    label=TSM - ACC 12 - AX
        4. etc.

    * The names of all devices are stored in the name_list; they are all unique string
      the labels are stored in the label_list and contain information about the location
      the labels contain information about the location name of the cold spot of the device
      (here, RX4, COFF SB FWD, and TSM, respectively), the number of the device (here, 5, 4, and
      12) the device short label (here, SG and AX) and the direction in case of an accelerometer
      (two time the X direction for 2 and 3)
    * In order to extract the information from the label, a search is applied on the label with
      the regular expression which is defined for each location in the settings file.
    * The regular expression are defined in the regular expression list in the input file for
      the accelerometers and strain gauges.
    * For a strain gauge, 2 regular expressions are given in a list, the first to recognise the
      name from the device, the second the recognise the label of the device:

      - BALDER__UI\d_A(\d+)_VI1 : regular expression matching the strain gauge and accelerometer
      - (.*) - SG (\d+) :  regular expression matching the strain gauge name. The first brackets
        store the name of the location in the group_list, the seconds brackets store the SG
        number in the group. So the group indices contain two values for each location

    * regular expression defined for the accelerometers. Again the first one is applied to the
      name field of the channel, the following are one by one applied to the label fields. As you
      can see, normal the first label field regular exp

      - BALDER__UI\d_A(\d+)_VI1  :  regular expression matching the accelerometer or strain
        gauge
      - (.*) - A([XYZ])\s*-\s*(\d+) : the regular expression matching the label field of all but
        TSM accelerometers
      - (.*) - ACC\s(\d+)\s*-\s*A([XYZ]) : RE matching the label field of the TSM

    * For all but the TSM location the RE matches the first label RE, hence the location name is
      stored in the group index 0, the location direction in the group 2 and the device number
      is stored in the 3 group index. However, the TSM they have swap the component label,
      stored now in group index 3, and the device number, now stored in the group 2.
      Therefore, for the TSM we have to swap the reference to the label stored in the group
      list. This is done by trail and error: first the normal order is tried, but in case of the
      TSM raises an error because we want to match a number with a string. If that happens,
      swap group 2 and 3
    """

    def __init__(self, name_list, label_list, group_list, title="device"):

        self.log = get_logger(__name__)

        # create a dictionary holding the names and labels of all the devices per device location
        self.title = title
        # depending on the title we are going to set the character used to refer to the channels
        self.short_character = "NA"
        self.name_list = list()
        if bool(re.search("strain", title, re.IGNORECASE)):
            # this is a strain gauge device
            self.short_character = "SG"
        elif bool(re.search("accelerometer", title, re.IGNORECASE)):
            # this is an accelerometer
            self.short_character = "A"

        self.device_locations = dict()
        self.add_devices_to_group(name_list, label_list, group_list)

    def add_devices_to_group(self, name_list, label_list, group_list):
        """Add the device to a group

        Parameters
        ----------
        name_list :, list
            List of names to add to a group
        label_list : list
            List of labels to add to a group
        group_list : list
            List of groups to add to a group

        """
        index_position = 0
        for index, name in enumerate(name_list):
            self.name_list.append(name)

            position = group_list[index][index_position]

            label = label_list[index]

            (index_number_label, index_component) = (1, 2)
            try:
                # try to get the number by converting first item to an int.  If this is not
                # possible, swap it
                number = int(group_list[index][index_number_label])
            except ValueError:
                # if a value error is raised this means we were trying to convert a X label to a
                # int, which means # we have to swp the number_label and component and try again
                (index_number_label, index_component) = (2, 1)
                number = int(group_list[index][index_number_label])

            short_label = "{}{:02d}".format(self.short_character, number)
            component = None
            try:
                # the component index is only stored in the group_list for the accelerometer device
                component = group_list[index][index_component]
            except IndexError:
                # no component is available. No problem, it is a strain gauge
                pass

            try:
                # try to add this device to the location 'position'. If it fails with a KeyError,
                # the location does not yet exist, so create a new class DevicesAtLocation to
                # initialise a new group for the location # 'position'
                self.log.debug("Adding to position={} label={} and number={:d} (component={})"
                               "".format(position, label, number, component))

                self.device_locations[position].add_device(label=label, name=name, number=number,
                                                           component=component,
                                                           short_label=short_label)
            except KeyError:
                self.log.debug("Initialising to position {} with gauge {} and number {:d} ({})"
                               "".format(position, label, number, short_label))

                # create a new object DevicesAtLocation to store all the devices at this location
                self.device_locations[position] = DevicesAtLocation(labels=[label], names=[name],
                                                                    numbers=[number],
                                                                    components=[component],
                                                                    short_labels=[short_label])

    def report(self):
        """ """
        # show the strain gauges per roset location
        for location, device in iter(sorted(self.device_locations.items())):
            self.log.debug("{} at location {}".format(self.title, location))
            for index, number in enumerate(device.numbers):
                self.log.debug("    {:02d}: names = {} (Component: {})"
                               "".format(number, device.names[index], device.components[index]))


class ChannelGroup(object):
    """
    Class to hold the properties of a group of channels

    Parameters
    ----------
    names : list
        List of channel names
    labels : Lift of channel labels
        List of channel labels
    groups : list of group names
        List of channel groups

    Notes
    -----

    * A channel as returned by the MRUParser has a *name*, *label*, and *group* attribute to
      identify the channel. A typical example of a channel in a MRU file would be

      name=BALDER__UI0_A105_VI1    label=RX4 - SG 05

      So the name is stored as name, the label is stored as *label* and the *group* is the
      returned group from the *re* module used with the regular expression match

    * The class groups a list of channels which logically belong to each other, so as all the
      channels of the MRU, or all the channels of the GPS,  and stores the name, label and group
      as returned by the MRUParser in three list

    """

    def __init__(self, channel_name="Unknown", names=None, labels=None, groups=None):
        self.logger = get_logger(__name__)
        self.channel_name = channel_name
        self.names = names
        self.labels = labels
        self.groups = groups

    def make_report(self):
        """Make a report of the channel properties"""
        msg = "{:20s} : {}"
        self.logger.info(msg.format("Properties of channel", self.channel_name))
        self.logger.info(msg.format("Names", self.names))
        self.logger.info(msg.format("Labels", self.labels))
        self.logger.info(msg.format("Groups", self.groups))
