"""
Fatigue Monitoring Tool
=======================

Software package for reading MDF time series and do several task

* Analyse channels containing MRU information and calculation 6-DOF motions at any location
* Analyse acceleration signal and calcalate motion and fatigue (based on SACS coeffienct) at any
  location
* Analyse strain gauge signal and report stress and fatigue at the locations of the sensors
* Build a data base with the 30 min SDA values

Notes
-----
* Fatigue calculation based on Matlab code by Richard Ogink et. al.
* Python version written by Eelco van Vliet and extended with fatigue and motion prediction and
  RAO tuning
"""
from __future__ import division, absolute_import
from __future__ import print_function, division

import argparse
import datetime
import logging
import os
import random
import re
import string
import sys
# this suppresses the annoying warning by WAFO. remove in case you need to debug
import warnings
from builtins import object
from builtins import range
from builtins import str
from collections import OrderedDict

import LatLon as ll
import hmc_marine
import hmc_marine.data_readers as dr
import hmc_utils
import matplotlib.pyplot as plt
import mlab_mdfreader
import netCDF4 as nc
import numpy as np
import pandas as pd
import progressbar as pb
import scipy.signal as sps
import seaborn as sns
import signal_processing
import signal_processing.filters as spf
import signal_processing.utils as spu
import wafo.objects as wo
from LatLon.LatLon import LatLon
from hmc_marine.data_readers import RELATIVE_DIRECTION_PREFIX
from hmc_marine.wave_spectra import (spectrum2d_to_spectrum2d_encountered,
                                     rayleigh_pdf)
from hmc_utils import Q_
from hmc_utils.file_and_directory import (make_directory, scan_base_directory)
from hmc_utils.misc import (print_banner, Timer, get_logger, read_value_from_dict_if_valid,
                            set_default_dimension, create_logger)
from hmc_utils.numerical import (get_nearest_index)
from hmc_utils.plotting import (analyse_annotations, set_limits, flip,
                                get_valid_legend_handles_labels, clear_all_legends)
from matplotlib.gridspec import GridSpec
from mlab_mdfreader import mdf_parser as mdf
from numpy import (sin, cos, array, vstack, deg2rad, pi, gradient, zeros, dot,
                   linspace,
                   mean, ones, rad2deg)
from numpy.linalg import (lstsq)
from scipy.constants import nautical_mile
from scipy.constants.constants import g as g0
from scipy.interpolate import UnivariateSpline, interp1d
from scipy.signal import (welch, csd)

import fatigue_monitoring.fms_plots as fpl
import fatigue_monitoring.fms_reader_classes as frc
import fatigue_monitoring.fms_utilities as fut
from fatigue_monitoring import __version__
from fatigue_monitoring.fms_utilities import (PROGRESS_WIDGETS, MESSAGE_FORMAT, RAO_ABS_PHASE_EXT,
                                              RAO_SYMMETRY_TYPES,
                                              INCL_G_NAME,
                                              MSG_FORMAT, HS_DAMAGE_ACC_NAMES, NC_ACC_NAMES,
                                              M2_ACC_NAMES,
                                              STRESS_COLUMNS,
                                              STRESS_COLUMNS_ACC, GENERAL_SHEET_NAME,
                                              INFO_SHEET_NAME,
                                              VOYAGE_DICT_NAME,
                                              VOYAGE_INDEX_NAMES, VOYAGE_SHEET_NAME,
                                              INDEX_NAME, DAMAGE_COLUMNS, filter_signal_robust)

warnings.filterwarnings("ignore")

# if a file with this name is found in the working directory, stop the batch process and write
# the results
STOP_FILE_NAME = "stop"

# flush all those ugly warning messages at the start of the script
sys.stderr.flush()


class SubPlotLines(object):
    """
    Class to hold information of lines we want to plot
    """

    def __init__(self, column_name):
        self.column_names = list()
        self.add_column(column_name)

    def add_column(self, column_name):
        self.column_names.append(column_name)


class RaoTune(object):
    """
    This class holds all the stuff we need for the Rao tunning model
    """

    def __init__(self, settings, spectra_2d_time_series, rao_camp, spectrum_info):
        self.logger = get_logger(__name__)

        self.settings = settings
        self.spectra_2d_time_series = self.spectra_2d_time_series
        self.rao_camp = rao_camp
        self.mask_directions = None

        # create some array to hold the global sums of the RAO's used for averaging
        self.raos_sum = np.zeros(self.rao_camp.RAOs.shape)
        self.rao_n_bin_count = np.zeros(self.rao_camp.RAOs[0].shape)
        self.rao_switch = np.ones(self.rao_camp.RAOs[0].shape)

        # create arrays to store the summation of the estimate RAO phase obtained from the
        # cross spectrum per direction the n_phase_count is a 1D array holding the number of
        # csd's added per direction
        self.rao_n_phase_count = np.zeros(self.rao_camp.RAOs[0].shape)

        self.n_rao_added = 0

        self.spectrum_info = spectrum_info
        self.phase_tower = None

        settings_rao = self.settings["rao_verssel"]
        settings_rao_tune = settings_rao["rao_tune"]

        try:
            self.dump_file_base = settings_rao["dump_file_base"]
        except KeyError:
            self.dump_file_base = "none"

        self.set_rao_phase_on_wave_direction_index = \
            settings_rao["rao_tune"]["set_rao_phase_on_wave_direction_index"]
        self.write_rao_per_time_step_to_netcdf = \
            settings_rao["rao_tune"]["write_rao_per_time_step_to_netcdf"]
        self.rao_netcdf_file_name = settings_rao_tune["rao_netcdf_file_name"]

        self.speed_threshold = set_default_dimension(settings_rao_tune["speed_threshold"],
                                                     "knots")
        self.hstot_threshold = set_default_dimension(settings_rao_tune["hstot_threshold"], "m")
        self.spectrum_thrhld_abs_waveheight = settings_rao_tune[
            "spectrum_threshold_for_rao_abs_in_wave_height"]
        self.clip_spectrum_in_1d = settings_rao_tune["clip_spectrum_in_1d_domain"]
        self.spectrum_thrhld_ang_waveheight = set_default_dimension(
            settings_rao["rao_tune"]["spectrum_threshold_for_rao_abs_in_wave_height"], "m")

        self.phase_resolution = settings_rao_tune["phase_resolution"]
        self.phase_range = np.linspace(-np.pi, np.pi, self.phase_resolution, endpoint=True)

        self.rao_phase_sum = np.zeros(self.rao_camp.RAOs.shape)
        self.shape_pdf = list(self.rao_camp.RAOs.shape) + list([int(self.phase_resolution)])
        self.rao_phase_pdf = np.zeros(self.shape_pdf)
        self.phase_delta = 2 * np.pi / self.phase_resolution
        self.phase_0 = -np.pi
        self.logger.debug("shape_pdf {} {} ".format(self.shape_pdf, self.rao_phase_pdf.shape))

        # the wave height threshold is given in wave height. Convert it to values of the 2D spectrum
        self.spectrum_threshold_abs = \
            self.spectrum_thrhld_abs_waveheight ** 2 / self.rao_camp.delta_area
        self.spectrum_threshold_abs_1d = \
            self.spectrum_thrhld_abs_waveheight ** 2 / self.rao_camp.delta_frequency
        self.spectrum_threshold_ang = \
            self.spectrum_thrhld_ang_waveheight ** 2 / self.rao_camp.delta_area
        self.logger.info(
            "Threshold of spectrum abs ROA wave height: {} -> clip values spectrum 2D lower than {}"
            "".format(self.spectrum_thrhld_abs_waveheight, self.spectrum_threshold_abs))
        self.logger.info(
            "Threshold of spectrum ang ROA wave height: {} -> clip values spectrum 2D lower than {}"
            "".format(self.spectrum_thrhld_ang_waveheight, self.spectrum_threshold_ang))

        self.replace_nan_to_zero = settings_rao_tune["replace_nan_to_zero"]
        self.tower_dof_phase_zero = settings_rao_tune["tower_dof_phase_zero"]
        self.rao_tune_absolute_phase = settings_rao_tune["absolute_phase"]
        self.n_points_per_block = settings_rao_tune["spectrum_n_points_per_block"]
        self.create_zero_speed_rao = settings_rao_tune["create_zero_speed_rao"]
        self.minimum_n_raos_per_bin = settings_rao_tune["minimum_number_of_added_raos_per_bin"]
        self.type_of_rao_average = settings_rao_tune["average_type"]
        self.type_of_rao_average_list = ["inverse", "normal"]
        self.rao_base_name = settings_rao_tune["rao_output_base_name"]
        self.rao_as_complex = settings_rao_tune["rao_output_as_complex"]
        self.rao_use_csd_for_phase = settings_rao_tune["estimate_phase_from_cross_spectra"]
        self.debug_plot = settings_rao_tune["raise_debug_plots"]

        self.symmetry_rao_settings = settings_rao_tune["symmetry_rao"]

        # turn the total time stamp into a hours since 1/1/1 value used for the netcdf file
        # use the to_datetime function to turn the pandas timestamp into a numpy datetime
        self.hours_since = nc.date2num(pd.to_datetime(self.time_stamp),
                                       units=self.spectra_2d_time_series.netcdf.dataset.variables[
                                           "time"].units,
                                       calendar=
                                       self.spectra_2d_time_series.netcdf.dataset.variables[
                                           "time"].calendar)
        self.logger.debug(
            "Converted time stamp to hours since {} -> {}".format(self.time_stamp,
                                                                  self.hours_since))

        self.time_series_settings = None
        self.time_series_name = None
        self.psd_file_name = None

        self.row = None
        self.time_stamp = None
        self.speed = None

        self.psd_2d = None
        self.csd_2d = None
        self.psd_tower = None
        self.psd = None
        self.psd_csd = None
        self.psd_phase = None
        self.rao_2d_estimate = None
        self.rao_estimate = None

        if self.type_of_rao_average not in self.type_of_rao_average_list:
            raise ValueError(
                "average_type  under rao_tune should be one of these: 'inverse' or 'normal'")

    def dump_rao(self, index):
        try:
            dump_out_dir = self.settings.rao_vessel["dump_out_dir"]
            # here we can dump the 2D spectrum and current RAO to file if requested in the settings
            # file
            date_time_string = index.strftime("%Y%m%dT%H%M%S")
            if self.settings.rao_vessel["dump_rao_acc_per_time_step"]:
                self.logger.info(
                    "Dumping the rao of time {} ".format(index.strftime("%Y%m%dT%H%M%S")))
                self.rao_camp.dump_current_rao_to_file(
                    os.path.join(dump_out_dir, "rao_acc_{}_2d_{}.msg_pack"
                                               "".format(self.dump_file_base, date_time_string)),
                    self.spectrum_info)
        except KeyError:
            self.logger.debug("dump_sea_state_per_time_step not set. older yaml version. "
                              "No problem, just skip")

    def check_status_ok(self, row, speed_in_knots, time_stamp):

        # we are in the rao_tuning mode. This means that we must check if the time series or
        # psd file exist and the the speed is higher than a certain threshold

        self.row = row
        self.speed = speed_in_knots.to_base_units()
        self.time_stamp = time_stamp

        status_ok = False
        if self.speed < self.speed_threshold.to_base_units():
            self.logger.info("In RAO tuning mode with current speed {} < {}. Skipping".format(
                speed_in_knots, self.speed_threshold))
        elif self.hstot_threshold is not None:
            try:
                Hs_tot = row["Hs_tot"]
            except KeyError:
                self.logger.debug("Hs tot threshold failed, there is no hstot")
            else:
                if Hs_tot < self.hstot_threshold:
                    self.logger.info("In RAO tuning mode: Hs tot {} < {}. Skipping"
                                     "".format(Hs_tot, self.hstot_threshold))
                else:
                    status_ok = True

        if status_ok:
            # if the first two test have passed it means we can continue. First check if we can
            # load or create a tower 6-DOF spectrum

            if not (os.path.exists(self.time_series_name) or os.path.exists(self.psd_file_name)):
                # we do not have a time serie of the 6-DOF and also we do not have a previously save
                # psd. Skip the record
                self.logger.info(
                    "In RAO tuning mode and both the time series {} and the psd {} do not exist. "
                    "Skipping (sofar: {})".format(self.time_series_name, self.psd_file_name,
                                                  self.n_rao_added))
            status_ok = False

        return status_ok

    def calculate_raos_from_spectrum(self):
        """
        Calculate the 2d rao from the current PSD of the tower origin DOF time series and the
        current sea state spectrum

        Notes
        -----
        First the 1-D rao is calculate using

        .. math ::

            S(\omega) = \int_\\theta F(\omega)^2 x D(\\theta)^2 S_sea(\omega, \\theta) d\\theta

        where theta is the direction and omega the frequency. The S(omega) is the power spectral
        density of the time series while th S_sea(omega, theta) is the 2D sea state spectrum we have
        supplied to the routine. We assume that the RAO :math:`\hat{R}` is decomposed as

        .. math ::

            \hat{R}(\omega, \\theta) =  F(\omega) x D(\\theta)

        such we can integrate over theta and then calculate the :math:`F(\omega)` by

        .. math ::

            F(\omega) = S(\omega)) / \int_\\theta  D(\\theta)^2 S_{sea}(\omega, \\theta) d\\theta

    """

        log = self.logger()

        # calculate the power spectral density of the tower time series of all 6DOF components on 
        # the frequency mesh of the rao
        try:
            self.calculate_psd_of_tower_time_series()
        except IOError as err:
            raise err

        # create a 3D matrix to store the RAOs
        self.rao_2d_estimate = np.zeros(self.rao_camp.RAOs.shape)

        if self.debug_plot:
            n_fig = 5
            figures = list()
            fig_names = [
                "0 - PSD",
                "1 - SEA STATE RESPONSE",
                "2 - RAO ABS contours",
                "3 - RAO ZERO contours",
                "4 - RAO eS",
            ]
            for ii in range(n_fig):
                figures.append(plt.figure(fig_names[ii]))
            gs = GridSpec(3, 2)
            i_row = 0
            j_col = 0

        # calculate the 1D spectrum based on the integral of the 2D spectrum in the theta direction
        s_sea_wave_one_d = self.psd_2d.sum(axis=1) * self.rao_camp.delta_directions

        # loop over the 6 dof components
        for i_dof, dof_name in enumerate(self.rao_camp.six_dof_names):

            # lood the current 2d distribution RAO function
            dir_2d = self.rao_camp.dirs_2d_uniform_list[i_dof]

            # calculate D(theta)^2 x S_sea
            sea_state_times_rao_dir = self.psd_2d * dir_2d * dir_2d

            # integrate in theta direction
            sea_state_response_1d = sea_state_times_rao_dir.sum(
                axis=1) * self.rao_camp.delta_directions

            # calculate the one-d rao for this direction
            rao_one_d = np.sqrt(self.psd_tower[dof_name] / sea_state_response_1d)

            if self.clip_spectrum_in_1d and self.spectrum_threshold_abs_1d is not None:
                # clip the one D rao on the spectrum 1 threshold
                rao_one_d = np.where(s_sea_wave_one_d > self.spectrum_threshold_abs_1d, rao_one_d,
                                     0)

            # construct the 2D RAO by multiplying with the directional RAO
            DD, RR = np.meshgrid(self.rao_camp.directional_distributions_1d["abs"][dof_name],
                                 rao_one_d)
            rao_estimate = DD * RR

            if self.debug_plot:
                # store the rao of the moving rao if we are going to plot it
                rao_moving = rao_estimate

            rao_zero = np.zeros(rao_estimate.shape)
            if self.speed > 0 and self.create_zero_speed_rao:
                # shift the RAO back to the zero velocity
                log.debug("Correcting the RAO back to the zero position with velocity {}"
                          "".format(self.speed))
                # not that again we shift in the same direction as we did in the RAO shifts. You
                omega_new = \
                    self.rao_camp.freq_2d_mesh * (
                            1 - np.cos(
                        self.rao_camp.dir_2d_mesh) * self.speed * self.rao_camp.freq_2d_mesh / g0)

                for i_dir in range(self.rao_camp.n_directions):
                    f_inter = interp1d(self.rao_camp.frequencies, rao_estimate[:, i_dir],
                                       bounds_error=False,
                                       fill_value=0)

                    rao_zero[:, i_dir] = f_inter(omega_new[i_dir, :])

                self.rao_estimate = rao_zero

            # store the current rao in the 3D RAO matrix
            self.rao_2d_estimate[i_dof] = rao_estimate

            # add the cross spectrum phase to the phase sum
            # rao_phase_sum[i_dof, :, theta_wave_index] += psd_tower[dof_name + "_phase"]

            if self.debug_plot:
                self.make_debug_rao_plot(fig_names, gs, i_row, j_col, dof_name, self.psd_tower,
                                         sea_state_response_1d, self.psd,
                                         sea_state_times_rao_dir,
                                         self.rao_camp, rao_moving, rao_zero)

                i_row += 1
                if i_row == 3:
                    i_row = 0
                    j_col += 1

        if self.debug_plot:
            plt.ioff()
            plt.show()

    def calculate_psd_of_tower_time_series(self, n_points_per_block=1024):
        """Calculate the power spectral density based on the time series of the tower origin DOF

        Parameters
        ----------
        rao : object
            object carrying all information of the RAO
        settings : dict
            object carrying all the settings
        time_stamp : data_time
            current record time
        tower_dof_phase_zero : bool
            take cross spectral density with respect of this component (Default value = None)
        n_points_per_block : int
            Number of points per block for PSD. Default value = 1024

        Returns
        -------
        ndarray:
            PSD
        """

        log = self.logger
        # obtain the time series setttings from the chapter 'time_series'
        time_series_settings = self.settings.time_series

        # create the time series file name. This must have been created before, because we are only 
        # tuning the ROA and not processing the MDF files
        self.time_series_name = fut.make_time_series_file_name(self.time_series_settings,
                                                               self.time_stamp,
                                                               psd_file_name=False)
        self.psd_file_name = fut.make_time_series_file_name(self.time_series_settings,
                                                            self.time_stamp,
                                                            psd_file_name=True)

        if os.path.exists(self.psd_file_name) and time_series_settings["read_spectrum_when_exist"]:
            # read the power spectral density from file. We have calculated it before
            log.info("Reading PSD from {}".format(self.psd_file_name))
            self.psd_tower = pd.read_msgpack(self.psd_file_name)
        else:
            # calculate the psd time series from the time series
            self.calculate_psd_tower_dof()

            if time_series_settings["write_spectrum_after_psd_calculation"]:
                log.info("Writing PSD to {}".format(self.psd_file_name))
                self.psd_tower.to_msgpack(self.psd_file_name)

    def calculate_psd_tower_dof(self):
        """Calculate psd of time series stored in the file


        Returns
        -------
        ndarray:
            PSD of the tower
        """
        log = get_logger(__name__)
        time_series = pd.read_msgpack(self.time_series_name)
        # get the time step in seconds from the time series
        delta_time = float(np.diff(time_series.index)[0].astype('timedelta64[ns]')) * 1e-9
        sample_frequency = 1 / delta_time
        rao_omega_max = self.rao_camp.frequencies[-1]
        rao_omega_min = self.rao_camp.frequencies[0]
        rao_period_min = 2 * np.pi / rao_omega_max
        rao_period_max = 2 * np.pi / rao_omega_min

        self.psd_tower = pd.DataFrame(index=self.rao_camp.frequencies,
                                      columns=self.rao_camp.six_dof_names)
        if self.tower_dof_phase_zero is not None:
            self.phase_tower = pd.DataFrame(index=self.rao_camp.frequencies,
                                            columns=[name + "_phase" for name in
                                                     self.rao_camp.six_dof_names])
        else:
            self.phase_tower = None

        time_serie_ref = time_series[self.tower_dof_phase_zero].values

        # the first power of 2 of point which covers the maximum frequency required
        # now replaced with an option passed through the configuration file
        # n_points_per_block = int(2 ** math.ceil(math.log(rao_period_max / delta_time, 2)) / 2)

        log.debug("rao_om_min/max {} {}".format(rao_omega_min, rao_omega_max))
        log.debug("rao_Tp_min/max {} {}".format(rao_period_min, rao_period_max))
        log.debug("np_per_block {} ".format(self.n_points_per_block))
        phase_shift = None
        freq_omega = None
        delta_omega = None
        # plt.figure()
        # color = ["r", "g", "b", "c", "k", "y"]
        for i_dof, six_dof_name in enumerate(self.rao_camp.six_dof_names):
            # calculate the  power spectral density
            time_serie = time_series[six_dof_name].values
            freq, psd = welch(time_serie, fs=sample_frequency, nperseg=self.n_points_per_block,
                              window="hanning")

            # calculate the cross spectral density
            if self.tower_dof_phase_zero is not None:
                freq2, cross_spectrum = csd(time_serie_ref, time_serie, fs=sample_frequency,
                                            nperseg=self.n_points_per_block,
                                            window="hanning")
                phase_shift = np.angle(cross_spectrum)

            freq_omega = freq * 2 * np.pi
            delta_omega = freq_omega[1] - freq_omega[0]
            psd_vs_omega = psd / (2 * np.pi)

            # interpolate the psd on the frequency mesh of the RAO
            f_interp = interp1d(freq_omega, psd_vs_omega)
            self.psd_tower[six_dof_name] = f_interp(self.rao_camp.frequencies)

            if self.tower_dof_phase_zero is not None:
                f_interp_phase = interp1d(freq_omega, phase_shift)
                self.phase_tower[six_dof_name + "_phase"] = f_interp_phase(
                    self.rao_camp.frequencies)

            energy_ts = time_serie.var()
            energy_psd1 = (psd_vs_omega * delta_omega).sum()
            energy_psd2 = (self.psd_tower[six_dof_name] * self.rao_camp.delta_frequency).sum()
            log.debug(
                "{} Ets={} E1={} E2={}".format(six_dof_name, energy_ts, energy_psd1,
                                               energy_psd2))

        self.log.debug(
            "freq_om_min_max {} {} {} ".format(freq_omega[0], freq_omega[1], freq_omega[-1]))
        self.log.debug(
            "delta_omega {} rao_delta_om {} ".format(delta_omega, self.rao_camp.delta_frequency))

        # combine both the tower psd and the cross spectral densities in the data frame and return
        if self.phase_tower is not None:
            self.psd_tower = pd.concat([self.psd_tower, self.phase_tower], axis=1,
                                       join_axes=[self.psd_tower.index])

    def update_rao_estimate(self, psd_2d):

        log = self.loger
        # this section is going to calculate the current RAO
        log.info("Calculate RAO {}".format(self.n_rao_added))

        self.psd_2d = psd_2d

        try:
            self.calculate_raos_from_spectrum()
        except IOError:
            log.info("Failed to calculate RAO")
            return

        # we have successfully calculate the current RAO. It the threshold is not None,
        # calculate the mask 'rao_switch' here. Otherwise the initialise value of one is taken
        if self.spectrum_thrhld_abs_waveheight is not None:
            # we have set the spectrum threshold and also did not yet threshold in the 1D
            # spectrum, so now do it on the 2D spectrum
            self.rao_switch = np.where(self.psd_2d > self.spectrum_threshold_abs, 1.0, np.nan)

        # depending on the value of 'average_type' in the settings file, calculate the masked
        # rao by the inverse or by the normal sum
        if self.type_of_rao_average == "inverse":
            log.debug("Adding inverse RAO to mask data")
            self.masked_raos_abs = self.rao_switch / self.raos_estimate
        elif self.type_of_rao_average == "normal":
            log.debug("Adding normal RAO to mask data")
            self.masked_raos_abs = self.rao_switch * self.raos_estimate
        else:
            raise ValueError(
                "The rao_tune settigs 'average_type' must be either 'normal' or 'inverse'")

        # update the rao sum. Note that masked_raos_abs can be the inverter RAO as well
        self.raos_sum += np.nan_to_num(self.masked_raos_abs)

        # in case we have no threshold, rao_n_bin_count is the same as n_rao_added, except on a
        # 2d plane
        self.rao_n_bin_count += np.nan_to_num(self.rao_switch)

        # total number of ROA's added so far
        self.n_rao_added += 1

        # create the 2d mask array for the phase. Can be either a mask on the wave direction or
        # the wave spectrum
        if self.set_rao_phase_on_wave_direction_index:
            self.mask_directions = np.zeros(self.rao_camp.shape2D)
            try:
                rel_direction_name = RELATIVE_DIRECTION_PREFIX + "direction_wave_tot"
                theta_wave_tot = np.deg2rad(self.row[rel_direction_name])
                theta_wave_index = get_nearest_index(self.rao_camp.directions, theta_wave_tot)
                log.debug("Found relative direction {} with index {}"
                          "".format(theta_wave_tot, theta_wave_index))
            except KeyError:
                log.info(
                    "Could not find the relative wave direction of the total wave field. "
                    "Set mask to zero")
            else:
                # create a mask with only zeros except for the current direction
                self.mask_directions[:, theta_wave_index] = 1.0
        else:
            # mask the rao with a threshold state spectrum
            self.mask_directions = np.where(self.psd_2d > self.spectrum_threshold_ang, 1.0, np.nan)

        # add the mask to the total sum
        self.rao_n_phase_count += np.nan_to_num(self.mask_directions)

        # mask the rao phase and store in the 3D masked array so we keep all the mask in time.
        # This allows us later to see which phase occurred the most time. This phase will be
        # pick (note that averaging phase does not # make sense, hence this method)
        self.masked_raos_phase = np.zeros(self.masked_raos_abs.shape)

        for i_dof, dof_name in enumerate(self.rao_camp.six_dof_names):
            var_name = "_".join([dof_name, "phase"])

            # create the phase response based on the cross spectral density and the current
            # 2d mask
            self.masked_raos_phase[i_dof] = self.mask_directions * self.psd_csd[var_name][:, None]

            # add this cross correlation masked with the switch to the total sum
            self.rao_phase_sum[i_dof] += np.nan_to_num(self.masked_raos_phase[i_dof])

            # turn the current phase angle into a index in the phase range space. Take care of
            # the boundaries
            self.rao_pdf_index = \
                (self.phase_resolution *
                 (self.masked_raos_phase[i_dof] - self.phase_0) / (2 * np.pi)).astype("int")
            rao_pdf_index = np.where(np.isnan(self.masked_raos_phase[i_dof]), np.nan,
                                     rao_pdf_index)
            rao_pdf_index = \
                np.where(rao_pdf_index > self.phase_resolution - 1,
                         self.phase_resolution - 1, rao_pdf_index)
            rao_pdf_index = np.where(rao_pdf_index < 0, 0, rao_pdf_index)
            log.debug("Updating rao pdf for {} {}".format(i_dof, dof_name))
            for ii in range(self.rao_camp.n_frequencies):
                for jj in range(self.rao_camp.n_directions):
                    index = rao_pdf_index[ii, jj]
                    if not np.isnan(index):
                        self.rao_phase_pdf[i_dof, ii, jj, index] += 1

        # finally ,write the rao estimated to the netcdf file if requested
        if self.write_rao_per_time_step_to_netcdf:
            # get the next time index in the data base
            next_rao_time_index, last_rao_time_value = fut.get_next_time_index(
                self.rao_database_times)
            # store the next time value
            self.rao_database_times[next_rao_time_index] = self.hours_since

            # write both the phase and magnitude to netcdf rao file file
            for i_dof, dof_name in enumerate(self.rao_camp.six_dof_names):
                for comp in RAO_ABS_PHASE_EXT:
                    # create variable name for current component (dof name + _abs or _phase
                    var_name = "_".join([dof_name, comp])

                    # pick the data belong to the abs or the phase
                    if comp == "abs":
                        data = self.masked_raos_abs[i_dof]
                    else:
                        # start with 2d plane of n_freq x n_dirs with zeros
                        data = self.masked_raos_phase[i_dof]

                    # store the psd in the netcdf data base
                    self.rao_var_netcdf[var_name][next_rao_time_index, :, :] = data

        if self.debug_plot:
            # plotting for debugging only
            log.debug("Make debug  plots")
            plt.figure("RAO estimate")
            gs = GridSpec(3, 2)
            i_row = 0
            j_col = 0
            for i_dof, six_dof_name in enumerate(self.rao_camp.six_dof_names):
                if self.type_of_rao_average is "inverse":
                    # we have used the inverse rao in the summation, so inverse back to normal
                    rao_est_nor = 1.0 / self.raos_sum[i_dof]
                else:
                    rao_est_nor = self.raos_sum[i_dof]
                ax = plt.subplot(gs[i_row, j_col])
                ax.set_title(six_dof_name)
                ax.plot(self.rao_camp.frequencies, rao_est_nor, '-+')
                i_row += 1
                if i_row == 3:
                    i_row = 0
                    j_col += 1

            plt.ioff()
            plt.show()

    def make_debug_rao_plot(self, fig_names, gs, i_row, j_col, dof_name, psd_tower,
                            sea_state_response_1d,
                            sea_state_spectrum, sea_state_times_rao_dir, rao, rao_moving,
                            rao_zero):
        """
        Create a plot for debugging

        Parameters
        ----------
        fig_names : list
           Name of the gigure
        gs : gridspec
           Grid for the plot
        i_row : int
           Which row to add
        j_col : int
           Which col to add
        dof_name : str
            Name of the DOF
        psd_tower : ndarray
            Plot the PSD
        sea_state_response_1d : ndarray
            Responses
        sea_state_spectrum : ndarray
           Spectrum to tplot
        sea_state_times_rao_dir : int
        rao : object
        rao_moving : bool
        rao_zero :

        """
        # below here, all the debugging plots are created
        fig = plt.figure(fig_names[0])
        ax2 = plt.subplot(gs[i_row, j_col])
        ax2.set_title("PSD {}".format(dof_name))
        ax2.set_yscale('log')
        ax2.plot(rao.frequencies, psd_tower[dof_name])

        fig = plt.figure(fig_names[1])
        ax1 = plt.subplot(gs[i_row, j_col])
        ax1.set_title("Sea state response")
        ax1.plot(rao.frequencies, sea_state_response_1d)
        ax1.set_yscale('log')
        for ii in range(sea_state_spectrum.shape[1]):
            ax1.plot(rao.frequencies, sea_state_times_rao_dir[:, ii], '--')

        # show in plot
        fig = plt.figure(fig_names[2])
        ax0 = plt.subplot(gs[i_row, j_col])
        ax0.set_title(dof_name)
        cs = ax0.contourf(rao.freq_2d_mesh, rao.dir_2d_mesh, rao_moving.T)
        # , locator = ticker.LogLocator())
        cbar = plt.colorbar(cs)
        cbar.ax.set_ylabel("{} [-]".format("RAO abs"))

        # show in plot
        fig = plt.figure(fig_names[3])
        ax3 = plt.subplot(gs[i_row, j_col])
        ax3.set_title("RAO ZERO " + dof_name)
        cs = ax3.contourf(rao.freq_2d_mesh, rao.dir_2d_mesh, rao_zero.T)
        # locator = ticker.LogLocator())
        cbar = plt.colorbar(cs)
        cbar.ax.set_ylabel("{} [-]".format("RAO ZERO abs"))

        fig = plt.figure(fig_names[4])
        ax4 = plt.subplot(gs[i_row, j_col])
        ax4.set_title("RAO est and zero {}" + dof_name)
        ax4.set_yscale('log')
        color = ["r", "g", "b", "c", "k", "y"]
        color += color
        color += color
        for i_dir in range(0, rao.n_directions, 2):
            ax4.plot(rao.frequencies, abs(rao_zero[:, i_dir]) + 1e-10,
                     '-' + color[int(i_dir / 2)],
                     label='zero {}'.format(int(i_dir / 2)))
            ax4.plot(rao.frequencies, abs(rao_moving[:, i_dir]) + 1e-10, '--' +
                     color[int(i_dir / 2) + 1], label='move {}'.format(int(i_dir / 2)))
        plt.legend()

    def rao_final_average(self):
        # only for the RAO tuning mode we need to calculate the average inverse RAO, invert it and
        # dump the mean rao file
        # for now, save them two time: one time as a complex and one time as a abs/phase
        nc_title = "RAO of tower based on {} data sets".format(self.n_rao_added)
        log = self.logger

        if self.type_of_rao_average == "inverse":
            log.info("Calculating RAO mean based on inverse average")
            nc_title += " using inverted mean"
            if self.spectrum_thrhld_abs_waveheight is not None:
                nc_title += " with threshold {}".format(self.spectrum_thrhld_abs_waveheight)
            rao_data = \
                np.where(self.rao_n_bin_count < self.minimum_n_raos_per_bin,
                         0, 1.0 / (self.raos_sum / self.rao_n_bin_count))
        elif self.type_of_rao_average == "normal":
            log.info("Calculating RAO mean based on normal average")
            nc_title += " using normal mean"
            if self.spectrum_thrhld_abs_waveheight is not None:
                nc_title += " with threshold {:.3f} m ({:.6e})" \
                            "".format(self.spectrum_thrhld_abs_waveheight,
                                      self.spectrum_threshold_abs)
            rao_data = np.where(self.rao_n_bin_count < self.minimum_n_raos_per_bin,
                                0, self.raos_sum / self.rao_n_bin_count)

        if self.rao_camp.f_clip_min is not None:
            log.info("Clip RAO frequencies below {} rad/s at index {}"
                     "".format(self.rao_camp.f_clip_min, self.rao_camp.f_clip_min_index))
            rao_data[:, :self.rao_camp.f_clip_min_index, :] = 0.0
        if self.rao_camp.f_clip_max is not None:
            log.info("Clip RAO frequencies below {} rad/s at index {}"
                     "".format(self.rao_camp.f_clip_max, self.rao_camp.f_clip_max_index))
            rao_data[:, self.rao_camp.f_clip_max_index:, :] = 0.0

        if self.rao_as_complex:
            save_as_complex = True
        else:
            save_as_complex = False

        rao_name = os.path.splitext(self.rao_base_name)[0] + ".nc"
        rao_name_bin = os.path.splitext(self.rao_base_name)[0] + "_bins.nc"

        if self.rao_use_csd_for_phase:
            # we used the cross spectra to calculate the phase. We are going to use the most
            # frequent occurred phase for each bin
            phase_data = np.zeros(self.rao_camp.RAOs.shape)
            for i_dof, dof_name in enumerate(self.rao_camp.six_dof_names):
                var_name = "_".join([dof_name, "phase"])
                for ii in range(self.rao_camp.n_frequencies):
                    for jj in range(self.rao_camp.n_directions):
                        max_index = np.argmax(self.rao_phase_pdf[i_dof, ii, jj])
                        # store the phase with the maximum index
                        if dof_name == self.tower_dof_phase_zero:
                            # by definition this phase is zero as the phase of the other channels is
                            # cross correlated to this one
                            phase_data[i_dof, ii, jj] = 0.0
                        else:
                            phase_data[i_dof, ii, jj] = self.phase_range[max_index]
        else:
            phase_data = None

        # at this point we have a estimation of the phase component of the RAO based on the cross
        # spectral density  below we are applying symmetry rules to be able to get a better estimate
        # of the phase and magnitude
        for component, symmetry_options in self.symmetry_rao_settings.items():
            if component == "phase":
                data = phase_data
                count = self.rao_n_phase_count
                phase_shift_component = self.tower_dof_phase_zero
                absolute_phase = self.rao_tune_absolute_phase
            elif component == "magnitude":
                data = rao_data
                count = self.rao_n_bin_count
                phase_shift_component = None
                absolute_phase = False
            else:
                raise ValueError(
                    "symmetry rao components must be either 'phase' or 'magnitude'. "
                    "Current value: {}".format(component))
            if not symmetry_options["impose_symmetry"]:
                log.info("No symmetry imposed for component {}".format(component))
                continue
            symmetry_type = symmetry_options["type"]
            if symmetry_type not in RAO_SYMMETRY_TYPES:
                # check if the symmetry type is set to one of: ["weighted", "keep_left",
                # "keep_right"]
                raise ValueError(
                    "Symmetry type must be one of: {}".format(RAO_SYMMETRY_TYPES))

            log.info(
                "Imposing symmetry on data '{}' of type {}".format(component,
                                                                   symmetry_type))

            # for each rao part (magnitude or phase) and each six-dof RAO  (ax, ay, az, rx, ry, rz)
            # we can specify a different symmetry rule in a list. If no list is given (for the
            # magnitude part), then mirror is assumed however, for the phase also other symmetry
            # rules may apply (periodic, periodic_negate and mirror)
            symmetry_rules = list()
            try:
                symmetry_rules = symmetry_options["symmetry_rules"]
                log.info("Using point mirror list {}".format(symmetry_rules))
            except KeyError:
                # no symmetry rules were given. No problem, this is probably the magnitude of the
                # RAO, so just impose mirror symmetry for all components
                symmetry_rules = ["mirror" for i_sym in range(6)]
                log.info(
                    "No point mirror list found for {}. Assuming mirror symmetry".format(
                        component))

            data = self.impose_symmetries(data, count, self.rao_camp, symmetry_type=symmetry_type,
                                          symmetry_rules=symmetry_rules,
                                          phase_shift_component=phase_shift_component,
                                          absolute_phase=absolute_phase,
                                          replace_nan_to_zero=self.replace_nan_to_zero)

            # make sure to copy the data back
            if component == "phase":
                phase_data = data
            else:
                rao_data = data

        # dump the rao data to file
        self.rao_camp.create_netcdf_rao(rao_data, rao_name, description=nc_title,
                                        save_as_complex=save_as_complex,
                                        rao_phase=phase_data)

        if self.spectrum_thrhld_abs_waveheight is not None:
            new_ds = nc.Dataset(rao_name_bin, "w", format="NETCDF4")
            self.rao_camp.write_dimensions_to_netcdf(new_ds)
            log.debug("HIER {} {}".format(self.rao_n_bin_count.shape, type(self.rao_n_bin_count)))
            bin_count = new_ds.createVariable("bin_count", "f8", ("frequency", "direction"),
                                              zlib=True)
            bin_count[:] = self.rao_n_bin_count
            phase_count = new_ds.createVariable("phase_count", "f8",
                                                ("frequency", "direction"),
                                                zlib=True)
            phase_count[:] = self.rao_n_phase_count
            new_ds.close()

        if self.debug_plot:
            plt.figure(figsize=(10, 8))
            gs = GridSpec(3, 2)
            i_row = 0
            j_col = 0
            ax_zero = None
            for i_dof, six_dof_name in enumerate(self.rao_camp.six_dof_names):

                if i_dof == 0:
                    ax = plt.subplot(gs[i_row, j_col])
                    ax_zero = ax
                else:
                    ax = plt.subplot(gs[i_row, j_col], sharex=ax_zero)
                if i_row == 2:
                    ax.set_xlabel("Frequency [rad/s]")
                if j_col == 0:
                    ax.set_ylabel("RAO mag [m/m]")
                else:
                    ax.set_ylabel("RAO mag [deg/m]")
                ax.plot(self.rao_camp.frequencies,
                        self.rao_camp.frequency_distributions_1d["abs"][six_dof_name],
                        '-xg')

                dof_label = re.sub(".*_", "", six_dof_name)
                ax.text(0.85, 0.9, "{}) {}".format(string.ascii_lowercase[i_dof], dof_label),
                        transform=ax.transAxes, fontsize=10)

                i_row += 1
                if i_row == 3:
                    i_row = 0
                    j_col += 1

            file_name_base = os.path.join(os.getcwd(),
                                          os.path.split(self.rao_camp.file_base_name)[1])

            plt.savefig(file_name_base + "_rao_frequency_vs_omega.png")

            plt.ioff()
            plt.show()

    def impose_symmetries(self, data, count, rao, symmetry_type="weighted", symmetry_rules=[],
                          phase_shift_component=None, absolute_phase=False,
                          replace_nan_to_zero=False):
        """Take the 6 DOF RAOS and (point)mirror  over the middle direction axis.

        Parameters
        ----------
        data : ndarray
            6 x n_freq x n_dirs data array with the current RAO component (can be abs or phase)
        count : int
            n_freq x n_dirs array with count per bin. Used to weight the mirrored average
        rao : object
            object carying the information of the rao
        symmetry_type : {'weighted', 'keep_left', 'keep_right'}
            Type of symmetry imposed. Default value = "weighted"
        symmetry_rules : list
            a list with the indices of the data planes that need to be point mirrored. Default = []
        phase_shift_component : None
            if not None, shift the phase of the rao to this component before applying symmetry
            (Default value = None)
        absolute_phase : bool
            if true, make the phase absolute to the wave field by using the phase of the
            phase_shift (Default value = False)
        replace_nan_to_zero :
            set all the nan values to zero.
            component (Default value = False)

        Returns
        -------
        ndarray
            Symmetric data
        """

        log = get_logger(__name__)

        # initialize a zero data array
        data_sym = np.zeros(data.shape)

        # retrieve the directional distribution of the
        if phase_shift_component is not None:
            rao_zero = rao.directional_distributions_1d["phase"][phase_shift_component].values
        else:
            rao_zero = np.zeros(rao.n_directions)

        # get the middle index im used for the mirroring of the data
        nd = rao.n_directions
        if nd % 2 != 0:
            raise ValueError(
                "Symmetry rule not checked for odd number of directions yet. Please use "
                "even amount of points in the Theta directions of the RAO")

        im = int(nd / 2)
        ip = im + 1

        if symmetry_rules is None:
            # make sure that also a None as input does not raise and error by settings it an empty
            # list
            symmetry_rules = list()

        for i_dof, six_dof_name in enumerate(rao.six_dof_names):

            # set the symmetry weight on
            weight = np.ones(rao.shape2D)

            if symmetry_type == "weighted":
                # for the RAO magnitude we can make an average between the left and right part RAO
                # based on the number of rao values added for each bin. Therefore, use count as a
                # weight
                weight = count
            elif symmetry_type == "keep_left":
                # for the RAO phase we keep the binary weight value: 0 or 1. If keep_left is true, 
                # set the right side 0
                weight[:, ip:] = 0
            elif symmetry_type == "keep_right":
                weight[:, :im] = 0
            else:
                raise ValueError("Symmetry weight must be one of {}".format(RAO_SYMMETRY_TYPES))

            rules = symmetry_rules[i_dof].split("_")
            copy_rule = rules[0]
            sign = 1
            # the symmetry rules are given as: 'mirror_negate', 'periodic_negate', or just 'mirror'.
            # If '_negate' was added apply a sign -1 when copying the data from left to right
            try:
                if re.search("negate", rules[1]):
                    sign = -1
            except IndexError:
                log.debug("No negation rule found. Not problem, just continue with negation")

            log.debug(
                "Found symmetry rules for {} {}: {}".format(i_dof, symmetry_rules[i_dof],
                                                            rules))

            # initialise a n_freq x n_dirs 2d array with the curring symmetric counts
            weight_sym = np.zeros(weight.shape)

            # for correct back shifting with the zero rao, also apply the same symmetry rule to the
            # zero rao
            rao_zero_sym = np.zeros(rao_zero.shape)

            # create the first half of the symmetric data by adding the weighted first part with the
            # weighted second part note the the point at dir =0 is not averaged with dir =pi as they
            # do not need to be the same finally we can also have a 'periodic' copy: in that case
            # the first half plane is just shift with pi to the right. This is more what happens
            # with the phase angle
            if copy_rule == "mirror":

                # calculate the weight sum. The weight array contain the number of measurement of
                # the data value in the bin add the rao in order to make the phase symmetric.
                # For the magnitude rao_zero is just zero
                weighted_sum = (data[i_dof] + rao_zero) * weight

                # we have added the rao_zero for the phase to make sure that the phase is symmetric.
                # In order to back shift the rao_zero later, we need to apply the same symmetry 
                # rules to the rao zero. Do that here
                weighted_sum_rao = rao_zero * weight[0]

                # copy the direction 0 and im (phi=pi) with averaging
                data_sym[i_dof, :, 0] += weighted_sum[:, 0]
                data_sym[i_dof, :, im] += weighted_sum[:, im]

                # copy the half plane combined with the second plane
                data_sym[i_dof, :, 1:im] += weighted_sum[:, 1:im] + sign * weighted_sum[:, -1:im:-1]
                # copy the first part to the second half of the data
                data_sym[i_dof, :, ip:] = sign * data_sym[i_dof, :, im - 1:0:-1]

                # do exactly the same with the data weight_sym per bin
                weight_sym[:, 0] += weight[:, 0]
                weight_sym[:, im] += weight[:, im]
                weight_sym[:, 1:im] += weight[:, 1:im] + weight[:, -1:im:-1]
                weight_sym[:, ip:] = weight_sym[:, im - 1:0: -1]

                rao_zero_sym[0] += weighted_sum_rao[0]
                rao_zero_sym[im] += weighted_sum_rao[im]
                rao_zero_sym[1:im] += weighted_sum_rao[1:im] + sign * weighted_sum_rao[-1:im:-1]
                rao_zero_sym[ip:] = sign * weighted_sum_rao[im - 1:0:-1]

            elif copy_rule == "periodic":

                weight[:, im] = 0
                weighted_sum = (data[i_dof] + rao_zero) * weight

                # keep the sum of the rao zero as well
                weighted_sum_rao = rao_zero * weight[0]

                # create a symmetric copy
                data_sym[i_dof, :, :im] += weighted_sum[:, :im] + sign * weighted_sum[:, im:]
                # copy the first part to the second half of the data
                data_sym[i_dof, :, im:] = sign * data_sym[i_dof, :, :im]

                # do exactly the same with the data weight_sym per bin
                weight_sym[:, :im] += weight[:, :im] + weight[:, im:]
                weight_sym[:, im:] = weight_sym[:, :im]

                rao_zero_sym[:im] += weighted_sum_rao[:im] + sign * weighted_sum_rao[im:]
                rao_zero_sym[im:] += sign * weighted_sum_rao[:im]

            # normalize the symmetric array with the weight
            data_sym[i_dof] /= weight_sym

            rao_zero_sym = np.nan_to_num(rao_zero_sym / weight_sym[0])

            if not absolute_phase:
                # in case we want to keep the phase of the tower_phase_zero_component equal to zero, 
                # subtract the zero phase again
                log.debug("Subtracting phase again")
                data_sym[i_dof] -= rao_zero_sym

        # finally replace all field with nan (where we divided by zero) with 0
        if replace_nan_to_zero:
            data_sym[np.isnan(data_sym)] = 0.0

        return data_sym


class FMSMonitorAndPrediction(object):
    """
    Base class for the Fatigue Monitoring System analysing software. This class can be used to do
    the following:

    * In the *monitoring* mode: read a series of monitored MDF file and do time series analyse. It
      includes:

      - Calculate statistics
      - Process MRU motion signals and transfer motions to N locations at the vessel
      - In case that strain gauges and/or accelerometers are present: calculate the Fatigue in the
        tower

    * In the *prediction* mode: read output files from Safetrans and calculate the motions and
      Fatigue along the route based on an RAO
    * In the *rao_tune* mode: Use monitored MDF file combined with externally supplied sea states in
      order to get an Estimation of the RAO of the vessel.


    Parameters
    ----------
    configuration_file : str
        The configuration file with all the settings")
    start_time: :obj:`date_time`
        Start time of the script
    mode: {"monitoring", "prediction", "rao_tune", "generate_spectra"}
       Set the type of operation of the script. Default is "monitoring". The options are

       * *monitoring*:  Loop over the MDF files with time series and process the data. An excel
         data base file is generated containing the statistics per MDF file vs data/time
       * *prediction*: Take trajectory files from safetrans as input and calculate the Motions and
         Fatigue using an motion RAO. Similar to the TransFatigueTool
       * *rao_tune*: Loop over the MDF files to obtain the monitored motions and calculate the
         2D sea states by an external source (ie. NOAA). With this information the RAO is estimated
       * *generate_spectra*: Loop over the MDF files to obtain the monitored motions and
         estimate the 6-DOF motion power spectral densities and cross spectral densities. Can be
         used later be *rao_tune* to tune the RAO for varying settings without having to
         import the MDF files and calculate the spectra.

    reset_database: bool, optional
        Do not read the existing data base but reset the existing data base start again.
        Default = False
    progress_bar: bool, optional
        Show the progress bar instead of the logging messages. Default = False
    dry_run: bool, optional, optional
        Only show what is going to happen, but do nothing. Default = False
    dump_settings: bool, optional
        Create a dump of the settings to <configuration_file>_dump.yaml to allow to see what exact
        settings have been used
    pick_maximum_rao_for_all_hotspots: bool, optional
        Use the maximum RAO for all hot spot. For testing purpose only. Default = False
    clear_database: bool, optional
        If the data base is in an erroneous state, only write it so it gets cleaned
    block_last_plot: bool, optional
        Block script at the last plot. Default = False
    show_images: bool, optional
        Show the image, overrules the show_images flag in the settings file. Default = False
    matlab_trajectory_data_base: str or None
        If this file is given the trajectories are read from the matlab trajectory data base.
        Default = None
    hindcast_database: str, optional
        If this file is given the trajectories are read from a monitored database such that the
        motion can be predicted based on a true measured journey
    hindcast_start_date_time: :obj:`date_time`, optional
        Start hind casting at this date. If not given, take first date in file
    hindcast_end_date_time: :obj:`date_time`, optional
        End hind casting at this date. If not given, take first date in file
    time_zone: str, optional
        Specify the time zone to use by the logging system. Note that on the Balder the time zone is
        set to Europe Summer time, even in the winter. To force this, use the Etc/GMT-2 convention,
        which corresponds to UTC+002 in POSSIX convention
    meteo_consult_report: str, optional
        If this file is given the trajectories are read from forecast report
    noaa_forecast_file: str, optional
        If this file is given the trajectories are read from forecast file obtained from NOAA
    msg_pack_reset: bool, optional
        Forces to reset the message pack, overruling the setting in the configuration file
    information_to_excel_output: bool, optional
        Write more information to the excel output file. Default = True
    show_all_channel_names: bool, optional
        Write all the channel names to screen and then leave
    dump_mdf_file_data: bool, optional
        Write all the MDF file time series to an excel file with a new name
    write_log_to_file: bool, optional
    log_file_base: str, optional
        Default name of the logging output file base
    heading_name: str, optional
        Name of the heading field in the MDF file. Default = "HEADER"
    start_date_string: str, optional
        A string YYMMDD representing the start date of the current voyage. Only needed when reading
        the matlab data base in order to add a date to each journey. Default = None
    trajectory_file: list, optional
        CSV or msg_pack file names required for the rao_vessel execution. Can be given multiple
        times to add more than one csv file
    store_settings: bool, optional
        Store the default settings to the configuration file *configuration_file*. Default = False
    execute: bool, optional
        After create the object immediately execute the code. This option is set to True when
        running from the command line utility.
    cache_mdf_read_or_write : bool
        Write the MDF file to msg pack cache files directly after reading, or read the MDF file
        if the cache file already exist from the previous run. Default = False
    reset_mdf_cache : bool
        Write the MDF file to msg pack cache files directly after reading even if the file already
        exist.
    max_trajectories: int, optional
        Maximum number of voyage to processes in case we are in *prediction* mode. Will override
        the value as given in the settings file if not None. Default = None
    start_voyage_id: int, optional
        Start at this voyage id. Default = None, which means just start at the beginning



    Attributes
    ----------
    fatigue_data_base_file : str
        Name of the excel file cotaining the monitored statistics
    fatigue_data_base : :obj:`DataFrame`
        Pandas data frame with all the monitored statistics per channel vs time stamp. Each rows
        represents a 30 min records

    """

    def __init__(self,
                 configuration_file=None,
                 mode="monitoring",
                 reset_database=False,
                 progress_bar=False,
                 dry_run=False,
                 dump_settings=False,
                 n_fatigue_dump_every=50,
                 pick_maximum_rao_for_all_hotspots=False,
                 clear_database=False,
                 block_last_plot=False,
                 show_images=False,
                 matlab_trajectory_data_base=None,
                 hindcast_database=None,
                 hindcast_start_date_time=None,
                 hindcast_end_date_time=None,
                 time_zone=None,
                 meteo_consult_report=None,
                 noaa_forecast_file=None,
                 msg_pack_reset=False,
                 information_to_excel_output=True,
                 show_all_channel_names=False,
                 dump_mdf_file_data=False,
                 write_log_to_file=False,
                 log_file_base="log",
                 heading_name=None,
                 start_date_string=None,
                 trajectory_file=list(),
                 store_settings=False,
                 execute=False,
                 cache_mdf_read_or_write=False,
                 reset_mdf_cache=False,
                 max_trajectories=False,
                 start_voyage_id=False,
                 monitoring_start_date_time=None,
                 monitoring_end_date_time=None,
                 ):

        self.logger = get_logger(__name__)
        self.configuration_file = configuration_file

        self.mode = mode
        self.reset_database = reset_database
        self.n_fatigue_dump_every = n_fatigue_dump_every
        self.progress_bar = progress_bar
        self.dry_run = dry_run
        self.dump_settings = dump_settings
        self.pick_maximum_rao_for_all_hotspots = pick_maximum_rao_for_all_hotspots
        self.clear_database = clear_database
        self.block_last_plot = block_last_plot
        self.show_images = show_images
        self.matlab_trajectory_data_base = matlab_trajectory_data_base
        self.hindcast_database = hindcast_database
        self.hindcast_start_date_time = hindcast_start_date_time
        self.hindcast_end_date_time = hindcast_end_date_time
        self.time_zone = time_zone
        self.meteo_consult_report = meteo_consult_report
        self.noaa_forecast_file = noaa_forecast_file
        self.msg_pack_reset = msg_pack_reset
        self.read_msg_pack = msg_pack_reset
        self.information_to_excel_output = information_to_excel_output
        self.show_all_channel_names = show_all_channel_names
        self.dump_mdf_file_data = dump_mdf_file_data
        self.write_log_to_file = write_log_to_file
        self.log_file_base = log_file_base
        self.heading_name = heading_name
        self.start_date_string = start_date_string
        self.trajectory_file = trajectory_file
        self.store_settings = store_settings
        self.max_trajectories = max_trajectories
        self.start_voyage_id = start_voyage_id

        self.monitoring_start_date_time = monitoring_start_date_time
        self.monitoring_end_date_time = monitoring_end_date_time

        self.cache_mdf_read_or_write = cache_mdf_read_or_write
        self.reset_mdf_cache = reset_mdf_cache

        # from here on we have internally derived quantities and filed

        self.mdf_file = None  # currently loaded mdf file only one at a time, so only one handle
        self.mat_file = None

        self.rao_camp = None  # complex rao amplitude object
        self.psd_2d = None  # power spectral density of the sea state
        self.freq_in_hz_2d = None  # frequencies of the psd_2d over the same 2d data array
        self.sea_state_duration = Q_(3, "hour")  # time duration of one sea state of the forecast

        # from here the internal attributes
        self.strain_gauge_groups = None
        self.accelerometer_groups = None
        self.mru_group = None
        self.dls_group = None
        self.gps_group = None
        self.huisman_group = None
        self.mru_reference_location = None

        self.fatigue_data_base_file = None
        self.fatigue_data_base_file_base = None
        self.fatigue_data_base = None
        self.spectra_2d_time_series = None
        self.spectrum_info = None

        self.last_coordinates = None
        self.last_distance = None
        self.apply_velocity_correction = None

        self.file_list = list()
        self.file_name = None
        self.file_base_name = None
        self.file_time_stamps = list()
        self.time_stamp = None  # holds the time stamp of the current loaded file
        self.trajectory_data_base = None

        # the prediction mode creates a voyage data base
        self.number_of_files_to_process = 0
        self.voyage_all_file_name = None
        self.voyage_data_base_all = None

        # store some handle information for each of the graphs plotted below. Per graph a handle is
        # stored to keep all the axis, figures, lines and text labels. This has the benefit that we
        # only have to draw the items for the first file, all other files only the data is updated
        self.handles = dict()
        for handle_name in (
                "acceleration", "strains", "mru", "huisman", "hotspots", "tower0", "tower1"):
            self.handles[handle_name] = dict(figure=None, axes=[], lines=[], text_labels=[])

        # the df_info data from will be used to store some information on the run, like start
        # date/time,
        self.df_info = None

        if self.mode == "monitoring":
            # we are monitoring time series, so not in the frequency domain
            self.frequency_domain = False
        else:
            self.frequency_domain = True

        # initialise the settings of the script by creating the settings object. If a configuration
        # file is given on the command line, this yaml file is loaded. Otherwise use the internal
        # defaults
        self.logger.debug(
            "Create Settings object with configuration_file={} and store_default_settings_to_{}"
            "and dump_settings {}".format(configuration_file, store_settings, dump_settings))

        self.settings = fut.Settings(configuration_file=configuration_file,
                                     store_default_settings_to_file=store_settings,
                                     dump_settings=dump_settings)
        self.settings.initialise_or_load_configuration()

        if time_zone is not None:
            if self.settings.time_zone != time_zone:
                self.logger.warning("Time zone specified out command line {} is different from"
                                    "settings file {}. Taking command line leading"
                                    "".format(time_zone, self.settings.time_zone))
            self.time_zone = time_zone
        elif self.settings.time_zone is not None:
            # take the time zone from the settings file
            self.time_zone = self.settings.time_zone
        else:
            raise AssertionError("Time zone not specified. Plot do this in the settings file or on "
                                 "the command line")

        self.start_time = pd.to_datetime("now").tz_localize("UTC").tz_convert(self.time_zone)

        if self.heading_name is None:
            try:
                self.heading_name = self.settings.gps_info_columns["heading"]
            except KeyError:
                self.heading_name = "HEADING"
                self.logger.info("No gps info column heading info found. Taking default heading "
                                 "name {} ".format(self.heading_name))

        self.logger.debug(self.settings)
        if self.store_settings or self.dump_settings:
            # this section allows you to generate a settings file
            if self.dump_settings:
                # we have requested to dump the currently loaded settings
                set_type = "current"
            elif self.store_settings:
                # no settings were externally loaded so the default settings are taken. This will be
                # dumped to file
                set_type = "default"
            else:
                set_type = "unknown"
            self.logger.info(
                "Only save the {} settings to {}. Run again without --store option to start "
                "the analyses:\n"
                "{} {}".format(set_type, configuration_file, os.path.split(sys.argv[0])[1],
                               configuration_file))
            sys.exit(0)

        if self.show_images:
            # over rull the setting in the settings file
            self.settings.show_images = True
            self.settings.set_data_dict("show_image", True)

        # create the directory if it does not yet exist (only if we create and save images)
        if self.settings.show_images and self.settings.save_images:
            make_directory(self.settings.image_directory)

        # plot the sn curves
        if self.settings.fatigue_settings is not None:
            if self.settings.fatigue_settings["show_sn_curves"]:
                fpl.plot_s_n_curve(self.settings)
                self.dump_s_n_curve()

        # set as global the MRU reference location
        try:
            self.mru_reference_location_name = self.settings.mru_reference_location_name
            self.mru_reference_location = np.array(
                self.settings.locations[self.settings.mru_reference_location_name]["coordinates"])
            self.logger.debug("MRU location : {} ".format(self.mru_reference_location))
        except KeyError as err:
            self.logger.debug("Tower origin name not recognised in data: {}".format(err))
            self.logger.debug("Please set a correct label for the tower origin")
            self.mru_reference_location = None

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # scan all files to be processed based on the regular expression defined in the settings
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        try:
            # check if we can set the start date and end date base on the settings input file
            start_date_time = self.settings.mdf_file_start_date_time
        except AttributeError:
            start_date_time = None
        try:
            end_date_time = self.settings.mdf_file_end_date_time
        except AttributeError:
            end_date_time = None

        # the end and start date are overruule by the command line options if given
        if self.monitoring_start_date_time:
            start_date_time = self.monitoring_start_date_time
        if self.monitoring_end_date_time:
            end_date_time = self.monitoring_end_date_time

        self.logger.info("Scanning Monitoring Data Directory: {}".format(
            self.settings.mdf_data_directory))
        self.file_list = list(scan_base_directory(self.settings.mdf_data_directory,
                                                  file_has_string_pattern=
                                                  self.settings.mdf_file_has_string_pattern,
                                                  extension=".mdf",
                                                  start_date_time=start_date_time,
                                                  end_date_time=end_date_time,
                                                  sort_file_base_names=True
                                                  ))
        self.logger.info("Start processing Monitoring files:")
        for cnt, file_name in enumerate(self.file_list):
            self.logger.debug("{:3d} : {}".format(cnt, file_name))
        self.logger.debug("-" * 40)

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Import the mdf data for the first file and create signal groups
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        try:
            # import first file to scan the dat fields
            self.mdf_first_file = mdf.MDFParser(
                self.file_list[0],
                import_data=False,
                replace_record_names=self.settings.replace_mdf_file_record_names,
                set_relative_time_column=True)
        except IndexError:
            raise FileNotFoundError(
                "Could not find any file matching the file pattern in directory {}"
                "".format(self.settings.mdf_data_directory))

        self.logger.debug("\n{}".format(self.mdf_first_file.header))

        self.logger.debug("\n{}".format("Names and labels found in the dataset records"))
        for record in self.mdf_first_file.dataset_records:
            self.logger.debug(
                "name: {:30s}    label: {:30s} unit: {}".format(record.name, record.label,
                                                                record.unit))

        # based on the regular expression in the settings file we make here a selection
        # of columns we want to import from a MDF file
        self.set_all_column_selections()

        if self.show_all_channel_names:
            self.mdf_first_file.import_data()
            self.mdf_first_file.make_report()
            sys.exit(0)

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # If the execute flag is true, we are going to either process all the MDF file (in the
        # monitoring mode) or do all the predictions along the voyages (in the *prediction* mode.
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if execute:
            print_banner("Start {} {} {} {} at {}"
                         "".format(os.path.split(__file__)[1], self.mode,
                                   __version__,
                                   self.configuration_file, self.start_time))
            # we can have two basic modes: 'monitoring' or one of the three other.
            if self.mode == "monitoring":
                # we requested monitoring, so perform the fms_live_monitoring method to
                # loop over all the MDF files and calculate the fatigue
                self.fatigue_monitoring_in_time_domain()
            else:
                # we want to do something else, either fatigue forecasting, or rao estimating
                self.fatigue_calculations_in_frequency_domain()

            try:
                # If we stopped because we added a stop file in the working directory, remove it
                os.remove(STOP_FILE_NAME)
            except OSError:
                pass

            # show the plots at the end
            print_banner("Done processing files.")
            if self.block_last_plot:
                self.logger.info("Close graph to finalize loop")
                plt.ioff()
                plt.show()

    def set_all_column_selections(self):
        """
        Set the column names based on the filters defined in the settings files

        Notes
        The following attributes are set in this method

        * *strain_gauges* : :obj:`DeviceGroups`
            All the strain gauges grouped per location at the vessel
        * *accelerometers* : :obj:`DeviceGroups`
            All the accelerometer information grouped per location at the vessel
        * *mru* : :obj:`ChannelGroups`
            All the MRU channel properties are collected in this group
        * *dls* : :obj:`ChannelGroups`
            All the DLS channel properties are collected in this group
        * *gps* : :obj:`ChannelGroups`
            All the GPS channel properties are collected in this group
        * *huisman* : :obj:`ChannelGroups`
            All the Huisman channel properties are collected in this group

        """

        # Start with the date time filter as this create a # time series include the milliseconds
        try:
            for reg_exp in self.settings.regular_expressions["record_names"]:
                self.mdf_first_file.set_column_selection([reg_exp])
        except KeyError:
            self.logger.warning("At least a record_name 'DateTime' must be given!")
            raise

        if self.settings.process_mru_signals:
            names, labels, groups = self.mdf_first_file.set_column_selection(
                self.settings.regular_expressions["signal_mru"], include_date_time=False)
            self.mru_group = fut.ChannelGroup("MRU", names=names, labels=labels, groups=groups)

        if self.settings.process_gps_signals:
            names, labels, groups = self.mdf_first_file.set_column_selection(
                self.settings.regular_expressions["signal_gps"], include_date_time=False)
            self.gps_group = fut.ChannelGroup("GPS", names=names, labels=labels, groups=groups)

        if self.settings.process_dls_signals:
            names, labels, groups = self.mdf_first_file.set_column_selection(
                self.settings.regular_expressions["signal_dls"], include_date_time=False)
            self.dls_group = fut.ChannelGroup("DLS", names=names, labels=labels, groups=groups)

        # create the column selection of the data we want to import based on the strain gauge and
        # accelerometer filters
        if self.settings.process_strain_gauge_signals:
            sg_names, sg_labels, sg_groups = self.mdf_first_file.set_column_selection(
                self.settings.regular_expressions["signal_strain_gauges"], include_date_time=False)
            self.strain_gauge_groups = fut.DeviceGroups(sg_names, sg_labels, sg_groups,
                                                        title="Strain Gauge")
            self.strain_gauge_groups.report()

            # we have defined the naming of the SG devices per group in the order as they appear in
            # the MDF file. The problem is that this order is assumed to be corresponding with the
            # (counter) clock wise rotating position of the SG devices around a tube where the
            # devices are mounted. This assumption is used in calculating the moments on the tube.
            # The problem is that in the new system it appears that the RX4 is order like
            # 0, 1, 3, 2, which means that the last two channels are swapped. Therefore in this
            # section we allow to define a new order for each device if given
            for i_group, (position, sg_group) in enumerate(
                    self.strain_gauge_groups.device_locations.items()):
                try:
                    strain_gauge_order = self.settings.locations[position]["strain_gauge_order"]
                except (KeyError, TypeError):
                    self.logger.debug(
                        "Location {} does not have a strain gauge order. No problem. Just keep "
                        "default order".format(position))
                else:
                    self.logger.debug("Changing the order of the strain gauges at {} to {}"
                                      "".format(position, strain_gauge_order))
                    channel_info_list = list()
                    for ii in strain_gauge_order:
                        channel_info = sg_group.get_channel_info_dict(ii)
                        self.logger.debug(
                            "Storing channel {} in the list with name {}".format(ii, channel_info[
                                "name"]))
                        channel_info_list.append(channel_info)
                    # now put the channel info back into the sg group
                    for ii, channel_info in enumerate(channel_info_list):
                        self.logger.debug(
                            "Putting back {} in the list with name {}".format(ii,
                                                                              channel_info["name"]))
                        sg_group.set_channel_info_dict(ii, channel_info)

        if self.settings.process_acceleration_signals:
            ac_names, ac_labels, ac_groups = self.mdf_first_file.set_column_selection(
                self.settings.regular_expressions["signal_accelerometers"], include_date_time=False)

            # group the accelerometers belonging to one location
            self.accelerometer_groups = fut.DeviceGroups(ac_names, ac_labels, ac_groups,
                                                         title="Accelerometer")
            self.accelerometer_groups.report()

        if self.settings.process_huisman_signals:
            self.huisman_group = fut.ChannelGroup("Huisman",
                                                  self.mdf_first_file.set_column_selection(
                                                      self.settings.regular_expressions[
                                                          "signal_huisman"]))

    def prepare_fatigue_data_base(self):
        """
        Prepare the data base in which the fatigue is stored
        """

        if self.settings.export_fatigue_database:
            # extract the fatigue data base file name from the settings
            self.fatigue_data_base_file = self.settings.get_fatigue_data_base_file_name()

            # check if we need to update the output file base based  on the start time
            if self.monitoring_start_date_time is not None or \
                    self.monitoring_end_date_time is not None:
                out_file, out_ext = os.path.splitext(self.fatigue_data_base_file)
                if self.monitoring_start_date_time is not None:
                    out_file += "_from_{}".format(self.monitoring_start_date_time)
                if self.monitoring_end_date_time is not None:
                    out_file += "_till_{}".format(self.monitoring_end_date_time)
                out_file += out_ext
                self.fatigue_data_base_file = out_file

            # initialise or read an old fatigue data base.
            self.read_or_init_fatigue_data_base()
        else:
            self.fatigue_data_base = None

        if self.clear_database:
            self.append_fatigue_data_base_to_file()
            self.logger.info("Cleared data base. Now quiting")
            sys.exit(0)

        if not self.dump_mdf_file_data:
            try:
                # in case dump_mdf_file was not passed on the command line argument, see if it
                # was set try in the settings file
                self.dump_mdf_file_data = self.settings.dump_mdf_data_to_file
            except AttributeError:
                pass

    def fatigue_monitoring_in_time_domain(self):
        """Calculate the fatigue based on the time series stored in the MDF files

        Notes
        -----

        * In this function the following it done

            1. Initialise or read an existing Fatigue DataFrame containing all the statics per 30
               min time frame
            2. Loop over all the files in the *file_list* input argument and process the file which
               means

               - Read the MDF file
               - Calculate stress and fatigue per monitoring channels
               - Calculate all statistics
               - Store  the results in teh Fatigue DataFrame
            3. Store the results in the data base file

        * The file loop (step 2) is done in the function *fatigue_monitoring_in_time_domain*.
        * This method loops over all the files present in the 'file_list'
        * Per file, the method *process_signal_time_series_of_current_records* is used to
          do all required steps to obtain the fatigue over a 30 min time span.
        * In *fatigue_monitoring_in_time_domain*  also the handling of the statistical data base
          is taken care of by reading old data and writing new data to the database
        """

        # before we start we are going to load the data base of a previous run or create a new one
        self.prepare_fatigue_data_base()

        self.logger.debug("Include list: {}".format(self.mdf_first_file.include_columns))

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Loop over the dat files, process all, store in the fatigue data base and create the plots
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        total_time = 0
        # here the *number_of_files_to_process* attribute is set
        self.count_number_of_files_to_process()
        if self.number_of_files_to_process == 0:
            self.logger.info("No files to process. Quitting")
            # return an empty dictionary, meaning that we do not have any data. Not so nice, but the
            # easiest way to solve this bug with the empty file list right now
        else:
            self.logger.debug("Start processing {} files".format(self.number_of_files_to_process))

        if self.settings.export_fatigue_database and not self.dry_run:
            # set the local flag to true if we are allowed to write to the data base
            write_to_database = True
        else:
            write_to_database = False

        progress = None
        wdg = fut.PROGRESS_WIDGETS

        last_coordinates = None
        last_distance = 0

        # loop over the file list with the mdf files and process them all
        number_processed = 0
        for cnt, file_name in enumerate(self.file_list):

            with Timer("process_sequence", verbose=False) as timer:

                # check if a stop file has been added to the directory. If so, make a clean stop
                if os.path.exists(STOP_FILE_NAME):
                    self.logger.info("Found STOP file in working directory. Aborting batch process "
                                     "and writing results...")
                    break

                # get the time stamp from the file name
                self.file_base_name = os.path.splitext(os.path.split(file_name)[1])[0]
                self.time_stamp = fut.get_time_stamp(self.file_base_name,
                                                     self.settings.mdf_file_base,
                                                     time_zone=self.time_zone)

                # if the time stamp is already in the data base, continue to tne next time step
                if self.check_if_monitoring_time_stamp_already_in_data_base():
                    message = "File  {} already processed. Skipping".format(self.file_base_name)
                    if self.progress_bar:
                        print(message)
                    else:
                        self.logger.info(message)
                    continue

                # update the process bar
                if self.progress_bar:
                    wdg[-1] = fut.MESSAGE_FORMAT.format(
                        fut.progress_bar_file(counter=number_processed + 1,
                                              maximum=self.number_of_files_to_process,
                                              file_name=self.time_stamp, total_time=total_time))
                    if progress is None:
                        # this is the first call, so initialise the progress bar
                        progress = pb.ProgressBar(widgets=wdg,
                                                  maxval=self.number_of_files_to_process,
                                                  fd=sys.stdout).start()
                    progress.update(number_processed + 1)
                    sys.stdout.flush()

                # print a banner for the current file to the screen
                print_banner("Processing file {}: {}".format(number_processed, file_name), "*")
                if self.dry_run:
                    # the dry_run option shows what would have happened without doing it
                    continue

                # read the mdf file and update the data link. If it fails, continue to the next time
                if not self.update_mdf_data_contents(file_name):
                    continue

                # only if requested also try to update the matlab data
                if self.settings.read_matlab_record_if_available:
                    self.update_matlab_data_contents(self.file_base_name)

                try:
                    # finally, do all you have to do: read the file, calculate stress and fatigue
                    self.process_signal_time_series_of_current_record()
                except ValueError as err:
                    self.logger.warning(
                        "Could not process this file: {}\nSkipping to next file".format(err))
                    continue

                print_banner("Updating General info sheet")
                last_coordinates, last_distance = \
                    self.update_general_info_sheet(last_coordinates=last_coordinates,
                                                   last_distance=last_distance)
                try:
                    export_time_series = self.settings.export_time_series
                except AttributeError:
                    self.logger.warning(
                        "Old fatigue_monitoring configuration file: no time series exporting yet")
                else:
                    if export_time_series:
                        self.export_time_series()

                if self.settings.show_images:
                    self.create_time_series_plots()

            # end of the over  file list. Increase total processed files and total time
            number_processed += 1

            total_time += timer.secs

            if self.dump_mdf_file_data:
                self.dump_the_mdf_file_to_excel()

            # if the n_fatigue_dump_every value it not none, dump the data base every n file
            if self.n_fatigue_dump_every is not None and write_to_database:
                if number_processed % self.n_fatigue_dump_every == 0:
                    self.logger.info("Dump every {} iteration: fatigue data base {}".format(
                        self.n_fatigue_dump_every, self.fatigue_data_base_file))
                    self.append_fatigue_data_base_to_file()

        # store the fatigue of this process in the return dictionary
        self.logger.debug("Return from here with  {}".format(self.fatigue_data_base))

        if number_processed > 0 and write_to_database:
            # dump the data base. The sorting on time is done as well
            self.logger.info("Exporting fatigue data base {}".format(self.fatigue_data_base_file))
            self.append_fatigue_data_base_to_file()

    def create_time_series_plots(self):
        """
        The plotting is done in this method
        """

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # All the plotting is done below
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if self.strain_gauge_groups and not self.frequency_domain:
            if self.settings.signal_strain_gauges_plot_properties["plot_this"]:
                print_banner("Plotting the strain gauge signals")
                fpl.plot_signal_strain_gauges(self.file_base_name, self.mdf_file,
                                              self.strain_gauge_groups,
                                              self.settings, self.mat_file,
                                              handles=self.handles["strains"])
            if self.settings.signal_hotspots_plot_properties["plot_this"]:
                print_banner("Plotting the hot spot gauge signals")
                fpl.plot_hot_spot_stresses(self.file_base_name, self.mdf_file,
                                           self.strain_gauge_groups,
                                           self.settings, self.mat_file,
                                           handles=self.handles["hotspots"])

        if self.settings.process_mru_signals and \
                self.settings.signal_mru_plot_properties[
                    "plot_this"] and not self.frequency_domain:
            print_banner("Plotting the MRU signals")
            # create a plot of the mru signal
            fpl.plot_signal_mru(self.file_base_name, self.mdf_file, self.mru_group.names,
                                self.settings,
                                handles=self.handles["mru"])

        try:
            plot_this = self.settings.signal_tower_origin_plot_properties["plot_this"]
        except AttributeError:
            self.logger.debug("No signal_tower_origin_plot_properties available. "
                              "Skipping")
        else:
            if plot_this and not self.frequency_domain:
                print_banner("Plotting the Tower origin accelerations")
                # create a plot of the mru signal
                fpl.plot_tower_origin_accelerations(self.file_base_name,
                                                    self.mdf_file,
                                                    self.settings,
                                                    self.mat_file, 0,
                                                    handles=self.handles["tower0"])
                fpl.plot_tower_origin_accelerations(self.file_base_name,
                                                    self.mdf_file,
                                                    self.settings,
                                                    self.mat_file, 1,
                                                    handles=self.handles["tower1"])

        if self.accelerometer_groups is not None and \
                self.settings.signal_accelerometers_plot_properties["plot_this"]:
            print_banner("Plotting the accelerometer signals")
            # create a plot of the acceleration signals
            fpl.plot_signal_accelerations(self.file_base_name, self.mdf_file,
                                          self.accelerometer_groups,
                                          self.settings,
                                          self.mat_file,
                                          handles=self.handles["acceleration"])

        if self.settings.process_huisman_signals and \
                self.settings.signal_huisman_plot_properties["plot_this"]:
            print_banner("Plotting the huisman signal")
            fpl.plot_signal_huisman(self.file_base_name, self.mdf_file,
                                    self.huisman_group.names,
                                    self.settings,
                                    handles=self.handles["huisman"])

    def update_mdf_data_contents(self, file_name):
        """
        Read the MDF file data based on the selection defined by the settings

        Parameters
        ----------
        file_name: str
            Name of the MDF file to read

        Returns
        -------
        bool:
            True on success, False on failure


        Notes
        -----
        In case a cached mdf file exist and the *read_mdf_cache* flag is set to true, files are
        store and read from message package files

        """
        msg_pack_file = os.path.splitext(file_name)[0] + ".msg_pack"
        if not self.reset_mdf_cache and (self.cache_mdf_read_or_write and os.path.exists(
                msg_pack_file)):
            # the cache_mdf_read_or_write flag is true and the file already exist: set the
            # import_data_from_mdf flag false as we are going to import from the message pack file
            import_data_from_mdf = False
        else:
            import_data_from_mdf = True

        try:
            # This create the mdf object and store it to mdf_file. In case import_data_form_mdf
            # is True, the actual data is imported as well. However, if we found a cache file,
            # we have set import_data_fro_mdf to False, which means only the header of the MDF is
            # read, and the binary data is read from the msg_pack binary file below, which is about
            # 100 x faster than reading from binary mdf.
            self.mdf_file = \
                mdf.MDFParser(file_name,
                              include_columns=self.mdf_first_file.include_columns,
                              set_relative_time_column=True,
                              replace_record_names=
                              self.settings.replace_mdf_file_record_names,
                              import_data=import_data_from_mdf)
        except (IOError, AttributeError) as err:
            self.logger.warning(err)
            self.logger.warning("Skipping {}".format(os.path.basename(file_name)))
            succeeded = False
        else:

            # success full read of the mdf file. See if we need to read the data still
            if not import_data_from_mdf:
                # up to now  we have only created MDFParser object without reading the binary data.
                # Read the data now from msg_pack and attach it to the data attribute
                self.logger.info("Reading from message pack cache file {}".format(msg_pack_file))
                self.mdf_file.data = pd.read_msgpack(msg_pack_file)
            elif self.cache_mdf_read_or_write:
                # the mdf file did not yet exist and we want to cache. So write the data to file
                self.logger.info("Writing mdf to msg pack cache file {}".format(msg_pack_file))
                self.mdf_file.data.to_msgpack(msg_pack_file)

            succeeded = True

        return succeeded

    def update_matlab_data_contents(self, file_base_name):
        """
        See if we can load the matlab file

        Parameters
        ----------
        file_base_name: str
            Base name of the matlab file (is the same as the mdf file but with a .mat extension

        Returns
        -------
        bool:
            True on success
        """
        try:
            self.mat_file = frc.FmsMatlabDatabase(file_base_name + ".mat")
            self.mat_file.create_time_series_database()
            self.mat_file.list_properties()
            self.logger.debug("Successful reading f matlabe file!")
            succeeded = True
        except IOError:
            succeeded = False

        return succeeded

    def process_signal_time_series_of_current_record(self):
        """
        Calculate all statistics of the stress and damage of the currently loaded MDF file

        Notes
        -----

        The following is done

        1. All data of the accelerometers is processed (*process_accelerometer_time_series*)
        2. All data of the mru's is processed (*process_mru_time_series*)
        3. All data of the strain gauges is processed (*process_strain_gauge_time_series*)
        4. After we have calculated all cold and hot spot stress, collect the statistics of the
           signals with *collect_statistics_cold_and_hot_spot_stress* into the Dataframe
           *fatigue_data_base*
        5. Finally, all the fatigue is calculated and collect in the *fatigue_data_base* as well

        """

        # process all the accelerometer data per location
        if self.accelerometer_groups is not None:
            self.process_accelerometer_time_series()

        # process all the mru data
        if self.settings.process_mru_signals:
            self.process_mru_time_series()

        # process all the strain gauge data per location
        if self.strain_gauge_groups is not None:
            self.calculate_all_stress_time_series()

            # collect all the cold and hot spot statistics into the fatigue_data_base Dataframe
            if self.fatigue_data_base is not None:
                # only collect the statistics if we have a fatigue_data_base data frame to
                # store the values so is we have set the option 'export_fatigue_data_base'
                # to false in the settings, we can skip this
                self.logger.info("Collecting all the cold and hto spot statistics")
                self.collect_statistics_cold_and_hot_spot_stress()

            print_banner("Calculating Fatigue with rain flow counting")
            self.calculate_fatigue_at_all_locations()

    def process_accelerometer_time_series(self):
        """
        All the processing of the accelerometer time series is done here

        Notes
        -----
        The following is done in this method

        1. Conversion of the raw mA signal to accelerations. Uses the conversion factors defined
           in the settings object given by Marin
        2. Optional: filter the acceleration signals with a filter of choice: kaiser, butterworth,
           block. Can be high pass, low pass of band pass filter
        3. Convert the (filtered) acceleration signals to the 6-DOF acceleration of the tower origin
           with *acceleration_to_dof_at_tower_origin*

        """
        print_banner("Converting the accelerometer signals and get the 6-DOF at the tower")
        # convert the channel signal of the accelerometer from ampere to m/s2
        self.signal_conversion_all_accelerometers()

        # see if we need to filter the accelerometer data
        if self.settings.filtering["accelerometers"]["type"] != "none":
            self.logger.debug("Filtering the acceleration signal")
            self.filter_all_accelerometer_signals()

        # convert the acceleration signals to a 6 dof at the tower origin
        self.acceleration_to_dof_at_tower_origin()

    def process_mru_time_series(self):
        """
        All the processing of the MRU time series is done here

        Notes
        -----
        The following is done

        1. Convert the 6-DOF MRU Motions to 6-DOF MRU accelerations (*mru_acceleration*)
        2. Only if we have accelerometer data: convert the acceleration from the tower to the MRU
           and the COG (*acceleration_to_dof_at_mru* and *acceleration_to_dof_at_cog*)
        3. Convert the MRU acceleration to the 6-DOF at the tower origin
           (*mru_motion_to_acceleration_at_tower_origin*) and at all the accelerometer location
           (*mru_motion_to_acceleration_at_tower_origin*)
        4. Convert the MRU 6-DOF motion, velocity, acceleration as defined in the settings file in
           the *dof_motions* section to the list of locations at the vessel

        """

        print_banner(
            "Converting the MRU signals and get the 6-DOF at the tower origin and COG")

        # calculate the smooth mru signal with the acceleration values
        self.mru_acceleration()

        if self.mru_reference_location is None:
            raise AssertionError(
                "mru_reference_location_name must be given. In the old Yaml file this "
                "was mru_location_name. But the location of the MRU is not "
                "necessarily the location where the data is logged. For the Balder set "
                "it to COG")

        if self.accelerometer_groups is not None:
            # this part is only required in case we have a tower origin
            if self.settings.signal_mru_plot_properties["show_accelerator_data"]:
                self.acceleration_to_dof_at_mru()
                self.acceleration_to_dof_at_cog()

            # convert the mru roll pitch heave acceleration to the tower origin
            self.mru_motion_to_acceleration_at_tower_origin()

            # convert the mru roll pitch heave acceleration to all the other locations
            self.mru_motion_to_acceleration_at_all_acc_locations()

        # now calculate all the transformed motions as well
        try:
            dof_motions = self.settings.dof_motions
        except AttributeError:
            self.logger.debug(
                "No dof motion monitoring positions defined. No problem, skip this")
        else:
            if dof_motions is not None:
                self.motion_to_all_dof_location(dof_motions=dof_motions)

    def calculate_all_stress_time_series(self):
        """
        All the processing of the strain gauge time series is done here

        Notes
        -----
        The following is done

        1. Convert the raw strain gauge signal from ampere to a strain using the coefficient in
           the settings object as given Marin
        2. Optionally: Filter the strain signal with the filter as defined in the settnig object
        3. Convert the strain gauge signal to stress based on the geometrical properties
        4. If we have accelerometer meter data, convert the 6-DOF motion at the tower origin
           calculated with *acceleration_to_dof_at_tower_origin* to the stress at the strain gate
           location using the SACS coefficients with
           *acceleration_dof_at_tower_origin_to_stress_at_cold_spots*
        5. Convert the cold spot stress obtained from both the strain gates (step 3) and the
           accelerometers (step 4.) to hot spot stress using the stress concentration factors
           with *cold_spot_stress_to_hotspots*
        """

        print_banner(
            "Converting the strain gauge signals and get calculate the Fatigue")

        # convert the strain gauge signal from amperes to a strain
        self.strain_gauges_signal_ampere_to_strain()

        if self.settings.filtering["strain_gauges"]["type"] != "none":
            # if the strain_gauge filter is not none, filter the signal with the filters
            self.logger.debug("Start filtering strain gauge signal")
            self.filter_strain_gauge_signal()

        # convert the strain signal into a stress. Do this after the filter so we only
        # have to filter one time
        self.logger.info("Convert strain to stress")
        self.strain_gauges_signal_strain_to_stress()

        if self.accelerometer_groups is not None:
            # convert the accelerometer data at the origin to the strain locations if
            # available
            mean_tower_angle = self.mdf_file.data[self.settings.tower_angle_name].mean()
            self.logger.info(
                "Tower origin acceleration based on ACC to stress at cold spots")
            self.acceleration_dof_at_tower_origin_to_stress_at_cold_spots(
                tower_angle=mean_tower_angle)

        # convert all the cold spot stress to the hot spots
        self.logger.info("All cold spot stress to hot spots")
        self.cold_spot_stress_to_hotspots_all_locations()

    def check_if_monitoring_time_stamp_already_in_data_base(self):
        """
        check if the current active was already processed before by looking in the fatigue data

        Returns
        -------
        bool:
            True if we have already processed this file

        """
        gps_longitude_name = self.settings.gps_info_columns["longitude"]
        gps_latitude_name = self.settings.gps_info_columns["latitude"]
        gps_distance_name = self.settings.gps_info_columns["distance"]
        already_processed = False

        if self.fatigue_data_base is not None:
            all_dates = self.fatigue_data_base.index.levels[1].get_level_values(
                "DateTime").unique()
            if self.time_stamp in all_dates:

                already_processed = True

                # if this time stamp was already processed, retrieve the location and travel
                # distance such that we can continue with the cumulative distance
                try:
                    gps_lon = self.fatigue_data_base.ix[(fut.GENERAL_SHEET_NAME,
                                                         self.time_stamp),
                                                        gps_longitude_name]
                    gps_lat = self.fatigue_data_base.ix[(fut.GENERAL_SHEET_NAME,
                                                         self.time_stamp),
                                                        gps_latitude_name]
                    gps_dis = self.fatigue_data_base.ix[(fut.GENERAL_SHEET_NAME,
                                                         self.time_stamp),
                                                        gps_distance_name]
                    self.last_coordinates = LatLon(gps_lon, gps_lat)
                    self.last_distance = Q_(gps_dis, "miles")
                except KeyError:
                    # ok, we do not have the right fields, so just continue
                    pass

        return already_processed

    def check_if_we_should_stop_processing(self, number_processed):
        """
        Check if we should stop processing because either a STOP file is found or we exceeded the
        maximum of number of voyages to calculate

        Parameters
        ----------
        number_processed: int
           Number of processed voyages so far

        Returns
        -------
        bool:
            True if we want to stop

        """
        stop_processing = False
        if fut.maximum_exceeded(number_processed, self.max_trajectories):
            self.logger.info(
                "Voyage id exceeds maximum of {}. Stop this voyage".format(self.max_trajectories))
            stop_processing = True
        if os.path.exists(STOP_FILE_NAME):
            self.logger.info(
                "Found STOP file in working directory. Aborting batch process and writing "
                "results...")
            stop_processing = True

        return stop_processing

    def check_if_this_voyage_should_be_skipped(self, voyage, id_voyage=None):
        """
        Check if we should skip this voyage because it has been processed already

        Parameters
        ----------
        voyage: :obj:`DataFrame`
            Reference to one row of the data base
        id_voyage: index field of voyage data frame
            The index of the voyage data base. Normally the voyage id number, but the first row
            is a string 'header', which should be skipped

        Returns
        -------
        bool:
            True if we want to skip this  voyage

        Notes
        -----
        There are three reasons why we want to skip this voyage

        * This is an empty voyage
        * This is not a value empty voyage
        * The start date/time of the current voyage is already in the database
        """
        skip_this_voyage = False
        if self.start_voyage_id is not None:
            if id_voyage < self.start_voyage_id:
                skip_this_voyage = True
        if not skip_this_voyage:
            try:
                if voyage.index.size == 0:
                    self.logger.info("Empty voyage. Please check your settings. Skipping this one")
                    skip_this_voyage = True
            except AttributeError:
                # in case voyage does not have a index, also skip it
                skip_this_voyage = True

        if id_voyage == "header":
            self.logger.info("processing data base with header{}".format(voyage))
            # this is not a voyage, so lower the counter and continue
            skip_this_voyage = True

        if not skip_this_voyage:
            if self.voyage_data_base_all is not None and not self.reset_database:
                # check if we have already processed this journey by comparing the start date/time
                # with the data base
                all_start_times = pd.to_datetime(
                    self.voyage_data_base_all["START_DATE_TIME"].values, utc=True)
                if pd.Timestamp(voyage.index.values[0]) in all_start_times:
                    skip_this_voyage = True
                    message = "Already processed voyage {} starting at {}. Skipping" \
                              "".format(id_voyage, voyage.index[0])
                    if self.progress_bar:
                        print(message)
                    else:
                        self.logger.info(message)

        return skip_this_voyage

    def fatigue_calculations_in_frequency_domain(self):
        """
        Calculate the motion and fatigue based on sea states data and an RAO in frequency domain

        Notes
        -----

        * This function can be executed in three modes

          1. Calculate Fatigue using an existing RAO when the *mode* is set to *prediction*
          2. Estimate the RAO using monitored time series (which preferable have been processed
             before using the mode 3) and a sea state 2D spectrum obtained from an external source
             such as NOAA or ARGOS when the *mode* is set to *rao_tune*
          3. Run the time series analyses of 1 and generate data files with power and cross spectral
             densities which can be used in the *rao_tune* mode to estimate the RAO
        """
        # The safe trans trans-fatigue analysis: calculate the fatigue based on the rao_vessel
        # data spectral data or tune the rao based on the time series data

        self.set_trajectory_data_base()

        start_message = "Start processing {}".format(
            self.settings.get_rao_vessel_data_base_file_names())
        if self.progress_bar:
            print(start_message)
        else:
            self.logger.info(start_message)

        out_dir = self.settings.rao_vessel["output_directory"]
        if out_dir is not None:
            self.logger.info(
                "Creating output directory {}".format(self.settings.rao_vessel["output_directory"]))
            make_directory(self.settings.rao_vessel["output_directory"])

        # read the RAO and do all preparations
        self.read_and_prepare_rao()

        # read the voyages with the sea states vs time
        self.read_voyage_database()

        # set the maximum number of voyages to process based on the user input and to total
        # available number of voyages
        if self.max_trajectories is None:
            # in case the max is not set via the command line, check if it was in the settings file
            self.max_trajectories = self.settings.rao_vessel["max_trajectories"]
        self.logger.debug("Found max trajectory to process: {}".format(self.max_trajectories))
        max_trajects_in_database = len(self.trajectory_data_base)
        if not self.max_trajectories:
            # in case max trajectories is still not set, just take the maximum number of voyages
            self.max_trajectories = max_trajects_in_database

        settings_rao_vessel = self.settings.rao_vessel
        settings_spectra = settings_rao_vessel["sea_state_spectra"]

        self.logger.debug("Running loop over: {}".format(self.max_trajectories))
        time_total = 0
        number_processed = 0
        for id_voyage, voyage in self.trajectory_data_base.items():

            # skip if we should stop are skip this voyage due to a STOP file or maximum exceeded
            if self.check_if_we_should_stop_processing(number_processed):
                break

            # Check if we should skip this voyage because it has been processed already
            if self.check_if_this_voyage_should_be_skipped(voyage, id_voyage):
                continue

            # ok we can not read the voyage or store it to the data base file if it does not yet
            # exist
            self.fatigue_data_base_file = \
                self.fatigue_data_base_file_base + "_voyage_id_{:04d}.xls".format(id_voyage)
            start_date = voyage.index[0]
            self.read_or_init_fatigue_data_base()
            self.file_base_name = self.fatigue_data_base_file_base
            file_location = os.path.split(self.fatigue_data_base_file_base)[0]

            try:
                voyage_date_times = voyage.index.tz_localize("UTC")
            except TypeError:
                voyage_date_times = voyage.index

            fatigue_data_base_date_times = self.fatigue_data_base.ix[GENERAL_SHEET_NAME].index

            # check if the date times in the data base are the same as in the current voyage.
            # if that is the case we have already calculated the voyage and can copy the values
            # to the data base
            if fatigue_data_base_date_times.equals(voyage_date_times):
                already_processed = True
            else:
                already_processed = False

            if not already_processed:
                # we have not yet processed this voyage. Do it now

                directions_in_deg = np.rad2deg(self.rao_camp.directions)
                frequencies_in_hz = array(self.rao_camp.frequencies) / (2 * pi)

                latitude_name = self.settings.gps_info_columns["latitude"]
                longitude_name = self.settings.gps_info_columns["longitude"]
                heading_name = self.settings.gps_info_columns["heading"]

                # build a series of 2D wave spectra based on the Hs/Tp/Theta of the wave components
                # and the headings (the wave spectra are stored with respect to the heading)
                # The wave spectra are used in the prediction routine to calculate the responses
                # via the RAO's
                self.spectrum_info = settings_spectra["spectral_components"]
                if settings_spectra["write_new_sea_state_psd_to_database"]:
                    file_prefix = os.path.splitext(settings_spectra["sea_state_psd_database_name"])[
                        0]
                    net_cdf = dr.NetCDFInfo(file_location=file_location, reset_netcdf=True,
                                            file_prefix=file_prefix)
                else:
                    net_cdf = None
                self.spectra_2d_time_series = \
                    dr.WaveSpectralComponentsReader(sea_state_data_base=voyage,
                                                    netcdf=net_cdf,
                                                    latitude_name=latitude_name,
                                                    longitude_name=longitude_name,
                                                    heading_name=heading_name,
                                                    data_base_name=GENERAL_SHEET_NAME,
                                                    spectral_components=self.spectrum_info,
                                                    wave_direction_coming_from=settings_spectra[
                                                        "wave_direction_coming_from"],
                                                    date_time=start_date,
                                                    directions_deg=directions_in_deg,
                                                    freq_in_hz=frequencies_in_hz)
                self.freq_in_hz_2d = self.spectra_2d_time_series.freq_in_hz_2d

                # start the processing of all the time steps in this voyage
                print_banner("Processing voyage {} of {} ({}/{})".format(
                    number_processed, self.max_trajectories, id_voyage, max_trajects_in_database))
                with Timer("voyage_damage_calculation", verbose=False) as timer:
                    self.predict_fatigue_for_this_voyage(voyage, id_voyage, time_total=time_total)
            else:
                with Timer("Skip damage calculation", verbose=False) as timer:
                    self.logger.info("Already processed voyage {} of {} at {} but not yet in data "
                                     "base. Store it now".format(id_voyage, self.max_trajectories,
                                                                 voyage_date_times[0]))

            time_total += timer.secs
            number_processed += 1

            if self.mode == "prediction":
                if not already_processed:
                    # write the current voyage to file. Only when we did not do this before
                    self.append_fatigue_data_base_to_file()

                # append the total damage of the current voyage to the total data base
                # also when we already processed as apparently it was not in the data base yet
                self.add_voyage_statistics_to_database(id_voyage, voyage)

                # if the n_fatigue_dump_every value it not none, dump the data base every n time
                if self.n_fatigue_dump_every is not None and self.voyage_data_base_all.size > 0:
                    if number_processed % self.n_fatigue_dump_every == 0:
                        self.logger.info("Dump every {} iteration: fatigue data base {}".format(
                            self.n_fatigue_dump_every, self.voyage_all_file_name))
                        try:
                            self.append_voyage_data_base_to_file()
                        except IOError:
                            self.logger.warning("Could not write to {}".format(
                                self.voyage_all_file_name))

        if self.mode == "prediction":
            if self.voyage_data_base_all.size > 0:
                self.append_voyage_data_base_to_file()
            else:
                self.logger.info("Could not append latest prediction as data base is empty")

    def read_and_prepare_rao(self):
        """
        Read the RAO and and do some preparations

        Notes
        -----

        The following preparations are done after reading the RAO

        * Loop over the velocity range and store the transformed RAO per velocity
        * Replace the phase of the RAO with the phase of an external source
        * Also apply the velocity correction to the RAO's of the hot spot stresses

        """

        self.apply_velocity_correction = self.settings.rao_vessel["rao_speed_correction"][
            "apply_correction"]

        # by default do not apply speed correction
        n_rao_velocities = 0
        create_rao_debug_plots = False
        # read the rao_speed correction section. Take care of backward compatibility in which
        # just a flag was given
        if self.apply_velocity_correction:
            # only if we want to create a rao speed correction we have to set a number of
            # discrete intervals to calculate the RAO speed shift in. The default min/max
            # velocity = 0 and 5 m/s (0 and 10 knots)
            n_rao_velocities = self.settings.rao_vessel["rao_speed_correction"][
                "n_velocity_bins"]
            max_speed = set_default_dimension(
                self.settings.rao_vessel["rao_speed_correction"]["maximum_speed"],
                "knots")
            create_rao_debug_plots = self.settings.rao_vessel["rao_speed_correction"][
                "create_debug_plots"]
        else:
            # the speed can be given in any units, as it will be transform to SI in the reader
            max_speed = Q_(0, "knots")

        # read the RAO data and create a data frame containing the complex amplitudes
        self.rao_camp = frc.RaoMatlabDatabase(self.settings.get_rao_data_file_name(),
                                              rao_settings=self.settings.rao_vessel,
                                              n_velocity=n_rao_velocities,
                                              debug_plot=create_rao_debug_plots,
                                              max_velocity=max_speed)
        try:
            # this section is only relevant when the rao_phase_data_file_name is defined and not
            # None. It replaces the  phase of the current RAO with the phase defined by the
            # rao_phase_data_file_name and write the new RAO to file
            rao_phase_data_file = self.settings.rao_vessel["rao_phase_data_file_name"]
        except KeyError:
            # a key error means that the rao_phase_data_file_name was not given or was None.
            # No problem, we just go on
            self.logger.info(
                "No rao phase defined. Take phase from primary RAO and continue with script")
            rao_phase_data_file = None
        else:
            # if we are here we have a rao_phase file which we are going to impose and write to file
            # then stop processing
            if rao_phase_data_file is None:
                raise KeyError
            rao_phase_data_file_core = os.path.splitext(os.path.split(rao_phase_data_file)[1])[
                0]
            rao_tmp = frc.RaoMatlabDatabase(rao_phase_data_file,
                                            rao_settings=self.settings.rao_vessel)
            self.logger.info("Replacing RAO phase with phase from {}".format(rao_phase_data_file))
            rao_mag = np.abs(self.rao_camp.RAOs)
            rao_ang = np.angle(rao_tmp.RAOs)
            self.rao_camp.RAOs = rao_mag * np.exp(1j * rao_ang)
            f_n = os.path.splitext(self.settings.get_rao_data_file_name())[
                      0] + "_phase_" + rao_phase_data_file_core + ".nc"
            self.logger.info("Writing new RAO file {}".format(f_n))
            self.rao_camp.create_netcdf_rao(rao_mag, f_n, save_as_complex=False)
            self.logger.info(
                "Stop script now. Remove  rao_phase_data_file_name field from settings to continue")
            sys.exit()

        tower_angle = self.settings.rao_vessel["tower_angle"]
        # loop over the velocity range and calculate the shifted rao for each velocity
        for velocity in self.rao_camp.velocity_range:
            speed_in_knots = velocity.to("knots")
            self.logger.info(
                "Transfer RAO acceleration to stress RAO at cold spots for tower angle {} velocity "
                "{} {}".format(tower_angle, velocity, speed_in_knots))

            if self.apply_velocity_correction:
                # only in case of rao speed correction we shift the data point to the proper RAO
                self.rao_camp.pick_rao_for_speed(speed_in_knots)

            if self.accelerometer_groups is not None:
                # not only we have to transform the sea state ROA, also the stress RAO need to be
                # transformed
                self.acceleration_dof_at_tower_origin_to_stress_at_cold_spots(
                    tower_angle=tower_angle)

                self.cold_spot_stress_to_hotspots_all_locations()

            if self.pick_maximum_rao_for_all_hotspots:
                # only needed for testing purpose
                self.logger.debug("Picking maximum RAO for all hot spots")
                self.pick_highest_response_at_all_locations()

            if not self.apply_velocity_correction:
                # if we are not correcting for the velocity we do not want to loop over the
                # velocities, only take v=0
                break

    def set_trajectory_data_base(self):
        """
        Set the data base containing the trajectories of the voyages to forecast
        """

        if self.matlab_trajectory_data_base:
            self.logger.info("Reading rao_vessel database from matlab input")
            # we have set the matlab_trajectory_data filename, which means the matlab data base must
            # be read in stead of the csv or msg_pack file
            self.settings.set_trajectory_file_name(self.matlab_trajectory_data_base)

            matlab_safe_trans_data_base = \
                frc.SafeTransMatlabDatabase(self.matlab_trajectory_data_base,
                                            start_date_string=self.start_date_string)
            self.trajectory_data_base = matlab_safe_trans_data_base.data_frames
            self.logger.info(self.trajectory_data_base["header"])
        elif self.meteo_consult_report:
            # reader of the PDF file
            self.logger.info("Reading the meteoconsult report")
            self.settings.set_trajectory_file_name(self.meteo_consult_report)
            self.settings.set_csv_trajectory_files([self.meteo_consult_report])
            meteoconsult_database = frc.MeteoConsultDatabase(self.meteo_consult_report)
            self.trajectory_data_base = meteoconsult_database.data_frames
        elif self.noaa_forecast_file:
            # reader of the noaa forecast file
            self.logger.info("Reading the NOAA forecast file {}".format(
                self.noaa_forecast_file))
            self.settings.set_trajectory_file_name(self.noaa_forecast_file)
            self.settings.set_csv_trajectory_files([self.noaa_forecast_file])
            noaa_forecast_database = frc.NOAADatabase(self.noaa_forecast_file)
            self.trajectory_data_base = noaa_forecast_database.data_frames
        elif self.hindcast_database:
            # option hindcast requires the excel data base generated by the monitoring tool over
            # being processed by the route_interpolation such that the Hs/Tp/dir are available also
            self.logger.info("Reading fatigue_monitoring data base {}".format(
                self.hindcast_database))
            # initialise or read an old fatigue data base.
            self.settings.set_trajectory_file_name(self.hindcast_database)
            self.settings.set_csv_trajectory_files([self.hindcast_database])
            fms_hindcast_database = frc.FMSDatabase(self.hindcast_database,
                                                    start_date_time=self.hindcast_start_date_time,
                                                    end_date_time=self.hindcast_end_date_time,
                                                    heading_name=self.heading_name)
            self.trajectory_data_base = fms_hindcast_database.data_frames
        else:
            self.logger.info("Reading rao_vessel database files")
            # in this section we read the trajectory information from either csv file as directly
            # generated by safe trans (multiple csv files can be given) or from a msg_pack file
            # which is always generated from the csv file after the first run. The msg_pack file
            # combines the trajectories of all the csv files and is in binary format, so it can be
            # read about 100 times faster. The csv file to read a initially taken from the yaml
            # configuration file (rao_vessel section with the csv file list), however, can here
            # also be overruled if the --trajectory_file option was given on the command line.
            csv_files_via_command_line = []
            for csv_file in self.trajectory_file:
                csv_files_via_command_line.append(csv_file[0])

            if bool(csv_files_via_command_line):
                self.logger.debug(
                    "Overruling the trajectory files in the settings file with the files from the "
                    "command line")
                self.logger.debug("{}".format(csv_files_via_command_line))
                self.settings.set_csv_trajectory_files(csv_files_via_command_line)

            if self.msg_pack_reset:
                # the option of the command line is leading
                self.read_msg_pack = False
            else:
                # otherwise, take the default choice from the configuration file
                self.read_msg_pack = self.settings.rao_vessel["read_msg_pack"]

            self.settings.set_safetrans_msg_data_base_file_name()

            self.read_safetrans_data_base()

    def count_number_of_files_to_process(self):
        """
        Count how many files we need to process

        Notes
        -----
        * Number of files to process is counted by looping over the files in the *file_list* and
          compare the date/time string in the file name with the date/times present in the
          fatigue_database.
        * If *reset_database* is true, we are going to process all the file in the file_list,
          which means that the results of the previous run are overwritten
        """
        if self.fatigue_data_base is not None and not self.reset_database:
            # we have a data base with the date/time stored which are already processed. Loop over
            # all the files to process and check how many file we still need to do
            self.number_of_files_to_process = 0
            time_list = self.fatigue_data_base.index.levels[1].get_level_values("DateTime")
            time_list = time_list.unique().astype(str)
            for cnt, file_name in enumerate(self.file_list):
                file_base_name = os.path.splitext(os.path.split(file_name)[1])[0]
                time_stamp = fut.get_time_stamp(file_base_name, self.settings.mdf_file_base,
                                                time_zone=self.time_zone)
                if not (str(time_stamp) in time_list):
                    self.number_of_files_to_process += 1
        else:
            self.number_of_files_to_process = len(self.file_list)

    @staticmethod
    def damage_at_position_name(position, prefix_damage="D"):
        """
        Create the name for the damage at the location  *position*

        Parameters
        ----------
        position : str
            Name of the position used to create the name

        prefix_damage : str
            Prefix used to add in front of the location to call it a damage. Default value = "D"

        Returns
        -------
        str:
            Name of the damage at the location *position*
        """
        # return the name of the sheet which stores the damage of the stresses for the position
        tmp = re.sub("\(|\)", "", re.sub("\s+", "_", position))
        return "{}_{}".format(prefix_damage, tmp)

    @staticmethod
    def stress_at_position_name(position, prefix_stress="S"):
        """
        Create the name for the stress at the location  *position*

        Parameters
        ----------
        position : str
            Name of the position used to create the name

        prefix_stress : str
            Prefix used to add in front of the location to call it a damage. Default value = "S"

        Returns
        -------
        str:
            Name of the stress at the location *position*
        """
        # return the name of the sheet which stores the damage of the stresses for the position
        tmp = re.sub("\(|\)", "", re.sub("\s+", "_", position))
        return "{}_{}".format(prefix_stress, tmp)

    @staticmethod
    def scf_at_position_name(position, prefix_scf="SCF_X"):
        """
        Create the name for the stress concentration factor at the position

        Parameters
        ----------
        position : str
            Name of the position used to create the name

        prefix_scf : str
            Prefix used to add in front of the location to call it a damage. Default value = "SCF_X"

        Returns
        -------
            Name of the stress concentratation factor at the location *position*
        """
        # return the name of the stress concentration factor of the stresses for the position
        tmp = re.sub("\(|\)", "", re.sub("\s+", "_", position))
        return "{}_{}".format(prefix_scf, tmp)

    def dump_s_n_curve(self):
        """Create a data dump of the sn curves"""

        file_name = os.path.join(self.settings.mdf_data_directory, "sn_curves.xls")

        self.logger.info("Writing sn curve data to {}".format(file_name))

        with pd.ExcelWriter(file_name, append=False) as writer:
            for location in list(self.settings.get_data_dict["locations"].keys()):
                try:
                    # get the sn data a location
                    sn_data = self.settings.sn_curve_data[location]
                except KeyError:
                    continue

                self.logger.info("adding location {}".format(location))
                # create a pandas data frame and safe to excel
                sn_db = pd.DataFrame(sn_data.T, columns=["N_cycles", "S_matlab", "S_wafo"])
                sn_db.set_index("N_cycles", drop=True, inplace=True)
                sn_db.to_excel(writer, sheet_name=location)

    def create_info_data_frame(self):
        """Create a data frame which hold all the information of this run"""

        log = get_logger(__name__)
        log.debug("Creating information data frame")
        # note that "now" return curent utc time with any time_zone. Therefore, first use
        # tz_localize to set the time zone to utc, and then tz_convert to convert to the local time
        # zone
        current_time = pd.to_datetime("now").tz_localize("UTC").tz_convert(self.time_zone)

        info_dict = OrderedDict()
        info_dict["script version"] = __version__
        info_dict["settings file"] = self.configuration_file
        info_dict["settings location"] = os.getcwd()
        info_dict["simulation start time"] = "{}".format(self.start_time)
        info_dict["simulation end time"] = "{}".format(current_time)
        info_dict["simulation duration"] = "{}".format((current_time - self.start_time))
        info_dict["time zone"] = self.time_zone
        info_dict["hmc_utils version"] = "{}".format(hmc_utils.__version__)
        info_dict["hmc_marine version"] = "{}".format(hmc_marine.__version__)
        info_dict["signal_processing version"] = "{}".format(signal_processing.__version__)
        info_dict["mlab_mdfreader version"] = "{}".format(mlab_mdfreader.__version__)
        info_dict["LatLon version"] = "{}".format(ll.__version__)

        self.df_info = pd.DataFrame.from_dict(info_dict, orient="index")
        self.df_info.index.name = "property"
        self.df_info.columns = ["value"]

    def append_fatigue_data_base_to_file(self, retry_after_failure=True):
        """ Append to new data base to file *fatigue_data_base_file*

        Parameters
        ----------
        retry_after_failure: bool, optional
            If true, after a failure to write, retry it with a random file name. Default = True
        """

        file_name = self.fatigue_data_base_file
        self.logger.debug("Sorting fatigue data base")

        # this trick is required to remove the duplicate indexes which may be there when multiple
        # cores were used
        # http://stackoverflow.com/questions/13035764/remove-rows-with-duplicate-indices-pandas-dataframe-and-timeseries
        groups = self.fatigue_data_base.groupby(level=self.fatigue_data_base.index.names)
        self.fatigue_data_base = groups.last()

        # now sort the indices
        self.fatigue_data_base.sort_index(inplace=True)

        self.logger.debug("Dumping fatigue data base to {}".format(file_name))
        general_columns = sorted(self.settings.general_info_sheet.keys())
        # always end the columns with the filename
        if not self.frequency_domain:
            general_columns.append("FILENAME")
        else:
            general_columns.extend(self.settings.rao_vessel['voyage_info_sheet'])

        self.logger.debug("Dump general_column  \n{}".format(general_columns))
        self.logger.debug("Dump fatigue index\n{}".format(self.fatigue_data_base.index))
        self.logger.debug("Dup fatigue_data_base db\n{}".format(self.fatigue_data_base))
        try:
            self.logger.debug(
                "AN fat db\n{}".format(self.fatigue_data_base.ix[GENERAL_SHEET_NAME][
                                           general_columns]))
        except KeyError:
            self.logger.debug("data base is still completely empty")

        # write the data to an excel file. Each slice is stored in a separate page
        self.logger.debug("Appending to {}".format(file_name))

        try:
            # write try to write it to the fatigue_data_base_file. If it fails, try again to
            # a random file name in order not to least a day of monitoring calculations
            self.data_base_to_excel(self.fatigue_data_base_file, general_columns=general_columns)
        except IOError:
            self.logger.warning("Failed writing to {}. ".format(self.fatigue_data_base_file))
            if retry_after_failure:
                # Try again with a random file name, probably the file was open by the user
                random_file_name = ''.join(random.choice(string.ascii_uppercase)
                                           for _ in range(8)) + ".xls"
                self.logger.warning("Creating back up to {}".format(random_file_name))
                self.data_base_to_excel(random_file_name, general_columns=general_columns)

    def data_base_to_excel(self, file_name, general_columns=None):
        """
        Write the data base to excel, including a general columns sheet

        Parameters
        ----------
        file_name: str
            Excel file name
        general_columns: :obj:`DataFrame`, option
            DataFrame containing the general info
        """
        with pd.ExcelWriter(file_name, append=True) as writer:
            # add an extra slide with running information
            try:
                self.create_info_data_frame()

                self.logger.debug("Writing the information sheet {}".format(INFO_SHEET_NAME))
                self.df_info.to_excel(writer, sheet_name=INFO_SHEET_NAME)
            except IndexError:
                self.logger.warning("Writing the information sheet failed")

            # first dump the general columns
            if general_columns is not None:
                try:
                    df = self.fatigue_data_base.ix[GENERAL_SHEET_NAME]
                    df.index = df.index.astype(str)
                    self.logger.debug("Writing the general sheet {}".format(GENERAL_SHEET_NAME))
                    df[general_columns].to_excel(writer, sheet_name=GENERAL_SHEET_NAME)
                except IndexError:
                    self.logger.warning("Writing the general info failed")

            # now loop over the rest to dump the stresses at all positions
            for (position, group_df) in self.fatigue_data_base.groupby(level="Position"):
                self.logger.debug("Appending sheet {}".format(position))
                if position == GENERAL_SHEET_NAME:
                    continue

                df = self.fatigue_data_base.ix[position]
                df.index = df.index.astype(str)

                # the position key starts with a D_ or a S_. Based on that store the damage or
                # stresses
                if position[0] == "D":
                    if not self.frequency_domain:
                        column_names = DAMAGE_COLUMNS
                    else:
                        column_names = HS_DAMAGE_ACC_NAMES
                elif position[0] == "S":
                    if not self.frequency_domain:
                        column_names = STRESS_COLUMNS
                    else:
                        column_names = STRESS_COLUMNS_ACC
                elif position[0] == "M":
                    column_names = M2_ACC_NAMES
                elif position[0] == "N":
                    column_names = NC_ACC_NAMES
                else:
                    raise AssertionError("Expected a D, S, M, or N as a first character in the "
                                         "column names")

                try:
                    df[column_names].to_excel(writer, sheet_name=position)
                except IndexError:
                    self.logger.warning("Failed writing position {}".format(position))
                    continue

    def append_voyage_data_base_to_file(self):
        """Append the data of the current data to file"""

        log = get_logger(__name__)

        log.info("Dumping VOYAGE statistics data base to {}".format(self.voyage_all_file_name))
        # voyage_columns = sorted(settings.rao_vessel["info_sheet"].keys())
        # remove the first column because that is the ID which we do not want to storre twice
        voyage_columns = self.voyage_data_base_all.columns
        voyage_columns = voyage_columns[voyage_columns != VOYAGE_INDEX_NAMES[1]]
        # writing of a multi-index is a bit tricky. You want to explicitly refer to the first and
        # second index level,otherwise these get written to the columns, which leads to nesting
        # of those columns as they are read as columns next time (so each run you got an extra
        # column with the name of the indices). To avoid this, use groupby to loop over the sheet

        # write the data to an excel file. Each slice is stored in a separate page
        with pd.ExcelWriter(self.voyage_all_file_name, append=True) as writer:

            try:
                for (sheet, group_df) in self.voyage_data_base_all.groupby(
                        level=VOYAGE_INDEX_NAMES[0]):
                    # the conversion of the sheet to str is necessary as excel can not handle
                    # Timestamps with timezones. by converting to a string first, you avoid an error
                    df = self.voyage_data_base_all.ix[sheet].astype(str)
                    df[voyage_columns].to_excel(writer, sheet_name=sheet)
            except IndexError:
                log.warning("Writing the general info failed")

    def add_voyage_statistics_to_database(self, id_voyage, voyage):
        """Add the current voyage to the total data base

        Parameters
        ----------
        id_voyage : id number of the current voyage

        voyage : :obj:`DataFrame`
            Current voyage

        """
        log = self.logger
        log.debug("Adding voyage {} to the total data frame".format(id_voyage))

        voyage_info = self.settings.rao_vessel["info_sheet"]
        voyage_info_columns = sorted(voyage_info.keys())

        # store the starting date time as START_DATE_TIME; then we can check if it was unique later
        self.voyage_data_base_all.ix[(VOYAGE_DICT_NAME, id_voyage), "START_DATE_TIME"] = \
            voyage.index[0]

        # just always add the info sheet to start with
        for column in voyage_info_columns:
            column_alias = voyage_info[column]["alias"]
            operation = voyage_info[column]["operation"]
            try:
                signal = voyage[column_alias]
            except KeyError:
                continue
            # we are in the time domain, so get the statistics from the time series directly
            if operation == "mean":
                stat_value = signal.mean()
            elif operation == "std":
                stat_value = signal.std()
            elif operation == "max":
                stat_value = signal.max()
            elif operation == "median":
                stat_value = signal.median()
            else:
                log.warning("operation {} not implemented. Set None".format(operation))
                stat_value = None

            self.voyage_data_base_all.ix[(VOYAGE_DICT_NAME, id_voyage), column] = stat_value

        for (position, group_df) in self.fatigue_data_base.groupby(level="Position"):
            if position == GENERAL_SHEET_NAME:
                continue

            # the position key starts with a D_ or a S_. Based on that store the damage or stresses
            if position[0] == "D":
                # the group contains all the columns of this location, with sum you add all the
                # columns, with the slice you select only the damage hot spot columns based on the
                # accelerometer between 0 and 7, and max give the maximum value. argmax() would give
                # the name of the locations as well
                max_hot_spot_damage = group_df.sum()["D_HS_AC0":"D_HS_AC7"].max()
                index_of_row = (VOYAGE_DICT_NAME, id_voyage)
                column_name = re.sub("\s+", "_", position)
                self.voyage_data_base_all.ix[index_of_row, column_name] = max_hot_spot_damage
                log.debug("Max Damage of Voyage {:3d} at {:15s}: {}".format(id_voyage, position,
                                                                            max_hot_spot_damage))

    def init_voyage_data_base(self, file_name):
        """Initialise a new data base for all the voyage with voyage ID as index and lots of
        properties in the volumes

        Parameters
        ----------
        file_name : str
            a list of file names we are going to process

        Returns
        -------
        dict:
            Voyage data base


        """

        # initialise the dictionary which is going to carry the dataframe for each location
        fms_database_dict = dict()

        # start the data base always with the start date of this journey
        voyage_info_columns = ["START_DATE_TIME"]
        # now add the columns which are defined in the info_sheet of the settings file
        voyage_info_columns.extend(sorted(self.settings.rao_vessel["info_sheet"].keys()))

        # finally create a column per position
        if self.strain_gauge_groups is not None:
            for i_group, (position, strain_group) in enumerate(
                    self.strain_gauge_groups.device_locations.items()):
                # exclude all location which were explicitly stored exclude_location list
                # (MAIN_BRACE)
                if self.settings is not None and position in self.settings.exclude_locations:
                    continue
                voyage_info_columns.append("D_{}".format(re.sub("\s+", "_", position)))

        # write the data to an excel file. Each slice is stored in a separate page
        with pd.ExcelWriter(file_name, append=False) as writer:
            # An excel is open: write per location all the fatigue channels

            # create the first sheet with some voyage info such as timestamp and file name
            fms_voyage = pd.DataFrame([], index=[], columns=voyage_info_columns)
            fms_voyage.index.name = VOYAGE_INDEX_NAMES[1]

            # append the voyage sheet to the dictionary containing all sheets
            fms_database_dict[VOYAGE_SHEET_NAME] = fms_voyage

            # write this sheet. Store it as a dictionary such that it appears a a seperated sheet
            fms_voyage.to_excel(writer, sheet_name=VOYAGE_SHEET_NAME)

        return fms_database_dict

    def init_fatigue_data_base(self, ):
        """Initialize a new data base containing the fatigue per record """

        log = self.logger

        # initialise the dictionary which is going to carry the dataframe for each location
        fms_database_dict = dict()

        general_info_columns = sorted(self.settings.general_info_sheet.keys())
        if not self.frequency_domain:
            general_info_columns.append("FILENAME")
        else:
            general_info_columns.extend(sorted(self.settings.rao_vessel['voyage_info_sheet']))

        # write the data to an excel file. Each slice is stored in a separate page
        with pd.ExcelWriter(self.fatigue_data_base_file, append=False) as writer:
            # An excel is open: write per location all the fatigue channels

            # create the first sheet with some general info such as timestamp and file name
            fms_general = pd.DataFrame([], index=[], columns=general_info_columns)
            fms_general.index.name = INDEX_NAME

            # append the general sheet to the dictionrary containing all sheets
            fms_database_dict[GENERAL_SHEET_NAME] = fms_general

            fms_general.to_excel(writer, sheet_name=GENERAL_SHEET_NAME)

            if self.strain_gauge_groups is not None:
                for i_group, (position, strain_group) in enumerate(
                        self.strain_gauge_groups.device_locations.items()):

                    # exclude all location which were explicitly stored exclude_location list
                    # (MAIN_BRACE)
                    if self.settings is not None and position in self.settings.exclude_locations:
                        continue

                    log.debug("creating data base entry for position {} names={}"
                              "".format(position, strain_group.names))

                    # a data frame for all cold and hot spots channels based on the accelerometer
                    # and strain gauges
                    # actually, forget about the cold spot fatigue.
                    # cs_stress_sgs_names = ["CS_SG{}".format(i) for i in range(4)]
                    # cs_stress_acc_names = ["CS_AC{}".format(i) for i in range(4)]
                    for char in ["D", "S", "M2", "NC"]:
                        # the position key starts with a D_ or a S_. Based on that store the damage
                        # or stresses
                        if char == "D":
                            if not self.frequency_domain:
                                column_names = DAMAGE_COLUMNS
                            else:
                                column_names = HS_DAMAGE_ACC_NAMES
                        elif char == "S":
                            if not self.frequency_domain:
                                column_names = STRESS_COLUMNS
                            else:
                                column_names = STRESS_COLUMNS_ACC
                        elif char == "M2":
                            column_names = M2_ACC_NAMES
                        elif char == "NC":
                            column_names = NC_ACC_NAMES
                        else:
                            log.warning("Something is wrong here")
                            raise AssertionError("char not one of the following: [D|S|M2|NC]. "
                                                 "Got {}".format(char))

                        # create new data frame for the colunms of eather a stress (char=='S')
                        # or damage ('D')
                        fms_hotspots = pd.DataFrame([], columns=column_names, index=[])
                        fms_hotspots.index.name = INDEX_NAME

                        # store this data base in the dictionary
                        sheet_name = fut.damage_at_position_name(position, char)
                        fms_database_dict[sheet_name] = fms_hotspots

                        # add this location as a separate sheet to the excel file
                        fms_hotspots.to_excel(writer, sheet_name=sheet_name)

        return fms_database_dict

    def read_safetrans_data_base(self):
        """Read  the safetrans data base which is a dictionary with each entry one

        Notes
        -----
        * The data base is read by default from a list of csv files as written by SafeTrans.
        * After reading the original csv files, a dictionary containing one entry per voyage is
          dumped as a message pack file (which is binary json).
        * The next time, this file can be loaded which is about 100x faster than reading the csv
          file.
        """

        log = self.logger

        rao_vessel = self.settings.rao_vessel

        # set the flag to read the message flag (if false, the csv is alway read) and the
        # message pack and csv list of file names
        self.read_msg_pack = rao_vessel["read_msg_pack"]
        msg_pack_file = self.settings.get_safetrans_msg_data_base_file_name()
        safetrans_file_names = self.settings.get_rao_vessel_data_base_file_names()

        # store all the voyage in this data base
        trajectory_data_frames = None

        # first read the message pack data file
        if self.read_msg_pack and os.path.exists(msg_pack_file):
            log.info(
                "Reading the rao_vessel msg_pack trajectory database {}.".format(msg_pack_file))
            try:
                with Timer(name="read_msgpack") as t:
                    trajectory_data_frames = pd.read_msgpack(msg_pack_file)
                log.debug("Successfully read the data base")
            except IOError as err:
                log.warning(err)
                raise

        if trajectory_data_frames is None:
            # If trajectory_data_frame is none the csv files are read again
            log.info(
                "Reading the rao_vessel csv trajectory files {}.".format(safetrans_file_names))
            with Timer(name="read_rao_vessel_from_csv_files") as t:
                trajectory_data_frames = self.read_safetrans_from_csv_files(safetrans_file_names)

            # read the data base from the csv file. Now dump it to the msg_pack file
            try:
                log.info("Dumping data base to {}".format(msg_pack_file))
                pd.to_msgpack(msg_pack_file, trajectory_data_frames)
            except IOError as err:
                log.warning(err)
                raise

        self.trajectory_data_base = trajectory_data_frames

    @staticmethod
    def msg_pack_information_dictionary(source_name="Safetrans", source_file_list=[]):
        """

        Parameters
        ----------
        source_name :
            (Default value = "Safetrans")
        source_file_list :
            (Default value = [])

        Returns
        -------


        """
        info_dict = dict(
            header=dict(
                infomation="Trajectory database generated from {}".format(source_name),
                source_file_list=source_file_list,
                generation_date=datetime.datetime.now())
        )
        return info_dict

    def read_safetrans_from_csv_files(self, rao_vessel_file_names):
        """

        Parameters
        ----------
        rao_vessel_file_names : str
            Name of the RAO of the vessel

        """
        log = self.logger

        data_frames = dict(
            information=
            self.msg_pack_information_dictionary(source_name="Safetrans",
                                                 source_file_list=rao_vessel_file_names)
        )

        for i_file, file_name in enumerate(rao_vessel_file_names):

            log.debug("Reading the rao_vessel trajectory file {}. This may take a while.."
                      "".format(file_name))
            try:
                self.read_safetrans_file(file_name, data_frames=data_frames)
            except IOError as e:
                log.warning("{}\nSkipping...".format(e))

        log.info("Read {} voyages".format(len(data_frames)))

        return data_frames

    def read_safetrans_file(self, file_name, data_frames):
        """
        Reader for one single Safetrans voyage file

        Parameters
        ----------
        file_name: str
            Voyage file name
        data_frames : dict
            Dictionary holding all the data frames. Each data frame represents one voyage

        Notes
        -----

        * Each safetrans file contains multiple voyages. Each voyage is stored as a separate entry
          in the the data_frames dictionary

        """

        log = self.logger

        # subtract one because we have one key with the information sheet
        total_number_of_voyages = len(data_frames) - 1  # start with the total of currently read
        # voyages
        column_names = list()
        column_values = list()

        # selection of columns to import. Based on the names in the csv file with all spaces
        # replaced to underscores _the second values is the replacement of the names to get more
        # convenient naming
        columns_selection = list([
            ("t_abs", "date_time"),
            ("t_rel", "travel_time"),
            ("pos_lat", "latitude"),
            ("pos_lon", "longitude"),
            ("distance_travelled", "travel_distance"),
            ("sustained_speed", "speed_sustained"),
            ("significant_wave_height_total", "Hs_tot"),
            ("peak_wave_period_total", "Tp_tot"),
            ("mean_wave_direction_total", "direction_wave_tot"),
            ("significant_wave_height_windsea", "Hs_wind"),
            ("peak_wave_period_windsea", "Tp_wind"),
            ("mean_wave_direction_windsea", "direction_wave_wind"),
            ("significant_wave_height_swell", "Hs_swell"),
            ("peak_wave_period_swell", "Tp_swell"),
            ("mean_wave_direction_swell", "direction_wave_swell"),
            ("wind_speed", "speed_wind"),
            ("wind_direction", "direction_wind"),
            ("current_speed", "speed_current"),
            ("current_direction", "direction_current"),
            ("speedThroughWater", "speed_trough_water"),
            ("water_depth", "depth"),
            ("heading", "heading"),
            ("drift_angle", "direction_drift")
        ])

        n_head_line_of_voyage = None
        settings_rao_vessel = self.settings.rao_vessel
        settings_sea_state_spec = settings_rao_vessel["sea_state_spectra"]

        with open(file_name, 'r') as fp:

            # retrieve all the lines
            lines = fp.readlines()

            if self.progress_bar:
                max_lines = len(lines)
                wdg = PROGRESS_WIDGETS
                wdg[-1] = MESSAGE_FORMAT.format(fut.progress_bar_message_reading(0))
                progress = pb.ProgressBar(widgets=wdg, maxval=max_lines,
                                          fd=sys.stdout).start()
            else:
                progress = None

            # loop over the lines of this csv file
            for cnt, line in enumerate(lines):

                if self.progress_bar:
                    progress.update(cnt)
                    sys.stdout.flush()

                # get all the columns
                columns = line.strip().split(',')

                # only with more than 3 columns we have a data line. With less, read the
                # info in the first column
                if len(columns) < 3:
                    if column_values:
                        # we have read a voyage already, so store it in a data frame  and
                        # clear the lists of values.

                        # replace all white spaces in the column names with underscores
                        names = [re.sub("\s+", "_", col_name) for col_name in column_names]

                        # create a DataFrame from from the list of values.
                        df = pd.DataFrame(data=column_values, columns=names)

                        # create a list of columns to select and the corresponding alias
                        # names
                        selection = list()
                        aliases = list()
                        for (col_name, alias) in columns_selection:
                            if col_name in df.columns:
                                # we have selected this column and it also exist in the data
                                # frame.
                                # Add it to the selection list and aliases
                                selection.append(col_name)
                                aliases.append(alias)
                            else:
                                log.warning("Column {} not in csv file".format(col_name))

                        # select the columns we want
                        df = df[selection]

                        # replace the names with the aliases
                        df.columns = aliases

                        # convert the string value into floats
                        df = df.apply(pd.to_numeric, errors='ignore')

                        # convert the datetime string (first column) into a TimeStamp and set index
                        df["date_time"] = pd.to_datetime(df["date_time"])
                        df.set_index("date_time", inplace=True)

                        # hard code the time per sea state in the rao_vessel file. Change later
                        # TODO: make time_per_sea_state an input variable
                        time_per_sea_state = 3  # time per sea state in hours
                        df = df[df["travel_time"] % time_per_sea_state == 0]

                        # store the time per sea state in the data frame
                        df["time_per_sea_state"] = time_per_sea_state

                        # store the relative direction with the respect to the heading
                        # rao_vessel the wave direction means 'going_to' in argos 'coming_from',
                        # to the argos wave direction should be shifted 180 to get a normal
                        # definition also, note that the RAO direction definition is defined via the
                        # counter-clockwise angle So a relative wave direction of 90 means that the
                        # waves are going to the west (90) if the # bow is to the north (0)
                        if settings_sea_state_spec["wave_direction_coming_from"]:
                            offset = 0
                        else:
                            offset = 180
                        for wave_type in ("wind", "swell", "tot", "swell2"):
                            dir_name = "direction_wave_" + wave_type
                            rel_dir_name = RELATIVE_DIRECTION_PREFIX + dir_name
                            try:
                                df[rel_dir_name] = np.mod(
                                    df["heading"] - (df[dir_name] - offset), 360)
                            except KeyError:
                                log.debug(
                                    "rel_direction name not calculated for wave {}".format(
                                        wave_type))

                        # store the current seastate in the dictionary
                        data_frames[total_number_of_voyages] = df

                        # we have stored this voyage, so increase the counter and reset the lists of
                        # values
                        total_number_of_voyages += 1
                        column_names = list()
                        column_values = list()

                    # in case with 2 or less columns we can read some info
                    if bool(re.match("Created: .*", columns[0])):
                        # print the line with creating date
                        log.debug("Reading file {}".format(columns[0]))
                    is_nv = re.match("Number of Voyages: (\d+)", columns[0])
                    if bool(is_nv):
                        n_voyages = int(is_nv.group(1))
                        log.debug("Number of voyages to read: {}.".format(n_voyages))

                    # we are entering a new voyage. Set the header to 2 and read the id
                    is_id = re.match("Voyage id: (\d+)", columns[0])
                    if bool(is_id):
                        n_head_line_of_voyage = 2
                        voyage_id = int(is_id.group(1))
                        if self.progress_bar:
                            wdg[-1] = MESSAGE_FORMAT.format(
                                fut.progress_bar_message_reading(voyage_id))
                        else:
                            log.info("Reading voyage # {}.".format(voyage_id))
                else:
                    # with more than 2 columns we are reading the data lines
                    if n_head_line_of_voyage == 2:
                        # this is the first row of a new voyage, which contains the names of the
                        # columns
                        column_names = columns
                        n_head_line_of_voyage -= 1
                    elif n_head_line_of_voyage == 1:
                        # this is the second row of a new voyage, which contains the units of the
                        # columns just skip it
                        n_head_line_of_voyage -= 1
                    else:
                        # all the rest contains the values. Just store them to a big list. The
                        # conversion will take place when converting the list to a data frame above
                        column_values.append(columns)

            # end of loop with cnt. Close the progress bar
            if self.progress_bar:
                progress.finish()

    def read_voyage_database(self):
        """
        Read the data base of the voyages

        Notes
        -----

        * The data base is read from an excel file data base which is previously stored from
          the save trans csv files.
        * After reading the csv files the data base will be stored to excel as this is much faster
          to read the next time you run the code
        """

        if self.fatigue_data_base_file_base is None:
            self.fatigue_data_base_file_base = \
                self.settings.get_fatigue_rao_vessel_output_database()

        # initialise the overall voyage database which hold the information per voyage (with the
        # index of the voyage ID)
        self.voyage_all_file_name = self.fatigue_data_base_file_base + "_voyages.xls"

        if not self.reset_database and os.path.exists(self.voyage_all_file_name):
            self.logger.debug("Reading the voyage  data base {}".format(self.voyage_all_file_name))
            try:
                voyage_db_dict = pd.read_excel(self.voyage_all_file_name, sheetname=None)
                # in the old version the data was sometimes saved with double datetime entries.
                # Here you can drop them
                # df.drop_duplicates(subset='Index', keep='last', inplace=True)
                # now base the dataframe index on the Datatime stored in the datatime column
                # df.set_index("DateTime", inplace=True, drop=True)
            except IOError:
                raise IOError(
                    "Failed reading the fatigue_monitoring data base. Do not generate any")
        else:
            # in any other cases: generate the data base from scratch

            self.logger.debug(
                "Creating voyage data base in Excel file {}".format(self.fatigue_data_base_file))
            out_dir = os.path.split(self.voyage_all_file_name)[0]
            make_directory(out_dir)

            voyage_db_dict = self.init_voyage_data_base(self.voyage_all_file_name)

        # put the data frames of all locations into one data frame
        self.logger.debug("concat with keys {}".format(list(voyage_db_dict.keys())))

        self.voyage_data_base_all = pd.concat(voyage_db_dict, names=VOYAGE_INDEX_NAMES)

    def read_or_init_fatigue_data_base(self):
        """
        Read the database from the Excel file containing the previously calculated statistics or
        initialize an empty data base in case the reset_database attribute is True or the file
        does not yet exist

        Returns
        -------
        :obj:`Dataframe`
            Database stored in a Pandas data frame with the statistics of previous runs
        """

        # get the time stamps from the file name list
        self.get_time_stamps_from_file_list()

        if not self.reset_database and os.path.exists(self.fatigue_data_base_file):
            self.logger.debug("Read the fatigue data base {}".format(self.fatigue_data_base_file))
            try:
                fms_database_dict = pd.read_excel(self.fatigue_data_base_file, sheetname=None)
            except IOError:
                self.logger.warning("Failed reading the fatigue_monitoring data base. Stop now")
                raise
            else:
                # remove the info sheet
                fms_database_dict.pop(INFO_SHEET_NAME, None)
                for df in fms_database_dict.values():
                    # in the old version the data was sometimes saved with duble datetime entries.
                    # Here you can drop them
                    df.drop_duplicates(subset='DateTime', keep='last', inplace=True)
                    # now base the dataframe index on the Datatime stored in the datatime column
                    df.set_index("DateTime", inplace=True, drop=True)
                    df.index = pd.to_datetime(df.index, utc=True).tz_convert(self.time_zone)
        else:
            # in any other cases: generate the data base from scratch
            self.logger.debug("Creating new data base in Excel file {}"
                              .format(self.fatigue_data_base_file))

            fms_database_dict = self.init_fatigue_data_base()

        # put the data frames of all locations into one data frame
        self.logger.debug("concat with keys {}".format(list(fms_database_dict.keys())))
        self.fatigue_data_base = pd.concat(fms_database_dict, names=["Position", "DateTime"])

        # return a multi-index data base (position, time) with columns multi-index (ACC_HOT,
        # H0) etc. If you do:
        # fms_database_all.info() =
        # MultiIndex: 9 entries, (AD(PS), 111122 T040000_UTC25958) to(SL1, 111122 T040000_UTC25958)
        # Data columns (total 19 columns):
        # (ACC_HOT, H0)       0 non-null float64
        # (ACC_HOT, H1)       0 non-null float64
        # (ACC_HOT, H2)       0 non-null float64

    def get_time_stamps_from_file_list(self):
        """turn the *file_list* with all the MDF file in a list of bare time stamps stored in
        *file_time_stamps*
        """

        self.file_time_stamps = list()
        for mdf_file in self.file_list:
            time_stamp = fut.get_time_stamp(mdf_file, self.settings.mdf_file_base)
            self.file_time_stamps.append(time_stamp)

    def signal_conversion_all_accelerometers(self):
        """Convert the signal of the accelerometers from amperes to m/s2 for all devices at
        all locations"""

        # loop over all locations. Store the devices at one location in acc_group
        for i_group, (position, acc_group) in enumerate(
                self.accelerometer_groups.device_locations.items()):
            self.logger.debug("converting signal amperes {} {} {}".format(position, acc_group.names,
                                                                          acc_group.labels))

            # loop over all devices at this location (stored in acc_group)
            for i_channel, name in enumerate(acc_group.names):
                component_label = acc_group.components[i_channel]
                short_label = acc_group.short_labels[i_channel]

                # in case the component label is in the Z direction, correct for the gravity
                if re.search("Z", component_label, re.IGNORECASE):
                    g_correction = True
                else:
                    g_correction = False

                self.ampere_to_acceleration(name, short_label, correct_for_gravity=g_correction)

    def ampere_to_acceleration(self, name, short_label, correct_for_gravity=False):
        """
        Convert the signal from ampere to acceleration of device 'name'

        Parameters
        ----------
        name: str
            Name of the current device
        short_label: str
            Short label of the current channel
        correct_for_gravity: bool
            If true subtract the gravity constant. Only used for the vertically placed
            accelerometers

        Returns
        -------
        str:
            The new name of the acceleration signal
        """

        ds_acc = self.mdf_file.data[name]

        # get the conversion coefficients from the settings
        acc_coef = self.settings.signal_accelerometers_coefficients[short_label]
        self.logger.debug("coefficients {} : {}".format(short_label, acc_coef))

        # have loaded the accelerometer coefficients of this devices as report by Marin
        # we can use those to convert the signal in mA to an acceleration
        values = fut.convert_ampere(ds_acc.values, acc_coef[0], acc_coef[1], acc_coef[2])

        if correct_for_gravity:
            values -= g0

        # build a new name of the convert data and store it in the mdf_file Data frame
        new_name = fut.accelerometer_name_of_converted_signal(name)
        self.logger.debug("storing converted signal to {}".format(new_name))
        self.mdf_file.data[new_name] = values

        return new_name

    def strain_gauges_signal_ampere_to_strain(self):
        """Convert the signal of the strain_gauges from amperes to strain for all groups"""

        log = self.logger

        for i_group, (position, strain_group) in enumerate(
                self.strain_gauge_groups.device_locations.items()):
            log.debug("converting signal amperes {} {} {}".format(position, strain_group.names,
                                                                  strain_group.labels))

            for i_channel, name in enumerate(strain_group.names):
                short_label = strain_group.short_labels[i_channel]

                # df refers to the data column holding the raw strain gauge data
                df = self.mdf_file.data[name]

                # create the new names for the strain and the stress
                new_name_strain = fut.strain_name_of_converted_signal(name, position, i_channel)

                # get the conversion coefficients stored in the settings which turn the A to strain
                sg_coef = self.settings.signal_strain_gauges_coefficients[short_label]
                log.debug("coefficients {} : {}".format(short_label, sg_coef))

                # store the converted signal (strain) in the data frame
                self.mdf_file.data[new_name_strain] = fut.convert_ampere(df.values, sg_coef[0],
                                                                         sg_coef[1],
                                                                         sg_coef[2])

    def strain_gauges_signal_strain_to_stress(self):
        """convert the strain signal of the strain_gauges to a stress by multiplying with Young's
        modules This is done in a separate routine such what we only have to apply the filter to the
        strain

        """

        log = self.logger

        # get the Young's modules in order to convert strain to stress
        Eyoung = self.settings.material_properties["E_youngs_modulus"]
        nu = self.settings.material_properties["nu_poison_coefficient"]
        previous_name_strain = None

        for i_group, (position, strain_group) in enumerate(
                self.strain_gauge_groups.device_locations.items()):
            log.debug("converting signal strain {} {} {}".format(position, strain_group.names,
                                                                 strain_group.labels))

            for i_channel, name in enumerate(strain_group.names):

                label_name = strain_group.short_labels[i_channel]

                # create the new names for the strain and the stress
                name_strain = fut.strain_name_of_converted_signal(name, position, i_channel)
                name_stress = fut.stress_name_of_converted_signal(name, position, i_channel)

                # turn the strain into a stress
                if self.settings.use_poison_for_pivots and bool(
                        re.match("PIV.*", position)) and i_channel > 0:
                    # this part takes care of the pivot position which only has 3 channels. The
                    # second channel will contain
                    # the true stress as calculated with the poison ration this part is only needed
                    # for the old roset configuration. But normally this code is obsolete
                    # as the pivots are in a line now so should not be carried out in the new system
                    log.info("Store stress signal {} based on  strain {} FOR PIV with nu={}".format(
                        name_stress,
                        name_strain, nu))
                    sig1 = self.mdf_file.data[previous_name_strain]
                    sig2 = self.mdf_file.data[name_strain]
                    self.mdf_file.data[name_stress] = Eyoung / (1 - nu ** 2) * (sig2 + nu * sig1)
                else:
                    # only for the none pivot position calculate stress by normal multiplication
                    log.debug("Store stress signal {} based on  strain {} : {}".format(name_stress,
                                                                                       name_strain,
                                                                                       label_name))
                    self.mdf_file.data[name_stress] = self.mdf_file.data[name_strain] * Eyoung

                # store the name for the next step, needed for PIV
                previous_name_strain = name_strain

    def acceleration_to_dof_at_tower_origin(self):
        """ Calculate the 6-DOF acceleration at the tower origin """
        try:
            self.logger.debug("get origin at {} ".format(self.settings.tower_origin_name))
            tower_origin = set_default_dimension(
                self.settings.locations[self.settings.tower_origin_name]["coordinates"],
                "m").to_base_units().magnitude
            self.logger.debug("Tower origin : {} ".format(tower_origin))
        except KeyError as err:
            self.logger.debug("Tower origin name not recognised in data: {}".format(err))
            self.logger.debug("Please set a correct label for the tower origin")
            raise AssertionError

        # calculate the six dof acceleration at the tower origin location
        six_dof = self.acceleration_to_dof_at_location(tower_origin)

        # add the result also to the data frame of the current mdf_file
        for i, comp in enumerate(fut.six_dof_names(self.settings.tower_origin_acc_name)):
            self.logger.debug("Adding channel {}/{} to the data frame".format(i, comp))
            self.mdf_file.data[comp] = six_dof[i, :]

    def acceleration_to_dof_at_location(self, location):
        """Calculate the 6-dof acceleration at a location based on the accelerometer data.

        Parameters
        ----------
        location : ndarray
            3x1 numpy array with the coordinates at which we want to calculate the 6-DOF vector

        Returns
        -------
        ndarray:
            6xN matrix with the 6-DOF accelerations at *location*

        Notes
        -----
        In case 6 Acc signals are used the method is the same as the inverse matrix of the matlab
        code. If more than 6 signals are used, a least square fit is employed

        """

        self.logger.debug("start calculating the acceleration at the tower origin")

        if self.settings.tower_angle_name is not None:
            try:
                tower_angle = self.mdf_file.data[self.settings.tower_angle_name].mean()
            except KeyError as err:
                self.logger.debug("Tower angle name not recognised in data: {}".format(err))
                self.logger.debug("Put the process_huisman flag to true: you need the tower angle")
        else:
            tower_angle = 90

        try:
            self.logger.debug("STAT tower angle :\n{}".format(tower_angle.describe()))
        except AttributeError:
            self.logger.debug("STAT tower angle : {} degrees".format(tower_angle))

        # We are going to solve A x = b, where x is the dof matrix we are looking for, A is the
        # transformation matrix where each corresponds to the transformation of 1 accelerometer
        # signal, and b contains the accelerometer signal
        transform_matrix = []  # list with the rows of matrix A
        acceleration_matrix = []  # list with b

        n_fit = 0  # counter to keep the number of accelerometers added to the matrix

        # loop over all the position of the accelero meters
        for i_group, (position, acc_group) in enumerate(
                self.accelerometer_groups.device_locations.items()):

            # per position, loop over the accelerometers belonging to this position
            self.logger.debug("checking {} {} {}".format(position, acc_group.names,
                                                         acc_group.labels))
            for i_comp, component in enumerate(acc_group.components):

                # weight determines if this accelerometer is going to be used. For weight=1, use it,
                # otherwise, skip it. If the flag use_all_accelerometers is set, always use all
                # accelerometers, except the ones stored in the exclude_locations list
                weight = self.settings.locations[position]["fit_weight"][component]
                if weight > 0 or (
                        self.settings.use_all_accelerometers and position not in
                        self.settings.exclude_locations):
                    # ok, we are going to use this accelerometer
                    geo_transform = self.geometric_transform(position_name=position,
                                                             location=location,
                                                             component=component,
                                                             tower_angle_deg=tower_angle)

                    # retrieve the name of the signal belonging to the current device we have just
                    # added
                    name = fut.accelerometer_name_of_converted_signal(acc_group.names[i_comp])
                    self.logger.debug("storing data of {} with name {}".format(component, name))

                    # add the signal of the current signal to the acceleration_matrix
                    acc_signal = self.mdf_file.data[name]

                    transform_matrix.append(geo_transform)
                    acceleration_matrix.append(acc_signal)
                    n_fit += 1

        # turn the list of arrays in a 2D array
        transform_matrix = vstack(transform_matrix)
        acceleration_matrix = vstack(acceleration_matrix)

        if n_fit < 6:
            self.logger.warning(
                "Needs at least 6 accelerometers to be able to get the 6-DOF. Please add more "
                "active meters")
            raise AssertionError("Not enough accelerometers")

        # calculate the six dof at the tower location using a least square fit. If n_fit == 6, the
        # result is identical to the inverse matrix as done in matlab
        self.logger.debug("Solving transform matrix of the accelerometers based on {} "
                          "devices".format(n_fit))

        six_dof = lstsq(transform_matrix, acceleration_matrix)[0]

        # the six dof rotational accelerations are expressed in degrees
        six_dof[3:] = rad2deg(six_dof[3:])

        return six_dof

    def geometric_transform(self, position_name, location,
                            component, tower_angle_deg=90.0):
        """Calculate the geometric transform of the accelerometer at *position_name* with
        *component* to the *location*

        Parameters
        ----------
        position_name: str
            Name of the position of the current accelerometer to get the transform vector for
        location: ndarray:
            Coordinates of the location to calculate the geometric transform
        component: {"X", "Y", "Z"}
            Component of the current acceleration
        tower_angle_deg: float, optional
            Mean angle of the tower in degrees. Default = 90, which means tower straigt up or
            a location on the deck

        Returns
        -------
        ndarray:
            Geometric transformation vector

        """
        # each position_name has a flag to see if we are in the tower
        in_tower = self.settings.locations[position_name]["in_tower"]

        # get the coordinates of this position_name
        self.logger.debug(
            "converting {}".format(self.settings.locations[position_name]["coordinates"]))
        coordinates = set_default_dimension(self.settings.locations[position_name]["coordinates"],
                                            default_dimension="m").to_base_units().magnitude

        if in_tower:
            # in the tower, the alpha is given by the tower angle (90 deg is tower straight up)
            alpha = deg2rad(tower_angle_deg)
        else:
            # in case we are not in the tower, just set the alpha equal to a straight up position
            alpha = pi / 2.0

        # get the relative position_name of the current position_name of the accelerometers with
        # the location were we need to get the dof vector.

        self.logger.debug("subtracting {} - {}".format(coordinates, location))
        r_vec = coordinates - location  # 'coordinates'= the location of the current device
        vx = r_vec[0]
        vy = r_vec[1]
        vz = r_vec[2]
        sa = sin(alpha)  # == 1 for  non-tower locations
        ca = cos(alpha)  # == 0 for non-tower locations

        self.logger.debug("coordinates {} at location {} {}".format(coordinates, alpha, r_vec))

        # add the rows to the matrix. Look at the component to determine which version you need
        if component == "X":
            # alpha=90: [1, 0, 0, 0, Z, -Y]
            row = array([sa, 0, ca, vy * ca, vz * sa - vx * ca, -vy * sa])
        elif component == "Y":
            row = array([0, 1, 0, -vz, 0, vx])
        elif component == "Z":
            # alpha=90: [0, 0, 1, vy,  -vx, 0]
            # note that this row is rowX(90)*ca + rowZ(90)*sa
            row = array([ca, 0, sa, vy * sa, vz * ca - vx * sa, -vy * ca])
        else:
            raise AssertionError("Component should be one of [XYZ]. Found".format(component))

        return row

    def filter_all_accelerometer_signals(self):
        """Filter the accelerometer signal at all the locations and all devices per location"""

        log = self.logger

        # get the filter properties for the accelerometer signal
        filter_type, f_cut_low, f_cut_high, f_width_edge, order, cval = \
            self.settings.get_filter_properties("accelerometers")

        f_sampling = self.mdf_file.time_record.sample_rate

        # loop over all the accelerometer devices
        for i_group, (position, acc_group) in enumerate(
                self.accelerometer_groups.device_locations.items()):
            log.debug("filtering acceleration at {} with fL = {} fH = {} order = {} type={}"
                      "".format(position, f_cut_low, f_cut_high, order, filter_type))

            # the outer loop goes over  the location, now loop over the accelerometers at this
            # location
            for i_channel, acc_name in enumerate(acc_group.names):

                acc_name = fut.accelerometer_name_of_converted_signal(acc_name)

                log.debug("filtering channel {}".format(acc_name))

                # note that the cut off frequency and edge width are use inputs which were force to
                # have dimension Hz therefore just take the magnitude. f_sampling is internally
                # calculated so is just a float

                if filter_type is not None:
                    signal = self.mdf_file.data[acc_name].values

                    filtered = filter_signal_robust(signal, filter_type=filter_type,
                                                    f_cut_low=f_cut_low, f_cut_high=f_cut_high,
                                                    f_sampling=f_sampling,
                                                    f_width_edge=f_width_edge, order=order,
                                                    constant_value=cval)

                    self.mdf_file.data[acc_name] = filtered
                else:
                    log.debug("Not filtering")

    def filter_strain_gauge_signal(self):
        """Apply the filter to the strain gauge signal"""

        log = self.logger

        # retrieve all the information of the filter settings for the strain gauges
        filter_type, f_cut_low, f_cut_high, f_width_edge, order, cval = \
            self.settings.get_filter_properties("strain_gauges")
        f_sampling = self.mdf_file.time_record.sample_rate

        # loop over all the devices at all locations
        for i_group, (position, strain_group) in enumerate(
                self.strain_gauge_groups.device_locations.items()):
            log.debug("filtering strain gauge signal at {} with fL = {} fH = {} order = {} type={}"
                      "".format(position, f_cut_low, f_cut_high, order, filter_type))

            for i_channel, name in enumerate(strain_group.names):

                # get the new name for the strain signal
                strain_name = fut.strain_name_of_converted_signal(name, position, i_channel)

                log.debug("filtering channel {}".format(strain_name))
                if filter_type is not None:

                    signal = self.mdf_file.data[strain_name]
                    filtered = filter_signal_robust(signal,
                                                    filter_type=filter_type,
                                                    f_cut_low=f_cut_low,
                                                    f_cut_high=f_cut_high,
                                                    f_sampling=f_sampling,
                                                    f_width_edge=f_width_edge,
                                                    order=order,
                                                    constant_value=cval)
                    self.mdf_file.data[strain_name] = filtered - filtered.mean()
                else:
                    log.debug("Not filtering")

    def get_motion_conversion_factor(self, name):
        """
        Get the conversion facot for the channel   *name*

        Parameters
        ----------
        name: str
            Name of the channel to get the conversion factor from

        Returns
        -------
        float:
            conversion factor for this DOF component

        """

        if self.settings.mru_angle_units == "degrees":
            angle_conversion_factor = 1.0
        elif self.settings.mru_angle_units == "radians":
            angle_conversion_factor = 180.0 / pi
        else:
            angle_conversion_factor = 1
            self.logger.warning("mru_angle_units should be either degrees or radians")

        # do the conversion from the Kongsberg mru system to the hmc coordinate system here.
        if re.search("Roll", name, re.IGNORECASE):
            roll_scale = self.settings.mru_roll_scale
            factor = roll_scale * angle_conversion_factor
        elif re.search("Pitch", name, re.IGNORECASE):
            pitch_scale = self.settings.mru_pitch_scale
            factor = pitch_scale * angle_conversion_factor
        elif re.search("Yaw", name, re.IGNORECASE) or re.search("Heading", name, re.IGNORECASE):
            yaw_scale = self.settings.mru_heading_scale
            factor = yaw_scale * angle_conversion_factor
        elif re.search("Heave", name, re.IGNORECASE):
            # the Kongsberg definition H+-> ship down, in HMC ship up, so multiply with -1
            heave_scale = self.settings.mru_heave_scale
            factor = heave_scale
        elif re.search("Surge", name, re.IGNORECASE):
            surge_scale = self.settings.mru_surge_scale
            factor = surge_scale
        elif re.search("Sway", name, re.IGNORECASE):
            sway_scale = self.settings.mru_sway_scale
            factor = sway_scale
        else:
            self.logger.warning("No matching label found to set the factor. Just take one ")
            factor = 1.0

        return factor

    def mru_acceleration(self):
        """Calculate the roll heave pitch acceleration based on the roll heave pitch displacement

        Notes
        -----
        * this routine convert the roll/pitch heave motion into an acceleration ax/ay/az at an
          arbitrary location
        * Take notice that the mru roll/pitch/heave definition is a bit different.

          At hmc

          * x: positive into the bow direction (looking from the cranes to the steering cabin)
          * y: positive into the portside direction (the tower is at the portside)
          * z: positive upward

          The mru has the following definition

          * x: positive into the bow direction
          * y: positive into the starboard direction
          * z: positive into downward direction

          Therefore, for the MRU

          * Positive Heave -> the ship moves down (so into the negative direction in the hmc
            framework)
          * Positive Roll -> Starboard side goes down (same as hmc)
          * Positive Pitch -> The bow comes up (whereas in hmc the bow goes down for positive pitch
            as y+ is into portside

        * The units of roll and pitch are in radians

        References
        ----------
        Document MRU5.doc

        """

        log = get_logger(__name__)
        log.debug(
            "Smoothing the mru sigal and calculate the accelerations of Heave, Roll and Pitch")

        spline_factor = self.settings.signal_mru_smooth_filter["spline_smoothing_factor"]
        spline_degree = self.settings.signal_mru_smooth_filter["spline_degree"]
        type_name = self.settings.signal_mru_smooth_filter["type"]
        resample_factor = self.settings.signal_mru_smooth_filter["resample_factor"]

        filter_type, f_cut_low, f_cut_high, f_width_edge, order, cval = \
            self.settings.get_filter_properties("mru")

        f_sampling = self.mdf_file.time_record.sample_rate
        delta_t = 1.0 / f_sampling

        for i_channel, name in enumerate(self.mru_group.names):
            log.debug("processing channel {}/{}. Smooth degree {} factor {}"
                      "".format(i_channel, name, spline_degree, spline_factor))

            # df is the data frame containing the current acceleration info
            mru_ds = self.mdf_file.data[name]

            log.debug("STATS {}\n{}".format(name, mru_ds.describe()))

            # get the time data
            time_r = self.mdf_file.data.time_r

            # get the conversion factor for this channel
            factor = self.get_motion_conversion_factor(name=name)

            # filter the data with the filter of choise
            if filter_type is not None:
                if re.search("Yaw", name, re.IGNORECASE):
                    # for the yaw, try to remove the phase shift at 0~360
                    signal = spf.remove_phase_shift(mru_ds.values)
                else:
                    signal = mru_ds.values

                values = filter_signal_robust(signal, filter_type=filter_type, f_cut_low=f_cut_low,
                                              f_cut_high=f_cut_high, f_sampling=f_sampling,
                                              f_width_edge=f_width_edge, order=order,
                                              constant_value=cval
                                              )

            else:
                log.debug("Not filtering")
                values = signal.values

            if values is None:
                values = signal.values

            values *= factor

            # we have pre-filtered the mru data now. Optionally we can still apply a spline filter
            # or resample filter, which results in a smoother gradient

            if type_name == "spline":
                # get the spline smoother. The reason for the spline is that we want to take the
                # second derivative of the signal to turn displacements into accelerations. This
                # goes better after a spline filter
                log.debug("Applying spline filter with degree = {} and f = {}"
                          "".format(spline_degree, spline_factor))
                spline = UnivariateSpline(time_r, values, k=spline_degree, s=spline_factor)

                # do the smoothing with the splines and rescale back to the normal values
                signal_smooth = spline(time_r)
                # signal_smooth = savgol_filter(df.values, 51, 8)
                d_signal_dt = gradient(signal_smooth, delta_t, edge_order=2)

                # calculate the second gradient of the smoothed signals, ie. the accelerations
                d2_signal_dt2 = gradient(d_signal_dt, delta_t, edge_order=2)

            elif type_name == "resample":

                log.debug("Down and upsample with resample_factor = {} ".format(resample_factor))

                # down sample and up sample
                n_point_low = values.size / resample_factor
                time_low = np.linspace(time_r[0], time_r[-1], n_point_low)
                delta_t_low = time_low[1] - time_low[0]
                signal_resampled = sps.resample(values, int(n_point_low))

                # signal_smooth = savgol_filter(df.values, 51, 8)
                d_signal_dt_low = gradient(signal_resampled, delta_t_low, edge_order=2)

                f = interp1d(time_low, signal_resampled)
                signal_smooth = f(time_r)

                f = interp1d(time_low, d_signal_dt_low)
                d_signal_dt = f(time_r)

                d_signal_dt_resampled = sps.resample(d_signal_dt, int(n_point_low))

                # calculate the second gradient of the smoothed signals, ie. the accelerations
                d2_signal_dt2_low = gradient(d_signal_dt_resampled, delta_t_low, edge_order=2)

                f = interp1d(time_low, d2_signal_dt2_low)
                d2_signal_dt2 = f(time_r)
            elif type_name == "none":
                log.debug("No extra filter on MRU data")
                signal_smooth = values
                d_signal_dt = gradient(values, delta_t, edge_order=2)
                d2_signal_dt2 = gradient(d_signal_dt, delta_t, edge_order=2)
            else:
                log.warning("Filter not recognised. Please choose spline, resample or none")
                raise AssertionError

            # create the new names for the smoothed signal and it gradient
            name_smooth = fut.mru_smooth_name(name)
            name_gradient = fut.mru_acceleration_name(name, 1)
            name_gradient2 = fut.mru_acceleration_name(name, 2)

            log.debug("Storing the smoothed MRU signal to {}, the velocity to {} and the "
                      "acceleration to {}".format(name_smooth, name_gradient, name_gradient2))

            # store the data in the data frame attached to the mdf_file object
            # note that roll/pitch/heave are in degrees here and have the hmc convention
            self.mdf_file.data[name_smooth] = signal_smooth
            self.mdf_file.data[name_gradient] = d_signal_dt
            self.mdf_file.data[name_gradient2] = d2_signal_dt2

    def mru_motion_to_acceleration_at_tower_origin(self):
        """Calculate the acceleration at the tower origin based on the MRU signal"""

        log = self.logger
        log.debug("Calculate the acceleration at the tower origin in mru_motion_to_"
                  "acceleration_at_tower_origin")

        try:
            log.debug("get origin at {} ".format(self.settings.tower_origin_name))
            tower_origin = np.array(self.settings.locations[self.settings.tower_origin_name][
                                        "coordinates"])
            log.debug("Tower origin : {} ".format(tower_origin))
        except KeyError as err:
            log.warning("Tower origin name not recognised in data: {}".format(err))
            log.warning("Please set a correct label for the tower origin")
            tower_origin = None

        self.mru_motion_to_acceleration_at_location(self.mru_group.names, tower_origin,
                                                    self.settings.tower_origin_name,
                                                    )

    def motion_to_all_dof_location(self, dof_motions=dict()):
        """
        Loop over all the dof location and transform the mru motion to that location

        Parameters
        ----------
        dof_motions : dict()
            This dictionary contains the location we want to have a DOF motion

        Examples
        --------
        An example of a dof_motions dictionary is::

            dof_motions:
                gangway:
                    position: GangWay
                    store_motion: true
                    store_velocity: true
                    store_acceleration: true
                poi1:
                    position: POI1
                    store_motion: true
                    store_velocity: true
                    store_acceleration: true
                mru:
                    position: MRU1
                    store_motion: true
                    store_velocity: true
                    store_acceleration: true

        Here the position is given by  a label which  is defined in the locations dictionary::

            locations:
                MRU1:
                    coordinates:
                    - 118.5
                    - -14.0
                    - 8.55 # 50.55 deck is at 42 m from keel
                    plot_this: false
                COG:
                    coordinates:
                    - 55.92
                    - 0
                    - -16.87 # 25.13 from keel; deck is at 42 m from keel
                    plot_this: false
                POI1:
                    coordinates:
                    - 95.0
                    - 0.0
                    - -16.0
                    plot_this: false
                GangWay:
                    coordinates:
                    - 0
                    - -3.0
                    - -8.0    # relative to deck
                    plot_this: false
        """

        logger = get_logger(__name__)

        for dof_name, dof_properties in dof_motions.items():
            location_name = dof_properties["position"]
            coordinates_at_location = set_default_dimension(
                self.settings.locations[location_name]["coordinates"],
                "m").to_base_units().magnitude
            store_motion = read_value_from_dict_if_valid(dof_properties, "store_motion", False)
            store_velocity = read_value_from_dict_if_valid(dof_properties, "store_velocity", False)
            store_acceleration = read_value_from_dict_if_valid(dof_properties, "store_acceleration",
                                                               False)
            if store_acceleration and not store_motion:
                logger.debug(
                    "You cannot calculate the acceleration with gravity without having the motion "
                    "as well. Set to True")
                store_motion = True

            logger.debug("Dof conversion {} @ {} ({}) mot={} vel={} acc={}".format(
                dof_name, location_name, coordinates_at_location, store_motion, store_velocity,
                store_acceleration))

            if store_motion:
                # note that for the motion transformation you want to use the filter mru data
                mru_names_filtered = [fut.mru_smooth_name(name) for name in self.mru_group.names]
                self.mru_motion_to_acceleration_at_location(mru_names_filtered,
                                                            coordinates_at_location,
                                                            location_name,
                                                            motion_derivative=0)
                # add the transformed statistics
                for dof_comp in fut.mru_six_dof_names(location_name, order=0):
                    self.settings.general_info_sheet[dof_comp + "_SDA"] = dict(alias=dof_comp,
                                                                               operation="sda")

            if store_velocity:
                # if we use the velocity we already have the filtered data
                self.mru_motion_to_acceleration_at_location(self.mru_group.names,
                                                            coordinates_at_location,
                                                            location_name,
                                                            motion_derivative=1)
                for dof_comp in fut.mru_six_dof_names(location_name, order=1):
                    self.settings.general_info_sheet[dof_comp + "_SDA"] = dict(alias=dof_comp,
                                                                               operation="sda")

            if store_acceleration:
                self.mru_motion_to_acceleration_at_location(self.mru_group.names,
                                                            coordinates_at_location,
                                                            location_name,
                                                            motion_derivative=2)
                # create the acceleration fields including gravity component

                self.add_gravity_to_mru_acceleration(location_name=location_name)
                # this adds the calculation of the acceleration to the general_info_sheet, such that
                # it is taken care of that this component's SDA gets written to the data base
                for dof_comp in fut.mru_six_dof_names(location_name, order=2):
                    self.settings.general_info_sheet[dof_comp + "_SDA"] = dict(alias=dof_comp,
                                                                               operation="sda")
                # for the first three (AX, AY, AZ) also include the acceleration with gravity
                for dof_comp in fut.mru_six_dof_names(location_name, order=2)[:3]:
                    dof_comp_incl_g = "_".join([dof_comp, INCL_G_NAME])
                    self.settings.general_info_sheet[dof_comp_incl_g + "_SDA"] = dict(
                        alias=dof_comp_incl_g,
                        operation="sda")

    def mru_motion_to_acceleration_at_all_acc_locations(self):
        """
        Convert the mru signal to ax, ay, az acceleration at all locations
        """

        log = get_logger(__name__)
        log.debug("Calculate the acceleration at the tower origin in "
                  "mru_motion_to_acceleration_at_tower_origin")

        for i_group, (position, acc_group) in enumerate(
                self.accelerometer_groups.device_locations.items()):
            coordinates_at_position = set_default_dimension(
                self.settings.locations[position]["coordinates"], "m").to_base_units().magnitude
            log.debug("Calculate the acceleration at {} ({}) in mru_motion_to_acceleration_at_all_"
                      "acc_locations".format(position, coordinates_at_position))

            self.mru_motion_to_acceleration_at_location(self.mru_group.names,
                                                        coordinates_at_position,
                                                        position,
                                                        motion_derivative=2)

    def mru_motion_to_acceleration_at_location(self, mru_names, location, location_name,
                                               motion_derivative=2):
        """
        Convert acceleration of list of mru positions to a new location

        Parameters
        ----------
        mru_names:
            List of the component of the MRU. Note that does not always have to be mru_group.names
            as sometimes we want to use the filtered channels
        location : ndarray
            3x1 numpy array holding the location at which to calculate the acceleration
        location_name : str
            string holding a name for the location. Used to store the 6 dof in the data frame
        motion_derivative : int
            which derivative of motion to calculate: 0 is zeroth derivative (motion), 1 is first
            derivative (velocity) and 2 is second derivative (acceleration) (Default value = 2)

        """
        self.logger.debug("Calculating the {} derivative at the location '{}' ({}) in "
                          "mru_motion_to_acceleration_at_location".format(motion_derivative,
                                                                          location_name, location))

        # get relative coordinates of the locations compared to the COG. It appears that the MRU
        # data is stored with resp to COG, but this may be different in other cases, so check again
        # later# mru_location = array(settings.locations["MRU1"]["coordinates"])
        # mru_location
        (cx, cy, cz) = location - self.mru_reference_location
        self.logger.debug("Converting MRU with arm ({}, {}, {}) wrt {} "
                          "".format(cx, cy, cz, self.mru_reference_location))

        # initialise the pitch roll data arrays to none.
        pitch = roll = heave = surge = sway = yaw = None

        # loop over the signals belong to the current MRU (Roll, heave, Pitch)
        for i_channel, name in enumerate(mru_names):

            # get the mru name converted to 1/s^order (0, 1, or 2 derivative) for the accelerations
            motion_name = fut.mru_acceleration_name(name, motion_derivative)

            self.logger.debug(
                "processing channel {}/{} with acc name {}.".format(i_channel, name, motion_name))

            # df is the data frame containing the current motion, velocity or acceleration info
            df_motion = self.mdf_file.data[motion_name]

            # first store the roll pitch heave into separate arrays for easy reference
            # also, make sure that you express it in radian for proper conversions
            if re.search("Roll", name, re.IGNORECASE):
                roll = np.deg2rad(df_motion.values)
            elif re.search("Pitch", name, re.IGNORECASE):
                pitch = np.deg2rad(df_motion.values)
            elif re.search("Yaw", name, re.IGNORECASE):
                yaw = np.deg2rad(df_motion.values)
            elif re.search("Surge", name, re.IGNORECASE):
                surge = df_motion.values
            elif re.search("Sway", name, re.IGNORECASE):
                sway = df_motion.values
            elif re.search("Heave", name, re.IGNORECASE):
                heave = df_motion.values
            else:
                self.logger.debug("component {} not recognised. Skipping".format(name))

        # in case the surge, sway and yaw motion were not stored (for the mru signal), fill them
        # with zeros in case we use an octans, these values are logged as well
        if surge is None:
            self.logger.debug("No surge found. Set to zero")
            surge = np.zeros(roll.size)
        if yaw is None:
            self.logger.debug("No yaw found. Set to zero")
            yaw = np.zeros(roll.size)
        if sway is None:
            self.logger.debug("No sway found. Set to zero")
            sway = np.zeros(roll.size)

        try:
            # calculate the 6 dof based on the MRU roll/pitch/heave) based on the hmc convention
            # x+ is to bow (cranes are at the stern), y+ is to portside (tower is at the portside),
            # z+ is upward Roll+ is around x+, so starboard goes down Pitch+ is around y+,
            # so bow goes down in HMC system with y+ to ps Heave+ is to z+, so ship moves up
            self.logger.debug("Conversion with surge={:.4e} sway={:.4e} heave={:.3e} roll={:.3e} "
                              "pitch={:.3e} yaw={:.3e}".format(surge.mean(), sway.mean(),
                                                               heave.mean(),
                                                               roll.mean(), pitch.mean(),
                                                               yaw.mean()))
            location_six_dof = zeros((6, df_motion.values.shape[0]))
            location_six_dof[0, :] = pitch * cz - yaw * cy + surge
            location_six_dof[1, :] = -roll * cz + yaw * cx + sway
            location_six_dof[2, :] = -pitch * cx + roll * cy + heave
            location_six_dof[3, :] = np.rad2deg(
                roll)  # we transferred roll and pitch to radians above, turn it back
            location_six_dof[4, :] = np.rad2deg(
                pitch)  # the conversion from kongsberg to hmc was done before
            location_six_dof[5, :] = np.rad2deg(
                yaw)  # the conversion from kongsberg to hmc was done before

        except TypeError:
            self.logger.warning(
                "Failed copying the pitch, roll or heave array. Probably this is related to the "
                "type in the Marin MDF file setup in which they define MRU_Picth in stead of "
                "MRU_Pitch. Please correct this by properly setting the "
                "'replace_mdf_file_record_names' dictionary to 'MRU_Picth: MRU_Pitch' in the "
                "'values' sections of the settings file")
        else:
            # copy the 6-Dof to the dataframe of the mru_file object
            for i_loc, comp in enumerate(fut.mru_six_dof_names(location_name,
                                                               order=motion_derivative)):
                self.logger.debug(
                    "FROM MRU Adding channel {}/{} to the data frame".format(i_loc, comp))
                self.mdf_file.data[comp] = location_six_dof[i_loc, :]

    def add_gravity_to_mru_acceleration(self, location_name):
        """Add the gravity to the mru x y z acceleration"""

        # add the gravity component to the first three X, Y, z as well
        dof_names = fut.mru_six_dof_names(location_name, order=0)
        roll_name = dof_names[3]
        pitch_name = dof_names[4]
        pitch = np.deg2rad(self.mdf_file.data[pitch_name])
        roll = np.deg2rad(self.mdf_file.data[roll_name])
        dof_acc_names = fut.mru_six_dof_names(location_name, order=2)[:3]
        for i_loc, comp in enumerate(dof_acc_names):
            comp_g = "_".join([comp, INCL_G_NAME])
            self.logger.debug(
                "FROM MRU Adding channel {}/{} to the data frame with Gravity".format(i_loc,
                                                                                      comp_g))
            if i_loc == 0:
                ax = self.mdf_file.data[comp]
                acc_incl_g = ax - g0 * np.sin(pitch)
            elif i_loc == 1:
                ay = self.mdf_file.data[comp]
                acc_incl_g = ay + g0 * np.sin(roll)
            elif i_loc == 2:
                az = self.mdf_file.data[comp]
                acc_incl_g = az + g0 * (np.cos(pitch) + np.cos(roll))
            else:
                raise AssertionError("Should only loop over three components here")

            # add the acceleration component including gravity to the mdf_file data
            self.mdf_file.data[comp_g] = acc_incl_g

    def acceleration_to_dof_at_mru(self):
        """calculate the 6-DOF acceleration at the mru location"""

        try:
            self.logger.debug("get mru location at {} ".format(
                self.settings.mru_reference_location_name))
            mru_location = set_default_dimension(
                array(self.settings.locations[self.settings.mru_reference_location_name][
                          "coordinates"]),
                "m").to_base_units().magnitude
            self.logger.debug("MRU location : {} ".format(mru_location))
        except KeyError as err:
            self.logger.debug("Tower origin name not recognised in data: {}".format(err))
            self.logger.debug("Please set a correct label for the tower origin")
        else:

            six_dof = self.acceleration_to_dof_at_location(mru_location)

            # add the result also to the data frame of the current mdf_file
            for i_comp, comp in enumerate(fut.six_dof_names(self.settings.mru_acc_name)):
                self.logger.debug("Adding channel {}/{} to the data frame".format(i_comp, comp))
                self.mdf_file.data[comp] = six_dof[i_comp, :]

    def acceleration_to_dof_at_cog(self):
        """get the 6-DOF acceleration at the cog location"""

        try:
            cog_location = set_default_dimension(
                array(self.settings.locations["COG"]["coordinates"]),
                "m").to_base_units().magnitude
            self.logger.debug("COG location : {} ".format(cog_location))
        except KeyError as err:
            self.logger.debug("COG location not recognised in data: {}".format(err))
            self.logger.debug("Please set a correct label for the tower origin")
        else:

            six_dof = self.acceleration_to_dof_at_location(cog_location)

            # add the result also to the data frame of the current mdf_file
            for i, comp in enumerate(fut.six_dof_names("COG")):
                self.logger.debug("Adding channel {}/{} to the data frame".format(i, comp))
                self.mdf_file.data[comp] = six_dof[i, :]

    def acceleration_dof_at_tower_origin_to_stress_at_cold_spots(self, tower_angle=90):
        """Turn the acceleration at the tower origin in to a stress signal at all the strain
        gauge locations

        Parameters
        ----------

        tower_angle: float
            Mean tower angle in degrees. 90 is straight up

        Returns
        -------

        """
        log = get_logger(__name__)
        log.debug("Converting the tower origin 6-DOF accelerations into stresses")

        # the SACS transformation matrix converting tower origin acceleration to stress is defined
        # for both the straight position of the tower (angle>80) and the transit position (<80).
        # Here the appropriate extension for both positions is set to retrieve the correct values
        # from the data frame which stored the transformation matrix earlier
        if tower_angle > 80:
            tower_extension = "_90"
        else:
            tower_extension = "_75"

        log.debug(
            "Tower angle mean = {} so take {} response matrix".format(tower_angle, tower_extension))

        if self.frequency_domain:
            # we are in the frequency domain, so do all operation on rao complex amplitudes
            container = self.rao_camp
        else:
            # we are in the time domain, so do all operation on time series stode in the mdf file
            container = self.mdf_file

        six_dof_location_name = self.settings.tower_origin_acc_name

        # obtain the 6-DOF matrix for the tower origin
        six_dof_list = list()
        for i, comp in enumerate(fut.six_dof_names(six_dof_location_name)):
            log.debug("Adding 6 DOF component {}/{} to the six_dof_list".format(i, comp))
            six_dof_list.append(container.data[comp])

        # this matrix is a 6xN matrix with AX, AY, AZ, RXX, RYY, RZZ in the rows and N time samples
        six_dof_matrix = array(six_dof_list)

        # loop over the groups of strain gauges
        for i_group, (position, sg_group) in enumerate(
                self.strain_gauge_groups.device_locations.items()):

            # obtain the response matrix of the locations holding
            # Fx/Ax Fx/Ay Fx/Az Fx/Rxx Fx/Ryy Fx/Rzz
            # My/Ax My/Ay My/Az My/Rxx My/Ryy My/Rzz
            # Mz/Ax Mz/Ay Mz/Az Mz/Rxx Mz/Ryy Mz/Rzz
            try:
                response_matrix = np.array(self.settings.locations[position]["response_matrix" +
                                                                             tower_extension])
            except KeyError:
                log.debug("Location {} does not have a response matrix. Skipping".format(position))
                continue

            try:
                # get the cross sectional properties for this location
                props = self.settings.locations[position]["cross_sectional_properties"]
                area = set_default_dimension(props["area"], "m^2").to_base_units().magnitude
                try:
                    # first read the dy, dz dimension
                    Dy = set_default_dimension(props["dy"], "m").to_base_units().magnitude
                    Dz = set_default_dimension(props["dz"], "m").to_base_units().magnitude
                except KeyError:
                    # if it fails read the radius
                    Dy = 2 * set_default_dimension(props["radius"], "m").to_base_units().magnitude
                    Dz = 2 * set_default_dimension(props["radius"], "m").to_base_units().magnitude

                Iy = set_default_dimension(props["I_moment_yy"], "m^4").to_base_units().magnitude
                Iz = set_default_dimension(props["I_moment_zz"], "m^4").to_base_units().magnitude
            except (KeyError, TypeError):
                log.debug(
                    "Location {} does not have cross_sectional_properties. Skipping".format(
                        position))
                continue

            try:
                # get the bending moment angles for this location
                bending_moment_angles = array(
                    self.settings.locations[position]["bending_moment_angles"])
                log.debug("Bending moment angles: {}".format(bending_moment_angles))
            except KeyError:
                bending_moment_angles = None
                log.debug(
                    "Location {} does not have bending moment angles. Must be pivot".format(
                        position))

            try:
                # get the force x sign to take into account the phase
                fx_factor = self.settings.locations[position]["force_x_factor"]
                log.debug("Picked force factor {}".format(fx_factor))
            except KeyError:
                log.debug("Location {} does not have a force_x sign. Assuming 1".format(position))
                fx_factor = 1

            log.debug("Response Matrix at {} :\n{}".format(position, response_matrix))
            log.debug("Geo props area={} Dy={} Dz={} Iy={} Iz={}".format(area, Dy, Dz, Iy, Iz))
            log.debug(MSG_FORMAT.format("Force x factor", fx_factor))

            # multiply the 3x6 matrix with the 6xN matrix -> give 3xN matrix with Fx, My, Mz
            [force_x, moment_y, moment_z] = dot(response_matrix, six_dof_matrix)
            log.debug("Created force and moments matrix vs of size {}".format(force_x.shape))

            # all devices at group 'sg_group' are at the same location. Now loop over the devices of
            # this location
            for i_channel, name in enumerate(sg_group.names):
                # short labels contains the reference to the channel such as SG01, SG02, etc.
                short_label = sg_group.short_labels[i_channel]
                new_name = fut.stress_from_acc_name(name, position, i_channel)

                log.debug("Converting {} to {}".format(short_label, new_name))

                # zeta rotates over the four directions of the tube. Define zeta in radians
                try:
                    angle_deg = bending_moment_angles[i_channel]
                    zeta = deg2rad(angle_deg)
                except TypeError:
                    # for the pivots no bending moment angles are defined. Put Dy and Dz values to
                    # zero so the the moment_y are tkaen out of the equation: sigma = Fx/Area
                    log.debug("Bending moment angle not defined for channel. Take moments out")
                    angle_deg = None
                    zeta = 0
                    Dy = Dz = 0

                try:
                    # in case of the old model the PIV used to have 3 force factors (in the matlab
                    # code) so here you can add them per i_channel. If this fails (which is always
                    # the case in the new model) go to the next exception to set the fct with the
                    # scalar values
                    fct = fx_factor[i_channel]
                    log.debug("Set factor from list {} to {}".format(i_channel, fct))
                except TypeError:
                    # add a multiplication factor. Used for the pivot sometimes
                    fct = fx_factor
                    log.debug("Set factor from scalar {} to {}".format(i_channel, fct))

                # calculate the stress based on the direction. The bending moment are defined in the
                # settings the dimensions are all in SI: force_x (N), area (m2), moment_y (m4),
                # Dz (m) -> sigma is in Pa whereas in matlab code sigma is in MPa
                sigma = fct * force_x / area + 0.5 * sin(zeta) * moment_y * Dz / Iy + 0.5 * cos(
                    zeta) * moment_z * Dy / Iz

                container.data[new_name] = sigma

    def collect_statistics_cold_and_hot_spot_stress(self):
        """
        Collect the current statistics  into the fatigue database
        """

        # loop over the groups of strain gauges
        for i_group, (position, sg_group) in enumerate(
                self.strain_gauge_groups.device_locations.items()):
            self.collect_statistics_cold_spot_stress_at_position(position, sg_group)
            self.collect_statistics_hot_spot_stress_at_position(position)

    def collect_statistics_cold_spot_stress_at_position(self, position, sg_group):
        """Collect the statistics of the cold spot stress
        Parameters
        ----------
        position: str
            Add all from location 'position'
        sg_group: :obj:`DevicesAtLocation`
            Group of devices at this location

        """

        # select the proper data object
        if self.frequency_domain:
            # we are in frequency domain. Get the rao_camp complex aplitudes
            container = self.rao_camp
        else:
            # are are in time domain. Get the time series object
            container = self.mdf_file

        log = get_logger(__name__)
        # the name of the sheet which stores all the stresses at this position
        str_at_pos = fut.stress_at_position_name(position)

        for i_type, stress_type in enumerate(["AC", "SG"]):

            if stress_type == "SG" and self.frequency_domain:
                # if we are in the frequency domain we don't have the strain gauge signal: skip this
                continue

            # collect the statistics of the cold spot stresses
            for i_channel, name in enumerate(sg_group.names):

                if stress_type == "SG":
                    stress_name = fut.stress_name_of_converted_signal(name, position, i_channel)
                else:
                    stress_name = fut.stress_from_acc_name(name, position, i_channel)

                # the stresses obtained from the accelerometers (acc) and strain gauges (str) in MPa
                try:
                    stress = container.data[stress_name] * 1e-6
                except KeyError:
                    log.debug("stress name {} not found. skipping".format(stress_name))
                    continue
                else:
                    if not self.frequency_domain:
                        # store the cold spot stress standard deviation per channel in the fatigue
                        # database stress_acc  is a time series, so the standard deviation can
                        # directly be calculated
                        stress_std = stress.std()
                    else:
                        # stress_acc  is a wave spectrum: multiply it with the rao of this location
                        # to get the location stress spectrum, the standard deviation is obtained
                        # by integrating the spectrum and taking the sqrt (the integral of a power
                        # spectrum equals the variance of the signal
                        mag_stress_sqr = abs(stress * stress)
                        stress_spectrum = mag_stress_sqr * self.psd_2d * container.delta_area
                        stress_std = np.sqrt(stress_spectrum.sum())

                    # store the standard deviation for the current location
                    column_name = "S_CS_{}{}".format(stress_type, i_channel)
                    row_index = (str_at_pos, self.time_stamp)
                    self.fatigue_data_base.ix[row_index, column_name] = stress_std

    def pick_highest_response_at_all_locations(self):
        """ Set the highest respons as leading for all locations"""

        # loop over the groups of strain gauges
        for i_group, (position, sg_group) in enumerate(
                self.strain_gauge_groups.device_locations.items()):
            self.pick_highest_response_rao(position)

    def pick_highest_response_rao(self, position):
        """ Set the highest responds as leading for location *position* """
        log = get_logger(__name__)

        # the name of the sheet which stores all the stresses at this position
        # create the hotspot angles 0:30:360
        n_hotspots = self.settings.fatigue_settings["n_hotspot_angles"]
        hot_spot_angles_deg = linspace(0, 360, n_hotspots, endpoint=False)

        # collect the statistics of the hot spot stresses
        max_val = 0
        max_stress = None
        for cnt, phi_deg in enumerate(hot_spot_angles_deg):
            # cnt == 0: signal based on strain gauge, cnt == 1: signal based on accelerometer
            stress_name, short_label = fut.hot_spot_stress_name(position, 1, phi_deg)
            try:
                stress = self.rao_camp.data[stress_name]
            except KeyError:
                log.debug(
                    "KeyError raised in setting highest response {}. Must be pivot".format(
                        stress_name))
                break
            mag_stress_sqr = abs(stress * stress).sum()
            if mag_stress_sqr > max_val:
                max_val = mag_stress_sqr
                max_stress = stress

        for cnt, phi_deg in enumerate(hot_spot_angles_deg):
            # cnt == 0: signal based on strain gauge, cnt == 1: signal based on accelerometer
            stress_name, short_label = fut.hot_spot_stress_name(position, 1, phi_deg)
            try:
                self.rao_camp.data[stress_name] = max_stress
            except KeyError:
                log.debug(
                    "KeyError raised in setting highest response {}. Must be pivot".format(
                        stress_name))
                break

    def collect_statistics_hot_spot_stress_at_position(self, position):
        """
        Collect the hot spot statistic for this location
        """
        log = get_logger(__name__)
        # the name of the sheet which stores all the stresses at this position
        str_at_pos = fut.stress_at_position_name(position)

        if self.frequency_domain:
            container = self.rao_camp
        else:
            container = self.mdf_file

        # create the hotspot angles 0:30:360
        n_hotspots = self.settings.fatigue_settings["n_hotspot_angles"]
        hot_spot_angles_deg = linspace(0, 360, n_hotspots, endpoint=False)

        # collect the statistics of the hot spot stresses
        for i_type, stress_type in enumerate(["SG", "AC"]):
            if stress_type == "SG" and self.frequency_domain:
                # In the frequency domain we don't have the strain gauge  signal: skip this
                continue

            for cnt, phi_deg in enumerate(hot_spot_angles_deg):
                # cnt == 0: signal based on strain gauge, cnt == 1: signal based on accelerometer
                stress_name, short_label = fut.hot_spot_stress_name(position, stress_type, phi_deg)
                try:
                    stress = container.data[stress_name] * 1e-6
                except KeyError:
                    log.debug("stress name {} not found. skipping".format(stress_name))
                    continue
                else:

                    if not self.frequency_domain:
                        # store the hot spot stress standard deviation per channel in the fatigue
                        # database # stress_acc  is a time series, so the standard deviation can
                        # directly be calculated
                        stress_sd = stress.std()
                    else:
                        # the stress contains the complex rao components. Calculate the spectrum of
                        # the signal with S = RAO^2 * S_wave (where S_wave is the wave spectrum and
                        # the RAO is the converted RAO for this hot spot.  S is the resulting
                        # stress spectrum, so the SD value is equal to the standard deviation of
                        # the sum (time the mesh spacing of the 2D spectrum)
                        mag_stress_sqr = abs(stress * stress)
                        stress_spectrum = mag_stress_sqr * self.psd_2d * container.delta_area
                        stress_sd = np.sqrt(stress_spectrum.sum())

                    # store the stress standard deviation of the this time into the data base
                    row_index = (str_at_pos, self.time_stamp)
                    column_name = "S_HS_{}{}".format(short_label, cnt)
                    self.fatigue_data_base.ix[row_index, column_name] = stress_sd

    @staticmethod
    def create_hot_spot_stresses_for_location(data,
                                              position,
                                              signal_type,
                                              stress_list,
                                              scf,
                                              n_hotspot_angles,
                                              is_square_tube=False,
                                              strain_gauge_distances=None):
        """Given a list of cold spot stresses at a position, create the hot spot stresses at the
        n_hotspot_angles

        Parameters
        ----------
        data : :obj:`DataFrame`
            input data frame to store the resulting hot spot data for this position
        position :
            name of the position where to create the new hot spot stresses
        signal_type : {"SG", "AC}
            Determines what type of signal is used. For SG: the hot spot based on the strain gauge,
            AC: the hot spot based on the accelerometer.
        stress_list :
            list of stresses at the cold spot. Should have 4 channels!
        scf :
            dictionary containing the scf_x, scf_My and scf_Mz stress concentration factors
        n_hotspot_angles :
            the number of angles at which to calculate the hot spot stresses
        is_square_tube :
            indicate if we have to use the pivot transformation (Default value = False)
        strain_gauge_distances :
            a dictionary carrying the distances of the SG to the hot spot. Only needed for the
            pivots

        Notes
        -----
        * Nothing is returned: store the values in the data frame

        """

        log = get_logger(__name__)
        fct = ones(8)

        if re.search("PIV", position, re.IGNORECASE):
            # handle the pivot locations. We only have one hot spot, so just set on angle
            hot_spot_angles_deg = [0]

            # store the outer stress channel. The SCFx has already been calculated
            try:
                i_outer = strain_gauge_distances["outer"]["index"]
            except (KeyError, TypeError):
                i_outer = 0
            sigma_x = stress_list[i_outer]
            sigma_in = sigma_out = zeros(sigma_x.size)
        else:
            # create the hotspot angles 0:30:330
            hot_spot_angles_deg = linspace(0, 360, n_hotspot_angles, endpoint=False)

            # derive the x stress component and the inner and outer moment
            # not that the sigma_x is just the mean of the (normally) four signals
            sigma_x = mean(stress_list, axis=0)

            # deal with all the other locations
            if len(stress_list) == 4:
                log.debug("Calculating sigma_in and out for four cold spot signals")
                sigma_in = 0.5 * (stress_list[0] - stress_list[2])
                sigma_out = 0.5 * (stress_list[1] - stress_list[3])
            elif len(stress_list) == 3:
                # for the PIV location we do not have 4 signals but 2. Base the in and out on the
                # average stress (i.e. sigma_x)
                log.debug(
                    "Three cold signal channels supplied to the hotspot calculation. "
                    "Only use second channel ")
                sigma_in = sigma_out = zeros(sigma_x.size)
                sigma_x = stress_list[1]
                fct = [1.0 for i in range(8)]
            elif len(stress_list) == 2:
                # just set the values to zero, we only have one signal (this does not happen
                # normally, so add a warning
                log.debug("Calculating stress on mean")
                sigma_in = 0.5 * (stress_list[1] - sigma_x)
                sigma_out = 0.5 * (stress_list[0] - sigma_x)
            else:
                log.debug(
                    "Only one signal channel supplied to the hotspot calculation. "
                    "Is something wrong ?")

        scf_x = scf["X"]
        scf_in = scf["inplane"]
        scf_out = scf["outplane"]

        sign_in = [1.0, 1.0, 1.0, 0.0, -1.0, -1.0, -1.0, 0.0]
        sign_out = [1.0, 0.0, -1.0, -1.0, -1.0, 0.0, 1.0, 1.0]

        log.debug(
            "Loop over hotspot angles for {} {} with {} {} {} {}".format(position,
                                                                         signal_type,
                                                                         scf_x,
                                                                         scf_in,
                                                                         scf_out,
                                                                         is_square_tube))
        for i, phi_deg in enumerate(hot_spot_angles_deg):
            phi = deg2rad(phi_deg)

            if not is_square_tube:
                # for the round tubes just take the cos/sin of the angles
                hot_spot_stress = fct[i] * scf_x * sigma_x + cos(phi) * scf_in * sigma_in + sin(
                    phi) * scf_out * sigma_out
                log.debug("hs tube {}".format(fct[i]))
            else:
                # for the square tubes (i.e. the adjustors) take the signs specified above
                hot_spot_stress = fct[i] * scf_x * sigma_x + sign_in[i] * scf_in * sigma_in + \
                                  sign_out[i] * scf_out * sigma_out
                log.debug("hs square {} {} {}".format(fct[i], sign_in[i], sign_out[i]))

            # store the hot spot stress for angle phi in the data frame
            new_name, short_label = fut.hot_spot_stress_name(position, signal_type, phi_deg)
            log.debug(
                "Store hot spot stress at position {} for source {} at phi {:03d} : {} with {:8.3f}"
                "".format(position, signal_type, int(phi_deg), new_name,
                          hot_spot_stress.std() * 1e-6))
            try:
                data[new_name] = hot_spot_stress
            except ValueError:
                log.warning("Did not match. Fix latex for complex spectra")

    @staticmethod
    def create_pivot_info_dict(label, distance, index):
        """

        Parameters
        ----------
        label :

        distance :

        index :


        Returns
        -------

        """
        info = dict(
            label=label,
            distance=distance,
            index=index)
        return info

    @staticmethod
    def sigma_ratio_vs_rho(delta_rho, radius_pivot=0.805, distance_offset=0.805):
        """
        Calculate the ration sigma/sigma_infinity as a function of :math:`\\rho`, the distance
        from the center of the pivot


        Parameters
        ----------
        delta_rho :
            distance from edge of the pivot with

              -rho: distance center pivot
              -delta_rho: rho-radius_pivot

        radius_pivot :
            radius of the pivot. This is not the inner radius because there the plates are thick,
            but equal to the edge of the pivot where the thin part changes into the thick part
            (Default value = 0.805)
        distance_offset :
            (Default value = 0.805)

        Returns
        -------
        float
            sigma_ratio: the ratio in stresses

        Notes
        -----
        * The relation between the stress :math:`\sigma` and the distance :math:`\\rho` from the
          pivot is

          .. math::

            \sigma = \\frac{1}{2} \sigma_{\infty}  (1+\\Big(\\frac{R}{\\rho}\\Big)^2) -
            \\frac{1}{2} \sigma_{\infty} (1+ 3\\Big(\\frac{R}{\\rho}\\Big)^4) \cos(2\\theta)

          with theta the angle from the top to the pivot and  :math:`\\theta=-\pi/2` is the front.
        * So the largest value at the front becomes

          .. math::

            \\frac{\sigma}{\sigma_{\infty}} = 1 + \\frac{1}{2}\\Big(\\frac{R}{\\rho}\\Big)^2  +
            \\frac{3}{2}\\Big(\\frac{R}{\\rho}\\Big)^4

          done

        References
        ----------

        http://www.ewp.rpi.edu/hartford/~ernesto/Su2012/EP/MaterialsforStudents/Aiello/Roark-Ch06.pdf
        """
        inv_rho_prime = radius_pivot / (delta_rho + distance_offset)
        sigma_ratio = 1 + 0.5 * inv_rho_prime ** 2 + 1.5 * inv_rho_prime ** 4
        return sigma_ratio

    def sigma_inf_vs_sigma_at_rho(self, delta_rho, sigma_rho):
        """Given a stress at delta_rho, return the sigma infinity given the relation above

        Parameters
        ----------
        delta_rho :
            return: sigma_inf
        sigma_rho :


        Returns
        -------

        """

        sigma_inf = sigma_rho / self.sigma_ratio_vs_rho(delta_rho)

        return sigma_inf

    @staticmethod
    def sigma_at_edge(sigma_inf):
        """return the hot spot stress given the sigma_inf and the relations above

        Parameters
        ----------
        sigma_inf :
            return: sigma_at_edge

        Returns
        -------


        """

        return 3 * sigma_inf

    def calculate_scf_from_sg_signals(self, strain_gauge_distances, stress_list):
        """Calculate the SCF for the pivot only when using the measured signal (SG).

        We are dealing with the pivots now because strain_gauge_distances is not None

        Parameters
        ----------
        strain_gauge_distances :
            dictionary with a distance per SG signal
        stress_list :
            list of the three SG signal

        Returns
        -------

        """

        i0 = strain_gauge_distances["outer"]["index"]
        i1 = strain_gauge_distances["middle"]["index"]
        i2 = strain_gauge_distances["inner"]["index"]

        d0 = strain_gauge_distances["outer"]["distance"]
        d1 = strain_gauge_distances["middle"]["distance"]
        d2 = strain_gauge_distances["inner"]["distance"]

        s0 = stress_list[
            i0]  # outer stress signal (with the lowest value because furthest away)
        s1 = stress_list[i1]  # middle stress signal
        s2 = stress_list[i2]  # inner stress signal closest to the hot spot

        sigma_inf = zeros((3, s0.size))
        sigma_inf[0] = self.sigma_inf_vs_sigma_at_rho(d0, s0)
        sigma_inf[1] = self.sigma_inf_vs_sigma_at_rho(d1, s1)
        sigma_inf[2] = self.sigma_inf_vs_sigma_at_rho(d2, s2)

        s_inf = sigma_inf.mean(axis=0)
        s_edge = self.sigma_at_edge(s_inf)

        # most simple way of getting the scf by the ratio between the inner and outer stress
        scf_x = s2 / s0

        # bit more advance by the stress at the edge from a plate with a hole
        scf_x_1 = s_edge / s0

        # alternative way to estimate scf. Assumes a relation sigma_rho/sigma_0 = 1 + A * rho^b
        # with rho the distance from the outer SG towards the weld
        # so for rho=0 sigma_rho=sigma_0 and for rho=delta_1  sigma_rho = sigma_1 and
        # rho=delta_2 -> sigma_rho = sigma_2
        # here the b and A coefficients are calculated and the scf is returned
        f1 = abs(s1 / s0)
        f2 = abs(s2 / s0)
        rho_0 = d0
        rho_1 = d0 - d1
        rho_2 = d0 - d2
        b = np.log((f1 - 1) / (f2 - 1)) / np.log(rho_1 / rho_2)
        A = (f1 - 1) / rho_1 ** b
        scf_x_2 = 1 + A * rho_0 ** b

        return scf_x, scf_x_1, scf_x_2

    def cold_spot_stress_to_hotspots_at_position(self,
                                                 stress_type,
                                                 position,
                                                 sg_group,
                                                 scf_parameters):
        """
        Calculate the hot spot stress at the location 'posision' based on the SCF factors

        Parameters
        ----------
        stress_type: {"SG", "AC"}
            Type of input signal: from the accelerometers (AC) or strain gauges (SG)
        position: str
            Name of the locations
        sg_group: :obj:`DevicesAtLocation`
            Object carrying  all information of the devices at this location
        scf_parameters: dict
            Dictionary with the normal, inplacne and out of plane stress concentration factos

        Notes
        -----
        * In case the location is the Pivot, the SCF are internally calculate when the
          *calculate_scf_pivots_internally*  option is set to True in the settings file.
        * If the *use_internally_calculated_scf_for_pivots* flag is set to True, the internally
          calculated SCF for the Pivot are also used in stead of the pre-defined SCF at the pivot
          locations
        """

        # set the reference to the time object or complex amplitude objects
        if self.frequency_domain:
            container = self.rao_camp
        else:
            container = self.mdf_file

        log = self.logger

        n_hotspots = self.settings.fatigue_settings["n_hotspot_angles"]
        log.debug("Converting the cold spot stresses to the {} hot spots at {}"
                  "".format(n_hotspots, position))

        try:
            # get the s concentration factors for this location
            strain_gauge_distances = self.settings.locations[position]["strain_gauge_distances"]
        except (KeyError, TypeError):
            strain_gauge_distances = None
            log.debug("Location {} does not have a strain gauge distances. No problem, is not "
                      "a PIVot".format(position))

        # read the flag that stores if we have a square tube
        is_square_square_tube = self.settings.locations[position]["is_square_tube"]

        # loop over the channels of this group and collect the signals in stress_list
        stress_list = list()
        for i_channel, name in enumerate(sg_group.names):

            if strain_gauge_distances is not None:
                # for the strain gauge signal, in case we have the pivot location the
                # strain_gauge_distance should contain a dictionary with a inner, outer and middle
                # field carrying the distance to the hotspot. Also the short label is stored in this
                # dictionary, which we use now to store the index of the channel in the stress_list
                # which belongs to the correct signal
                short_label = sg_group.short_labels[i_channel]
                for piv_loc, piv_info in strain_gauge_distances.items():
                    # loop over the location 'outer', 'middle', and 'inner'  and compare the label
                    # assigned in the settings file with the current channel name. IF there is a
                    # match, store the index so we can use this index later to refer to the correct
                    # signal (inner, middle or outer)
                    if piv_info["label"] == short_label:
                        piv_info["index"] = i_channel

            # obtain the name belonging to the column of the current stress type and channel
            if stress_type == "SG":
                stress_name = fut.stress_name_of_converted_signal(name, position, i_channel)
            else:
                # name of the stress based on the accelerometer
                stress_name = fut.stress_from_acc_name(name, position, i_channel)

            # store the stress signal obtained from the strain gauges of this channel
            stress_list.append(container.data[stress_name])

        if stress_type == "SG":
            # if the strain gauge distances are given and one of the flags is true,
            # calculate the scf_x based on the SG signals on the PIV
            calculate_scf_at_pivot = False
            if self.settings.calculate_scf_pivots_internally:
                # if the *calculate_scf_pivots_internally* option is true in the settings file,
                # calculate the SCF at the pivot
                calculate_scf_at_pivot = True
            if self.settings.use_internally_calculated_scf_for_pivots:
                # if the *use_internally_calculated_scf_for_pivots* flag is true in the setting file
                # calculate the SCF at the pivot AND use them in stead of the values given at the
                # pivot properties
                calculate_scf_at_pivot = True

            if strain_gauge_distances and calculate_scf_at_pivot:
                # calculate the scf_x in three ways based on the SG signals (result is
                # stored in a tuple)
                scf_x = self.calculate_scf_from_sg_signals(strain_gauge_distances,
                                                           stress_list)

                # store the calculate scf_x in three new field in the data container
                scf_label = fut.scf_at_position_name(position,
                                                     "SCF_X")  # simple ratio method
                scf_label_1 = fut.scf_at_position_name(position,
                                                       "SCF_X_1")  # plate with hole
                scf_label_2 = fut.scf_at_position_name(position,
                                                       "SCF_X_2")  # heuristic exponential
                container.data[scf_label] = scf_x[0]
                container.data[scf_label_1] = scf_x[1]
                container.data[scf_label_2] = scf_x[2]

                if self.settings.use_internally_calculated_scf_for_pivots:
                    # only use the calculate SCF for the Pivot if requested. But the
                    # parameters are always calculated
                    scf_parameters["X"] = scf_x[
                        2].median()  # internally take the exponential increase
                    scf_parameters["inplane"] = 0
                    scf_parameters["outplane"] = 0

        # we are done with preparing the cold spot channels, now calculate the hot spot
        # channels
        self.create_hot_spot_stresses_for_location(container.data,
                                                   position,
                                                   stress_type,
                                                   stress_list,
                                                   scf_parameters, n_hotspots,
                                                   is_square_square_tube,
                                                   strain_gauge_distances)

    def cold_spot_stress_to_hotspots_all_locations(self):
        """Convert the stress of the cold spot to the hot spot"""

        log = self.logger

        # loop over the groups of strain gauges
        for i_group, (position, sg_group) in enumerate(
                self.strain_gauge_groups.device_locations.items()):

            try:
                # get the stress concentration factors for this location
                scf_parameters = self.settings.locations[position]["stress_concentration_factors"]
            except (KeyError, TypeError):
                log.debug("Location {} does not have a stress concentration factor. Skipping"
                          "".format(position))
                continue

            # loop over the two stress type: based on the strain gauge (SG) and the
            # accelerometers (AC)
            for cnt, stress_type in enumerate(["SG", "AC"]):
                if stress_type == "SG" and self.frequency_domain:
                    # in the frequency domain we do not have the signal from the strain gauges, so
                    # skip to next
                    continue

                self.cold_spot_stress_to_hotspots_at_position(stress_type, position, sg_group,
                                                              scf_parameters)

    def get_damage(self,
                   stress_name,
                   sn_curve_data,
                   dump_hot_spot_per_location=None,
                   file_base=None,
                   out_dir=None,
                   ):
        """
        Calculate the damage of the time signal of the stress based on rainflow counting, or,
        in case that the sea_state_spectrum is not None, calculate it based on a spectral
        approach with Rayleigh

        Parameters
        ----------
        stress_name : str
            the stress signal name
        sn_curve_data : ndarray
            2D array with number of critical bins in row 0 and stress in MPa
        dump_hot_spot_per_location : dict, optional
            a dictionary holding the hot spot rao's we want to dump to file (Default = None
        file_base : str, optional
            the base name of the file to dump.  Default = None
        out_dir: str, optional
            Output directory to write the dump data to. Default = None
        """

        log = self.logger

        # get the properties of the sn curve at this location
        s_bins = sn_curve_data[0, :]
        n_damage = sn_curve_data[1, :]
        n_bins = n_damage.shape[0]
        s_min = s_bins[0]
        s_max = s_bins[-1]
        delta_s_bins = s_bins[1] - s_bins[0]

        moment_2 = None
        n_up_cross_per_second = None

        if not self.frequency_domain:
            # we are in the time domain, so use rain flow couting to get fatigue
            # the rain flow counted cycles
            log.debug("Calculate fatigue of time series stress signal with rain flow")
            # get the stress signal from the container data

            # get the time series of the current hot spot stress *stress_name* from the mdf_file
            stress = self.mdf_file.data[stress_name]

            # check if the stress is valid everywhere
            if stress.isnull().values.any():
                raise ValueError

            # now create a wafo time series object
            # The factor to is to convert the stress amplitudes to the stress range
            # by adding the factor two
            signal_and_time_matrix = vstack((self.mdf_file.data.time_r, 2 * stress.values / 1.0e6))

            time_series_object = wo.mat2timeseries(signal_and_time_matrix.T)

            # calculate the turning points
            turning_points = time_series_object.turning_points(wavetype='astm')

            # get the cycle pairs. Note that the astm version of cyclepairs is used, as the
            # cycle_pairs it self give # min/max rainflow count. sig_cyclese is a Ncycx3 numpy
            # array with in the three rows respectively the amplitude, average stress and half or
            # full cyclces (0.5 or 1). We need to rain flow amplitudes only
            sig_cycles = turning_points.cycle_astm()

            # divide stress by 1e6 as the sn curve is defined in MPa
            hist, bin_edges = np.histogram(sig_cycles[:, 0], n_bins, range=(s_min, s_max))

            stress_amplitude_sd = stress.std() / 1.0e6
        else:
            # the sea_state_spectrum is given, which means we are carrying the RAO complex component
            # in the container in state of the time series. We can calculate the fatigue with the
            # Rayleigh approach now
            log.debug("Calculate fatigue of spectrum  with rayleigh")

            # get the rao from the container data.
            rao = self.rao_camp.data[stress_name]

            delta_area = self.rao_camp.delta_area

            mag_rao_sqr = abs(rao * rao)

            # divide by 1e12 to turn stress variance into MPa**2
            # the stress spectrum is the power spectral density of the stress AMPLITUDE, because the
            # zeroth adn second stress moment refer to the stress amplitude values in the Rayleigh
            # PDF
            stress_spectrum = mag_rao_sqr * self.psd_2d / 1e12

            # rao relates sea state to amplitude stress and we calculate m_0 and m_2 of the stress
            # amplitude. This is correct, because although the Rayleigh distribution is convert to
            # stress range (S = 2 A), the sigma_x is kept to the values of the stress amplitude
            moment_0 = stress_spectrum.sum() * delta_area
            moment_2 = (stress_spectrum * (2 * pi * self.freq_in_hz_2d) ** 2).sum() * delta_area

            # moment_0 is the sigma^2 of the stress amplitude signal, which is required in the
            # rayleigh distribution of S
            stress_amplitude_sd = np.sqrt(moment_0)

            # number of up crossing per unit of time
            n_up_cross_per_second = np.sqrt(moment_2 / moment_0) / (2.0 * np.pi)

            # calculate the cycles based on the rayleigh distribution and average number of
            # crossings per time
            chance_to_be_in_bin = rayleigh_pdf(s_bins, stress_amplitude_sd) * delta_s_bins
            n_seconds = self.sea_state_duration.to("second").magnitude
            hist = chance_to_be_in_bin * n_up_cross_per_second * n_seconds

            if fut.dump_this_hot_spot_location(dump_hot_spot_per_location, stress_name):
                log.info("Dumping rao stress name {}".format(stress_name))
                time_string = pd.Timestamp(self.time_stamp).strftime("%Y%m%dT%H%M%S")
                file_name = os.path.join(out_dir,
                                         "rao_{}_{}_{}.msg_pack".format(file_base, stress_name,
                                                                        time_string))
                fut.dump_hot_spot_rao_to_file(hot_spot_rao=rao, hot_spot_spectrum=stress_spectrum,
                                              freq=self.rao_camp.frequencies,
                                              dir=self.rao_camp.directions,
                                              file_name=file_name)

        damage = np.sum(hist / n_damage)

        return damage, stress_amplitude_sd, moment_2, n_up_cross_per_second

    def calculate_hot_spot_fatigue_at_location(self, position):
        """
        Calculate the hot spot fatigue at *position*

        Parameters
        ----------
        position :
            Location name for which we want to calculate the fatigue
        """

        log = self.logger

        if not self.frequency_domain:
            # in the time domain the fatigue is calculate both for stress signal (0) and the
            # acceleration signal (1)
            columns = DAMAGE_COLUMNS
            stress_types = ["SG", "AC"]
        else:
            # in the frequency domain the fatigue is calculate only the acceleration signal (1)
            columns = HS_DAMAGE_ACC_NAMES
            stress_types = ["AC"]

        # get the time series from the data frame
        n_hotspot_angles = self.settings.fatigue_settings["n_hotspot_angles"]
        hot_spot_angles_deg = linspace(0, 360, n_hotspot_angles, endpoint=False)
        dam_at_pos = fut.damage_at_position_name(position)
        str_at_pos = fut.stress_at_position_name(position)
        if self.fatigue_data_base is not None:
            self.fatigue_data_base.ix[(dam_at_pos, self.time_stamp), columns] = None

        try:
            sn_curve_data = self.settings.sn_curve_data[position]
        except (KeyError, TypeError):
            log.debug("Location {} does not have a s/n curve data. Skipping".format(position))
            return

        # see if we have a dictionary with hot spot locations we want to dump to file in the
        # settings file
        try:
            dump_hot_spot_per_location = self.settings.rao_vessel["dump_rao_hot_spot_per_time_step"]
            dump_file_base = self.settings.rao_vessel["dump_file_base"]
            dump_out_dir = self.settings.rao_vessel["dump_out_dir"]
        except KeyError:
            # just make it an empty dict if it is not given in the settings file
            dump_hot_spot_per_location = dict()
            dump_file_base = None
            dump_out_dir = None

        # loop over the phase of the hotspot over the tube
        for cnt, phi_deg in enumerate(hot_spot_angles_deg):

            # calculate damage two times: one for the stress signal of the strain gauges (SG) and
            # one for the accelerometers (AC)
            for stress_type in stress_types:
                # for ii=0: short_label=SG, for ii==1: short_label = AC
                stress_name, short_label = fut.hot_spot_stress_name(position, stress_type, phi_deg)

                try:
                    # calculate the damage
                    damage, signal_std, m2, Nc = \
                        self.get_damage(stress_name=stress_name,
                                        sn_curve_data=sn_curve_data,
                                        dump_hot_spot_per_location=dump_hot_spot_per_location,
                                        file_base=dump_file_base,
                                        out_dir=dump_out_dir
                                        )
                except ValueError:
                    log.info(
                        "NaN found in HOT {} STRESS signal {}. Skipping".format(short_label,
                                                                                stress_name))
                except KeyError:
                    log.debug(
                        "HOT {} STRESS signal {} does not exist. Skipping".format(short_label,
                                                                                  stress_name))
                else:
                    # Store the standard deviation of the hot spot stress in the fatigue data base
                    # command below adds the measured stress  (sgs)  standard deviation at the row
                    # with  'time_stamp' at the excel sheet 'str_at_pos' which will look like S_HZ3,
                    #  S_AD (PS) etc, and at the row str_stress_name which look like S_HS_SG0 upt
                    # 7 (8 hotspot angles)
                    log.debug(
                        "Damage of short_label {} stress {} is : {}".format(short_label,
                                                                            stress_name,
                                                                            damage))
                    if self.fatigue_data_base is not None:
                        self.fatigue_data_base.ix[
                            (str_at_pos, self.time_stamp), "S_HS_{}{}".format(short_label,
                                                                              cnt)] = signal_std
                        self.fatigue_data_base.ix[
                            (dam_at_pos, self.time_stamp), "D_HS_{}{}".format(short_label,
                                                                              cnt)] = damage
                        self.fatigue_data_base.ix[
                            (dam_at_pos, self.time_stamp), "M2_HS_{}{}".format(short_label,
                                                                               cnt)] = m2
                        self.fatigue_data_base.ix[
                            (dam_at_pos, self.time_stamp), "NC_HS_{}{}".format(short_label,
                                                                               cnt)] = Nc

    def calculate_fatigue_at_all_locations(self):
        """ Calculate the rainflow counted fatigue of all signals of all locations """

        log = self.logger

        # loop over the groups of strain gauges
        for i_group, (position, sg_group) in enumerate(
                self.strain_gauge_groups.device_locations.items()):

            # skip all locations store in the exclude location list
            if position in self.settings.exclude_locations:
                continue

            log.debug(
                "Calculating Fatigue at the hot spot at position {} with sea state duration {} [h]"
                "".format(position, self.sea_state_duration))
            self.calculate_hot_spot_fatigue_at_location(position)

    def predict_fatigue_for_this_voyage(self, voyage, id, time_total=0):
        """Calculate the Raleigh counted fatigue of all signals of all locations

        Parameters
        ----------
        voyage : DataFrame
            the object carrying all the data (sea states) of the current voyage
        id : int
            Id of the current voyage
        time_total : float
            Keep the total simulation time (Default value = 0)
        """

        log = self.logger

        # make a short reference to the rao_vess settings
        rao_vessel = self.settings.rao_vessel

        # for dumping the spectra and rao, we need to create and output directory. See if this
        # needed and do it
        try:
            if rao_vessel["dump_sea_state_per_time_step"] or \
                    rao_vessel["dump_rao_acc_per_time_step"]:
                dump_out_dir = rao_vessel["dump_out_dir"]
                make_directory(dump_out_dir)
        except KeyError:
            log.debug("Failed to create an output directory for the contour files. "
                      "No problem, just don't dump them")

        max_time_steps = rao_vessel["max_time_steps"]
        if not max_time_steps:
            max_time_steps = voyage.index.size
        log.debug("Found max time steps to process: {}".format(max_time_steps))

        # loop over the time steps of this voyage
        if self.progress_bar:
            wdg = PROGRESS_WIDGETS
            wdg[-1] = MESSAGE_FORMAT.format(
                fut.progress_bar_message(id, self.max_trajectories, time_total))
            n_voyages = max(max_time_steps, len(list(voyage.keys())))
            progress = pb.ProgressBar(widgets=wdg, maxval=n_voyages, fd=sys.stdout).start()

        if self.mode == "rao_tune":
            # for the rao tuning mode make an object which carie all the stuff so that this method
            # does not get bloated
            rao_tune = RaoTune(self.settings, self.spectra_2d_time_series, self.rao_camp,
                               self.spectrum_info)
        else:
            rao_tune = None

        i_time = 0
        # loop over all the sea states of this journey. In case we are predicting a particular
        # journey with the measured sea states, this is only one loop. For a 'safetrans' simulation
        # this can be multiple journeys
        for index, row in voyage.iterrows():

            try:
                speed_in_knots = Q_(row["speed_sustained"], "knots")
            except KeyError:
                log.info("Could not get speed in knots from the data frame. Assuming zero")
                speed_in_knots = Q_(0.0, "knots")

            log.info("Processing time step {} with speed {}".format(index, speed_in_knots))

            if self.apply_velocity_correction:
                self.rao_camp.pick_rao_for_speed(speed_in_knots)

            if fut.maximum_exceeded(i_time, max_time_steps):
                log.debug("Nr times  in this voyage exceeds maximum of {}. Stop this voyage"
                          "".format(max_time_steps))
                break

            try:
                self.time_stamp = row["DateTime"]
            except KeyError:
                # No datetime available as DatTime column. Try the index
                self.time_stamp = index

            log.debug("Time stamp here : {}".format(self.time_stamp))

            if self.mode == "rao_tune":
                if not rao_tune.check_status_ok():
                    # we checked for this time step if all is ok but failed. So continue to the
                    # next time step
                    continue

            # retrieve the wave 2D spectrum  from the spectral time series for time 'time_stamp'
            try:
                self.psd_2d = self.spectra_2d_time_series.spectral_records[
                    self.time_stamp].get_psd_2d_in_omega_dir_rad_domain()
            except KeyError:
                self.logger.debug("Time {} does not exist in the data base".format(self.time_stamp))
                continue

            self.freq_in_hz_2d = self.spectra_2d_time_series.spectral_records[
                self.time_stamp].freq_in_hz_2d

            if self.settings.rao_vessel["spectrum_speed_correction"]:
                # shift the spectrum with the velocity in m/s
                speed = speed_in_knots.to_base_units()
                log.info("Correcting current spectrum for speed  {} ({})"
                         "".format(speed_in_knots, speed))
                self.psd_2d = spectrum2d_to_spectrum2d_encountered(self.psd_2d,
                                                                   self.rao_camp.freq_2d_mesh.T,
                                                                   self.rao_camp.dir_2d_mesh.T,
                                                                   speed)

            if rao_tune is not None:
                rao_tune.dump_rao(index)

            try:
                self.sea_state_duration = Q_(row.time_per_sea_state, "hour")
            except AttributeError:
                self.sea_state_duration = Q_(3, "hour")

            log.debug(
                "Info of calculated spectrum:\nShape : {}\nMean  : {} \nMin   : {}\nMax   : {}\n"
                "".format(self.psd_2d.shape, self.psd_2d.mean(), self.psd_2d.min(),
                          self.psd_2d.max()))

            if self.mode == "generate_spectra":
                log.debug(
                    "We are in spectrum mode, so only a data base is build. Continue to next")
                continue

            # try to store the position and heading from the voyage data base
            for name, alias in self.settings.gps_info_columns.items():
                try:
                    self.fatigue_data_base.ix[(GENERAL_SHEET_NAME, self.time_stamp), name] = row[
                        alias]
                except KeyError:
                    log.debug("could not store {}".format(name))

            if self.mode == "prediction":
                # this section is going to use the
                log.debug("Predict Fatigue")
                # just as the rao 2D spectrum turn the sea state spectrum in a single column
                self.psd_2d = self.psd_2d.flatten()
                self.freq_in_hz_2d = self.freq_in_hz_2d.flatten()

                log.debug(
                    "Rao shape vs spec shape {}  {} ".format(self.psd_2d.shape,
                                                             self.rao_camp.shape2D))

                if self.strain_gauge_groups is not None:
                    log.debug("Calculate fatigue")
                    self.calculate_fatigue_at_all_locations()

                    log.debug("Collect stress statistics")
                    self.collect_statistics_cold_and_hot_spot_stress()

                log.debug("Updating general info sheet")
                # call with the current row containing the sea states props. Because is is not None,
                # also the rao_vessel info sheet is added
                self.update_general_info_sheet(row=row)
            else:

                rao_tune.update_rao_estimate(self.psd_2d)

            if self.progress_bar:
                progress.update(i_time)
                sys.stdout.flush()

            i_time += 1

        if self.mode == "rao_tune":
            rao_tune.rao_final_average()

        if self.progress_bar:
            progress.finish()

    def export_time_series(self):
        """the same series are dumped to file in this routine

        Parameters
        ----------
        container :
            container holding the data which can be the time series or complex fourier components
            (RAO)
        settings :
            dictionary carrying all the settings
        time_stamp :
            current data_time string

        Returns
        -------
        """

        log = get_logger(__name__)

        if self.frequency_domain:
            container = self.rao_camp
        else:
            container = self.mdf_file

        # get the settings related to the export
        time_series_settings = self.settings.time_series

        # create the output file name of the current time series to write to file
        out_file = fut.make_time_series_file_name(time_series_settings, self.time_stamp,
                                                  create_directory=True)

        # make data selection
        column_list = read_value_from_dict_if_valid(time_series_settings, "export_name_list",
                                                    [])
        if column_list:
            data_to_export = container.data[column_list]

            file_type = os.path.splitext(out_file)[1]
            if file_type == ".msg_pack":
                log.info("Export time series to {}".format(out_file))
                data_to_export.to_msgpack(out_file)
            elif file_type == ".xls":
                data_to_export.index = data_to_export.index.astype(str)
                with pd.ExcelWriter(out_file, append=False) as writer:
                    # create a pandas data frame and safe to excel
                    data_to_export.to_excel(writer, sheet_name="TimeSeries")
            elif file_type == ".csv":
                # create a pandas data frame and safe to excel
                data_to_export.to_csv(out_file)
            else:
                raise ValueError("Not implemented yet for " + file_type)

    def update_general_info_sheet(self, row=None, last_coordinates=None, last_distance=0):
        """Store this routine the general information of the current records

        Parameters
        ----------
        row :
            the current row in the data frame which will be updated (Default value = None)
        last_coordinates :
            the previous coordinates, which can be used to calculate the displacement if we have GPS
            (Default value = None)
        last_distance :
            previous distance to calculate the cumulative distance (Default value = 0)

        Returns
        -------
        tuple (last_coordinates, last_distance)
            Last coordinate of the GPS position and the distance traveled so far

        Notes
        -----
        * The information from the data base is stored is stored, such as the information from the
          GPS, the Tp, etc.
        * Anything which is not specifically related to a position in the tower (such as mean damage
          or stress)
        * What exactly will be stored is defined by the settings in the configuration file in the
          *general_info_sheet* chapter.
        * In this section you can entries add which look like::

            TowO_ACC_AY_SD:
              alias: TowO_ACC_AY
              operation: std

          where the first line is the name as will occur in the General tab of the excel file,
          the alias refers to the name of this quantity as used in the internal dataframe
          (which may be different).
        * *operation* is the statistic operation which you will apply on this quantity, such as
          'mean', 'std', 'var'. This statistical quantity may even carry a integerer like 4*std such
          that you can directly calculate the Hs from the Unity RAO contribution
        """

        log = get_logger(__name__)

        # check if the container data holds data in the time domain by looking to the
        # sea_state_spectrum: if this is given we assume that we are in the frequency domain
        if self.frequency_domain:
            container = self.rao_camp
        else:
            container = self.mdf_file

        # get the general info section from the settings and store in info
        info = self.settings.general_info_sheet
        general_info_columns = sorted(info.keys())

        # get the cut values
        try:
            n_cut_head = self.settings.n_cut_head
        except AttributeError:
            n_cut_head = None
        try:
            n_cut_tail = self.settings.n_cut_tail
        except AttributeError:
            n_cut_tail = None

        log.debug(" columns in the info sheet: {}".format(container.data.columns))

        # just always add the General sheet to start with
        for column in general_info_columns:
            try:
                column_alias = info[column]["alias"]
            except KeyError:
                column_alias = column
            operation = info[column]["operation"]
            # here we need to check if the
            if isinstance(column_alias, str):
                # for a ordinary string as alias, do your normal thing.
                try:
                    # if this fail, we are supplying a normal signal
                    signal = container.data[column_alias]
                    log.debug("Got signal of {} with alias {}".format(column, column_alias))
                except KeyError:
                    log.debug("Failed to get data for {}. Skipping".format(column_alias))
                    continue
            else:
                # for calculating the distance you have to give 2 aliases for both the longitude and
                # latitude, hence the column_alias is actually a list (of two coordinates).
                # If this is the case, the operation -must- be a distance, so check for that
                try:
                    if operation != "distance":
                        log.warning(
                            "For coordinates you need to specify 'distance' as an operator. "
                            "Skipping this one")
                        continue
                    # if it is not a string, it is a list of two aliases, which we assume are the
                    # longitude and latitude
                    coordinates = LatLon(container.data[column_alias[0]].mean(),
                                         container.data[column_alias[1]].mean())
                except KeyError:
                    continue

            # check if we have a multiplier in front of the operation, eg 4*std, and take it off
            operation, multiplier = fut.get_multiplier_and_operation(operation)

            log.debug(
                "Going to apply operation {} with multiplier {}".format(operation, multiplier))

            if not self.frequency_domain:
                # we are in the time domain, so get the statistics from the time series directly

                # make a selection of point
                if n_cut_head is not None:
                    signal = signal.iloc[n_cut_head:]
                if n_cut_tail is not None:
                    signal = signal.iloc[:n_cut_tail]

                if operation == "mean":
                    value = signal.mean()
                elif operation == "std":
                    # standard deviation
                    value = signal.std()
                elif operation == "sda":
                    # significant double amplitude
                    value = 4 * signal.std()
                elif operation == "mpm":
                    # 1000 cycles single most probable maximum
                    value = 4 * signal.std() * 0.5 * 1.86
                elif operation == "var":
                    value = signal.var()
                elif operation == "median":
                    value = signal.median()
                elif operation == "distance":
                    displacement = 0
                    if last_coordinates is not None:
                        try:
                            # LatLon return distance in km. Miles is given in m, to convert to km
                            displacement = coordinates.distance(last_coordinates) / (
                                    nautical_mile / 1000.0)
                        except ValueError:
                            self.logger.warning(
                                "Something went wrong in calculating the displacement")
                    value = last_distance + displacement
                    last_coordinates = coordinates
                    last_distance = value
                else:
                    log.warning("operation {} not implemented. Set None".format(operation))
                    value = None
            else:
                # in the frequency domain the statistics is obtained by multiplying the rao for the
                # different quantities with the sea spectrum. The variance is for example equal to
                # the area under the spectrum
                if operation == "mean":
                    value = abs(signal.values[0])
                elif operation in ("std", "sda", "mpm"):
                    mag_stress_sqr = abs(signal * signal)
                    stress_spectrum = mag_stress_sqr * self.psd_2d * container.delta_area

                    # calculate the standard deviation
                    value = np.sqrt(stress_spectrum.sum())
                    if operation in ("sda", "mpm"):

                        # turn standard deviation into significant double amplitude: sda = 4 * sigma
                        value *= 4.0
                        if operation == "mpm":
                            # turn sda into mpm
                            value *= 0.5 * 1.86

                    if column_alias == "Unity":
                        log.debug("Unity operation multiplier: {} {} {}: {}".format(
                            mag_stress_sqr.mean(), mag_stress_sqr.min(), mag_stress_sqr.max(),
                            4 * value))

                        # fig = plt.figure()
                        # fig.canvas.set_window_title("{} {} {}".format(column, operation, value))
                        # gs = gridspec.GridSpec(1,3)
                        # ax = plt.subplot(gs[0,0])
                        # ax.contourf(abs(mag_stress_sqr.reshape(container.shape2D)))
                        # ax = plt.subplot(gs[0,1])
                        # ax.contourf(abs(sea_state_spectrum.reshape(container.shape2D)))
                        # ax = plt.subplot(gs[0,2])
                        # ax.contourf(abs(stress_spectrum.reshape(container.shape2D)))
                        # plt.ioff()
                        # plt.show()

                elif operation == "var":
                    mag_stress_sqr = abs(signal * signal)
                    stress_spectrum = mag_stress_sqr * self.psd_2d * container.delta_area
                    value = stress_spectrum.sum()
                else:
                    log.warning("operation {} not implemented. Set None".format(operation))
                    value = None

            # apply the multiplier we can be passed in front of the operation
            value *= multiplier

            # finally store, the value into the general info sheet
            if self.fatigue_data_base is not None and value is not None:
                self.fatigue_data_base.ix[(GENERAL_SHEET_NAME, self.time_stamp), column] = value

        if row is not None:
            # row is only not None if it is called from the prediction routine
            voyage_info = self.settings.rao_vessel["voyage_info_sheet"]
            log.debug("ADDING TO {} at {}".format(GENERAL_SHEET_NAME, self.time_stamp))
            # just always add the info sheet to start with
            for column in voyage_info:

                # a bit tricky. The row of this is a array with the values. The index of each
                # element is stored in the _fields tuple. To get the index of the tuple, use the
                # index key word and # then obtain the value
                # try:
                #    index_of_value = row._fields.index(column)
                # except ValueError:
                #    continue

                try:
                    self.fatigue_data_base.ix[(GENERAL_SHEET_NAME, self.time_stamp), column] = row[
                        column]
                except KeyError:
                    log.debug(
                        "Failed to add {} {} {}".format(GENERAL_SHEET_NAME, self.time_stamp,
                                                        column))
                    continue
                else:
                    log.debug("Successfully added {} {} {} {}"
                              "".format(GENERAL_SHEET_NAME, self.time_stamp, column, row[column]))

        if self.fatigue_data_base is not None and self.fatigue_data_base_file is not None:
            # always put the last column with the filename base
            self.fatigue_data_base.ix[(GENERAL_SHEET_NAME, self.time_stamp), "FILENAME"] = \
                os.path.splitext(os.path.split(self.fatigue_data_base_file)[1])[0]

        log.debug("Return with {} {}".format(last_coordinates, last_distance))
        return last_coordinates, last_distance

    def dump_the_mdf_file_to_excel(self):
        """Dump the MDF data for each file to a more readable file 

        Notes
        -----
        * This method is carried out if the 'dump_mdf_data_to_file' in the 'flags' chapter of the
          yaml setting file is true. The format is given by the 'dump_mdf_type' field in the
          the values chapter 
        * In case the --dump_mdf_file_data is given on the command line this method is also 
          carried out
        """
        log = get_logger(__name__)
        dump_file_type = self.settings.dump_mdf_type

        if dump_file_type == "msg_pack":
            file_name = self.fatigue_data_base_file_base + ".msg_pack"
            log.info("dumping mdf data to {}".format(file_name))
            self.mdf_file.data.to_msgpack(file_name)
        elif dump_file_type == "csv":
            file_name = self.file_base_name + ".csv"
            log.info("dumping mdf data to {}".format(file_name))
            self.mdf_file.data.to_csv(file_name)
        elif dump_file_type == "xls":
            file_name = self.file_base_name + ".xls"
            log.info("dumping xls data to {}".format(file_name))
            self.mdf_file.data.to_excel(file_name)
        else:
            raise AssertionError("Dumping file only implemented for msg_pack, csv and xls")

    def plot_time_series(self, column_names, fig=None, axis=None,
                         figsize=(8, 9),
                         y_labels=None, annotations=None,
                         y_lim=None,
                         use_relative_time_at_x_axis=True,
                         relative_start_time=None,
                         sample_length=None,
                         replace_legend_labels=False,
                         legend_label=None,
                         legend_x_position=0.91,
                         legend_y_position=0.93,
                         number_of_columns=3,
                         right_space=None,
                         font_scale=1.1,
                         line_style="-"
                         ):
        """
        Plot the time series of the columns defined in the *column_names* argument

        Parameters
        ----------
        column_names: list
            List of strings with the names of the columns we want to plot. Each column creates an
            extra subplot in vertical direction
        fig: :obj:`Figure` or None, optional
            If None a figure will be initialised. Otherwise we take over the Figure passed to this
            method and the lines we want to add. In this way you can accumulate the lines of
            several cases to one plot
        axis: :obj:`AxisSubPlot` or None, optional
            If None the axis are  initialised. Otherwise we take over the Axis passed to this
            method and the lines we want to add
        figsize: tuple, optional, optional
            Size of the image.  Default = (8,9)
        y_labels: list or None, optional
            List of strings of the y labels we want to add to the subplot. Default is None. If given
            the list of y_labels must match the number of columns to plot
        y_lim: list of tuple or None, optional
            If given, set the limits on the y axis. The limit must be given in a list which N tuples
            (y_min, y_max) (where N is the number of columns we have added, i.e. the number of
            subplots). So for 2 subplots (columns): y_lim = [(-1, 1), (-2, 2)] would set the y
            limit -1 and 1 to the first subplot and -2, 2 to the second subplot. If none is given,
            auto-scaling is applied. Also only one subplot can be forced to a limit, e.g.
            y_lim=[(-1, 1), (None, None)] sets the limts of the first plot to -1, 1 and auto-scales
            the second subplot
        annotations: list or None, optional
            List of strings of annotations, one for each subplot
        use_relative_time_at_x_axis: bool
            Use relative time at the x-axis in stead of the DateTime
        relative_start_time: :obj:`DeltaTime` or None, optional
            If given, start plotting from this relative time from the start of the MDF file in
            seconds. Default is None, ie start at the beginning
        sample_length: :obj:`DeltaTime` or None, optional
            If given, only plot this time length in seconds. Default is None, ie whole time range
            is plotted
        replace_legend_labels: bool, optional
            Only for the last call set True in order to change the legend for a flipped version
        legend_label: str or None, optional
            String to add the legend of the current line we want to plot. Note that we add multiple
            columns, but each colum will get its own subplot. In each subplot, all lines of the
            column have the same color (as they belong to the same case) so we can impose the
            same name to the legend to it. Default is None, i.e. the column name itself is
            taken for the legend key
        legend_x_position: float, optional
            x location of the flipped legend
        legend_y_position: float, optional
            y location of the flipped legend
        number_of_columns: int, optional
            number of columns to use for the legend
        right_space: float or None, optional
            Add some space to the right of the figure in case you want to add the legend there
        font_scale : float, optional
            Scale the fonts. Default = 1.1
        line_style: str
            Line style to use

        Returns
        -------
        tuple
            (fig, axis) the figure and the axis which we can pass to plot the next line

        Notes
        -----
        * For each column name, a new subplot is created
        * For the first call to this routine, keep the *fig* and *axis* argument None, in that case
          the plot is initialised.
        * The method returns the *fig* and *axis* it has created.
        * You can call this function multiple times for different cases. When you pass the returned
          fig, axis to the next call, to lines will accumulate.
        """

        n_rows = len(list(column_names))
        n_cols = 1

        # check some input arguments and raise a error if we got a non-match
        if y_labels is not None:
            n_labels = len(list(y_labels))
            if n_labels != n_rows:
                raise AssertionError("Number of y labels passed ({}) does not match the number of "
                                     "row to plot ({}). Please match up ".format(n_labels, n_rows))
        if y_lim is not None:
            n_y_lim = len(list(y_lim))
            if n_y_lim != n_rows:
                raise AssertionError("Number of y limits passed ({}) does not match the number of "
                                     "row to plot ({}). Please match up ".format(n_y_lim, n_rows))

        if fig is None:
            # if fig is None we still need to initialise it
            fig, axis = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=figsize, sharex="all")
            fig.canvas.set_window_title("Time series")
            sns.set(font_scale=font_scale)
            plt.subplots_adjust(right=right_space)
            if n_rows == 1:
                # in case we only have on sublot, the axis returned by subplots is not a list, but
                # the SubPlotFigure itself. Turn it into a list with one item so that we can refer
                # to the the first subplot as axis[0] below, regardless on how many subplots we have
                axis = [axis]

        # check if the relative start time was given. If so, set it wrt the start of the mdf file
        if relative_start_time is not None:
            start_date_time = self.mdf_file.data.index[0] + relative_start_time
        else:
            start_date_time = None

        # check if the sample length was pass. If so, set the end time wrt the relative_start_time
        # if we did not set the  relative start time, we just start at the beginning
        if sample_length is not None:
            if start_date_time is None:
                # start at the beginning at start_date_time was not set yet
                start_date_time = self.mdf_file.data.index[0]
            end_date_time = start_date_time + sample_length
        else:
            # the sample length was not given. In case we defined a relative start time, set the
            # limit to the end. If no relative start time was given, also keep the end time at None
            # so that the full time range is automatically selected
            if start_date_time is not None:
                end_date_time = self.mdf_file.data.index[-1]
            else:
                end_date_time = None

        # loop over the column names we want to plot
        for cnt, column_name in enumerate(column_names):
            if legend_label is None:
                # no legend_label was given, just use the column name
                ll = column_name
            else:
                # in case a string is passed to the legend_label, we are going to replace the legend
                # keys with this string. This means that the lines with the same color (all in a
                # different subplot, as each column plots a line in its own subplot) all get the
                # same key label (which for instance reflects the case type)
                ll = legend_label

            # we call the plot command not with the plot(y=[name to plot], ...) construction but
            # first turn the dataframe into a data series and add a label to it explicitly. Other
            # wise always the column name is used as the legend key
            df_col = self.mdf_file.data[column_name]
            if use_relative_time_at_x_axis:
                df_col.index = (df_col.index - df_col.index[0]) / pd.Timedelta(1, "s")

            df_col.plot(ax=axis[cnt], label=ll, style=line_style)

            if y_labels is not None:
                # in case y labels were passed, add them too. Th
                axis[cnt].set_ylabel(y_labels[cnt])

            if annotations is not None:
                label, xp, yp, col, size, ax_an = analyse_annotations(annotations[cnt])
                axis[cnt].text(xp, yp, label, transform=axis[cnt].transAxes,
                               fontdict=dict(size=size, color=col))
            axis[cnt].legend(loc="center left", bbox_to_anchor=(1, 0.9))

            if y_lim is not None:
                set_limits(axis[cnt], v_min=y_lim[cnt][0], v_max=y_lim[cnt][1], direction=1)

        if start_date_time is not None:
            # we have given time ranges, so impose them here
            if use_relative_time_at_x_axis:
                start_in_sec = relative_start_time / pd.Timedelta(1, "s")
                end_in_sec = start_in_sec + sample_length / pd.Timedelta(1, "s")
                axis[0].set_xlim(start_in_sec, end_in_sec)
            else:
                axis[0].set_xlim(start_date_time, end_date_time)

        if replace_legend_labels:
            handles, labels = get_valid_legend_handles_labels(axis[0])
            # explicitly remove all the legends
            clear_all_legends(axis)

            # important to explicitly set the focus to fig as figlegend will put the legend
            # to the current figure, which will be the next figure in case we plot two plots
            # at the same time
            plt.figure(fig.number)
            plt.figlegend(flip(handles, number_of_columns), flip(labels, number_of_columns),
                          "upper right", ncol=number_of_columns,
                          bbox_to_anchor=(legend_x_position, legend_y_position))

        if use_relative_time_at_x_axis:
            axis[-1].set_xlabel("Time [s]")

        return fig, axis

    def plot_time_series_spectra(self, column_names, fig=None, axis=None, figsize=(8, 9),
                                 y_labels=None, annotations=None,
                                 y_lim=None,
                                 x_lim=None,
                                 set_y_log=False,
                                 nfft=None,
                                 nperseg=1024,
                                 right_space=None,
                                 legend_label=None,
                                 replace_legend_labels=False,
                                 legend_x_position=0.91,
                                 legend_y_position=0.93,
                                 line_style="-",
                                 number_of_columns=3,
                                 font_scale=1.1,
                                 number_of_peak_to_find=1,
                                 show_peaks=False
                                 ):
        """
        Plot the power spectral densities of the time series given in the *column_names* argument

        Parameters
        ----------
        column_names: list
            List of strings with the names of the columns we want to plot. Each column create one
            subplot (an extra row)
        fig: :obj:`Figure` or None
            If None a figure will be initialised. Otherwise we take over the Figure passed to this
            method and the lines we want to add
        axis: :obj:`AxisSubPlot` or None
            If None the axis are  initialised. Otherwise we take over the Axis passed to this
            method and the lines we want to add
        figsize: tuple, optional
            Size of the image.  Default = (16,8)
        y_labels: list or None, optional
            List of strings of the y labels we want to add to the subplot. Default is None
        annotations: list or None, optional
            List of strings of annotations, one for each subplot. Default = None
        x_lim: tuple or None, optional
            If given, set the frequency axis range. Default = None, ie. the whole spectral range is
            plotted
        y_lim: tuple or list of tuple or None, optional
            If given, set the limits on the y axis. The limit must be given in a list which N tuples
            (y_min, y_max) (where N is the number of columns we have added, i.e. the number of
            subplots). So for 2 subplots (columns): y_lim = [(0, 1), (0, 2)] would set the y
            limit 0 and 1 to the first subplot and 0, 2 to the second subplot. If none is given,
            autoscaling is applied. Also only one subplot can be forced to a limit, e.g.
            y_lim=[(0, 1), (None, None)] sets the limts of the first plot to 0, 1 and autoscales
            the second subplot
        set_y_log : bool, optional
            Use a log scale for the y axis
        nfft: int, optional
            Number of points per block to use. Default is None which means nfft=nperseg. See Welch
        nperseg: int, optional
            Number of points in a segment
        replace_legend_labels: bool, optional
            Only for the last call set True in order to change the legend for a flipped version
        legend_label: str, optional
            String to add the legend of the current line
        legend_x_position: float, optional
            x location of the flipped legend
        legend_y_position: float, optional
            y location of the flipped legend
        number_of_columns: int, optional
            Number of columns to use for the legend in case there are plotted in a grid (which
            happens if *replace_legend_labels* is set to True on the last call of the plotting
            routine
        right_space: float or None, optional
            Add some space to the right of the figure in case you want to add the legend there
        font_scale : float, optional
            Scale the fonts. Makes the fonts slightly larger. Default = 1
        number_of_peak_to_find: int
            Find the first *number_of_peaks_to_find* peaks in the spectrum
        show_peaks: bool
            Show the peaks with a dot
        line_style: str
            Line style to use

        Returns
        -------
        tuple
            (fig, axis) the figure and the axis which we can pass to plot the next line
        """

        n_rows = len(column_names)
        n_cols = 1

        # check some input arguments and raise a error if we got a non-match
        if y_labels is not None:
            n_labels = len(list(y_labels))
            if n_labels != n_rows:
                raise AssertionError("Number of y labels passed ({}) does not match the number of "
                                     "row to plot ({}). Please match up ".format(n_labels, n_rows))
        if y_lim is not None:
            n_y_lim = len(list(y_lim))
            if n_y_lim not in (n_rows, 2):
                raise AssertionError("Number of y limits passed ({}) does not match the number of "
                                     "row to plot ({}) and is also not a tuple of 2."
                                     "".format(n_y_lim, n_rows))

        if fig is None:
            # if fig is None we still need to initialise it
            fig, axis = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=figsize, sharex="all")
            fig.canvas.set_window_title("PSD")
            sns.set(font_scale=font_scale)
            plt.subplots_adjust(right=right_space)
            if n_rows == 1:
                axis = [axis]

        f_sampling = self.mdf_file.time_record.sample_rate

        # loop over the column names we want to plot
        for cnt, column_name in enumerate(column_names):

            if cnt == 0:
                if legend_label is None:
                    ll = column_name
                else:
                    ll = legend_label
            else:
                ll = None

            ff, psd = welch(self.mdf_file.data[column_name],
                            nfft=nfft,
                            nperseg=nperseg,
                            window="hann",
                            fs=f_sampling)

            axis[cnt].plot(ff, psd, label=ll, linestyle=line_style)

            if set_y_log:
                axis[cnt].semilogy()

            # add the frequency of the peak of the psd to the statistics data base
            if number_of_peak_to_find > 0:
                freq_peaks, psd_peaks = spu.get_peaks(ff, psd,
                                                      max_number_of_peaks=number_of_peak_to_find)
                self.logger.info("Found peak at {}".format(freq_peaks))
                if show_peaks:
                    axis[cnt].plot(freq_peaks, psd_peaks, "o")

            if y_labels is not None:
                # in case y labels were passed, add them too

                axis[cnt].set_ylabel(y_labels[cnt])

            if annotations is not None:
                label, xp, yp, col, size, ax_an = analyse_annotations(annotations[cnt])
                axis[cnt].text(xp, yp, label, transform=axis[cnt].transAxes,
                               fontdict=dict(size=size, color=col))

            axis[cnt].legend(loc="center left", bbox_to_anchor=(1, 0.9))

            if y_lim is not None:
                try:
                    set_limits(axis[cnt], v_min=y_lim[cnt][0], v_max=y_lim[cnt][1], direction=1)
                except (TypeError, IndexError):
                    set_limits(axis[cnt], v_min=y_lim[0], v_max=y_lim[1], direction=1)

        if x_lim is not None:
            # we have given time ranges, so impose them here
            axis[0].set_xlim(x_lim)

        if replace_legend_labels:
            handles, labels = get_valid_legend_handles_labels(axis[0])
            clear_all_legends(axis)
            plt.figure(fig.number)
            plt.figlegend(flip(handles, number_of_columns), flip(labels, number_of_columns),
                          "upper right", ncol=number_of_columns,
                          bbox_to_anchor=(legend_x_position, legend_y_position))

        return fig, axis


def _parse_the_command_line_arguments(args):
    """
    Parse the command line to set some options

    Parameters
    ----------
    args: list
        The system command line arguments

    Returns
    -------
    tuple (parsed_argumenet, parser)
        The parsed arguments are stored in the args bjec

    """

    parser = argparse.ArgumentParser(description='Start the Fatigue Monitoring Processing tool',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # set the verbosity level command line arguments
    parser.add_argument('configuration_file', help="The configuration file with all the settings")
    parser.add_argument('-d', '--debug', help="Print lots of debugging statements",
                        action="store_const", dest="log_level", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('-v', '--verbose', help="Be verbose", action="store_const",
                        dest="log_level", const=logging.INFO)
    parser.add_argument('-q', '--quiet', help="Be quiet: no output", action="store_const",
                        dest="log_level", const=logging.WARNING)
    parser.add_argument('--log_file_debug', help="Print lots of debugging statements to file",
                        action="store_const", dest="log_level_file", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('--log_file_verbose', help="Be verbose to file", action="store_const",
                        dest="log_level_file", const=logging.INFO)
    parser.add_argument('--log_file_quiet', help="Be quiet: no output to file",
                        action="store_const", dest="log_level_file", const=logging.WARNING)
    parser.add_argument("--mode", help=
                        "Set the processing mode of the script; "
                        "'monitoring': read and process the monitored MDF files and calculate the "
                        "fatigue based on the time series. "
                        "'prediction': read the RAO and sea-state file and give a predition of the "
                        "fatigue. "
                        "'rao_tune': read the RAO and monitored spectra as produced in the "
                        "'generate_spectra' mode and calculate the RAO. "
                        "'generate_spectra': read the MDF files and "
                        "generate the spectral data of the time series",
                        choices=["monitoring", "prediction", "rao_tune", "generate_spectra"],
                        default="monitoring")
    parser.add_argument("--version", help="Show the current version", action="version",
                        version="fatigue_monitoring: Version = {ver}".format(ver=__version__))
    parser.add_argument("--reset_database", action="store_true",
                        help="Do not read the existing data base but start again)")
    parser.add_argument("--max_trajectories", type=int,
                        help="Maximum number of voyages to processes in case we are in the "
                             "*prediction* mode. Overrides the value given in the settings file "
                             "if not None. Default = None")
    parser.add_argument("--start_voyage_id", type=int,
                        help="Start prediction at voyage with this id in case we are in the "
                             "*prediction* mode. Default = None")
    parser.add_argument("--n_fatigue_dump_every", type=int, default=50,
                        help="Dump the data base containing the statistics per sea state time "
                             "every *n_fatigue_dump_every* iterations. Works for both the "
                             "monitoring and prediction mode. Prevents loss of processed data in "
                             "case failure of script at some point (which happens if the screen "
                             "saver turns on for instance)")
    parser.add_argument("--progress_bar", action="store_true", default=False,
                        help="Just show a progress bar instaad of the logging message.)")
    parser.add_argument("--no_progress_bar", action="store_false", dest="progress_bar",
                        help="Do not show the progress bar but generate logging information.)")
    parser.add_argument("--dry-run", action="store_true",
                        help="Only show what is going to happen, but do nothing.)")
    parser.add_argument("--dump_settings", action="store_true",
                        help="Create a dump of the settings to <configuration_file>_dump.yaml to "
                             "allow to see what exact settings have been used")
    parser.add_argument("--pick_maximum_rao_for_all_hotspots", action="store_true",
                        help="USe the maximum RAO for all hot spot. For testing purpose only)")
    parser.add_argument("--clear_database", action="store_true",
                        help="If the data base is in an erroneous state, only write it so it gets "
                             "cleaned.)")
    parser.add_argument("--block_last_plot", action="store_true",
                        help="Block script at the last plot")
    parser.add_argument("--show_images", action="store_true",
                        help="Show the images even if this flag is false in the settings file ")
    parser.add_argument("--matlab_trajectory_data_base", default=None, type=str,
                        help="If this file is given the trajectories are read from the matlab "
                             "trajectory data base")
    parser.add_argument("--hindcast_database", default=None, type=str,
                        help="If this file is given the trajectories are read from monitored "
                             "database")
    parser.add_argument("--hindcast_start_date_time", default=None, type=str,
                        help="Start hind casting at this date. If not given, take first date in "
                             "file")
    parser.add_argument("--hindcast_end_date_time", default=None, type=str,
                        help="End hind casting at this date. If not given, take last date in file")
    parser.add_argument('--monitoring_start_date_time',
                        help="If given, only select the data file names with a time stamp equal "
                             "or large than the *monitoring_start_date_time*")
    parser.add_argument('--monitoring_end_date_time',
                        help="If given, only select the data file names with a time stamp smaller "
                             "than the *monitoring_end_date_time*. Equal times are not included")
    parser.add_argument("--time_zone", action="store",
                        help="Specify the time zone to use by the logging system. Note that on the "
                             "Balder the time zone is set to Europe Summer time, even in the "
                             "winter. To force this, use the Etc/GMT-2 convention, "
                             "which corresponds to UTC+002 in POSSIX convention. Default it None, "
                             "which means that the time zone is read from the configuration yaml"
                             "file. If it is specified on the command line, it will over rule "
                             "the settings file")
    parser.add_argument("--meteo_consult_report", default=None, type=str,
                        help="If this file is given the trajectories are read from forecast report")
    parser.add_argument("--noaa_forecast_file", default=None, type=str,
                        help="If this file is given the trajectories are read from forecast file "
                             "obtained from NOAA")
    parser.add_argument("--msg_pack_reset", action="store_true",
                        help="Forces to reset the message pack, overruling the setting in the yaml "
                             "configuration file")
    parser.add_argument("--information_to_excel_output", action="store_true", default=True,
                        help="Forces to reset the message pack, overruling the setting in the yaml "
                             "configuration file")
    parser.add_argument("--no_information_to_excel_output", action="store_false",
                        help="Forces to reset the message pack, overruling the setting in the yaml "
                             "configuration file",
                        default="False", dest="information_to_excel_output")
    parser.add_argument("--show_all_channel_names", action="store_true",
                        help="Write all the channel names to screen")
    parser.add_argument("--cache_mdf_read_or_write", action="store_true",
                        help="During processing write MDF files directly after reader to speed up "
                             "reading time next run")
    parser.add_argument("--reset_mdf_cache", action="store_true",
                        help="During processing force writing MDF files, even they already exist")
    parser.add_argument("--dump_mdf_file_data", action="store_true",
                        help="Write all the MDF file time series to an "
                             "excel file with a new name")
    parser.add_argument("--write_log_to_file", action="store_true",
                        help="Write the logging information to file")
    parser.add_argument("--log_file_base", default="log", help="Default name of the logging output")
    parser.add_argument("--heading_name", help="Allows to change the name off the Heading field")
    parser.add_argument("--start_date_string", default="20100921",
                        help="A string YYMMDD representing the start date of the current voyage. "
                             "Only needed when reading the matlab data base in order to add a date "
                             "to each journey")
    parser.add_argument("--trajectory_file", default=[],
                        help="CSV or msg_pack file names required for the rao_vessel "
                             "execution. Can be given multiple times to add more than "
                             "one csv file.", nargs="*", action='append')
    parser.add_argument("--execute", action="store_true", default=True,
                        help="Immediately  execute the code after having created the object")
    parser.add_argument("--no_execute", action="store_false", dest="execute",
                        help="Don't execute the code after having created the object")
    group2 = parser.add_mutually_exclusive_group()
    group2.add_argument("-s", "--store_settings", action="store_true",
                        help="Store the default settings to the configuration file")

    # parse the command line
    parsed_argumenents = parser.parse_args(args)

    return parsed_argumenents, parser


def setup_logging(write_log_to_file=False,
                  log_file_base="log",
                  log_level_file=logging.INFO,
                  log_level=None,
                  progress_bar=False,
                  ):
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Initialise the logging system
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if write_log_to_file:
        # http://stackoverflow.com/questions/29087297/
        # is-there-a-way-to-change-the-filemode-for-a-logger-object-that-is-not-configured
        sys.stderr = open(log_file_base + ".err", 'w')
    else:
        log_file_base = None

    _logger = create_logger(file_log_level=log_level_file,
                            console_log_level=log_level,
                            log_file=log_file_base)

    if progress_bar:
        # switch of all logging because we are showing the progress bar via the print statement
        # logger.disabled = True
        # logger.disabled = True
        # logger.setLevel(logging.CRITICAL)
        for handle in _logger.handlers:
            try:
                getattr(handle, "baseFilename")
            except AttributeError:
                # this is the stream handle because we get an AtrributeError. Set it to critical
                handle.setLevel(logging.CRITICAL)

    _logger.info("{:10s}: {}".format("Running", sys.argv))
    _logger.info("{:10s}: {}".format("Version", __version__))
    _logger.info("{:10s}: {}".format("Directory", os.getcwd()))
    _logger.debug("Debug message")
    return _logger


def main(args_in):
    """
    Main entry to run the script

    Parameters
    ----------
    args: list
        System command line arguments
    """
    args, parser = _parse_the_command_line_arguments(args_in)
    _logger = setup_logging(
        write_log_to_file=args.write_log_to_file,
        log_file_base=args.log_file_base,
        log_level_file=args.log_level_file,
        log_level=args.log_level,
        progress_bar=args.progress_bar
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # load the configuration file if requested and store all settings
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    configuration_file = None
    try:
        configuration_file = args.configuration_file
        _logger.debug("Found configuration file on command line: {}".format(configuration_file))
    except IndexError:
        if args.store_settings:
            parser.error("Load configuration requested but no file name give on command line")
        else:
            _logger.debug("No configuration file given. Using internal default settings")
    else:
        _logger.debug("Using the internal default settings")

    # start the FMS engine
    FMSMonitorAndPrediction(
        configuration_file=configuration_file,
        mode=args.mode,
        reset_database=args.reset_database,
        n_fatigue_dump_every=args.n_fatigue_dump_every,
        progress_bar=args.progress_bar,
        dry_run=args.dry_run,
        dump_settings=args.dump_settings,
        pick_maximum_rao_for_all_hotspots=args.pick_maximum_rao_for_all_hotspots,
        clear_database=args.clear_database,
        block_last_plot=args.block_last_plot,
        show_images=args.show_images,
        matlab_trajectory_data_base=args.matlab_trajectory_data_base,
        hindcast_database=args.hindcast_database,
        hindcast_start_date_time=args.hindcast_start_date_time,
        hindcast_end_date_time=args.hindcast_end_date_time,
        time_zone=args.time_zone,
        meteo_consult_report=args.meteo_consult_report,
        noaa_forecast_file=args.noaa_forecast_file,
        msg_pack_reset=args.msg_pack_reset,
        information_to_excel_output=args.information_to_excel_output,
        show_all_channel_names=args.show_all_channel_names,
        dump_mdf_file_data=args.dump_mdf_file_data,
        write_log_to_file=args.write_log_to_file,
        log_file_base=args.log_file_base,
        heading_name=args.heading_name,
        start_date_string=args.start_date_string,
        trajectory_file=args.trajectory_file,
        store_settings=args.store_settings,
        execute=args.execute,
        cache_mdf_read_or_write=args.cache_mdf_read_or_write,
        reset_mdf_cache=args.reset_mdf_cache,
        max_trajectories=args.max_trajectories,
        start_voyage_id=args.start_voyage_id,
        monitoring_start_date_time=args.monitoring_start_date_time,
        monitoring_end_date_time=args.monitoring_end_date_time
    )
    _logger.info("Script ends here")


def _run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == '__main__':
    _run()
