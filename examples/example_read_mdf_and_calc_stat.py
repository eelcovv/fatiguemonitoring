"""
Example demonstrating how to read a yaml file and calculate stress and damage

We run the code by creating an object *MonitoringTool*. All the arguments that can be
passed to this object are also available for the command line script.
"""

import argparse
import logging
import re

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from hmc_utils.misc import (create_logger, get_logger)

from fatigue_monitoring import fms_monitor_and_predict as ft

sns.set(context="notebook")


def parse_the_command_line_arguments():
    parser = argparse.ArgumentParser(description='Example for Fatigue Monitoring Processing tool',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # set the verbosity level command line arguments
    parser.add_argument('--debug', help="Print lots of debugging statements", action="store_const",
                        dest="log_level",
                        const=logging.DEBUG, default=logging.INFO)
    parser.add_argument('--verbose', help="Be verbose", action="store_const", dest="log_level",
                        const=logging.INFO)
    parser.add_argument('--prediction', help="Run the prediction example", action="store_true")
    parser.add_argument('--progress', help="Use the progress bar", action="store_true")
    parser.add_argument('--show_channels', help="Show the channel names of the MDF file ",
                        action="store_true")
    parser.add_argument('--monitoring', help="Run the monitoring example", action="store_true")
    parser.add_argument('--plot_time_series', help="Plot the time series", action="store_true")
    parser.add_argument('--block_last_plot', help="Block the plots", action="store_true")
    parser.add_argument('--reset_database', help="Reset the database the plots",
                        action="store_true")
    args = parser.parse_args()

    return args, parser


def main(run_prediction=False,
         run_monitoring=False,
         show_channels=False,
         plot_time_series=False,
         reset_data_base=True,
         block_last_plot=False,
         fig_size=(9, 6),
         progress=False):
    """
    Run the fatigue monitoring script in varying ways

    Parameters
    ----------
    run_prediction: bool, optional
        Run the fatigue monitoring tool in prediction mode
    run_monitoring: bool, optional
        Run the fatigue monitoring tool in monitoring mode
    show_channels: bool, optional
        Show the names of all the channels
    plot_time_series: bool, optional
        Make a plot of the time series
    reset_data_base:  bool, optional
        Reset the data base
    block_last_plot: bool, optional
        Stop at the last plot
    fig_size: tuple, optional
        Size of the figure. Default = (9, 6)
    progress: bool, optional
        Only show a progress bar during processing

    """
    _logger = get_logger(__name__)
    if run_prediction:
        configuration_file_predict = "fms6_scf_old2_bp_rao_predict.yml"
        _logger.info("Running preditiocn with {}".format(configuration_file_predict))
        ft.FMSMonitorAndPrediction(
            configuration_file=configuration_file_predict,
            execute=True,
            block_last_plot=block_last_plot,
            mode="prediction",
            reset_database=reset_data_base,
            progress_bar=progress
        )

    if show_channels:
        configuration_file_monitor = "fms7_scf_old2_hp_kaiser_onefile.yml"
        _logger.info("Show channels of {}".format(configuration_file_monitor))
        ft.FMSMonitorAndPrediction(configuration_file=configuration_file_monitor,
                                   execute=True,
                                   show_all_channel_names=show_channels
                                   )

    if run_monitoring:
        configuration_file_monitor = "fms7_scf_old2_hp_kaiser_onefile.yml"
        _logger.info("Run monitoring {}".format(configuration_file_monitor))
        monitor = ft.FMSMonitorAndPrediction(configuration_file=configuration_file_monitor,
                                             execute=True,
                                             block_last_plot=block_last_plot,
                                             mode="monitoring",
                                             reset_database=reset_data_base,
                                             cache_mdf_read_or_write=True,
                                             progress_bar=progress)

        monitor2 = ft.FMSMonitorAndPrediction(configuration_file=configuration_file_monitor,
                                              execute=False,
                                              block_last_plot=block_last_plot,
                                              mode="monitoring",
                                              reset_database=reset_data_base,
                                              cache_mdf_read_or_write=True,
                                              progress_bar=progress)

        # in the settings set 'ue_all_accelerometers' to True to use them all. Also, change the
        # name of the tower_origin from TowO_ACC -> TowOp_ACC, otherwise we can not pu both of
        # them in one plot
        monitor2.settings.set_data_dict("use_all_accelerometers", True)
        monitor2.settings.set_data_dict("tower_origin_acc_name", "TowOp_ACC")

        # we had the 'execute' argument with monitor2 to False such that we could change the
        # settings. Now explicitly call the executation routine
        monitor2.fatigue_monitoring_in_time_domain()

        monitor.mdf_file.data.info()

        # we are going to plot the tower 6-DOF obtained with 6 accelerometers and all accelerometers
        # and compare that with the 6-DOF obtained with the MRU. We split the plots up in one time
        # with the AX, AY, AZ components (dof_type="A") and one time the RXX, RYY, and RZZ component
        # (dof_type="R").
        relative_start_time = pd.Timedelta('100 s')
        sample_length = pd.Timedelta("120 s")
        y_labels = ["X [m/s2]", "Y [m/s2]", "Z [m/s2]"]

        # we do all the plotting first for the three linear acceleration components A_X, A_Y, A_Z
        # and then for the rotation components RXX, RYY, RZZ. Sincee
        dof_types = ["A"]  # ["A", "R[XYZ]"]
        annotation_per_dof_type = [
            [r"a) DOF linear acceleration $a_x$@(0.0,1.05)cxkcd:blue",
             r"b) DOF linear acceleration $a_y$@(0.0,1.05)cxkcd:blue",
             r"c) DOF linear acceleration $a_z$@(0.0,1.05)cxkcd:blue"],
            [r"a) DOF rotational acceleration $a_{rx}$@(0.0,1.05)cxkcd:blue",
             r"b) DOF rotational acceleration $a_{ry}$@(0.0,1.05)cxkcd:blue",
             r"c) DOF rotational acceleration $a_{rz}$@(0.0,1.05)cxkcd:blue"]
        ]
        legend_label_per_case = ["6-Acc", "All-Acc", "MRU"]

        plot_spectra = True

        time_series_y_lim_per_dof = \
            [
                [(-0.4, 0.4), (-0.2, 0.2), (-0.4, 0.4)],
                [(-0.5, 0.5), (-0.5, 0.5), (-0.1, 0.1)]
            ]
        spectra_y_lim = (10e-14, 10e-2)
        y_log = False

        for i_dof, dof_type in enumerate(dof_types):

            yl = [dof_type[0] + label for label in y_labels]

            # plot the tower 6-DOF for the 6 settings
            column_names = list()
            for cnt, column_name in enumerate(monitor.mdf_file.data.columns):
                print("{:03d} : {}".format(cnt, column_name))
                if re.search("^TowO_ACC_{}[XYZ]".format(dof_type), column_name):
                    column_names.append(column_name)
            if plot_time_series:
                fig, axis = monitor.plot_time_series(column_names=column_names, figsize=fig_size,
                                                     legend_label=legend_label_per_case[0])
            if plot_spectra:
                fig2, axis2 = monitor.plot_time_series_spectra(column_names=column_names,
                                                               legend_label=
                                                               legend_label_per_case[0])

            # plot the tower 6-DOF for the all settings
            column_names = list()
            for cnt, column_name in enumerate(monitor2.mdf_file.data.columns):
                print("{:03d} : {}".format(cnt, column_name))
                if re.search("^TowOp_ACC_{}[XYZ]".format(dof_type), column_name):
                    column_names.append(column_name)
            if plot_time_series:
                fig, axis = monitor2.plot_time_series(column_names=column_names, fig=fig, axis=axis,
                                                      legend_label=legend_label_per_case[1])
            if plot_spectra:
                fig2, axis2 = monitor2.plot_time_series_spectra(column_names=column_names, fig=fig2,
                                                                axis=axis2,
                                                                legend_label=
                                                                legend_label_per_case[1])

            # plot the MRU signal
            column_names = list()
            for cnt, column_name in enumerate(monitor.mdf_file.data.columns):
                print("{:03d} : {}".format(cnt, column_name))
                if re.search("^MRU_at_PIVot \(SB\)_{}[XYZ]".format(dof_type), column_name):
                    column_names.append(column_name)

            # for the last call set the time limits and the y limnits
            if plot_time_series:
                monitor.plot_time_series(column_names=column_names,
                                         y_labels=yl,
                                         fig=fig,
                                         axis=axis,
                                         y_lim=time_series_y_lim_per_dof[i_dof],
                                         annotations=annotation_per_dof_type[i_dof],
                                         relative_start_time=relative_start_time,
                                         sample_length=sample_length,
                                         legend_label=legend_label_per_case[2],
                                         replace_legend_labels=True,
                                         line_style="--"
                                         )
            if plot_spectra:
                monitor2.plot_time_series_spectra(column_names=column_names,
                                                  annotations=annotation_per_dof_type[i_dof],
                                                  y_lim=spectra_y_lim,
                                                  set_y_log=y_log,
                                                  y_labels=yl, fig=fig2, axis=axis2,
                                                  legend_label=legend_label_per_case[2],
                                                  replace_legend_labels=True,
                                                  line_style="--"
                                                  )
        plt.ioff()
        plt.show()


if __name__ == "__main__":
    args, parser = parse_the_command_line_arguments()

    logger = create_logger(console_log_level=args.log_level)

    main(run_monitoring=args.monitoring,
         run_prediction=args.prediction,
         block_last_plot=args.block_last_plot,
         reset_data_base=args.reset_database,
         progress=args.progress,
         show_channels=args.show_channels,
         plot_time_series=args.plot_time_series
         )
