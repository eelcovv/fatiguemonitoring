#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from hmc_utils.misc import (create_logger, get_logger)
from numpy import array
from numpy.testing import (assert_equal)

from src.fatigue_monitoring import (Q_, ureg)
from src.fatigue_monitoring.fms_utilities import set_default_dimension

DATA_DIR = "data"

FUGRO_FILE_NAME = "Fugro_I0450_20170510_06.csv"
WRB_FILE_NAME = "example 2017}2017-05-10T00h01Z.spt"

logger = create_logger(console_log_level=logging.CRITICAL)


def test_set_default_dimensions():
    logger = get_logger(__name__)

    val = 1
    val_dim = set_default_dimension(val, "m")
    assert_equal(Q_(val, ureg.meter), val_dim)

    val = Q_(1, ureg.meter)
    val_dim = set_default_dimension(val, "m")
    assert_equal(Q_(val, ureg.meter), val_dim)

    val = array([0, 1, 2, 3])
    val_dim = set_default_dimension(val, "m")
    val_dim2 = Q_(val, "m")
    assert_equal(val_dim.all(), val_dim2.all())

    val = val_dim2
    val_dim = set_default_dimension(val, "m")
    val_dim2 = Q_(val, "m")
    assert_equal(val_dim.all(), val_dim2.all())

    val = [110.44, -25.7, -26.76]
    valin = ["{} m".format(x) for x in val]
    val_dim = set_default_dimension(valin, "m")
    assert_equal(array(val).all(), val_dim.magnitude.all())
